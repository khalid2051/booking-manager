﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace License
{
    public class LicenseService
    {
        public const string ExpiryDate = "2025-11-21";
        public const string WarningDate = "2025-10-21";
        public const string LicenseWarningMessage = "License will be expired by " + ExpiryDate;
        public const string LicenseExpiringMessage = "License expired! Contact with Administrator!";

        public static string LicenseCheck()
        {
            if (DateTime.Now > Convert.ToDateTime(ExpiryDate))
            {
                throw new Exception(LicenseExpiringMessage);
            }

            if (DateTime.Now > Convert.ToDateTime(WarningDate))
            {
                return LicenseWarningMessage;
            }

            return string.Empty;
        }

    }
}
