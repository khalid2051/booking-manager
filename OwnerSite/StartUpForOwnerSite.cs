﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: OwinStartup("OwnerSite",typeof(OwnerSite.StartUpForOwnerSite))]

namespace OwnerSite
{
    public class StartUpForOwnerSite
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}