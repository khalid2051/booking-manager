﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OwnerSite.ViewModels;
using StarterProjectV2;
using StarterProjectV2.Models;

namespace OwnerSite.Controllers
{
    public class HomeController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult Index()
        {
            LoginViewModel mdl = new LoginViewModel()
            {
                ErrorFlag = false
            };
            return View(mdl);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel mdl)
        {

            using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
            {
                var usr = db.OwnersInfos.FirstOrDefault(u => u.SlotId == mdl.OwnerSmartId || u.OwnerSmartId == mdl.OwnerSmartId || u.OwenerId == mdl.OwnerSmartId);
                if (usr != null)
                {
                    if (EncryptDecryptString.Decrypt(usr.Password, "1234") == mdl.Password)
                    {
                        Session[SessionKeys.USERNAME] = usr.SlotId.ToString();
                        Session[SessionKeys.OWNERID] = usr.OwenerId.ToString();
                        Session[SessionKeys.OWNERNAME] = usr.Name.ToString();

                        Session[SessionKeys.NO_OF_NOTIFICATION] = Convert.ToString(db.Notifications.Where(n => n.isSeen == false && n.NotificationHeader == "Dividend" && n.DividendHolder == usr.OwenerId).ToList().Count);
                        Session[SessionKeys.LIST_OF_NOTIFICATION] = (List<Notification>)db.Notifications.Where(n => n.NotificationHeader == "Dividend" && n.DividendHolder == usr.OwenerId).ToList();

                        // //Audit Code goes here
                        // var thisOwner = usr;
                        // Audit objectAudit = new Audit()
                        // {
                        //     AuditDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                        //     LogInIp = "NIL",
                        //     TableName = "0",
                        //     UserLoginDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                        //     UserLogoutDTTM = "0",
                        //     UserName = thisOwner.SlotId,
                        //     TaskName = "OwnerSite",
                        //     ActionString = "LogIn",
                        //     ActionDetails = "Owner with Slot Id: " + usr.SlotId + " has Logged Into his/her Account at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm."),
                        // };
                        // StarterProjectV2.Common.insertInAuditTable(objectAudit);

                        db.SaveChanges();
                        return RedirectToAction("OwnerHomePage", "Owner", new { ownerId = usr.OwenerId });
                        //return RedirectToAction("OwnerHomePage");
                    }
                    else
                    {
                        return RedirectToAction("Index");

                    }
                }
                else
                {
                    return RedirectToAction("Index");

                }
            }
            //            }

            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(LoginViewModel mdl)
        {
            LoginViewModel modelLog = new LoginViewModel()
            {
                OwnerSmartId = "",
                Mobile = "",
                ErrorFlag = true
            };
            return View("OTPReceptionOption", modelLog);
            //using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            //{   

            //    var ownerOneTimeLoginOtps = adbc.OwnerOneTimeLoginOtps.Where(o => o.OwnerSmartId == mdl.OwnerSmartId || o.OwnerSlotId == mdl.OwnerSmartId).FirstOrDefault();

            //    if (ownerOneTimeLoginOtps == null)
            //    {
            //        LoginViewModel modelLog = new LoginViewModel()
            //        {   
            //            OwnerSmartId="",
            //            Mobile = "",
            //            ErrorFlag = true
            //        };
            //        return View("OTPReceptionOption", modelLog);
            //    }
            //    return View("Index");
            //}
        }

        [HttpGet]
        public ActionResult PasswordCreateGet(LoginViewModel mdl)
        {
            return View(mdl);
        }

        [HttpPost]
        public ActionResult OTPVerification(LoginViewModel model)
        {
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var otp = adbc.OwnerOneTimeLoginOtps.FirstOrDefault(o => o.OwnerSlotId == model.OwnerSmartId).OwnerLoginOTPCode;

                if (otp != model.OwnerLoginOTPCode)
                {
                    return RedirectToAction("SendingOTPViaEmail");
                }
                LoginViewModel mdl = new LoginViewModel()
                {
                    OwnerSmartId = model.OwnerSmartId,
                    SlotId = model.SlotId
                };
                return RedirectToAction("PasswordCreateGet", "Home", mdl);
            }
        }

        [HttpPost]
        public ActionResult PasswordCreate(LoginViewModel model)
        {
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var owner = adbc.OwnersInfos.FirstOrDefault(u => u.SlotId == model.OwnerSmartId);

                if (model.Password == model.ConfirmPassword)
                {
                    owner.Password = EncryptDecryptString.Encrypt(model.Password, "1234");

                    adbc.SaveChanges();
                    return RedirectToAction("Index");
                }

                else
                {
                    return RedirectToAction("PasswordCreateGet");
                }
                //adbc.OwnersInfos.Add(mdl);
            }
        }

        public string GenerateOTP(string OwnerSmartId)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerOneTimeLoginOtps = adbc.OwnerOneTimeLoginOtps.Where(o => o.OwnerSmartId == OwnerSmartId || o.OwnerSlotId == OwnerSmartId).FirstOrDefault();
                string ownerLoginOTPSMSCode = "";


                string token = "";
                bool checkPoint = false;
                while (checkPoint == false)
                {
                    Random r = new Random();
                    var x = r.Next(0, 1000000);
                    token = x.ToString("000000");

                    var OwnerOTPSMSCodeList = adbc.OwnerOtpCodes.Select(u => u.OwnerOTPSMSCode).ToList();
                    if (OwnerOTPSMSCodeList.Contains(token) == false)
                    {
                        ownerLoginOTPSMSCode = token;
                        checkPoint = true;
                    }
                }
                ownerOneTimeLoginOtps.OwnerLoginOTPCode = ownerLoginOTPSMSCode;
                ownerOneTimeLoginOtps.IsOTPUsed = false;
                //Audit Code goes here
                adbc.SaveChanges();
                return ownerLoginOTPSMSCode;
            }

        }

        [HttpPost]
        public ActionResult SendingOTPViaSMS_prev(string OwnerSmartId)
        {
            if (OwnerSmartId != "")
            {

                using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
                {
                    var ownerMobileNumber = adbc.OwnersInfos.Where(o => o.OwnerSmartId == OwnerSmartId || o.SlotId == OwnerSmartId).
                    Select(o => o.Mobile).FirstOrDefault();
                    var ownerLoginOTPSMSCode = GenerateOTP(OwnerSmartId);
                    LoginViewModel mdl = new LoginViewModel()
                    {
                        OwnerSmartId = OwnerSmartId,
                        OTPErrorFlag = false
                    };
                    //When Sending SMS is needed, Uncomment
                    //Common.SMSSending(ownerMobileNumber, "Your One Time Login Code: " + ownerLoginOTPSMSCode);

                    //var isSMSSent = "Processing";

                    //var txt = "Your One Time Login Code: " + ownerLoginOTPSMSCode;
                    //if (ownerMobileNumber != null && ownerMobileNumber != "")
                    //{
                    //    var returnStatus = Common.SMSSending(ownerMobileNumber, txt);
                    //    if (returnStatus == true)
                    //    {
                    //        isSMSSent = "Delivered";
                    //    }
                    //    else
                    //    {
                    //        isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                    //    }
                    //}
                    //else
                    //{
                    //    isSMSSent = "Not Delivered, Check whether Mobile No for this owner is correct";
                    //}

                    var Owner = adbc.OwnersInfos.Where(o => o.OwnerSmartId == OwnerSmartId || o.SlotId == OwnerSmartId)
                        .FirstOrDefault();
                    //EmailandSMSReport emailsmsreport = new EmailandSMSReport()
                    //{
                    //    IsSMSSent = isSMSSent,
                    //    Mobile = ownerMobileNumber,
                    //    Email = Owner.Email,
                    //    SlotId = Owner.SlotId,
                    //    Name = Owner.Name,
                    //    TimeOfDelivery = DateTime.Now,
                    //    NotificationType = "SMS",
                    //    Text = txt,
                    //    NotificationSerialNo = "AUTO GENERATED:- OTP",
                    //};
                    //Common.insertInEmailAndSMSTable(emailsmsreport);

                    return View("OTPCode", mdl);
                }
            }
            else
            {
                return View("OTPReceptionOption");

            }
        }

        [HttpGet]
        public ActionResult SendingOTPViaSMS(string OwnerSmartId)
        {
            if (OwnerSmartId != "")
            {
                using (ApplicationDbContext adbc = new ApplicationDbContext())
                {
                    var ownerMobile = adbc.OwnersInfos.Where(o => o.OwnerSmartId == OwnerSmartId || o.SlotId == OwnerSmartId).Select(o => o.Mobile).FirstOrDefault();
                    var ownerLoginOTPSMSCode = GenerateOTP(OwnerSmartId);
                    LoginViewModel mdl = new LoginViewModel()
                    {
                        OwnerSmartId = OwnerSmartId,
                        OTPErrorFlag = false,
                        OTPForEmailorSMS = true,
                        MobileEmail = "Mobile No: 0" + ownerMobile.Substring(0, 3 )+ "****"+ ownerMobile.Substring(ownerMobile.Length - 3),
                    };

                    var owner = adbc.OwnersInfos.Where(o => o.OwnerSmartId == OwnerSmartId || o.SlotId == OwnerSmartId)
                        .FirstOrDefault();
                    var smsBody = "Welcome to BW Heritage Owner Software,Your BW Heritage Owner Software password reset OTP is " + ownerLoginOTPSMSCode + ". Please put this code to reset your password.Thanks.";
                    var isSMSSent = "Processing";
                    
                    if (ownerMobile != null && ownerMobile != "")
                    {
                        string res = ownerMobile.Substring(0, 1);
                        if (res != "0")
                        {
                            ownerMobile = '0' + ownerMobile;
                        }
                        Common.SMSSending(ownerMobile, smsBody);
                        isSMSSent = "Delivered";
                    }
                    else
                    {
                        isSMSSent = "Failed";
                    }

                    EmailandSMSReport emailsmsreportForEmail = new EmailandSMSReport()
                    {
                        IsSMSSent = isSMSSent,
                        Mobile = ownerMobile,
                        Email = "",
                        SlotId = owner.SlotId,
                        Name = owner.Name,
                        TimeOfDelivery = DateTime.Now,
                        NotificationType = "Email",
                        //Text = model.EmailText,
                        Text = smsBody,
                        NotificationSerialNo = "AUTO GENERATED:- OTP",
                    };
                    Common.InsertEmailandSMSReports(emailsmsreportForEmail);


                    return View("OTPCodeForEmail", mdl);

                }
            }
            else
            {
                return View("OTPReceptionOption");
            }

        }

        [HttpGet] 
        public ActionResult SendingOTPViaEmail(string OwnerSmartId)
        {
            if (OwnerSmartId != "")
            {

                using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
                {

                    var ownerEmail = adbc.OwnersInfos.Where(o => o.OwnerSmartId == OwnerSmartId || o.SlotId == OwnerSmartId).Select(o => o.Email).FirstOrDefault();
                    var ownerLoginOTPSMSCode = GenerateOTP(OwnerSmartId);
                    LoginViewModel mdl = new LoginViewModel()
                    {
                        OwnerSmartId = OwnerSmartId,
                        OTPErrorFlag = false,
                        OTPForEmailorSMS = true,
                        MobileEmail= "Email: " + ownerEmail
                    };

                    var owner = adbc.OwnersInfos.Where(o => o.OwnerSmartId == OwnerSmartId || o.SlotId == OwnerSmartId)
                    .FirstOrDefault();
                    var Message ="Welcome to BW Heritage Owner Software,Your BW Heritage Owner Software password reset OTP is " + ownerLoginOTPSMSCode + "Please put this code to reset your password.Thanks.";
                    var isEmailSent = "Processing";
                    if (ownerEmail != null && ownerEmail != "")
                    {
                        Common.EmailSending(ownerEmail, "OTP Code From Heritage BD", Message);
                        isEmailSent = "Delivered";
                    }
                    else
                    {
                        isEmailSent = "Failed";
                    }

                    EmailandSMSReport emailsmsreportForEmail = new EmailandSMSReport()
                    {
                        IsSMSSent = isEmailSent,
                        Mobile = owner.Mobile,
                        Email = ownerEmail,
                        SlotId = owner.SlotId,
                        Name = owner.Name,
                        TimeOfDelivery = DateTime.Now,
                        NotificationType = "Email",
                        //Text = model.EmailText,
                        Text = Message,
                        NotificationSerialNo = "AUTO GENERATED:- OTP",
                    };
                    Common.InsertEmailandSMSReports(emailsmsreportForEmail);
                    return View("OTPCodeForEmail", mdl);

                }
            }
            else
            {
                return View("OTPReceptionOption");
                //return RedirectToAction("SendingOTPViaEmail");
                //Console.WriteLine("Give Your ID");
            }

        }

        public JsonResult GetNotifications()
        {
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var ownerId = Convert.ToString(Session[SessionKeys.USERNAME]);
                var list = adbc.Notifications.Where((n => n.isSeen == false && (n.DividendHolder == ownerId || n.NotificationHeader == "Notice"))).ToList();
                List<Notification> templist = new List<Notification>();
                foreach (Notification notification in list)
                {
                    if (notification.NotificationHeader == "Dividend" || notification.NotificationHeader == "Notice")
                    {
                        templist.Add(notification);
                    }
                }
                SortedDictionary<int, Notification> NotificationList = new SortedDictionary<int, Notification>();

                foreach (var notification in templist)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    NotificationList.Add(key, notification);
                }
                Dictionary<int, Notification> NotificationListReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in NotificationList.Reverse())
                {
                    NotificationListReverse.Add(notificationKeyValuePair.Key, notificationKeyValuePair.Value);
                }
                //adbc.SaveChanges();
                return Json(NotificationListReverse.Values.ToList());
            }
        }

        public PartialViewResult _GetTickers()
        {
            using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
            {

                var ticker = db.Tickers.ToList();
                //if (!string.IsNullOrEmpty(searchText))
                //{
                //    result = Tickers.Where(a => a.SlotName.ToLower().Contains(searchText.ToLower())).ToList();
                //}
                return PartialView("_TickerView", ticker);
            }

        }

        public JsonResult UpdateNotification(string notificationId)
        {
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var NotificationInDb =
                    adbc.Notifications.Where(n => n.NotificationId == notificationId).FirstOrDefault();
                NotificationInDb.isSeen = true;
                adbc.SaveChanges();
                return Json("nothing");
            }
        }

        [HttpGet]
        public ActionResult GoBack()
        {
            LoginViewModel mdl = new LoginViewModel()
            {
                ErrorFlag = false
            };
            return View("Index", mdl);
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            if (IsUserLoggedIn())
            {
                //Audit Code goes here
                // using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
                // {
                //     string slotId= Session[SessionKeys.USERNAME].ToString();
                //     var thisOwner = adbc.OwnersInfos.FirstOrDefault(r => r.SlotId == slotId);
                //     Audit objectAudit = new Audit()
                //     {
                //         AuditDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                //         LogInIp = "NIL",
                //         TableName = "0",
                //         UserLoginDTTM = "0",
                //         UserLogoutDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                //         UserName = thisOwner.SlotId,
                //         TaskName = "OwnerSite",
                //         ActionString = "LogOut",
                //         ActionDetails = "Owner with Slot Id: " + thisOwner.SlotId + " has Logged Out of his/her Account at " +
                //                         DateTime.Now.ToString("dd-MMM-yyyy hh:mm."),
                //     };
                //     StarterProjectV2.Common.insertInAuditTable(objectAudit);
                // }

                HttpContext.Response.Cache.SetNoStore();

                Session.Abandon();

                var x = Session[SessionKeys.USERNAME];
            }
            LoginViewModel mdl = new LoginViewModel()
            {
                ErrorFlag = false
            };
            return View("Index", mdl);
        }

        public ViewResult Error(string errorMessage)
        {
            ViewBag.Error = errorMessage;
            return View("Error");
        }
    }
}