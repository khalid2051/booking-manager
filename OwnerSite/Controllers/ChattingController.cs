﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OwnerSite.ViewModels;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;

namespace OwnerSite.Controllers
{
    public class ChattingController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult TermsAndConditions()
        {
            return View();
        }

        [HttpGet]
        public ActionResult WallPage()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                ViewBag.Current = "WallPage";
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var Comments = adbc.Comments.ToList();
                var PostList = adbc.Posts.Where(p => p.Approval == true || p.PostedBy == OwnerId).ToList().OrderByDescending(o => o.Time).ToList();
         
                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                var Likes = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).ToList();
                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                List<Post> selectedPosts = new List<Post>();
                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();
                foreach (var postId in MyPostIds)
                {
                    var tempList = adbc.Comments.Where(c => c.PostId == postId).Select(c => c.CommentId).ToList();
                    foreach (var commentId in tempList)
                    {
                        MyCommentIds.Add(commentId);
                    }
                }
                int noOfPages;
                if (PostList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedPosts.Add(PostList.ElementAt(i));
                    }
                    int x = (PostList.Count % 10 == 0) ? 0 : 1;

                    noOfPages = (PostList.Count / 10) + x;
                }
                else
                {
                    selectedPosts = PostList;
                    noOfPages = 1;
                }
                List<CommentView> CommentsViewList = new List<CommentView>();
                foreach (var comment in Comments)
                {
                    CommentsViewList.Add(new CommentView()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                            .Select(o => o.Name).FirstOrDefault()
                    });
                }
                List<string> PosterNameList = new List<string>();
                foreach (var post in selectedPosts)
                {
                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault());
                }


                var model = new WallPostingViewModel
                {
                    Posts = selectedPosts,
                    //TaskRoles = taskRoles,
                    NoOfPages = noOfPages,
                    index = 1,
                    MyPostIds = MyPostIds,
                    MyCommentIds = MyCommentIds,
                    Likes = LikedPostIds,
                    PostedByName = PosterNameList,
                    Comments = CommentsViewList
                };
                return View(model);
                //                ViewBag.Current = "WallPage";
                //                var OwnerId = Session[SessionKeys.USERNAME].ToString();
                //                var Posts = adbc.Posts.Where(p => p.Approval == true || p.PostedBy == OwnerId).ToList();
                //                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p=>p.PostId).ToList();
                ////                var Likes = adbc.Likes.Where(l=>l.LikedOwnerId==OwnerId).ToList();
                //                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l=>l.PostId).ToList();
                //                var Comments = adbc.Comments.ToList();
                //                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();
                ////                foreach (var postId in MyPostIds)
                ////                {
                ////                    var tempList = adbc.Comments.Where(c => c.PostId == postId).Select(c => c.CommentId).ToList();
                ////                    foreach (var commentId in tempList)
                ////                    {
                ////                        MyCommentIds.Add(commentId);
                ////                    }
                ////                }

                //                List<CommentView> CommentsViewList = new List<CommentView>();
                //                foreach (var comment in Comments)
                //                {
                //                    CommentsViewList.Add(new CommentView()
                //                    {
                //                        Comment = comment,
                //                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                //                            .Select(o => o.Name).FirstOrDefault()
                //                    });
                //                }

                //                SortedDictionary<int, Post> PostsList = new SortedDictionary<int, Post>();

                //                foreach (var post in Posts)
                //                {
                //                    int key = int.Parse(post.PostId.Split('-')[1]);
                //                    PostsList.Add(key, post);
                //                }

                //                Dictionary<int, Post> PostsListReverse = new Dictionary<int, Post>();

                //                foreach (var postKeyValuePair in PostsList.Reverse())
                //                {
                //                    PostsListReverse.Add(postKeyValuePair.Key, postKeyValuePair.Value);
                //                }

                //                var SortedPostList = PostsListReverse.Values.ToList();

                //                var LastPostDate = SortedPostList[0].Time.Date;
                //                var FirstPostDate = SortedPostList[SortedPostList.Count-1].Time.Date;

                //                int NoOfDays = ((TimeSpan) (LastPostDate - FirstPostDate)).Days;

                //                int NoOfWeeks = ((NoOfDays / 7) + 1);

                //                List<Post> selectedPosts = new List<Post>();

                //                Dictionary<int, List<Post>> SortedPostListByWeeks = new Dictionary<int, List<Post>>();

                ////                int WeekNoAsKey = 0;
                //                DateTime StartDayOfWeek = LastPostDate;
                //                DateTime LastDayOfWeek = LastPostDate.AddDays(-6);



                //                for (int i = 0; i < NoOfWeeks; i++)
                //                {
                //                    List<Post> PostsInAWeek = new List<Post>();
                //                    foreach (var SortedPost in SortedPostList)
                //                    {
                //                        if (StartDayOfWeek >= SortedPost.Time.Date && LastDayOfWeek <= SortedPost.Time.Date)
                //                        {
                //                            PostsInAWeek.Add(SortedPost);
                //                        }
                //                    }
                //                    SortedPostListByWeeks.Add(i, PostsInAWeek);
                //                    StartDayOfWeek = LastDayOfWeek.AddDays(-1);
                //                    LastDayOfWeek = StartDayOfWeek.AddDays(-6);
                //                    PostsInAWeek = new List<Post>();
                //                }

                //                List<string> PosterNameList = new List<string>();
                //                foreach (var post in SortedPostListByWeeks[0])
                //                {
                //                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                //                        .FirstOrDefault());
                //                }

                //                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                //                WallPostingViewModel mdl = new WallPostingViewModel()
                //                {
                //                    Posts = SortedPostListByWeeks[0],
                //                    Likes = LikedPostIds,
                //                    Comments = CommentsViewList,
                //                    PostedByName = PosterNameList,
                //                    MyPostIds = MyPostIds,
                //                    MyCommentIds = MyCommentIds,
                //                    SortByStatus = false,
                //                    NoOfWeeks = NoOfWeeks,
                //                    index = 1,
                //                    StartDate = StartDayOfWeek,
                //                    EndDate = LastDayOfWeek
                //                };
                //                return View(mdl);
            }
        }

        [HttpGet]
        public ActionResult WallPagePagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var Comments = adbc.Comments.ToList();
                var PostList = adbc.Posts.Where(p => p.Approval == true || p.PostedBy == OwnerId).ToList().OrderByDescending(o => o.Time).ToList();

                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                var Likes = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).ToList();
                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                List<Post> selectedPosts = new List<Post>();
                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();
                foreach (var postId in MyPostIds)
                {
                    var tempList = adbc.Comments.Where(c => c.PostId == postId).Select(c => c.CommentId).ToList();
                    foreach (var commentId in tempList)
                    {
                        MyCommentIds.Add(commentId);
                    }
                }
                int noOfPages;
        
                if (PostList.Count >= 10)
            {
                    var pageNo = (PostList.Count < (index * 10)) ? PostList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedPosts.Add(PostList.ElementAt(i));
                    }
                    int x = (selectedPosts.Count % 10 == 0) ? 0 : 1;

                    noOfPages = (selectedPosts.Count / 10) + x;
                }
                else
                {
                    selectedPosts = PostList;
                    noOfPages = 1;
                }
                List<CommentView> CommentsViewList = new List<CommentView>();
                foreach (var comment in Comments)
                {
                    CommentsViewList.Add(new CommentView()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                            .Select(o => o.Name).FirstOrDefault()
                    });
                }
                List<string> PosterNameList = new List<string>();
                foreach (var post in selectedPosts)
                {
                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault());
                }


                WallPostingViewModel mdl = new WallPostingViewModel()
                {
                    Posts = selectedPosts,
                    //TaskRoles = taskRoles,
                    NoOfPages = noOfPages,
                    index = index,
                    MyPostIds = MyPostIds,
                    MyCommentIds = MyCommentIds,
                    Likes = LikedPostIds,
                    PostedByName = PosterNameList,
                    Comments = CommentsViewList
                };
                return View("WallPage", mdl);
                //var OwnerId = Session[SessionKeys.USERNAME].ToString();
                //var Posts = adbc.Posts.ToList();
                //var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                ////                var Likes = adbc.Likes.Where(l=>l.LikedOwnerId==OwnerId).ToList();
                //var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                //var Comments = adbc.Comments.ToList();
                //var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();
                ////                foreach (var postId in MyPostIds)
                ////                {
                ////                    var tempList = adbc.Comments.Where(c => c.PostId == postId).Select(c => c.CommentId).ToList();
                ////                    foreach (var commentId in tempList)
                ////                    {
                ////                        MyCommentIds.Add(commentId);
                ////                    }
                ////                }

                //List<CommentView> CommentsViewList = new List<CommentView>();
                //foreach (var comment in Comments)
                //{
                //    CommentsViewList.Add(new CommentView()
                //    {
                //        Comment = comment,
                //        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                //            .Select(o => o.Name).FirstOrDefault()
                //    });
                //}

                //SortedDictionary<int, Post> PostsList = new SortedDictionary<int, Post>();

                //foreach (var post in Posts)
                //{
                //    int key = int.Parse(post.PostId.Split('-')[1]);
                //    PostsList.Add(key, post);
                //}

                //Dictionary<int, Post> PostsListReverse = new Dictionary<int, Post>();

                //foreach (var postKeyValuePair in PostsList.Reverse())
                //{
                //    PostsListReverse.Add(postKeyValuePair.Key, postKeyValuePair.Value);
                //}

                //var SortedPostList = PostsListReverse.Values.ToList();

                //var LastPostDate = SortedPostList[0].Time.Date;
                //var FirstPostDate = SortedPostList[SortedPostList.Count - 1].Time.Date;

                //int NoOfDays = ((TimeSpan)(LastPostDate - FirstPostDate)).Days;

                //int NoOfWeeks = ((NoOfDays / 7) + 1);

                //List<Post> selectedPosts = new List<Post>();

                //Dictionary<int, List<Post>> SortedPostListByWeeks = new Dictionary<int, List<Post>>();

                ////                int WeekNoAsKey = 0;
                //DateTime StartDayOfWeek = LastPostDate;
                //DateTime LastDayOfWeek = LastPostDate.AddDays(-6);

                //DateTime StartDate = StartDayOfWeek;
                //DateTime EndDate = LastDayOfWeek;

                //for (int i = 0; i < NoOfWeeks; i++)
                //{
                //    List<Post> PostsInAWeek = new List<Post>();
                //    foreach (var SortedPost in SortedPostList)
                //    {
                //        if (StartDayOfWeek >= SortedPost.Time.Date && LastDayOfWeek <= SortedPost.Time.Date)
                //        {
                //            PostsInAWeek.Add(SortedPost);
                //        }
                //    }
                //    if (index-1 == i)
                //    {
                //        StartDate = StartDayOfWeek;
                //        EndDate = LastDayOfWeek;
                //    }
                //    SortedPostListByWeeks.Add(i, PostsInAWeek);
                //    StartDayOfWeek = LastDayOfWeek.AddDays(-1);
                //    LastDayOfWeek = StartDayOfWeek.AddDays(-6);
                //    PostsInAWeek = new List<Post>();
                //}

                //List<string> PosterNameList = new List<string>();
                //foreach (var post in SortedPostListByWeeks[index-1])
                //{
                //    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                //        .FirstOrDefault());
                //}

                //SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                //WallPostingViewModel mdl = new WallPostingViewModel()
                //{
                //    Posts = SortedPostListByWeeks[index-1],
                //    Likes = LikedPostIds,
                //    Comments = CommentsViewList,
                //    PostedByName = PosterNameList,
                //    MyPostIds = MyPostIds,
                //    MyCommentIds = MyCommentIds,
                //    SortByStatus = false,
                //    NoOfWeeks = NoOfWeeks,
                //    index = index,
                //    StartDate = StartDate,
                //    EndDate = EndDate
                //};
                //return View("WallPage",mdl);
            }
        }

        [HttpGet]
        public ActionResult WallPagePaginationWithChange(int index, string postId)

        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.USERNAME].ToString();
                var Posts = adbc.Posts.ToList();
                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                //                var Likes = adbc.Likes.Where(l=>l.LikedOwnerId==OwnerId).ToList();
                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                var Comments = adbc.Comments.ToList();
                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();
                //                foreach (var postId in MyPostIds)
                //                {
                //                    var tempList = adbc.Comments.Where(c => c.PostId == postId).Select(c => c.CommentId).ToList();
                //                    foreach (var commentId in tempList)
                //                    {
                //                        MyCommentIds.Add(commentId);
                //                    }
                //                }

                List<CommentView> CommentsViewList = new List<CommentView>();
                foreach (var comment in Comments)
                {
                    CommentsViewList.Add(new CommentView()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                            .Select(o => o.Name).FirstOrDefault()
                    });
                }

                SortedDictionary<int, Post> PostsList = new SortedDictionary<int, Post>();

                foreach (var post in Posts)
                {
                    int key = int.Parse(post.PostId.Split('-')[1]);
                    PostsList.Add(key, post);
                }

                Dictionary<int, Post> PostsListReverse = new Dictionary<int, Post>();

                foreach (var postKeyValuePair in PostsList.Reverse())
                {
                    PostsListReverse.Add(postKeyValuePair.Key, postKeyValuePair.Value);
                }

                var SortedPostList = PostsListReverse.Values.ToList();

                var LastPostDate = SortedPostList[0].Time.Date;
                var FirstPostDate = SortedPostList[SortedPostList.Count - 1].Time.Date;

                int NoOfDays = ((TimeSpan)(LastPostDate - FirstPostDate)).Days;

                int NoOfWeeks = ((NoOfDays / 7) + 1);

                List<Post> selectedPosts = new List<Post>();

                Dictionary<int, List<Post>> SortedPostListByWeeks = new Dictionary<int, List<Post>>();

                //                int WeekNoAsKey = 0;
                DateTime StartDayOfWeek = LastPostDate;
                DateTime LastDayOfWeek = LastPostDate.AddDays(-6);

                DateTime StartDate = StartDayOfWeek;
                DateTime EndDate = LastDayOfWeek;

                for (int i = 0; i < NoOfWeeks; i++)
                {
                    List<Post> PostsInAWeek = new List<Post>();
                    foreach (var SortedPost in SortedPostList)
                    {
                        if (StartDayOfWeek >= SortedPost.Time.Date && LastDayOfWeek <= SortedPost.Time.Date)
                        {
                            PostsInAWeek.Add(SortedPost);
                        }
                    }
                    if (index - 1 == i)
                    {
                        StartDate = StartDayOfWeek;
                        EndDate = LastDayOfWeek;
                    }
                    SortedPostListByWeeks.Add(i, PostsInAWeek);
                    StartDayOfWeek = LastDayOfWeek.AddDays(-1);
                    LastDayOfWeek = StartDayOfWeek.AddDays(-6);
                    PostsInAWeek = new List<Post>();
                }

                List<Post> SortedPostListByWeeksWithAPostOnTop = new List<Post>();
                foreach (var post in SortedPostListByWeeks[index-1])
                {
                    if (post.PostId == postId)
                    {
                        SortedPostListByWeeksWithAPostOnTop.Add(post);
                    }
                }
                foreach (var post in SortedPostListByWeeks[index - 1])
                {
                    if (post.PostId != postId)
                    {
                        SortedPostListByWeeksWithAPostOnTop.Add(post);
                    }
                }

                List<string> PosterNameList = new List<string>();
                foreach (var post in SortedPostListByWeeksWithAPostOnTop)
                {
                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault());
                }

                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                WallPostingViewModel mdl = new WallPostingViewModel()
                {
                    Posts = SortedPostListByWeeksWithAPostOnTop,
                    Likes = LikedPostIds,
                    Comments = CommentsViewList,
                    PostedByName = PosterNameList,
                    MyPostIds = MyPostIds,
                    MyCommentIds = MyCommentIds,
                    SortByStatus = false,
                    NoOfWeeks = NoOfWeeks,
                    index = index,
                    StartDate = StartDate,
                    EndDate = EndDate
                };
                return View("WallPage", mdl);
            }
        }

        [HttpGet]
        public ActionResult WallPagePaginationWithChangeOnComment(int index, string commentId)

        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.USERNAME].ToString();
                var Posts = adbc.Posts.ToList();
                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                //                var Likes = adbc.Likes.Where(l=>l.LikedOwnerId==OwnerId).ToList();
                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                var Comments = adbc.Comments.ToList();
                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();
                //                foreach (var postId in MyPostIds)
                //                {
                //                    var tempList = adbc.Comments.Where(c => c.PostId == postId).Select(c => c.CommentId).ToList();
                //                    foreach (var commentId in tempList)
                //                    {
                //                        MyCommentIds.Add(commentId);
                //                    }
                //                }

                List<CommentView> CommentsViewList = new List<CommentView>();
                foreach (var comment in Comments)
                {
                    CommentsViewList.Add(new CommentView()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                            .Select(o => o.Name).FirstOrDefault()
                    });
                }

                SortedDictionary<int, Post> PostsList = new SortedDictionary<int, Post>();

                foreach (var post in Posts)
                {
                    int key = int.Parse(post.PostId.Split('-')[1]);
                    PostsList.Add(key, post);
                }

                Dictionary<int, Post> PostsListReverse = new Dictionary<int, Post>();

                foreach (var postKeyValuePair in PostsList.Reverse())
                {
                    PostsListReverse.Add(postKeyValuePair.Key, postKeyValuePair.Value);
                }

                var SortedPostList = PostsListReverse.Values.ToList();

                var LastPostDate = SortedPostList[0].Time.Date;
                var FirstPostDate = SortedPostList[SortedPostList.Count - 1].Time.Date;

                int NoOfDays = ((TimeSpan)(LastPostDate - FirstPostDate)).Days;

                int NoOfWeeks = ((NoOfDays / 7) + 1);

                List<Post> selectedPosts = new List<Post>();

                Dictionary<int, List<Post>> SortedPostListByWeeks = new Dictionary<int, List<Post>>();

                //                int WeekNoAsKey = 0;
                DateTime StartDayOfWeek = LastPostDate;
                DateTime LastDayOfWeek = LastPostDate.AddDays(-6);

                DateTime StartDate = StartDayOfWeek;
                DateTime EndDate = LastDayOfWeek;

                for (int i = 0; i < NoOfWeeks; i++)
                {
                    List<Post> PostsInAWeek = new List<Post>();
                    foreach (var SortedPost in SortedPostList)
                    {
                        if (StartDayOfWeek >= SortedPost.Time.Date && LastDayOfWeek <= SortedPost.Time.Date)
                        {
                            PostsInAWeek.Add(SortedPost);
                        }
                    }
                    if (index - 1 == i)
                    {
                        StartDate = StartDayOfWeek;
                        EndDate = LastDayOfWeek;
                    }
                    SortedPostListByWeeks.Add(i, PostsInAWeek);
                    StartDayOfWeek = LastDayOfWeek.AddDays(-1);
                    LastDayOfWeek = StartDayOfWeek.AddDays(-6);
                    PostsInAWeek = new List<Post>();
                }

                List<Post> SortedPostListByWeeksWithAPostOnTop = new List<Post>();
                var postId = adbc.Comments.Where(c => c.CommentId == commentId).Select(c => c.PostId).FirstOrDefault();
                foreach (var post in SortedPostListByWeeks[index - 1])
                {
                    if (post.PostId == postId)
                    {
                        SortedPostListByWeeksWithAPostOnTop.Add(post);
                    }
                }
                foreach (var post in SortedPostListByWeeks[index - 1])
                {
                    if (post.PostId != postId)
                    {
                        SortedPostListByWeeksWithAPostOnTop.Add(post);
                    }
                }

                List<string> PosterNameList = new List<string>();
                foreach (var post in SortedPostListByWeeksWithAPostOnTop)
                {
                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault());
                }

                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                WallPostingViewModel mdl = new WallPostingViewModel()
                {
                    Posts = SortedPostListByWeeksWithAPostOnTop,
                    Likes = LikedPostIds,
                    Comments = CommentsViewList,
                    PostedByName = PosterNameList,
                    MyPostIds = MyPostIds,
                    MyCommentIds = MyCommentIds,
                    SortByStatus = false,
                    NoOfWeeks = NoOfWeeks,
                    index = index,
                    StartDate = StartDate,
                    EndDate = EndDate
                };
                return View("WallPage", mdl);
            }
        }

        [HttpGet]
        public ActionResult WallPageByApprovalTimeSort()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.USERNAME].ToString();
                var Posts = adbc.Posts.ToList();
                var OrderByApprovalTimePostList = Posts.OrderBy(p => p.ApprovalTime).ToList();
                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                //                var Likes = adbc.Likes.Where(l=>l.LikedOwnerId==OwnerId).ToList();
                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                var Comments = adbc.Comments.ToList();
                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();


                List<CommentView> CommentsViewList = new List<CommentView>();
                foreach (var comment in Comments)
                {
                    CommentsViewList.Add(new CommentView()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                            .Select(o => o.Name).FirstOrDefault()
                    });
                }


                List<Post> PostsListReverse = new List<Post>();
                for (int i = OrderByApprovalTimePostList.Count - 1; i >= 0; i--)
                {
                    PostsListReverse.Add(OrderByApprovalTimePostList.ElementAt(i));
                }

                var SortedPostList = PostsListReverse;

                var LastPostDate = SortedPostList[0].ApprovalTime.Date;

                int indexForApprovalList = 0;
                foreach (var post in PostsListReverse)
                {
                    if (post.ApprovalTime != Convert.ToDateTime("1900-01-01 00:00:00.000"))
                    {
                        indexForApprovalList++;
                    }
                }

                var FirstPostDate = SortedPostList[indexForApprovalList - 1].ApprovalTime.Date;

                int NoOfDays = ((TimeSpan)(LastPostDate - FirstPostDate)).Days;

                int NoOfWeeks = ((NoOfDays / 7) + 1);

                List<Post> selectedPosts = new List<Post>();

                Dictionary<int, List<Post>> SortedPostListByWeeks = new Dictionary<int, List<Post>>();

                //                int WeekNoAsKey = 0;
                DateTime StartDayOfWeek = LastPostDate;
                DateTime LastDayOfWeek = LastPostDate.AddDays(-6);



                for (int i = 0; i < NoOfWeeks; i++)
                {
                    List<Post> PostsInAWeek = new List<Post>();
                    foreach (var SortedPost in SortedPostList)
                    {
                        if (StartDayOfWeek >= SortedPost.ApprovalTime.Date && LastDayOfWeek <= SortedPost.ApprovalTime.Date)
                        {
                            PostsInAWeek.Add(SortedPost);
                        }
                    }
                    SortedPostListByWeeks.Add(i, PostsInAWeek);
                    StartDayOfWeek = LastDayOfWeek.AddDays(-1);
                    LastDayOfWeek = StartDayOfWeek.AddDays(-6);
                    PostsInAWeek = new List<Post>();
                }

                List<Post> UnApprovedPosts = new List<Post>();
                foreach (var post in SortedPostList)
                {
                    if (post.ApprovalTime == Convert.ToDateTime("1900-01-01 00:00:00.000"))
                    {
                        UnApprovedPosts.Add(post);
                    }
                }

                List<Post> UnApprovedPostsSortedByPostTime = new List<Post>();

                foreach (var UnSortedPost in UnApprovedPosts.OrderBy(p => p.Time).Reverse())
                {
                    UnApprovedPostsSortedByPostTime.Add(UnSortedPost);
                }

                SortedPostListByWeeks.Add(NoOfWeeks, UnApprovedPostsSortedByPostTime);
                List<string> PosterNameList = new List<string>();
                foreach (var post in SortedPostListByWeeks[0])
                {
                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault());
                }

                WallPostingViewModel mdl = new WallPostingViewModel()
                {
                    Posts = SortedPostListByWeeks[0],
                    Likes = LikedPostIds,
                    Comments = CommentsViewList,
                    PostedByName = PosterNameList,
                    MyPostIds = MyPostIds,
                    MyCommentIds = MyCommentIds,
                    SortByStatus = true,
                    NoOfWeeks = NoOfWeeks+1,
                    index = 1,
                    StartDate = StartDayOfWeek,
                    EndDate = LastDayOfWeek
                };
                return View("WallPage", mdl);
            }
        }

        [HttpGet]
        public ActionResult WallPageByApprovalTimeSortByPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.USERNAME].ToString();
                var Posts = adbc.Posts.ToList();
                var OrderByApprovalTimePostList = Posts.OrderBy(p => p.ApprovalTime).ToList();
                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                //                var Likes = adbc.Likes.Where(l=>l.LikedOwnerId==OwnerId).ToList();
                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                var Comments = adbc.Comments.ToList();
                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();


                List<CommentView> CommentsViewList = new List<CommentView>();
                foreach (var comment in Comments)
                {
                    CommentsViewList.Add(new CommentView()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                            .Select(o => o.Name).FirstOrDefault()
                    });
                }


                List<Post> PostsListReverse = new List<Post>();
                for (int i = OrderByApprovalTimePostList.Count - 1; i >= 0; i--)
                {
                    PostsListReverse.Add(OrderByApprovalTimePostList.ElementAt(i));
                }

                var SortedPostList = PostsListReverse;

                var LastPostDate = SortedPostList[0].ApprovalTime.Date;

                int indexForApprovalList = 0;
                foreach (var post in PostsListReverse)
                {
                    if (post.ApprovalTime != Convert.ToDateTime("1900-01-01 00:00:00.000"))
                    {
                        indexForApprovalList++;
                    }
                }

                var FirstPostDate = SortedPostList[indexForApprovalList - 1].ApprovalTime.Date;

                int NoOfDays = ((TimeSpan)(LastPostDate - FirstPostDate)).Days;

                int NoOfWeeks = ((NoOfDays / 7) + 1);

                List<Post> selectedPosts = new List<Post>();

                Dictionary<int, List<Post>> SortedPostListByWeeks = new Dictionary<int, List<Post>>();

                //                int WeekNoAsKey = 0;
                DateTime StartDayOfWeek = LastPostDate;
                DateTime LastDayOfWeek = LastPostDate.AddDays(-6);

                DateTime StartDate = StartDayOfWeek;
                DateTime EndDate = LastDayOfWeek;

                for (int i = 0; i < NoOfWeeks; i++)
                {
                    List<Post> PostsInAWeek = new List<Post>();
                    foreach (var SortedPost in SortedPostList)
                    {
                        if (StartDayOfWeek >= SortedPost.ApprovalTime.Date && LastDayOfWeek <= SortedPost.ApprovalTime.Date)
                        {
                            PostsInAWeek.Add(SortedPost);
                        }
                    }
                    SortedPostListByWeeks.Add(i, PostsInAWeek);
                    if (index-1 == i)
                    {
                        StartDate = StartDayOfWeek;
                        EndDate = LastDayOfWeek;
                    }
                    StartDayOfWeek = LastDayOfWeek.AddDays(-1);
                    LastDayOfWeek = StartDayOfWeek.AddDays(-6);
                    PostsInAWeek = new List<Post>();
                }

                List<Post> UnApprovedPosts = new List<Post>();
                foreach (var post in SortedPostList)
                {
                    if (post.ApprovalTime == Convert.ToDateTime("1900-01-01 00:00:00.000"))
                    {
                        UnApprovedPosts.Add(post);
                    }
                }
                List<Post> UnApprovedPostsSortedByPostTime = new List<Post>();

                foreach (var UnSortedPost in UnApprovedPosts.OrderBy(p => p.Time).Reverse())
                {
                    UnApprovedPostsSortedByPostTime.Add(UnSortedPost);
                }

                SortedPostListByWeeks.Add(NoOfWeeks, UnApprovedPostsSortedByPostTime);
                List<string> PosterNameList = new List<string>();
                foreach (var post in SortedPostListByWeeks[index-1])
                {
                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault());
                }

                WallPostingViewModel mdl = new WallPostingViewModel()
                {
                    Posts = SortedPostListByWeeks[index-1],
                    Likes = LikedPostIds,
                    Comments = CommentsViewList,
                    PostedByName = PosterNameList,
                    MyPostIds = MyPostIds,
                    MyCommentIds = MyCommentIds,
                    SortByStatus = true,
                    NoOfWeeks = NoOfWeeks + 1,
                    index = index,
                    StartDate = StartDate,
                    EndDate = EndDate
                };
                return View("WallPage", mdl);
            }
        }

        [HttpGet]
        public ActionResult WallPageByApprovalTimeSortByPaginationWithChange(int index, string postId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.USERNAME].ToString();
                var Posts = adbc.Posts.ToList();
                var OrderByApprovalTimePostList = Posts.OrderBy(p => p.ApprovalTime).ToList();
                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                //                var Likes = adbc.Likes.Where(l=>l.LikedOwnerId==OwnerId).ToList();
                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                var Comments = adbc.Comments.ToList();
                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();


                List<CommentView> CommentsViewList = new List<CommentView>();
                foreach (var comment in Comments)
                {
                    CommentsViewList.Add(new CommentView()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                            .Select(o => o.Name).FirstOrDefault()
                    });
                }


                List<Post> PostsListReverse = new List<Post>();
                for (int i = OrderByApprovalTimePostList.Count - 1; i >= 0; i--)
                {
                    PostsListReverse.Add(OrderByApprovalTimePostList.ElementAt(i));
                }

                var SortedPostList = PostsListReverse;

                var LastPostDate = SortedPostList[0].ApprovalTime.Date;

                int indexForApprovalList = 0;
                foreach (var post in PostsListReverse)
                {
                    if (post.ApprovalTime != Convert.ToDateTime("1900-01-01 00:00:00.000"))
                    {
                        indexForApprovalList++;
                    }
                }

                var FirstPostDate = SortedPostList[indexForApprovalList - 1].ApprovalTime.Date;

                int NoOfDays = ((TimeSpan)(LastPostDate - FirstPostDate)).Days;

                int NoOfWeeks = ((NoOfDays / 7) + 1);

                List<Post> selectedPosts = new List<Post>();

                Dictionary<int, List<Post>> SortedPostListByWeeks = new Dictionary<int, List<Post>>();

                //                int WeekNoAsKey = 0;
                DateTime StartDayOfWeek = LastPostDate;
                DateTime LastDayOfWeek = LastPostDate.AddDays(-6);

                DateTime StartDate = StartDayOfWeek;
                DateTime EndDate = LastDayOfWeek;

                for (int i = 0; i < NoOfWeeks; i++)
                {
                    List<Post> PostsInAWeek = new List<Post>();
                    foreach (var SortedPost in SortedPostList)
                    {
                        if (StartDayOfWeek >= SortedPost.ApprovalTime.Date && LastDayOfWeek <= SortedPost.ApprovalTime.Date)
                        {
                            PostsInAWeek.Add(SortedPost);
                        }
                    }
                    SortedPostListByWeeks.Add(i, PostsInAWeek);
                    if (index - 1 == i)
                    {
                        StartDate = StartDayOfWeek;
                        EndDate = LastDayOfWeek;
                    }
                    StartDayOfWeek = LastDayOfWeek.AddDays(-1);
                    LastDayOfWeek = StartDayOfWeek.AddDays(-6);
                    PostsInAWeek = new List<Post>();
                }

                List<Post> UnApprovedPosts = new List<Post>();
                foreach (var post in SortedPostList)
                {
                    if (post.ApprovalTime == Convert.ToDateTime("1900-01-01 00:00:00.000"))
                    {
                        UnApprovedPosts.Add(post);
                    }
                }
                List<Post> UnApprovedPostsSortedByPostTime = new List<Post>();

                foreach (var UnSortedPost in UnApprovedPosts.OrderBy(p => p.Time).Reverse())
                {
                    UnApprovedPostsSortedByPostTime.Add(UnSortedPost);
                }

                SortedPostListByWeeks.Add(NoOfWeeks, UnApprovedPostsSortedByPostTime);
                List<Post> SortedPostListByWeeksWithAPostOnTop = new List<Post>();
                foreach (var post in SortedPostListByWeeks[index - 1])
                {
                    if (post.PostId == postId)
                    {
                        SortedPostListByWeeksWithAPostOnTop.Add(post);
                    }
                }
                foreach (var post in SortedPostListByWeeks[index - 1])
                {
                    if (post.PostId != postId)
                    {
                        SortedPostListByWeeksWithAPostOnTop.Add(post);
                    }
                }

                List<string> PosterNameList = new List<string>();
                foreach (var post in SortedPostListByWeeksWithAPostOnTop)
                {
                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault());
                }

                WallPostingViewModel mdl = new WallPostingViewModel()
                {
                    Posts = SortedPostListByWeeksWithAPostOnTop,
                    Likes = LikedPostIds,
                    Comments = CommentsViewList,
                    PostedByName = PosterNameList,
                    MyPostIds = MyPostIds,
                    MyCommentIds = MyCommentIds,
                    SortByStatus = true,
                    NoOfWeeks = NoOfWeeks + 1,
                    index = index,
                    StartDate = StartDate,
                    EndDate = EndDate
                };
                return View("WallPage", mdl);
            }
        }

        [HttpGet]
        public ActionResult WallPageByApprovalTimeSortByPaginationWithChangeOnComment(int index, string commentId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.USERNAME].ToString();
                var Posts = adbc.Posts.ToList();
                var OrderByApprovalTimePostList = Posts.OrderBy(p => p.ApprovalTime).ToList();
                var MyPostIds = adbc.Posts.Where(p => p.PostedBy == OwnerId).Select(p => p.PostId).ToList();
                //                var Likes = adbc.Likes.Where(l=>l.LikedOwnerId==OwnerId).ToList();
                var LikedPostIds = adbc.Likes.Where(l => l.LikedOwnerId == OwnerId).Select(l => l.PostId).ToList();
                var Comments = adbc.Comments.ToList();
                var MyCommentIds = adbc.Comments.Where(c => c.CommentorId == OwnerId).Select(c => c.CommentId).ToList();


                List<CommentView> CommentsViewList = new List<CommentView>();
                foreach (var comment in Comments)
                {
                    CommentsViewList.Add(new CommentView()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId)
                            .Select(o => o.Name).FirstOrDefault()
                    });
                }


                List<Post> PostsListReverse = new List<Post>();
                for (int i = OrderByApprovalTimePostList.Count - 1; i >= 0; i--)
                {
                    PostsListReverse.Add(OrderByApprovalTimePostList.ElementAt(i));
                }

                var SortedPostList = PostsListReverse;

                var LastPostDate = SortedPostList[0].ApprovalTime.Date;

                int indexForApprovalList = 0;
                foreach (var post in PostsListReverse)
                {
                    if (post.ApprovalTime != Convert.ToDateTime("1900-01-01 00:00:00.000"))
                    {
                        indexForApprovalList++;
                    }
                }

                var FirstPostDate = SortedPostList[indexForApprovalList - 1].ApprovalTime.Date;

                int NoOfDays = ((TimeSpan)(LastPostDate - FirstPostDate)).Days;

                int NoOfWeeks = ((NoOfDays / 7) + 1);

                List<Post> selectedPosts = new List<Post>();

                Dictionary<int, List<Post>> SortedPostListByWeeks = new Dictionary<int, List<Post>>();

                //                int WeekNoAsKey = 0;
                DateTime StartDayOfWeek = LastPostDate;
                DateTime LastDayOfWeek = LastPostDate.AddDays(-6);

                DateTime StartDate = StartDayOfWeek;
                DateTime EndDate = LastDayOfWeek;

                for (int i = 0; i < NoOfWeeks; i++)
                {
                    List<Post> PostsInAWeek = new List<Post>();
                    foreach (var SortedPost in SortedPostList)
                    {
                        if (StartDayOfWeek >= SortedPost.ApprovalTime.Date && LastDayOfWeek <= SortedPost.ApprovalTime.Date)
                        {
                            PostsInAWeek.Add(SortedPost);
                        }
                    }
                    SortedPostListByWeeks.Add(i, PostsInAWeek);
                    if (index - 1 == i)
                    {
                        StartDate = StartDayOfWeek;
                        EndDate = LastDayOfWeek;
                    }
                    StartDayOfWeek = LastDayOfWeek.AddDays(-1);
                    LastDayOfWeek = StartDayOfWeek.AddDays(-6);
                    PostsInAWeek = new List<Post>();
                }

                List<Post> UnApprovedPosts = new List<Post>();
                foreach (var post in SortedPostList)
                {
                    if (post.ApprovalTime == Convert.ToDateTime("1900-01-01 00:00:00.000"))
                    {
                        UnApprovedPosts.Add(post);
                    }
                }
                List<Post> UnApprovedPostsSortedByPostTime = new List<Post>();

                foreach (var UnSortedPost in UnApprovedPosts.OrderBy(p => p.Time).Reverse())
                {
                    UnApprovedPostsSortedByPostTime.Add(UnSortedPost);
                }

                SortedPostListByWeeks.Add(NoOfWeeks, UnApprovedPostsSortedByPostTime);
                var postId = adbc.Comments.Where(c => c.CommentId == commentId).Select(c => c.PostId).FirstOrDefault();
                List<Post> SortedPostListByWeeksWithAPostOnTop = new List<Post>();
                foreach (var post in SortedPostListByWeeks[index - 1])
                {
                    if (post.PostId == postId)
                    {
                        SortedPostListByWeeksWithAPostOnTop.Add(post);
                    }
                }
                foreach (var post in SortedPostListByWeeks[index - 1])
                {
                    if (post.PostId != postId)
                    {
                        SortedPostListByWeeksWithAPostOnTop.Add(post);
                    }
                }

                List<string> PosterNameList = new List<string>();
                foreach (var post in SortedPostListByWeeksWithAPostOnTop)
                {
                    PosterNameList.Add(adbc.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault());
                }

                WallPostingViewModel mdl = new WallPostingViewModel()
                {
                    Posts = SortedPostListByWeeksWithAPostOnTop,
                    Likes = LikedPostIds,
                    Comments = CommentsViewList,
                    PostedByName = PosterNameList,
                    MyPostIds = MyPostIds,
                    MyCommentIds = MyCommentIds,
                    SortByStatus = true,
                    NoOfWeeks = NoOfWeeks + 1,
                    index = index,
                    StartDate = StartDate,
                    EndDate = EndDate
                };
                return View("WallPage", mdl);
            }
        }

        public JsonResult gettingAPost(PostingJsonClass mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var PostList = adbc.Posts.ToList();
                var PostIdList = new List<int>();
                foreach (var posts in PostList)
                {
                    PostIdList.Add(int.Parse(posts.PostId.ToString().Split('-')[1]));
                }

                var postToken = PostList.Count();
                if (postToken > 0)
                {
                    PostIdList.Sort();
                    postToken = PostIdList[PostIdList.Count - 1];
                }

                postToken++;
                var approval = adbc.ConfPostCommentApprovals.FirstOrDefault().PostAutoApprovalEnabled;
                Post model = new Post()
                {
                    Approval = approval,
                    NoOfLikes = 0,
                    PostId = "POST-" + postToken,
                    PostedBy = OwnerId,
                    PostText = mdl.Text,
                    PostHeader = mdl.Title,
                    Time = DateTime.Now,
                    ApprovalTime = DateTime.Now
                };
                adbc.Posts.Add(model);

                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }

                notificationToken++;
                var thisOwner = adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).FirstOrDefault();
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "Post",
                    NotificationText = thisOwner.SlotId + " has made a new post at "+ DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") +".",
                    NotificationLink = "/OwnersInfo/PostView?postId=POST-" + postToken,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-"+notificationToken,
                    IdForRecognition = "POST-"+postToken
                };
                adbc.Notifications.Add(notificationModel);
                adbc.SaveChanges();
            }

            return Json("Okay");
        }
        public JsonResult gettingAComment(CommentingJsonClass mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var CommentList = adbc.Comments.ToList();
                var CommentIdList = new List<int>();
                foreach (var comments in CommentList)
                {
                    CommentIdList.Add(int.Parse(comments.CommentId.ToString().Split('-')[1]));
                }

                var commentToken = CommentList.Count();
                if (commentToken > 0)
                {
                    CommentIdList.Sort();
                    commentToken = CommentIdList[CommentIdList.Count - 1];
                }

                commentToken++;
                var approval = adbc.ConfPostCommentApprovals.FirstOrDefault().CommentAutoApprovalEnabled;
                Comment model = new Comment()
                {
                    Approval = approval,
                    CommentId = "CMNT-"+commentToken,
                    CommentText = mdl.CommentText,
                    CommentorId = OwnerId,
                    PostId = mdl.PostId,
                    CommentTime = DateTime.Now,
                    ApprovalTime = DateTime.Now
                };
                adbc.Comments.Add(model);
                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }

                notificationToken++;
                var thisOwner = adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).FirstOrDefault();
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "Comment",
                    NotificationText = thisOwner.SlotId + " has made a new comment at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy")+".",
                    NotificationLink = "/OwnersInfo/CommentView?commentId=CMNT-" + commentToken,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = "CMNT-" + commentToken
                };
                adbc.Notifications.Add(notificationModel);


                adbc.SaveChanges();
            }

            return Json("okay");
        }

        public JsonResult gettingALike(string PostId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();

                var LikeList = adbc.Likes.ToList();
                var LikeIdList = new List<int>();
                foreach (var likes in LikeList)
                {
                    LikeIdList.Add(int.Parse(likes.LikeId.ToString().Split('-')[1]));
                }

                var likeToken = LikeList.Count();
                if (likeToken > 0)
                {
                    LikeIdList.Sort();
                    likeToken = LikeIdList[LikeIdList.Count - 1];
                }

                likeToken++;
                Like model = new Like()
                {
                    LikeId = "LIKE-"+likeToken,
                    LikedOwnerId = OwnerId,
                    PostId = PostId,
                };
                var NoOfLikesInDb = adbc.Posts.Where(p => p.PostId == PostId).FirstOrDefault();
                NoOfLikesInDb.NoOfLikes += 1; 
                adbc.Likes.Add(model);
                adbc.SaveChanges();
            }
            return Json("Yep");
        }

        public JsonResult gettingAnUnlike(string PostId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var LikedObject = adbc.Likes.Where(l=>l.PostId==PostId&&l.LikedOwnerId==OwnerId).FirstOrDefault();
                adbc.Likes.Remove(LikedObject);
                var NoOfLikesInDb = adbc.Posts.Where(p => p.PostId == PostId).FirstOrDefault();
                NoOfLikesInDb.NoOfLikes -= 1;
                adbc.SaveChanges();
            }
            return Json("Yep");
        }

        public JsonResult deletingAPost(string PostId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var PostObject = adbc.Posts.Where(p => p.PostId == PostId).FirstOrDefault();
                var PreviousPostText = PostObject.PostText;
                var CommentObjectLists = adbc.Comments.Where(c => c.PostId == PostId).ToList();
                foreach (var commentObject in CommentObjectLists)
                {
                    adbc.Comments.Remove(commentObject);
                }
                var LikeObjectLists = adbc.Likes.Where(c => c.PostId == PostId).ToList();
                foreach (var likeObject in LikeObjectLists)
                {
                    adbc.Likes.Remove(likeObject);
                }
                adbc.Posts.Remove(PostObject);
                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }

                notificationToken++;
                var thisOwner = adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).FirstOrDefault();
                //Audit Code goes here
                Audit objectAudit = new Audit()
                {
                    AuditDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                    LogInIp = "NIL",
                    TableName = "Post",
                    UserLoginDTTM = "0",
                    UserLogoutDTTM = "0",
                    UserName = thisOwner.SlotId,
                    TaskName = "Post Delete",
                    ActionString = "Delete",
                    ActionDetails = "Owner with Slot Id: " + thisOwner.SlotId + " has deleted a post at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm.") + " where the previous post was: " + PreviousPostText,
                };
                string AuditId = StarterProjectV2.Common.InsertAudit(objectAudit);

                
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "DeletedPost",
                    NotificationText = thisOwner.SlotId + " has deleted a post at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                    NotificationLink = "/Audit/AuditView?auditId=" + AuditId,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = "none"
                };
                adbc.Notifications.Add(notificationModel);
                var NotificationObj = adbc.Notifications.Where(n => n.IdForRecognition == PostId).ToList();
                foreach (Notification notification in NotificationObj)
                {
                    notification.NotificationLink = "/Audit/AuditView?auditId=" + AuditId;
                }
                adbc.SaveChanges();
            }
            return Json("llll");
        }

        public JsonResult deletingAComment(string CommentId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            var postId = "";
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var CommentObject = adbc.Comments.Where(c => c.CommentId == CommentId).FirstOrDefault();
                postId = CommentObject.PostId;
                adbc.Comments.Remove(CommentObject);
                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }

                notificationToken++;
                var thisOwner = adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).FirstOrDefault();

                //Audit Code goes here
                Audit objectAudit = new Audit()
                {
                    AuditDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                    LogInIp = "NIL",
                    TableName = "Comment",
                    UserLoginDTTM = "0",
                    UserLogoutDTTM = "0",
                    UserName = thisOwner.SlotId,
                    TaskName = "Comment Delete",
                    ActionString = "Delete",
                    ActionDetails = "Owner with Slot Id: " + thisOwner.SlotId + " has deleted a comment at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm.") + " where the previous comment was: " + CommentObject.CommentText,
                };
                string AuditId = StarterProjectV2.Common.InsertAudit(objectAudit);

                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "DeletedComment",
                    NotificationText = thisOwner.SlotId + " has deleted a comment at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                    NotificationLink = "/Audit/AuditView?auditId=" + AuditId,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = "none"
                };
                adbc.Notifications.Add(notificationModel);
                var NotificationObj = adbc.Notifications.Where(n => n.IdForRecognition == CommentId).ToList();
                foreach (Notification notification in NotificationObj)
                {
                    notification.NotificationLink = "/Audit/AuditView?auditId=" + AuditId;
                }
                adbc.SaveChanges();
            }
            return Json(postId);
        }

        public JsonResult editingAPost(EditingJsonClass mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var PostInDb = adbc.Posts.Where(p => p.PostId == mdl.PostId).FirstOrDefault();
                var PrevPostText = PostInDb.PostText;
                PostInDb.PostText = mdl.PostText;
                PostInDb.PostHeader = mdl.PostTitle;

                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }

                notificationToken++;
                var thisOwner = adbc.OwnersInfos.Where(o => o.OwenerId == PostInDb.PostedBy).FirstOrDefault();
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "EditedPost",
                    NotificationText = thisOwner.SlotId + " has edited a post at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy")+".",
                    NotificationLink = "/OwnersInfo/PostView?postId=" + PostInDb.PostId,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = PostInDb.PostId
                };
                adbc.Notifications.Add(notificationModel);
                adbc.SaveChanges();
            }
            return Json("YYY");
        }

        public JsonResult editingAComment(EditingCommentJsonClass mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            string postId = "";
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var CommentInDb = adbc.Comments.Where(c => c.CommentId == mdl.CommentId).FirstOrDefault();
                var PrevCommentText = CommentInDb.CommentText;
                postId = CommentInDb.PostId;
                CommentInDb.CommentText = mdl.CommentText;

                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }

                notificationToken++;
                var thisOwner = adbc.OwnersInfos.Where(o => o.OwenerId == CommentInDb.CommentorId).FirstOrDefault();
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "EditedComment",
                    NotificationText = thisOwner.SlotId + " has edited a comment at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy")+".",
                    NotificationLink = "/OwnersInfo/CommentView?commentId=" + CommentInDb.CommentId,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = CommentInDb.CommentId
                };
                adbc.Notifications.Add(notificationModel);
                adbc.SaveChanges();
            }
            return Json(postId);
        }

        public JsonResult gettingSearchResultForPost(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
            {
                var PostList = db.Posts.ToList();
                List<Post> searchedPosts = new List<Post>();
                var searchfield1 = searchfield.ToUpper();
                List<searchedPostResultViewModel> res = new List<searchedPostResultViewModel>();
                foreach (var post in PostList)
                {
                    var PostHeader = "";
                    if (post.PostHeader != null)
                    {
                        PostHeader = post.PostHeader.ToUpper();
                    }

                    var PostText = "";
                    if (post.PostText != null)
                    {
                        PostText = post.PostText.ToUpper();
                    }

                    var PosterName = "";
                    var val = db.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault();
                    if (val != null)
                    {
                        PosterName = val.ToUpper();
                    }

                    var ApprovalTime = "";
                    if (post.ApprovalTime != null)
                    {
                        ApprovalTime = ApprovalTime.ToUpper();
                    }

                    var PostHeader1 = PostHeader;
                    var PostText1 = PostText;
                    var PosterName1 = PosterName;
                    var ApprovalTime1 = ApprovalTime;

                    

                    if (PostHeader1.Contains(searchfield1) == true || PostText1.Contains(searchfield1) == true ||
                        PosterName1.Contains(searchfield1) == true || ApprovalTime1.Contains(searchfield1) == true)
                    {
                        res.Add(new searchedPostResultViewModel()
                        {
                            PostedBy = post.PostedBy,
                            PostId = post.PostId,
                            PostHeader = post.PostHeader,
                            Time = Convert.ToString(post.Time),
                            OwnerName = db.OwnersInfos.Where(o=>o.OwenerId==post.PostedBy).Select(o=>o.Name).FirstOrDefault()
                        });
                    }

                }

                return Json(res);
            }
        }

        [HttpGet]
        public ActionResult PostView(string postId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var model = adbc.Posts.Where(p => p.PostId == postId).SingleOrDefault();
                var CommentList = adbc.Comments.Where(c => c.PostId == postId).ToList();
                List<CommentViewFromAdminSite> Comments = new List<CommentViewFromAdminSite>();

                foreach (var comment in CommentList)
                {
                    Comments.Add(new CommentViewFromAdminSite()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(c=>c.OwenerId==comment.CommentorId).Select(c=>c.Name).FirstOrDefault(),
                    });
                }

                StarterProjectV2.ViewModels.PostViewModel mdl = new StarterProjectV2.ViewModels.PostViewModel()
                {
                    PostId = model.PostId,
                    Approval = model.Approval,
                    NoOfLikes = model.NoOfLikes,
                    OwnersName = adbc.OwnersInfos.Where(o => o.OwenerId == model.PostedBy).Select(o => o.Name).FirstOrDefault(),
                    PostedBy = model.PostedBy,
                    PostText = model.PostText,
                    PostHeader = model.PostHeader,
                    FlagLike = (adbc.Likes.Where(l=>l.PostId==postId && l.LikedOwnerId==OwnerId).FirstOrDefault()==null)?false:true,
                    Time = model.Time,
                    ApproveTime = model.ApprovalTime,
                    Comments = Comments,
                    NoOfComments = CommentList.Count
                };
                //adbc.Modules.Remove(delobj);
                //adbc.SaveChanges();
                return View("PostView", mdl);
            }
        }

        public JsonResult gettingSearchResultForComment(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
            {
                var CommentList = db.Comments.ToList();
                List<Comment> searchedPosts = new List<Comment>();
                var searchfield1 = searchfield.ToUpper();
                List<searchedCommentResultViewModel> res = new List<searchedCommentResultViewModel>();
                foreach (var comment in CommentList)
                {
                    var CommentId = "";
                    if (comment.CommentId != null)
                    {
                        CommentId = comment.CommentId.ToUpper();
                    }

                    var CommentText = "";
                    if (comment.CommentText != null)
                    {
                        CommentText = comment.CommentText.ToUpper();
                    }

                    var CommentorName = "";
                    var val = db.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId).Select(o => o.Name)
                        .FirstOrDefault();
                    if (val != null)
                    {
                        CommentorName = val.ToUpper();
                    }

                    var CommentTime = "";
                    if (comment.CommentTime != null)
                    {
                        CommentTime = Convert.ToString(comment.CommentTime).ToUpper();
                    }

                    var PostId = "";
                    if (comment.PostId != null)
                    {
                        PostId = comment.PostId;
                    }

                    var CommentId1 = CommentId;
                    var CommentText1 = CommentText;
                    var CommentorName1 = CommentorName;
                    var CommentTime1 = CommentTime;
                    var PostId1 = PostId;

                    if (CommentId1.Contains(searchfield1) == true || CommentText1.Contains(searchfield1) == true ||
                        CommentorName1.Contains(searchfield1) == true || CommentTime1.Contains(searchfield1) == true ||
                        PostId1.Contains(searchfield1) == true)
                    {
                        res.Add(new searchedCommentResultViewModel()
                        {
                            CommentId = comment.CommentId,
                            CommentText = comment.CommentText,
                            CommentorName = val,
                            CommentTime = Convert.ToString(comment.CommentTime),
                            PostHeader = db.Posts.Where(p=>p.PostId==comment.PostId).Select(p=>p.PostHeader).FirstOrDefault()
                        });
                    }

                }

                return Json(res);
            }
        }

        [HttpGet]
        public ActionResult CommentView(string commentId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.OWNERID].ToString();
                var postId = adbc.Comments.Where(c => c.CommentId == commentId).Select(c => c.PostId).FirstOrDefault();
                var model = adbc.Posts.Where(p => p.PostId == postId).SingleOrDefault();
                var CommentList = adbc.Comments.Where(c => c.PostId == postId).ToList();
                List<CommentViewFromAdminSite> Comments = new List<CommentViewFromAdminSite>();

                foreach (var comment in CommentList)
                {
                    Comments.Add(new CommentViewFromAdminSite()
                    {
                        Comment = comment,
                        CommentorName = adbc.OwnersInfos.Where(c => c.OwenerId == comment.CommentorId).Select(c => c.Name).FirstOrDefault(),
                    });
                }

                StarterProjectV2.ViewModels.PostViewModel mdl = new StarterProjectV2.ViewModels.PostViewModel()
                {
                    PostId = model.PostId,
                    Approval = model.Approval,
                    NoOfLikes = model.NoOfLikes,
                    OwnersName = adbc.OwnersInfos.Where(o => o.OwenerId == model.PostedBy).Select(o => o.Name).FirstOrDefault(),
                    PostedBy = model.PostedBy,
                    PostText = model.PostText,
                    PostHeader = model.PostHeader,
                    FlagLike = (adbc.Likes.Where(l => l.PostId == postId && l.LikedOwnerId == OwnerId).FirstOrDefault() == null) ? false : true,
                    Time = model.Time,
                    ApproveTime = model.ApprovalTime,
                    Comments = Comments,
                    NoOfComments = CommentList.Count
                };
                //adbc.Modules.Remove(delobj);
                //adbc.SaveChanges();
                return View("PostView", mdl);
            }
        }
    }
}
