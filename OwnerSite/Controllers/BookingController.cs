﻿using StarterProjectV2;
using StarterProjectV2.Models;
using StarterProjectV2.Services;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace OwnerSite.Controllers
{
    public class BookingController : Controller
    {
        private readonly StarterProjectV2.Controllers.BookingController booking = new StarterProjectV2.Controllers.BookingController();
        private readonly BookingService _bookingService = new BookingService();
        private readonly ConfigurationService _configurationService = new ConfigurationService();
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();

        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        public ActionResult InvoiceReport(string bookingId)
        {
            return booking.InvoiceReport(bookingId);
        }

        [HttpGet]
        public ActionResult FolioDetail(string OwnerRoomBookngId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {

                OwnerRoomBooking booking = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == OwnerRoomBookngId).ToList().FirstOrDefault();

                int daysSpent = (int)((booking.CheckOutDate - booking.CheckInDate).TotalDays);
                string[] ARRs = new string[daysSpent];
                int totalCost = 0;
                for (int i = 0; i < daysSpent; i++)
                {
                    var currentDay = booking.CheckInDate.AddDays(i);
                    var ARR = adbc.ConfOwnerAvgRoomRates.Where(o => o.ApplicableDate == currentDay).ToList().FirstOrDefault();
                    if (ARR != null)
                    {
                        ARRs[i] = Convert.ToString(ARR.AverageRoomRate);
                        totalCost += ARR.AverageRoomRate * booking.NoOfRoomAllotted;
                    }
                    else
                        ARRs[i] = "-----";

                }
                var model = new FolioDetail()
                {
                    Booking = booking,
                    ARR = ARRs,
                    NoOfDaysSpent = daysSpent,
                    TotalCost = totalCost,
                };
                return View(model);
            }

        }

        public ActionResult CalendarView(int offset = 0)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            offset = NewMethod3(offset, out DateTime today, out List<OwnerRoomBooking> selectedBookings);

            NewMethod(offset, out CalendarViewModel calendarViewmodel);

            NewMethod1(today, selectedBookings, calendarViewmodel);

            ////today to final day booking
            for (int i = 1; i < 16; i++)
            {
                NewMethod2(today, selectedBookings, i, calendarViewmodel);
            }

            return View("CalendarView", calendarViewmodel);
        }

        private int NewMethod3(int offset, out DateTime today, out List<OwnerRoomBooking> selectedBookings)
        {
            offset = Math.Max(0, offset);

            today = DateTime.Today.AddDays(15 * offset);
            var finalDate = today.AddDays(15);

            var todayQuery = today;
            ////Get Booking Information
            selectedBookings = _dbContext.OwnerRoomBookings.Where(o => o.CheckOutDate >= todayQuery && o.CheckInDate < finalDate && o.IsBookingCompleted != "Cancelled").ToList();
            if (offset < 0)
            {
                selectedBookings = selectedBookings.Where(o => o.HasStayedOrNot == "Yes").ToList();
            }
            else if (offset == 0)
            {
                selectedBookings = selectedBookings.Where(o => o.HasStayedOrNot == "Yes" || ((o.HasStayedOrNot == "No" || String.IsNullOrEmpty(o.HasStayedOrNot)) && o.CheckInDate >= todayQuery)).ToList();
            }

            return offset;
        }

        private DateTime NewMethod2(DateTime today, List<OwnerRoomBooking> selectedBookings, int i, CalendarViewModel CalendarViewmodel)
        {
            DateTime date = today.AddDays(i - 1);
            List<OwnerRoomBooking> todayCheckInBookings = selectedBookings.Where(o => o.CheckInDate == date).OrderBy(o => Convert.ToInt16((o.CheckOutDate - o.CheckInDate).TotalDays)).ToList();

            foreach (var booking in todayCheckInBookings)
            {
                var days = Convert.ToInt16((booking.CheckOutDate - booking.CheckInDate).TotalDays);
                var rooms = booking.NoOfRoomRequested;
                var slotid = booking.SlotId;
                var bookingId = booking.OwnerRoomBookngId;
                int j = 1, k2;
                for (; j < 36; j++)
                {
                    for (k2 = 0; k2 < rooms; k2++)
                    {
                        if (CalendarViewmodel.calendarMatrix[j + k2, i] != "")
                        {
                            break;
                        }
                    }
                    if (k2 == rooms) break;
                }
                CalendarViewmodel.setMatrix(j, i, slotid, days, rooms, bookingId);
                j += rooms;
                //i += days;
            }


            int roomsReserved = 0;
            for (int j = 1; j <= 36; j++)
            {
                if (CalendarViewmodel.calendarMatrix[j, i] != "")
                    roomsReserved++;
            }

            CalendarViewmodel.calendarMatrix[36 + 2, i] = Convert.ToString(roomsReserved);


            var temp = _dbContext.ConfigInvRooms.Where(o => o.FromDate <= date && date <= o.ToDate).ToList().LastOrDefault();
            int inventorycount;
            inventorycount = (temp != null) ? temp.InventoryCount : 36;
            CalendarViewmodel.calendarMatrix[36 + 1, i] = Convert.ToString(inventorycount);

            int roomsAvailable2 = inventorycount - roomsReserved;
            CalendarViewmodel.calendarMatrix[36 + 3, i] = Convert.ToString(roomsAvailable2);
            return date;
        }

        private static void NewMethod1(DateTime today, List<OwnerRoomBooking> selectedBookings, CalendarViewModel CalendarViewmodel)
        {
            ////Load Matrix
            ////Today bookings
            List<OwnerRoomBooking> todayBookings = selectedBookings.Where(o => o.CheckOutDate == today).ToList();
            int j = 1;
            foreach (var booking in todayBookings)
            {
                var slotid = booking.SlotId;
                var rooms = booking.NoOfRoomRequested;
                var bookingId = booking.OwnerRoomBookngId;

                CalendarViewmodel.setMatrix(j, 0, slotid, 1, rooms, bookingId);
                j += rooms;
            }

            ////Previous bookings
            List<OwnerRoomBooking> previousBookings = selectedBookings.Where(o => o.CheckOutDate >= today && o.CheckInDate < today).ToList();
            j = 1;
            foreach (var booking in previousBookings)
            {
                var days = Convert.ToInt16((booking.CheckOutDate - today).TotalDays);
                var rooms = booking.NoOfRoomRequested; // booking.NoOfRoomAllotted
                var slotid = booking.SlotId;
                var bookingId = booking.OwnerRoomBookngId;
                CalendarViewmodel.setMatrix(j, 1, slotid, days, rooms, bookingId);
                j += rooms;
            }
        }

        private static void NewMethod(int offset, out CalendarViewModel CalendarViewmodel)
        {
            ////Initialize Matrix
            string[,] matrix = new string[40, 16];
            string[,] matrixColor = new string[37, 16];
            string[,] matrixBooking = new string[37, 16];

            CalendarViewmodel = new CalendarViewModel()
            {
                calendarMatrix = matrix,
                calendarMatrixColor = matrixColor,
                calendarMatrixBookingId = matrixBooking,
                Offset = offset
            };

            CalendarViewmodel.GenerateColor();

            for (int i = 1; i < 16; i++)
            {
                int j = 1;
                for (; j <= 36; j++)
                {
                    CalendarViewmodel.calendarMatrix[j, i] = "";
                    CalendarViewmodel.calendarMatrixColor[j, i] = "#FFFFFF";
                }
            }
        }

        [HttpGet]
        public ActionResult BookingCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            ViewBag.Current = "Booking";

            string slotId = Session[SessionKeys.USERNAME] as string;
            OwnerRoomBookingView model = _bookingService.PreBooking(slotId);
            return View(model);

        }

        [HttpGet]
        public ActionResult ReferralBookingCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                ViewBag.Current = "Referral";
                //string taskId = adbc.Tasks.Where(u => u.TaskPath == "Booking\\BookingManagement").Select(u => u.TaskId)
                //    .FirstOrDefault();
                var OBOwnerId = Session[SessionKeys.OWNERID].ToString();
                var owner = adbc.OwnersInfos.FirstOrDefault(o => o.OwenerId == OBOwnerId);

                var model = new OwnerRoomBookingView()
                {
                    IsError = false,
                    //RoomList = adbc.RoomInfos.ToList(),
                    RoomList = adbc.RoomInfos.Select(r => r.RoomNo).ToList(), //Take all the RoomName(==RoomNo)
                    OBOwnerId = OBOwnerId.ToString(),
                    IsBookingCompleted = "No",
                    RoomTypeList = adbc.RoomInfos.Select(r => r.RoomType).Distinct().ToList(), //Take all the Distinct Room Types
                    //NoOfDaySpent = adbc.OwnerRoomBookings.Where(r=>r.OBOwnerId == OBOwnerId).Max(r=>r.NoOfDaySpent),

                    SlotId = (owner == null) ? "" : owner.SlotId,
                    HasStayedOrNot = "",
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult BookingAddorEdit(OwnerRoomBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            if (ModelState.IsValid)
            {
                ViewBag.Current = "Booking";

                ResponseModel response = new ResponseModel
                {
                    Status = Messages.StatusSuccess
                };

                int.TryParse(_configurationService.GetSettingValue(SettingCode.BookingRestrictionHours), out int bookingRestrictionHours);

                if (info.Startdate < DateTime.Today.AddDays(bookingRestrictionHours / 24))
                {
                    response.Status = Messages.StatusFail;
                    response.Message = $"Owner cannot request for booking within {bookingRestrictionHours} hours!";
                }

                if (response.Status != Messages.StatusFail)
                {
                    //CheckInDate and CheckOut Date Setup
                    info.CheckInDate = info.Startdate;
                    info.CheckOutDate = info.Enddate;

                    response = _bookingService.BookingInsert(info, true);

                    if (response.Status != Messages.StatusFail)
                    {
                        return RedirectToAction("BookingManagement");
                    }
                }

                OwnerRoomBookingView viewmodel = _bookingService.GetOwnerRoomBookingView(info, response.Message);

                return View("BookingNotPermitted", viewmodel);
            }
            else
            {
                OwnerRoomBookingView viewmodel = _bookingService.GetOwnerRoomBookingView(info, "");
                return View("BookingNotPermitted", viewmodel);
            }
        }


        [HttpPost]
        public ActionResult ReferralBookingAddorEdit(OwnerRoomBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            if (info.Startdate < DateTime.Today)
            {
                //cannot allocate due to room unavailibility for this owner
                OwnerRoomBookingView viewmodel = new OwnerRoomBookingView()
                {
                    NoOfDaysCanStay = info.NoOfDaysCanStay,
                    NoOfDaySpent = info.NoOfDaySpent,
                    NoOfDaysCanStayMonthly = 0,
                    ErrorMessage = "You cannot place a booking requested in the past.\n"
                };
                return View("BookingNotPermitted", viewmodel);
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                if (ModelState.IsValid)
                {
                    ViewBag.Current = "Referral";
                    //Here RoomId means actually OwnerRoomBookingId

                    var roomNo = adbc.RoomInfos.Where(r => r.RoomId == info.PreferredRoomNo).Select(r => r.RoomNo).FirstOrDefault();
                    var ttt = adbc.OwnersInfos.Where(o => o.OwenerId == info.OBOwnerId);
                    var mobileNo = ttt.Select(o => o.Mobile)
                        .FirstOrDefault();
                    var email = ttt.Select(o => o.Email)
                        .FirstOrDefault();
                    var nameOfOwner = ttt.Select(o => o.Name)
                        .FirstOrDefault();
                    var owner = adbc.OwnersInfos.FirstOrDefault(o => o.OwenerId == info.OBOwnerId);
                    var slotId = owner.SlotId;

                    //var allconfigrooms = adbc.ConfigInvRooms.ToList();
                    //var allBookingComplete = OwnerRoomBookingsList.Where(o => o.IsBookingCompleted == "Yes");
                    //var allBookedRoomsForOwner = allBookingComplete.Where(o => o.SlotId == slotId);


                    //bool WindowClear = true;
                    var ReferralBookingsList = adbc.ReferralBookings.ToList();
                    List<int> ReferralBookingsIDList = new List<int>();
                    foreach (var item in ReferralBookingsList)
                    {
                        ReferralBookingsIDList.Add(int.Parse(item.ReferralBookingId));
                    }
                    int ReferralBookingPK;
                    if (ReferralBookingsIDList.Count() == 0)
                        ReferralBookingPK = 0;
                    else
                        ReferralBookingPK = ReferralBookingsIDList.Max();

                    ReferralBookingPK++;
                    ReferralBooking model = new ReferralBooking()
                    {
                        ReferralBookingId = Convert.ToString(ReferralBookingPK),
                        RequestedStartDate = info.Startdate,
                        RequestedEndDate = info.Enddate,
                        OBOwnerId = info.OBOwnerId,
                        SlotId = slotId,
                        MainGuestName = info.GuestNameMain,
                        MainGuestAddress = info.GuestAddress,
                        NoOfAdult = info.NoOfAdult,
                        NoOfChild = info.NoOfChild,
                        ApplicationDate = DateTime.Now,
                        BookingToken = "",
                        GuestMobile = info.GuestMobile,
                        Email = info.Email,
                        DateOfBirth = info.DateOfBirth,
                        HasStayedOrNot = "",
                        /////////////
                        CheckInDate = info.Startdate,
                        ExtraCharges = 0.00,
                        CheckOutDate = info.Enddate,
                        TotalExpense = 0.00,
                        FolioNo = ""
                    };
                    adbc.ReferralBookings.Add(model);

                    //Notification
                    var NotificationList = adbc.Notifications.ToList();
                    var NotificationIdList = new List<int>();
                    foreach (var notification in NotificationList)
                    {
                        NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                    }

                    var notificationToken = NotificationList.Count();
                    if (notificationToken > 0)
                    {
                        notificationToken = NotificationIdList.Max();
                    }

                    notificationToken++;
                    var OwnersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.OwenerId == info.OBOwnerId);
                    Notification notificationModel = new Notification()
                    {
                        isSeen = false,
                        NotificationHeader = "Booking",
                        NotificationText = OwnersInfo.SlotId + " has requested a room at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                        NotificationLink = "/Booking/OwnerRoomBookingEdit?ownerRoomBookngId=" + ReferralBookingPK,
                        NotificationTime = DateTime.Now,
                        NotificationId = "NOT-" + notificationToken,
                        IdForRecognition = Convert.ToString(ReferralBookingPK)
                    };
                    adbc.Notifications.Add(notificationModel);


                    //Audit Code goes here
                    var thisOwner = owner;

                    adbc.SaveChanges();
                    //sending sms

                    //Common.SMSSending(mobileNo, "Dear " + nameOfOwner + ", Your request is being processed, soon you will receive an email with details instructions.");
                    var mdl = adbc.OwnersInfos.Where(o => o.OwenerId == info.OBOwnerId).FirstOrDefault();
                    var Owner = mdl;
                    var mobileNumber = mdl.Mobile;
                    //var Email = mdl.Email;
                    //var isSMSSent = "Processing";
                    //var txt = "Dear " + nameOfOwner +
                    //          ", Your request is being processed, soon you will receive an sms and an email from coral reef.";
                    //if (mobileNumber != null && mobileNumber != "")
                    //{
                    //    var returnStatus = Common.SMSSending(mobileNumber, txt);
                    //    if (returnStatus == true)
                    //    {
                    //        isSMSSent = "Delivered";
                    //    }
                    //    else
                    //    {
                    //        isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                    //    }
                    //}
                    //else
                    //{
                    //    isSMSSent = "Not Delivered, Check whether Mobile No for this owner is correct";
                    //}
                    //EmailandSMSReport emailsmsreport = new EmailandSMSReport()
                    //{
                    //    IsSMSSent = isSMSSent,
                    //    Mobile = mobileNumber,
                    //    Email = Owner.Email,
                    //    SlotId = Owner.SlotId,
                    //    Name = Owner.Name,
                    //    TimeOfDelivery = DateTime.Now,
                    //    NotificationType = "SMS",
                    //    Text = txt,
                    //    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                    //};
                    //Common.insertInEmailAndSMSTable(emailsmsreport);


                    var admin = adbc.Admins.Where(a => a.Name == "BookingAdmin").FirstOrDefault();
                    //string isSMSSentAdmin = "Processing";
                    //if (admin.Mobile != null && admin.Mobile != "")
                    //{
                    //    var returnStatus = Common.SMSSending(admin.Mobile, Owner.SlotId + " has requested for room bookings, Please see to that.");
                    //    if (returnStatus == true)
                    //    {
                    //        isSMSSentAdmin = "Delivered";
                    //    }
                    //    else
                    //    {
                    //        isSMSSentAdmin = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                    //    }
                    //}
                    //else
                    //{
                    //    isSMSSentAdmin = "Not Delivered, Check whether Mobile No for this owner is correct";
                    //}
                    //EmailandSMSReport emailsmsreportForAdmin = new EmailandSMSReport()
                    //{
                    //    IsSMSSent = isSMSSent,
                    //    Mobile = admin.Mobile,
                    //    Email = admin.Email,
                    //    SlotId = "",
                    //    Name = admin.Name,
                    //    TimeOfDelivery = DateTime.Now,
                    //    NotificationType = "SMS",
                    //    Text = info.OBOwnerId + " has requested for room bookings, Please see to that.",
                    //    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                    //};
                    //Common.insertInEmailAndSMSTable(emailsmsreportForAdmin);
                    //Common.SMSSending(admin.Mobile, info.OBOwnerId + " has requested for room bookings, Please see to that.");

                    //sending email
                    //var Message = "Dear " + nameOfOwner + ", Your request is being processed, you will receive an sms and an email from coral reef.";
                    //var isEmailSent = "Processing";
                    ////Common.EmailSending(email, "Notification of Booking from Heritage BD", "Dear " + nameOfOwner + ", Your request is being processed, soon you will receive an email with details instructions.");
                    //if (Email != null && Email != "")
                    //{
                    //    Common.EmailSending(Email, "Notification of Booking from Heritage BD", Message);
                    //    isEmailSent = "Delivered";
                    //}
                    //else
                    //{
                    //    isEmailSent = "Failed";
                    //}
                    //EmailandSMSReport emailsmsreportForEmail = new EmailandSMSReport()
                    //{
                    //    IsSMSSent = isEmailSent,
                    //    Mobile = mdl.Mobile,
                    //    Email = Email,
                    //    SlotId = mdl.SlotId,
                    //    Name = mdl.Name,
                    //    TimeOfDelivery = DateTime.Now,
                    //    NotificationType = "Email",
                    //    //Text = model.EmailText,
                    //    Text = Message,
                    //    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                    //};
                    //Common.insertInEmailAndSMSTable(emailsmsreportForEmail);

                    //var messageAdmin = mdl.SlotId + " has requested for room bookings, Please see to that.";
                    //Common.EmailSending("dummy_admin@gmail.com", "Request for Booking", info.OBOwnerId + " has requested for room bookings, Please see to that.");
                    var adminForEmail = adbc.Admins.Where(a => a.Name == "BookingAdmin").FirstOrDefault();

                    ////var isEmailSentAdmin = "Processing";
                    //////Common.EmailSending(email, "Notification of Booking from Heritage BD", "Dear " + nameOfOwner + ", Your request is being processed, soon you will receive an email with details instructions.");
                    ////if (Email != null && Email != "")
                    ////{
                    ////    Common.EmailSending(adminForEmail.Email, "Request for Booking", messageAdmin);
                    ////    isEmailSentAdmin = "Delivered";
                    ////}
                    ////else
                    ////{
                    ////    isEmailSentAdmin = "Failed";
                    ////}
                    ////EmailandSMSReport emailsmsreportForEmailAdmin = new EmailandSMSReport()
                    ////{
                    ////    IsSMSSent = isEmailSentAdmin,
                    ////    Mobile = adminForEmail.Mobile,
                    ////    Email = adminForEmail.Email,
                    ////    SlotId = "",
                    ////    Name = adminForEmail.Name,
                    ////    TimeOfDelivery = DateTime.Now,
                    ////    NotificationType = "Email",
                    ////    //Text = model.EmailText,
                    ////    Text = messageAdmin,
                    ////    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                    ////};
                    ////Common.insertInEmailAndSMSTable(emailsmsreportForEmailAdmin);

                    return RedirectToAction("ReferralBookingManagement");
                }
                else
                {
                    OwnerRoomBookingView viewmodel = new OwnerRoomBookingView()
                    {
                        NoOfDaysCanStay = info.NoOfDaysCanStay,
                        NoOfDaySpent = info.NoOfDaySpent,
                        NoOfDaysCanStayMonthly = 0,
                        //ErrorMessage = "Email or Mobile No. not inserted properly.",
                        isOwner = false
                    };
                    return View("BookingNotPermitted", viewmodel);
                }
            }
            //return RedirectToAction("BookingManagement");
        }

        [HttpGet]
        public ActionResult BookingManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                ViewBag.Current = "Booking";

                if (!string.IsNullOrWhiteSpace(TempData["Status"] as string))
                {
                    ViewBag.Status = TempData["Status"];
                    ViewBag.Message = TempData["Message"];
                }

                var OBOwnerId = Session[SessionKeys.OWNERID].ToString();
                List<OwnerRoomBooking> OwnerRoomBooking =
                    db.OwnerRoomBookings.Where(r => r.OBOwnerId == OBOwnerId).ToList();

                List<OwnerRoomBooking> OwnerRoomBookingFuture = new List<OwnerRoomBooking>();
                foreach (var ownerRoomBooking in OwnerRoomBooking)
                {
                    //if (ownerRoomBooking.Startdate-DateTime.Today.AddDays(-1) >= 1)
                    if (ownerRoomBooking.IsBookingCompleted != "Cancelled" && ownerRoomBooking.Startdate > DateTime.Today)
                    {
                        OwnerRoomBookingFuture.Add(ownerRoomBooking);
                    }
                }
                //List<OwnerRoomBooking> OwnerRoomBookingCurrent =
                //db.OwnerRoomBookings.Where(r => r.OBOwnerId == OBOwnerId && r.Startdate.AddDays(-1) >= DateTime.Now).ToList();
                List<OwnerRoomBooking> OwnerRoomBookingExceptFuture =
                    OwnerRoomBooking.Except(OwnerRoomBookingFuture).ToList();
                List<OwnerRoomBooking> OwnerRoomBookingCurrent = new List<OwnerRoomBooking>();
                foreach (var ownerRoomBooking in OwnerRoomBookingExceptFuture)
                {
                    if (((DateTime.Now >= ownerRoomBooking.Startdate) || (ownerRoomBooking.Startdate - DateTime.Now).Days <= 1) && DateTime.Now <= ownerRoomBooking.Enddate && ownerRoomBooking.IsBookingCompleted != "Cancelled")
                    {
                        OwnerRoomBookingCurrent.Add(ownerRoomBooking);
                    }
                }
                //List<OwnerRoomBooking> OwnerRoomBookingHistory = db.OwnerRoomBookings.Where(r => r.OBOwnerId == OBOwnerId && DateTime.Now > Convert.ToDateTime(r.Startdate)).ToList();

                List<OwnerRoomBooking> OwnerRoomBookingHistory = OwnerRoomBookingExceptFuture.Except(OwnerRoomBookingCurrent).ToList();

                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionary = new SortedDictionary<int, OwnerRoomBooking>();
                foreach (var ownerRoomBookingCurrent in OwnerRoomBookingCurrent)
                {
                    int key = int.Parse(ownerRoomBookingCurrent.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingCurrentDictionary.Add(key, ownerRoomBookingCurrent);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingCurrentDictionary.Reverse())
                {
                    OwnerRoomBookingCurrentDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }

                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();



                foreach (var ownerRoomBookingHistory in OwnerRoomBookingHistory)
                {
                    int key = int.Parse(ownerRoomBookingHistory.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingHistoryDictionary.Add(key, ownerRoomBookingHistory);
                }

                //                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionaryReverse = OwnerRoomBookingHistoryDictionary.Reverse();
                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingHistoryDictionary.Reverse())
                {
                    OwnerRoomBookingHistoryDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }




                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingFutureDictionary = new SortedDictionary<int, OwnerRoomBooking>();
                foreach (var ownerRoomBookingFuture in OwnerRoomBookingFuture)
                {
                    int key = int.Parse(ownerRoomBookingFuture.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingFutureDictionary.Add(key, ownerRoomBookingFuture);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingFutureDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingFutureDictionary.Reverse())
                {
                    OwnerRoomBookingCurrentDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                    // Current and future has been merged together here
                }
                var model = new OwnerRoomBookingManagementView()
                {
                    //OwnerRoomBookingListCurrent = db.OwnerRoomBookings.ToList(),
                    OwnerRoomBookingListCurrent = OwnerRoomBookingCurrentDictionaryReverse.Values.ToList(),
                    OwnerRoomBookingListHistory = OwnerRoomBookingHistoryDictionaryReverse.Values.ToList(),
                    //OwnerRoomBookingListFuture = OwnerRoomBookingFutureDictionaryReverse.Values.ToList(),
                };
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult BookingManagementHistory(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
            {
                ViewBag.Current = "Booking";

                var OBOwnerId = Session[SessionKeys.OWNERID].ToString();
                List<OwnerRoomBooking> OwnerRoomBooking =
                    db.OwnerRoomBookings.Where(r => r.OBOwnerId == OBOwnerId).ToList();

                List<OwnerRoomBooking> OwnerRoomBookingFuture = new List<OwnerRoomBooking>();
                foreach (var booking in OwnerRoomBooking)
                {
                    //if (ownerRoomBooking.Startdate-DateTime.Now.AddDays(-1) >= 1)
                    if (booking.IsBookingCompleted != "Cancelled" && (DateTime.Compare(booking.Startdate, DateTime.Today.AddDays(1)) >= 0))
                    {
                        OwnerRoomBookingFuture.Add(booking);
                    }
                }
                List<OwnerRoomBooking> OwnerRoomBookingExceptFuture =
                    OwnerRoomBooking.Except(OwnerRoomBookingFuture).ToList();
                List<OwnerRoomBooking> OwnerRoomBookingCurrent = new List<OwnerRoomBooking>();
                foreach (var booking in OwnerRoomBookingExceptFuture)
                {
                    if (((DateTime.Today >= booking.Startdate) || (booking.Startdate - DateTime.Today).Days <= 1) && DateTime.Today <= booking.Enddate && booking.IsBookingCompleted != "Cancelled")
                    {
                        OwnerRoomBookingCurrent.Add(booking);
                    }
                }
                //List<OwnerRoomBooking> OwnerRoomBookingHistory = db.OwnerRoomBookings.Where(r => r.OBOwnerId == OBOwnerId && DateTime.Now > Convert.ToDateTime(r.Startdate)).ToList();
                List<OwnerRoomBooking> OwnerRoomBookingHistoryAll = OwnerRoomBookingExceptFuture.Except(OwnerRoomBookingCurrent).ToList();


                var FinancialYears = db.FinancialYears.ToList().OrderBy(f => f.FromMonth).Reverse();
                List<int> NoOfBookingsList = new List<int>();
                List<string> FinancialYearNameList = new List<string>();
                List<string> FinancialYearIDList = new List<string>();
                List<List<OwnerRoomBooking>> OwnerRoomBookingHistory = new List<List<OwnerRoomBooking>>();
                for (var i = 0; i < FinancialYears.Count(); i++)
                {
                    OwnerRoomBookingHistory.Add(new List<OwnerRoomBooking>());
                }

                var Id = db.OwnersInfos.FirstOrDefault(o => o.OwenerId == OBOwnerId).SlotId;
                for (var i = 0; i < FinancialYears.Count(); i++)
                {
                    var financialYear = FinancialYears.ElementAt(i);
                    string financialYearName = financialYear.Year;
                    FinancialYearNameList.Add(financialYearName);
                    FinancialYearIDList.Add(financialYear.FinId);
                    foreach (var ownerRoomBookingHistory in OwnerRoomBookingHistoryAll)
                    {
                        if (ownerRoomBookingHistory.Startdate >= financialYear.FromMonth &&
                            ownerRoomBookingHistory.Enddate <= financialYear.ToMonth)
                        {
                            OwnerRoomBookingHistory.ElementAt(i).Add(ownerRoomBookingHistory);
                        }
                    }

                    var NoOfBookingsForThisFiscalYear = OwnerRoomBookingHistory.ElementAt(i).Count;
                    NoOfBookingsList.Add(NoOfBookingsForThisFiscalYear);
                }

                var model = new BookingHistoryByFinancialYearView()
                {
                    Id = Id.ToString(),
                    OwnerRoomBookingHistory = OwnerRoomBookingHistory,
                    FinancialYearNameList = FinancialYearNameList,
                    FinancialYearIDList = FinancialYearIDList,
                    NoOfBookings = NoOfBookingsList,
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult ReferralBookingManagement(string searchfield = "", int index = 1)
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                ViewBag.Current = "Referral";

                var OBOwnerId = Session[SessionKeys.OWNERID].ToString();
                var FullReferralBookingsList = adbc.ReferralBookings.Where(r => r.OBOwnerId == OBOwnerId && DateTime.Today <= r.RequestedEndDate).ToList();
                //Searching
                List<ReferralBooking> referralBookingList = new List<ReferralBooking>();

                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var referralBooking in FullReferralBookingsList)
                    {
                        var MainGuestName = referralBooking.MainGuestName.ToUpper();
                        var GuestMobile = referralBooking.GuestMobile;
                        //var AuditDTTM1 = AuditDTTM;

                        if (MainGuestName.Contains(searchfield1) == true || GuestMobile.Contains(searchfield) == true)
                        {
                            referralBookingList.Add(referralBooking);
                        }
                    }
                }
                else
                {
                    referralBookingList = FullReferralBookingsList;
                }
                //Search Finished
                //Pagination Start
                List<ReferralBooking> selectedReferralBookings = new List<ReferralBooking>();
                int noOfAudits;
                if (referralBookingList.Count >= 10)
                {

                    var pageNo = (referralBookingList.Count < (index * 10)) ? referralBookingList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedReferralBookings.Add(referralBookingList.ElementAt(i));
                    }

                    noOfAudits = (referralBookingList.Count + 9) / 10;
                }
                else
                {
                    selectedReferralBookings = referralBookingList;
                    noOfAudits = 1;
                }
                var a = adbc.ReferralBookings.Where(r => r.OBOwnerId == OBOwnerId).ToList();
                double referralpoint = a.Sum(r => r.ReferralPoint);
                var model = new ReferralRoomBookingManagementView
                {
                    ReferralBookingListCurrent = selectedReferralBookings,
                    ReferralPoint = referralpoint,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }

        }

        [HttpGet]
        public ActionResult ReferralBookingManagementHistory(string searchfield = "", int index = 1)
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                ViewBag.Current = "Referral";

                var OBOwnerId = Session[SessionKeys.OWNERID].ToString();
                var FullReferralBookingsListHistory = adbc.ReferralBookings.Where(r => r.OBOwnerId == OBOwnerId && r.RequestedEndDate < DateTime.Today).ToList();
                //Searching
                List<ReferralBooking> referralBookingList = new List<ReferralBooking>();

                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var referralBooking in FullReferralBookingsListHistory)
                    {
                        var MainGuestName = referralBooking.MainGuestName.ToUpper();
                        var GuestMobile = referralBooking.GuestMobile;
                        //var AuditDTTM1 = AuditDTTM;

                        if (MainGuestName.Contains(searchfield1) == true || GuestMobile.Contains(searchfield) == true)
                        {
                            referralBookingList.Add(referralBooking);
                        }
                    }
                }
                else
                {
                    referralBookingList = FullReferralBookingsListHistory;
                }
                //Search Finished
                //Pagination Start
                List<ReferralBooking> selectedReferralBookings = new List<ReferralBooking>();
                int noOfAudits;
                if (referralBookingList.Count >= 10)
                {

                    var pageNo = (referralBookingList.Count < (index * 10)) ? referralBookingList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedReferralBookings.Add(referralBookingList.ElementAt(i));
                    }

                    noOfAudits = (referralBookingList.Count + 9) / 10;
                }
                else
                {
                    selectedReferralBookings = referralBookingList;
                    noOfAudits = 1;
                }
                var model = new ReferralRoomBookingManagementView
                {
                    ReferralBookingListHistory = selectedReferralBookings,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }

        }

        [HttpPost]
        public ActionResult BookingDelete(string ownerRoomBookingId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                ViewBag.Current = "Booking";




                var allBookings = adbc.OwnerRoomBookings.AsEnumerable();
                var OwnerRoomBookingInDb = allBookings.Where(r => r.OwnerRoomBookngId == ownerRoomBookingId).FirstOrDefault();

                int.TryParse(_configurationService.GetSettingValue(SettingCode.BookingRestrictionHours), out int bookingRestrictionHours);

                if (OwnerRoomBookingInDb.Startdate < DateTime.Today.AddDays(bookingRestrictionHours / 24))
                {
                    ResponseModel response = new ResponseModel();
                    response.Message = $"Owner cannot cancel booking within {bookingRestrictionHours} hours!";

                    TempData["Status"] = response.Status;
                    TempData["Message"] = response.Message;

                    return RedirectToAction("BookingManagement");
                }



                var allBookingWaiting = allBookings.Where(o => o.IsBookingCompleted == "No" && OwnerRoomBookingInDb.Startdate <= o.Startdate
                && o.Enddate <= OwnerRoomBookingInDb.Enddate);
                var allBookingComplete = allBookings.Where(o => o.IsBookingCompleted == "Yes").ToList();
                var allconfigrooms = adbc.ConfigInvRooms.ToList();
                //Checking If owner has the permission to cancel or not
                if (OwnerRoomBookingInDb.Startdate.AddMinutes(1) <= DateTime.Today.AddDays(1))
                {
                    //cannot cancel, too late
                    //you must checkin.
                    // use day close to solve the non checked in people's cut
                    return RedirectToAction("BookingManagement");
                }

                OwnerRoomBookingInDb.IsBookingCompleted = "Cancelled";
                foreach (var curBooking in allBookingWaiting)
                {
                    bool WindowClear = true;
                    for (DateTime date = curBooking.Startdate; date < curBooking.Enddate; date = date.AddDays(1))
                    {
                        var configRoomForDate = allconfigrooms.LastOrDefault(c => c.FromDate <= date && date < c.ToDate);
                        int inventoryCount = configRoomForDate.InventoryCount; //36
                                                                               //Calculate the Already Booked no of rooms for this date
                        int bookedRoomForDate = allBookingComplete.Where(c => c.Startdate <= date && date < c.Enddate).Sum(r => r.NoOfRoomRequested);
                        if (inventoryCount < bookedRoomForDate + curBooking.NoOfRoomRequested)
                        {
                            //cannot allocate due to room unavailibility in hotel, booking requested will stay in waiting list.
                            WindowClear = false;
                            break;
                        }
                    }
                    if (WindowClear)
                    {
                        curBooking.IsBookingCompleted = "Yes";
                        allBookingComplete.Add(curBooking);
                    }
                }

                int roomRequested = OwnerRoomBookingInDb.NoOfRoomRequested;
                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    notificationToken = NotificationIdList.Max();
                }

                notificationToken++;
                //Audit Code goes here
                var thisOwner = adbc.OwnersInfos.FirstOrDefault(r => r.OwenerId == OwnerRoomBookingInDb.OBOwnerId);
                Audit objectAudit = new Audit()
                {
                    AuditDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                    LogInIp = "NIL",
                    TableName = "OwnerRoomBookings",
                    UserLoginDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                    UserLogoutDTTM = "0",
                    UserName = thisOwner.SlotId,
                    TaskName = "OwnerSite",
                    ActionString = "Booking Request By Owner",
                    ActionDetails = thisOwner.SlotId + " has cancelled " + roomRequested + " room at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm."),
                };
                string AuditId = Common.InsertAudit(objectAudit);
                var OwnersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.OwenerId == OwnerRoomBookingInDb.OBOwnerId);
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "CancelledBooking",
                    NotificationText = OwnersInfo.SlotId + " has cancelled " + roomRequested + " room at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                    NotificationLink = "/Audit/AuditView?auditId=" + AuditId,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = OwnerRoomBookingInDb.OwnerRoomBookngId
                };
                adbc.Notifications.Add(notificationModel);
                adbc.SaveChanges();


                //Here do these task:
                //Send Notification  To The Room Admin about the cancellation of Room Booking by Owner  
                //Common.SMSSending("01521220223", OwnerRoomBookingInDb.OBOwnerId + " has cancelled his/her request for bookings, Please see to that");
                var admin = adbc.Admins.Where(a => a.Name == "BookingAdmin").FirstOrDefault();
                //var isSMSSent = "Processing";
                //if (admin.Mobile != null && admin.Mobile != "")
                //{
                //    var returnStatus = Common.SMSSending(admin.Mobile, thisOwner.SlotId + " has cancelled his/her request for bookings, Please see to that");
                //    if (returnStatus == true)
                //    {
                //        isSMSSent = "Delivered";
                //    }
                //    else
                //    {
                //        isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                //    }
                //}
                //else
                //{
                //    isSMSSent = "Not Delivered, Check whether Mobile No for this owner is correct";
                //}
                //EmailandSMSReport emailsmsreportForAdmin = new EmailandSMSReport()
                //{
                //    IsSMSSent = isSMSSent,
                //    Mobile = admin.Mobile,
                //    Email = admin.Email,
                //    SlotId = "",
                //    Name = admin.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "SMS",
                //    Text = OwnerRoomBookingInDb.SlotId + " has cancelled his/her request for bookings, Please see to it.",
                //    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreportForAdmin);

                //Common.EmailSending("dummy_admin@gmail.com", "Cancelling Booking", OwnerRoomBookingInDb.OBOwnerId + " has cancelled a room booking, Please see to that.");

                //var messageAdmin = thisOwner.SlotId + " has cancelled a room booking, Please see to that.";
                //var adminForEmail = adbc.Admins.Where(a => a.Name == "BookingAdmin").FirstOrDefault();

                //var isEmailSentAdmin = "Processing";
                //if (adminForEmail.Email != null && adminForEmail.Email != "")
                //{
                //    Common.EmailSending(adminForEmail.Email, "Cancelling Booking", messageAdmin);
                //    isEmailSentAdmin = "Delivered";
                //}
                //else
                //{
                //    isEmailSentAdmin = "Failed";
                //}
                //EmailandSMSReport emailsmsreportForEmailAdmin = new EmailandSMSReport()
                //{
                //    IsSMSSent = isEmailSentAdmin,
                //    Mobile = adminForEmail.Mobile,
                //    Email = adminForEmail.Email,
                //    SlotId = "",
                //    Name = adminForEmail.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "Email",
                //    //Text = model.EmailText,
                //    Text = messageAdmin,
                //    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreportForEmailAdmin);
                ////return RedirectToAction("OwnerRoomBookingManagement", "OwnersInfo");
                return RedirectToAction("BookingManagement");
            }
        }

        [HttpPost]
        public ActionResult ReferralBookingDelete(string ReferralBookingId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                ViewBag.Current = "Referral";
                var allBookings = adbc.ReferralBookings.ToList();
                var referralBookingInDb = allBookings.Where(r => r.ReferralBookingId == ReferralBookingId).FirstOrDefault();
                //Checking If owner has the permission to cancel or not
                //if (referralBookingInDb.RequestedStartDate.AddMinutes(1) <= DateTime.Today.AddDays(1))
                {
                    //cannot cancel, too late
                    //you must checkin.
                    //return RedirectToAction("ReferralBookingManagement");
                }

                referralBookingInDb.IsBookingCompleted = "Cancelled";
                int roomRequested = referralBookingInDb.NoOfRoomRequested;
                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    notificationToken = NotificationIdList.Max();
                }

                notificationToken++;
                //Audit Code goes here
                var thisOwner = adbc.OwnersInfos.FirstOrDefault(r => r.OwenerId == referralBookingInDb.OBOwnerId);
                Audit objectAudit = new Audit()
                {
                    AuditDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                    LogInIp = "NIL",
                    TableName = "ReferralBookings",
                    UserLoginDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                    UserLogoutDTTM = "0",
                    UserName = thisOwner.SlotId,
                    TaskName = "OwnerSite",
                    ActionString = "Referral Booking Request By Owner",
                    ActionDetails = thisOwner.SlotId + " has cancelled " + roomRequested + " room at " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm."),
                };
                string AuditId = StarterProjectV2.Common.InsertAudit(objectAudit);
                var OwnersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.OwenerId == referralBookingInDb.OBOwnerId);
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "CancelledBooking",
                    NotificationText = OwnersInfo.SlotId + " has cancelled " + roomRequested + " room at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                    NotificationLink = "/Booking/ReferralBookingView?ReferralBookingId=" + AuditId,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = referralBookingInDb.ReferralBookingId
                };
                adbc.Notifications.Add(notificationModel);
                adbc.SaveChanges();


                //Here do these task:
                //Send Notification  To The Room Admin about the cancellation of Room Booking by Owner  
                //Common.SMSSending("01521220223", OwnerRoomBookingInDb.OBOwnerId + " has cancelled his/her request for bookings, Please see to that");
                //var admin = adbc.Admins.Where(a => a.Name == "BookingAdmin").FirstOrDefault();
                //var isSMSSent = "Processing";
                //if (admin.Mobile != null && admin.Mobile != "")
                //{
                //    var returnStatus = Common.SMSSending(admin.Mobile, thisOwner.SlotId + " has cancelled referral booking request, Please see to that");
                //    if (returnStatus == true)
                //    {
                //        isSMSSent = "Delivered";
                //    }
                //    else
                //    {
                //        isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                //    }
                //}
                //else
                //{
                //    isSMSSent = "Not Delivered, Check whether Mobile No for this owner is correct";
                //}
                //EmailandSMSReport emailsmsreportForAdmin = new EmailandSMSReport()
                //{
                //    IsSMSSent = isSMSSent,
                //    Mobile = admin.Mobile,
                //    Email = admin.Email,
                //    SlotId = "",
                //    Name = admin.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "SMS",
                //    Text = referralBookingInDb.SlotId + " has cancelled his/her request for bookings, Please see to it.",
                //    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreportForAdmin);

                ////Common.EmailSending("dummy_admin@gmail.com", "Cancelling Booking", OwnerRoomBookingInDb.OBOwnerId + " has cancelled a room booking, Please see to that.");

                //var messageAdmin = thisOwner.SlotId + " has cancelled a referral room booking, Please see to that.";
                //var adminForEmail = adbc.Admins.Where(a => a.Name == "BookingAdmin").FirstOrDefault();

                //var isEmailSentAdmin = "Processing";
                //if (adminForEmail.Email != null && adminForEmail.Email != "")
                //{
                //    Common.EmailSending(adminForEmail.Email, "Cancelling Booking", messageAdmin);
                //    isEmailSentAdmin = "Delivered";
                //}
                //else
                //{
                //    isEmailSentAdmin = "Failed";
                //}
                //EmailandSMSReport emailsmsreportForEmailAdmin = new EmailandSMSReport()
                //{
                //    IsSMSSent = isEmailSentAdmin,
                //    Mobile = adminForEmail.Mobile,
                //    Email = adminForEmail.Email,
                //    SlotId = "",
                //    Name = adminForEmail.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "Email",
                //    //Text = model.EmailText,
                //    Text = messageAdmin,
                //    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreportForEmailAdmin);
                ////return RedirectToAction("OwnerRoomBookingManagement", "OwnersInfo");
                return RedirectToAction("ReferralBookingManagement");
            }
        }

        //This is for OwnerRoomSlot Create, not for RoomSlot Create
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult gettingSlotList2(string roomType)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var roomList = adbc.RoomInfos.Where(m => m.RoomType == roomType).ToList();
                List<SelectListItem> someArray = new List<SelectListItem>();
                foreach (var x in roomList)
                {
                    //var slotidofNotSold = adbc.RoomSlotAssigments.Where(s => s.SlotId == x.SlotId && s.SoldStatus == "Not Sold")
                    //.Select(s => s.SlotId).FirstOrDefault();
                    var nm = new SelectListItem()
                    {
                        //Text = adbc.SlotInfos.Where(s => s.SlotId == slotidofNotSold).Select(s => s.SlotName).FirstOrDefault(),
                        Text = adbc.RoomInfos.Where(r => r.RoomId == x.RoomId).Select(r => r.RoomNo).FirstOrDefault(),
                        Value = x.RoomNo
                    };
                    someArray.Add(nm);
                }
                return Json(someArray, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult TermsAndConditions()
        {
            return View();
        }
    }
}