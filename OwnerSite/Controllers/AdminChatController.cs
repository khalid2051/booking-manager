﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OwnerSite.ViewModels;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
//using Newtonsoft.Json;

namespace OwnerSite.Controllers
{
    public class AdminChatController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult ChatWithAdmin()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var SlotId = Session[SessionKeys.USERNAME].ToString();
                var chatsOfThisSlotOwner = adbc.Chats.Where(c => c.SlotId == SlotId).ToList();
                //var chatsOfThisSlotOwnerOrdered = chatsOfThisSlotOwner.OrderByDescending(c => c.ID).ToList();
                var id = chatsOfThisSlotOwner.Count != 0 ? chatsOfThisSlotOwner.Last().ID : 0;

                var ChatsList3 = chatsOfThisSlotOwner.Where(p => p.ID <= id).ToList();
                List<Chat> chatList2 = null;
                List<Chat> chatList = null;
                if (ChatsList3 != null)
                {
                    chatList2 = ChatsList3.OrderByDescending(o => o.ID).Take(5).ToList();
                    chatList = chatList2.OrderBy(o => o.ID).ToList();
                }
                if (chatList.Count() > 0 && chatList2.Count() > 0)
                {
                    var id2 = chatList2.Last().ID;
                    var modelWithData = new AdminChatView
                    {
                        Chats = chatList,
                        ID = id2
                    };
                    return View(modelWithData);
                }
                var model = new AdminChatView
                {
                    Chats = null,
                    ID = -1
                };
                return View(model);
            }
        }

        public JsonResult gettingChatWithAdmin(int lastId)
        {
            if (!IsUserLoggedIn())
                return Json("Forbidden Http Request");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var SlotId = Session[SessionKeys.USERNAME].ToString();
                var chatsOfThisSlotOwner = adbc.Chats.Where(c => c.SlotId == SlotId).ToList();
                //var chatsOfThisSlotOwnerOrdered = chatsOfThisSlotOwner.OrderByDescending(c => c.ID).ToList();
                lastId = chatsOfThisSlotOwner != null ? lastId : 0;



                var chatsList3 = adbc.Chats.Where(p => p.SlotId == SlotId && p.ID < lastId).ToList();
                var a = chatsList3.Count();
                List<Chat> chatList2 = null;
                if (chatsList3.Count() >= 5)
                {
                    //chatList2 = chatsList3.OrderByDescending(o => o.DeliveryTime).Take(5).ToList();
                    chatList2 = chatsList3.OrderByDescending(o => o.ID).Take(5).ToList();
                }
                else if (chatsList3.Count() > 0)
                {
                    //chatList2 = chatsList3.OrderByDescending(o => o.DeliveryTime).Take(chatsList3.Count()).ToList();
                    chatList2 = chatsList3.OrderByDescending(o => o.ID).Take(5).ToList();
                }
                else if (chatsList3.Count() <= 0)
                {
                    chatList2 = null;
                    return Json("Error");
                }
                // var LastMessageTime = chatList2.Last().DeliveryTime.ToString();
                var LastID = chatList2.Last().ID;

                //return Json(new { chatList = chatList2, LastMessageTime = LastMessageTime });
                return Json(new { chatList = chatList2, LastID = LastID });
            }
        }
        public JsonResult gettingNewMessages(int latestId)
        {
            if (!IsUserLoggedIn())
                return Json("Forbidden Http Request");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                //latestMessageTime = String.IsNullOrEmpty(latestMessageTime) == true ? DateTime.Now.ToString() : latestMessageTime;
                //var latestMessageDateTime = latestMessageTime == null ? DateTime.Now : DateTime.Parse(latestMessageTime);

                var SlotId = Session[SessionKeys.USERNAME].ToString();
                var chatsOfThisSlotOwner = adbc.Chats.Where(c => c.SlotId == SlotId).ToList();
                latestId = chatsOfThisSlotOwner != null ? latestId : 0;

                //var chatList4 = adbc.Chats.Where(p => p.SlotId == SlotId).ToList();
                //var chatList3 = chatList4.Where(p=> p.DeliveryTime > latestMessageDateTime.AddSeconds(1)).OrderByDescending(o=> o.DeliveryTime).ToList();

                var chatList3 = adbc.Chats.Where(p => p.SlotId == SlotId && p.ID > latestId).ToList();
                var a = chatList3.Count();
                var LatestID = 0;
                if (chatList3.Count() > 0)
                {
                    LatestID = chatList3.Last().ID;
                }
                else
                {
                    LatestID = latestId;
                }
                return Json(new { chatList = chatList3, LatestID = LatestID });
            }
        }
        //public JsonResult gettingChatWithAdmin(string lastMessageTime)
        //{
        //    if (!IsUserLoggedIn())
        //        return Json("error");
        //    using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
        //    {
        //        lastMessageTime = String.IsNullOrEmpty(lastMessageTime) == true ? DateTime.Now.ToString() : lastMessageTime;

        //        var lastMessageDateTime = lastMessageTime == null ? DateTime.Now : DateTime.Parse(lastMessageTime);
        //        var SlotId = Session[SessionKeys.USERNAME].ToString();
        //        var ChatsList3 = adbc.Chats.Where(p => p.SlotId == SlotId && p.DeliveryTime < lastMessageDateTime).ToList();
        //        var chatList2 = ChatsList3.OrderByDescending(o => o.DeliveryTime).Take(5).ToList();

        //        var model = new AdminChatView
        //        {
        //            Chats = chatList2,
        //            LastMessageTime = chatList2[4].DeliveryTime.ToString(),
        //        };
        //        var a = Json(model);
        //        return a;
        //    }
        //}


        //[HttpGet]
        //public ActionResult ChatWithAdmin()
        ////public JsonResult ChatWithAdmin()
        //{
        //    if (!IsUserLoggedIn())
        //        return RedirectToAction("Index", "Home");
        //    using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
        //    {
        //        var SlotId = Session[SessionKeys.USERNAME].ToString();
        //        //.OrderByDescending(o => o.ID) .Take(5)
        //        var ChatsList = adbc.Chats.Where(p => p.SlotId == SlotId).Take(2).ToList();
        //        var model = new AdminChatView
        //        {
        //            Chats = ChatsList,
        //            //TaskRoles = taskRoles,
        //        };
        //        return View(model);
        //        //returnn Json(model)

        //    }
        //}
        //public JsonResult getAdditionalChatHistory()
        //{
        //    if (!IsUserLoggedIn())
        //        return RedirectToAction("Index", "Home");
        //    using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
        //    {
        //        var SlotId = Session[SessionKeys.USERNAME].ToString();
        //        //.OrderByDescending(o => o.ID)
        //        var ChatsList = adbc.Chats.Where(p => p.SlotId == SlotId).Take(2).ToList();
        //        var model = new AdminChatView
        //        {
        //            Chats = ChatsList,
        //            //TaskRoles = taskRoles,
        //        };
        //                    return View(model);
        //        //returnn Json(model)

        //    }
        //}
        public JsonResult addMessage(AdminChatView mdl)
        {
            if (!IsUserLoggedIn())
                return Json("Forbidden Http Request");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var SlotId = Session[SessionKeys.USERNAME].ToString();
                //var ChatsList = adbc.Chats.Where(p => p.SlotId == SlotId).ToList().OrderBy(o => o.ID).ToList();

                //var model = new AdminChatView
                //{
                //    Chats = ChatsList,
                //    //TaskRoles = taskRoles,
                //    SlotId = SlotId
                //};
                //return View(model);
                Chat chat = new Chat()
                {
                    SlotId = Session[SessionKeys.USERNAME].ToString(),
                    Text = mdl.CurrentMessage,
                    DeliveryTime = DateTime.Now,
                    Sender = Session[SessionKeys.USERNAME].ToString(),
                    Receiver = "Admin"
                };
                adbc.Chats.Add(chat);
                adbc.SaveChanges();
                Json("Success", JsonRequestBehavior.AllowGet);

                Audit objectAudit = new Audit()
                {
                    AuditDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                    LogInIp = "djksf",
                    TableName = "OwnersInfos",
                    UserLoginDTTM = "0",
                    UserLogoutDTTM = "0",
                    UserName = "Imran",
                    TaskName = "Adding",
                    ActionString = "Adding Message",
                    ActionDetails = Newtonsoft.Json.JsonConvert.SerializeObject(new { chat = chat })
                };
                StarterProjectV2.Common.InsertAudit(objectAudit);

            }
            return Json("Error", JsonRequestBehavior.DenyGet);
        }

    }
}