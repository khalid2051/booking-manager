﻿using OwnerSite.ViewModels;
using StarterProjectV2;
using StarterProjectV2.Models;
using StarterProjectV2.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OwnerSite.Controllers
{
    public class OwnerController : Controller
    {
        private readonly BookingService _bookingService = new BookingService();
        private readonly OwnerInfoService _ownerInfoService = new OwnerInfoService();
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();

        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult Ledger()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            ViewBag.Current = "Ledger";

            var slotId = Session[SessionKeys.USERNAME].ToString();
            StarterProjectV2.ViewModels.OwnersInfoView model = new StarterProjectV2.ViewModels.OwnersInfoView
            {
                LedgerViewModel = _bookingService.GetBookingLedger(slotId)
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult FolioDetail(string OwnerRoomBookngId)
        {
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                OwnerRoomBooking booking = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == OwnerRoomBookngId).ToList().FirstOrDefault();
                int daysSpent = (int)((booking.CheckOutDate - booking.CheckInDate).TotalDays);
                string[] ARRs = new string[daysSpent];
                int totalCost = 0;
                for (int i = 0; i < daysSpent; i++)
                {
                    var currentDay = booking.CheckInDate.AddDays(i);
                    var ARR = adbc.ConfOwnerAvgRoomRates.Where(o => o.ApplicableDate == currentDay).ToList().FirstOrDefault();
                    if (ARR != null)
                    {
                        ARRs[i] = Convert.ToString(ARR.AverageRoomRate);
                        totalCost += ARR.AverageRoomRate * booking.NoOfRoomAllotted;
                    }
                    else
                        ARRs[i] = "-----";

                }
                var model = new StarterProjectV2.ViewModels.FolioDetail()
                {
                    Booking = booking,
                    ARR = ARRs,
                    NoOfDaysSpent = daysSpent,
                    TotalCost = totalCost,
                };
                return View(model);
            }

        }
        [HttpGet]
        public ActionResult OwnerInfoIncomeVieworEdit(string ownerId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var mdl = adbc.OwnersInfos.Where(o => o.OwenerId == ownerId).FirstOrDefault();
                bool IsProfilePictureExists = true;
                bool IsNIDFileExists = true;
                bool IsTINFileExists = true;
                if (string.IsNullOrEmpty(mdl.PicturePath))
                {
                    //mdl.PicturePath = "~/Content/ownerprofilepictures/" + mdl.SlotId;
                    IsProfilePictureExists = false;
                }

                if (string.IsNullOrEmpty(mdl.NIDFilePath))
                {
                    //mdl.NIDFilePath = "~/Content/ownernidfiles/" + mdl.SlotId + ".jpg";
                    IsNIDFileExists = false;
                }

                if (string.IsNullOrEmpty(mdl.TINFilePath))
                {
                    //mdl.NIDFilePath = "~/Content/ownertinfiles/" + mdl.SlotId + ".jpg";
                    IsTINFileExists = false;
                }

                OwnersInfoView model = new OwnersInfoView()
                {
                    IsEdited = false,
                    OwnerInfo = mdl,
                    IsProfilePictureExists = IsProfilePictureExists,
                    IsNIDFileExists = IsNIDFileExists,
                    IsTINFileExists = IsTINFileExists
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult OwnerHomePage(OwnersInfoView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var slotId = Session[SessionKeys.USERNAME].ToString();
                var allNotices = adbc.DashboardNotices.ToList();
                OwnersInfoView model = new OwnersInfoView()
                {
                    OwnerInfo = adbc.OwnersInfos.Where(o => o.SlotId == slotId).FirstOrDefault(),
                    AllNotices = allNotices.OrderBy(n => n.Date).Reverse().ToList(),
                    AdminComment = adbc.OwnersInfos.FirstOrDefault(o => o.SlotId == slotId).AdminComment,
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult OwnersEdit(OwnersInfoView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnersInformationDB =
                    adbc.OwnersInfos.Where(o => o.OwenerId == mdl.OwnerInfo.OwenerId).FirstOrDefault();

                OwnersInformationDB.Email = mdl.OwnerInfo.Email;
                OwnersInformationDB.Mobile = mdl.OwnerInfo.Mobile;
                OwnersInformationDB.PermanantAddress = mdl.OwnerInfo.PermanantAddress;
                OwnersInformationDB.PresentAddress = mdl.OwnerInfo.PresentAddress;
                OwnersInformationDB.OwnersLastEditTime = DateTime.Now;

                //////////
                //var NotificationList = adbc.Notifications.ToList();
                //var NotificationIdList = new List<int>();
                //foreach (var notification in NotificationList)
                //{
                //    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                //}

                //var notificationToken = NotificationList.Count();
                //if (notificationToken > 0)
                //{
                //    NotificationIdList.Sort();
                //    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                //}

                //notificationToken++;
                //var thisOwner = adbc.OwnersInfos.Where(o => o.OwenerId == mdl.OwnerInfo.OwenerId).FirstOrDefault();
                //Notification notificationModel = new Notification()
                //{
                //    isSeen = false,
                //    NotificationHeader = "OE",
                //    NotificationText = thisOwner.SlotId + " has requested to update his information at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                //    NotificationLink = "/OwnersInfo/OwnersInfoView?ownerId=" + mdl.OwnerInfo.OwenerId,
                //    NotificationTime = DateTime.Now,
                //    NotificationId = "NOT-" + notificationToken,
                //    IdForRecognition = mdl.OwnerInfo.OwenerId
                //};
                //adbc.Notifications.Add(notificationModel);
                ///
                adbc.SaveChanges();

                var mdl2 = adbc.OwnersInfos.Where(o => o.OwenerId == mdl.OwnerInfo.OwenerId).FirstOrDefault();

                var allNotices = adbc.DashboardNotices.ToList();
                OwnersInfoView model = new OwnersInfoView()
                {
                    IsEdited = true,
                    OwnerInfo = mdl.OwnerInfo,
                    AllNotices = allNotices.OrderBy(n => n.Date).Reverse().ToList(),
                    AdminComment = adbc.OwnersInfos.FirstOrDefault(o => o.SlotId == mdl2.SlotId).AdminComment,
                };

                return View("OwnerHomePage", model);
            }
        }

        [HttpPost]
        public ActionResult OwnersEditAfterLogin(OwnersInfoView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            ViewBag.Current = "OwnersInfoIncomeViewOrEditAfterLogin";

            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnersInformationDB =
                    adbc.OwnersInfos.Where(o => o.OwenerId == mdl.OwnerInfo.OwenerId).FirstOrDefault();
                var nameOfOwner = adbc.OwnersInfos.Where(o => o.OwenerId == mdl.OwnerInfo.OwenerId).Select(o => o.Name)
                  .FirstOrDefault();
                OwnersInformationDB.Email = mdl.OwnerInfo.Email;
                OwnersInformationDB.Mobile = mdl.OwnerInfo.Mobile;
                OwnersInformationDB.NID = mdl.OwnerInfo.NID;
                OwnersInformationDB.TIN = mdl.OwnerInfo.TIN;
                OwnersInformationDB.AccountName = mdl.OwnerInfo.AccountName;
                OwnersInformationDB.AccountNo = mdl.OwnerInfo.AccountNo;
                OwnersInformationDB.BankName = mdl.OwnerInfo.BankName;
                OwnersInformationDB.RoutingNo = mdl.OwnerInfo.RoutingNo;
                OwnersInformationDB.BranchName = mdl.OwnerInfo.BranchName;
                OwnersInformationDB.Password = EncryptDecryptString.Encrypt(mdl.OwnerInfo.Password, "1234");
                OwnersInformationDB.PermanantAddress = mdl.OwnerInfo.PermanantAddress;
                OwnersInformationDB.PresentAddress = mdl.OwnerInfo.PresentAddress;
                OwnersInformationDB.OwnersLastEditTime = DateTime.Now;
                //OwnersInformationDB.IsEditApproved = mdl.OwnerInfo.IsEditApproved;
                OwnersInformationDB.IsEditApproved = "NO";
                adbc.SaveChanges();

                //Notification
                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }

                notificationToken++;
                var OwnersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.OwenerId == mdl.OwnerInfo.OwenerId);
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "EditInfo",
                    NotificationText = OwnersInfo.SlotId + " has requested an edit " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                    NotificationLink = "/OwnersInfo/OwnersInfoEdit?ownerId=" + mdl.OwnerInfo.OwenerId,
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = mdl.OwnerInfo.OwenerId
                };
                adbc.Notifications.Add(notificationModel);



                var thisOwner = OwnersInformationDB;


                adbc.SaveChanges();
                return RedirectToAction("OwnersInfoIncomeViewOrEditAfterLogin", "Owner", new { ownerId = mdl.OwnerInfo.OwenerId });
            }
        }

        public JsonResult Upload_Edit()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
                {
                    string OwnerId = Session[SessionKeys.USERNAME].ToString();
                    HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                    //Use the following properties to get file's name, size and MIMEType
                    int fileSize = file.ContentLength;
                    var OwnerInfoObject = db.OwnersInfos.Where(o => o.SlotId == OwnerId)
                        .FirstOrDefault();
                    string fileName = OwnerInfoObject.SlotId;
                    //                using (ApplicationDbContext db = new ApplicationDbContext())
                    //                {
                    //                    var ownerIdList = db.OwnersFamilies.Select(o => o.OwnerId).ToList();
                    //                    if (ownerIdList.Contains(ownerIdToken[0]))
                    //                    {
                    //                        return Json("Duplicate");
                    //   C:\Users\project\Desktop\hams-ter\OwnerSite\Content\ownerprofilepictures\                 }
                    //                }
                    string mimeType = file.ContentType;
                    System.IO.Stream fileContent = file.InputStream;
                    //To save file, use SaveAs method
                    string type = mimeType.Split('/')[0];
                    if (type == "image")
                    {
                        file.SaveAs(Server.MapPath("~/Content/ownerprofilepictures/") +
                                    fileName + ".jpg"); //File will be saved in application root
                        OwnerInfoObject.PicturePath = "~/Content/ownerprofilepictures/" + fileName + ".jpg";
                    }

                    db.SaveChanges();
                }
            }


            return Json("Uploaded " + Request.Files.Count + " files");
        }

        public JsonResult Upload_ChildEdit()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
                {
                    string SlotId = Session[SessionKeys.USERNAME].ToString();
                    HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                    //Use the following properties to get file's name, size and MIMEType
                    int fileSize = file.ContentLength;

                    var OwnerId = db.OwnersInfos.Where(o => o.SlotId == SlotId).FirstOrDefault()?.OwenerId;
                    var OwnerInfoObject = db.OwnersFamilies.Where(o => o.OwnerId == OwnerId)
                        .FirstOrDefault();
                    string fileName = OwnerInfoObject.OwnerId;

                    string mimeType = file.ContentType;
                    System.IO.Stream fileContent = file.InputStream;
                    //To save file, use SaveAs method
                    string type = mimeType.Split('/')[0];
                    if (type == "image")
                    {
                        file.SaveAs(Server.MapPath("~/Content/childpictures/") +
                                    fileName + ".jpg"); //File will be saved in application root
                        OwnerInfoObject.ChildPicture = "~/Content/childpictures/" + fileName + ".jpg";
                    }

                    db.SaveChanges();
                }
            }


            return Json("Uploaded " + Request.Files.Count + " files");
        }
        public JsonResult Upload_NIDFile_Edit()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
                {
                    string OwnerId = Session[SessionKeys.USERNAME].ToString();
                    HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                    //Use the following properties to get file's name, size and MIMEType
                    var OwnerInfoObject = db.OwnersInfos.Where(o => o.SlotId == OwnerId)
                        .FirstOrDefault();
                    string fileName = OwnerInfoObject.SlotId + Path.GetExtension(file.FileName);

                    //To save file, use SaveAs method
                    string fileSavingPath = "~/Content/ownernidfiles/";
                    string mapPath = Server.MapPath(fileSavingPath);

                    file.SaveAs(mapPath + fileName); //File will be saved in application root
                    OwnerInfoObject.NIDFilePath = fileSavingPath + fileName;

                    db.SaveChanges();
                }
            }

            return Json("Uploaded " + Request.Files.Count + " files");
        }

        public JsonResult Upload_NomineeNIDFile_Edit()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
                {
                    string slotid = Session[SessionKeys.USERNAME].ToString();
                    var ownerid = db.OwnersInfos.Where(o => o.SlotId == slotid).FirstOrDefault().OwenerId;
                    HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                    //Use the following properties to get file's name, size and MIMEType
                    int fileSize = file.ContentLength;
                    var OwnerInfoObject = db.OwnersNominees.Where(o => o.OwnerId == ownerid)
                        .FirstOrDefault();
                    string fileName = OwnerInfoObject.OwnerId;
                    string mimeType = file.ContentType;
                    System.IO.Stream fileContent = file.InputStream;
                    //To save file, use SaveAs method
                    if (mimeType == "application/pdf")
                    {
                        file.SaveAs(Server.MapPath("~/Content/ownernomineenidfiles/") +
                                    fileName + ".pdf"); //File will be saved in application root
                        OwnerInfoObject.NomineeNIDPath = "~/Content/ownernomineenidfiles/" + fileName + ".pdf";
                    }

                    db.SaveChanges();
                }
            }

            return Json("Uploaded " + Request.Files.Count + " files");
        }
        public JsonResult Upload_TINFile_Edit()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
                {
                    string OwnerId = Session[SessionKeys.USERNAME].ToString();
                    HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                    //Use the following properties to get file's name, size and MIMEType
                    var OwnerInfoObject = db.OwnersInfos.Where(o => o.SlotId == OwnerId)
                        .FirstOrDefault();
                    string fileName = OwnerInfoObject.SlotId + Path.GetExtension(file.FileName);

                    //To save file, use SaveAs method
                    string fileSavingPath = "~/Content/ownertinfiles/";
                    string mapPath = Server.MapPath(fileSavingPath);

                    //To save file, use SaveAs method
                    file.SaveAs(mapPath + fileName); //File will be saved in application root
                    OwnerInfoObject.TINFilePath = fileSavingPath + fileName;

                    db.SaveChanges();
                }
            }

            return Json("Uploaded " + Request.Files.Count + " files");
        }

        [HttpGet]
        public ActionResult OwnersInfoIncomeViewOrEditAfterLogin()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            ViewBag.Current = "OwnersInfoIncomeViewOrEditAfterLogin";
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {


                var slotId = Session[SessionKeys.USERNAME].ToString();
                var mdl = adbc.OwnersInfos.Where(o => o.SlotId == slotId).FirstOrDefault();
                bool IsProfilePictureExists = true;
                bool IsNIDFileExists = true;
                bool IsTINFileExists = true;
                if (string.IsNullOrEmpty(mdl.PicturePath))
                {
                    //mdl.PicturePath = "~/Content/ownerprofilepictures/" + mdl.SlotId;
                    IsProfilePictureExists = false;
                }

                if (string.IsNullOrEmpty(mdl.NIDFilePath))
                {
                    //mdl.NIDFilePath = "~/Content/ownernidfiles/" + mdl.SlotId + ".jpg";
                    IsNIDFileExists = false;
                }

                if (string.IsNullOrEmpty(mdl.TINFilePath))
                {
                    //mdl.NIDFilePath = "~/Content/ownertinfiles/" + mdl.SlotId + ".jpg";
                    IsTINFileExists = false;
                }

                OwnersInfoView model = new OwnersInfoView()
                {
                    OwnerInfo = mdl,
                    IsProfilePictureExists = IsProfilePictureExists,
                    IsNIDFileExists = IsNIDFileExists,
                    IsTINFileExists = IsTINFileExists
                };
                return View("OwnersInfoIncomeViewOrEditAfterLogin", model);
            }
        }

        //        [HttpPost]
        //
        //        public ActionResult OwnerRoomSlotInfoView(OwnerRoomSlotInfoView mdl)
        //        {
        //            return View(mdl);
        //        }
        [HttpGet]
        public ActionResult OwnerRoomSlotInfoView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            ViewBag.Current = "OwnerRoomSlotInfoView";

            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerId = Session[SessionKeys.USERNAME].ToString();
                var OwnerInfoObj = adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).FirstOrDefault();
                var OwnerTransactionHistoryList =
                    adbc.OwnerTransationHistories.Where(o => o.SlotID == OwnerInfoObj.SlotId).ToList();
                var AccommodationInfo = adbc.AccomadationInfos.ToList();
                double TotalSquareFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1")
                    .Select(c => c.TotalSquareFeet).FirstOrDefault();
                double totalSquareFeetPerPerson = 0;
                foreach (var transaction in OwnerTransactionHistoryList)
                {
                    totalSquareFeetPerPerson += transaction.SFTperPerson;
                }

                OwnerRoomSlotInfoViewModel model = new OwnerRoomSlotInfoViewModel()
                {
                    ownerInfo = OwnerInfoObj,
                    OwnerAccommodationInfoListBySlotId = AccommodationInfo,
                    OwnerSquareFeet = totalSquareFeetPerPerson,
                    TotalSqFeet = TotalSquareFeet
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult OwnerTransactionHistory()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            ViewBag.Current = "OwnerTransactionHistory";
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var SlotId = Session[SessionKeys.USERNAME].ToString();
                var OwnerInfoObj = adbc.OwnersInfos.Where(o => o.SlotId == SlotId).FirstOrDefault();
                var OwnerTransactionHistoryList =
                    adbc.OwnerTransationHistories.Where(o => o.SlotID == OwnerInfoObj.SlotId).ToList();

                OwnerTransactionInfoViewModel model = new OwnerTransactionInfoViewModel()
                {
                    ownerInfo = OwnerInfoObj,
                    ownerTransactionHistoryList = OwnerTransactionHistoryList
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult OwnerPaymentHistory()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            ViewBag.Current = "OwnerPaymentHistory";

            OwnerPaymentInfoViewModel model = PreparePaymentModel();

            return View(model);
        }

        public OwnerPaymentInfoViewModel PreparePaymentModel()
        {
            string slotId = Session[SessionKeys.USERNAME].ToString();
            var payments = _dbContext.OwnerPayments.Where(o => o.SlotId == slotId).ToList();

            OwnerPaymentInfoViewModel model = new OwnerPaymentInfoViewModel()
            {
                OwnerInfo = _ownerInfoService.GetOwnerBasicInfo(slotId),
                PaymentList = payments
            };
            return model;
        }

        [HttpGet]
        public ActionResult DividendView(string dividendId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                Dividend mdl = adbc.Dividends.Where(d => d.DividendId == dividendId).FirstOrDefault();
                OwnersInfo ownerObj = adbc.OwnersInfos.Where(o => o.OwenerId == mdl.OwnerId || o.SlotId == mdl.SlotId).FirstOrDefault();
                Dividend model = new Dividend()
                {
                    DividendAmount = mdl.DividendAmount,
                    FromMonth = mdl.FromMonth,
                    ToMonth = mdl.ToMonth,
                    SlotId = ownerObj.SlotId,
                    MobileNumber = ownerObj.Mobile,
                    OwnerName = ownerObj.Name,
                    OwnerId = ownerObj.OwenerId
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult NotificationView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            var ownerId = Session[SessionKeys.USERNAME].ToString();
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                List<Notification> NotificationObjList = new List<Notification>();

                var UnseenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == false && ((n.NotificationHeader == "Dividend" && n.DividendHolder == ownerId) || (n.NotificationHeader == "Notice"))).ToList();

                SortedDictionary<int, Notification> UnseenNotificationsListForSorting = new SortedDictionary<int, Notification>();

                foreach (var notification in UnseenNotificationObjList)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    UnseenNotificationsListForSorting.Add(key, notification);
                }

                Dictionary<int, Notification> UnseenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in UnseenNotificationsListForSorting.Reverse())
                {
                    NotificationObjList.Add(notificationKeyValuePair.Value);
                }

                var SeenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == true && ((n.NotificationHeader == "Dividend" && n.DividendHolder == ownerId) || (n.NotificationHeader == "Notice"))).ToList();

                SortedDictionary<int, Notification> SeenNotificationsListForSorting = new SortedDictionary<int, Notification>();

                foreach (var notification in SeenNotificationObjList)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    SeenNotificationsListForSorting.Add(key, notification);
                }

                Dictionary<int, Notification> SeenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in SeenNotificationsListForSorting.Reverse())
                {
                    NotificationObjList.Add(notificationKeyValuePair.Value);
                }

                List<Notification> selectedNotifications = new List<Notification>();
                int noOfNotifications;
                if (NotificationObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedNotifications.Add(NotificationObjList.ElementAt(i));
                    }
                    int x = (NotificationObjList.Count % 10 == 0) ? 0 : 1;

                    noOfNotifications = (NotificationObjList.Count / 10) + x;
                }
                else
                {
                    selectedNotifications = NotificationObjList;
                    noOfNotifications = 1;
                }

                NotificationViewModel mdl = new NotificationViewModel()
                {
                    NotificationList = selectedNotifications,
                    NoOfNotification = noOfNotifications,
                    index = 1
                };

                return View(mdl);
            }
        }

        [HttpGet]
        public ActionResult NotificationsPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            var ownerId = Session[SessionKeys.USERNAME].ToString();
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                List<Notification> NotificationObjList = new List<Notification>();

                var UnseenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == false && ((n.NotificationHeader == "Dividend" && n.DividendHolder == ownerId) || (n.NotificationHeader == "Notice"))).ToList();

                SortedDictionary<int, Notification> UnseenNotificationsListForSorting = new SortedDictionary<int, Notification>();

                foreach (var notification in UnseenNotificationObjList)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    UnseenNotificationsListForSorting.Add(key, notification);
                }

                Dictionary<int, Notification> UnseenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in UnseenNotificationsListForSorting.Reverse())
                {
                    NotificationObjList.Add(notificationKeyValuePair.Value);
                }

                var SeenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == true && ((n.NotificationHeader == "Dividend" && n.DividendHolder == ownerId) || (n.NotificationHeader == "Notice"))).ToList();

                SortedDictionary<int, Notification> SeenNotificationsListForSorting = new SortedDictionary<int, Notification>();

                foreach (var notification in SeenNotificationObjList)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    SeenNotificationsListForSorting.Add(key, notification);
                }

                Dictionary<int, Notification> SeenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in SeenNotificationsListForSorting.Reverse())
                {
                    NotificationObjList.Add(notificationKeyValuePair.Value);
                }

                List<Notification> selectedNotifications = new List<Notification>();
                int noOfNotifications;
                if (NotificationObjList.Count >= 10)
                {
                    var pageNo = (NotificationObjList.Count < (index * 10)) ? NotificationObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedNotifications.Add(NotificationObjList.ElementAt(i));
                    }
                    int x = (NotificationObjList.Count % 10 == 0) ? 0 : 1;

                    noOfNotifications = (NotificationObjList.Count / 10) + x;
                }
                else
                {
                    selectedNotifications = NotificationObjList;
                    noOfNotifications = 1;
                }

                NotificationViewModel mdl = new NotificationViewModel()
                {
                    NotificationList = selectedNotifications,
                    NoOfNotification = noOfNotifications,
                    index = index
                };

                return View("NotificationView", mdl);
            }
        }

        [HttpGet]

        public ActionResult OwnersInfoEdit()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            ViewBag.Current = "OwnersInfoIncomeViewOrEditAfterLogin";
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var SlotId = Session[SessionKeys.USERNAME].ToString();
                var mdl = adbc.OwnersInfos.Where(o => o.SlotId == SlotId).FirstOrDefault();
                bool IsProfilePictureExists = true;
                bool IsNIDFileExists = true;
                bool IsTINFileExists = true;
                if (string.IsNullOrEmpty(mdl.PicturePath))
                {
                    IsProfilePictureExists = false;
                }

                if (string.IsNullOrEmpty(mdl.NIDFilePath))
                {
                    IsNIDFileExists = false;
                }

                if (string.IsNullOrEmpty(mdl.TINFilePath))
                {
                    IsTINFileExists = false;
                }


                mdl.Password = EncryptDecryptString.Decrypt(mdl.Password, "1234");
                OwnersInfoView model = new OwnersInfoView()
                {
                    OwnerInfo = mdl,

                    IsProfilePictureExists = IsProfilePictureExists,
                    IsNIDFileExists = IsNIDFileExists,
                    IsTINFileExists = IsTINFileExists
                };
                return View("OwnersInfoEdit", model);
            }
        }

        [HttpGet]
        public ActionResult OwnersFamilyInfoEdit()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            ViewBag.Current = "OwnersInfoIncomeViewOrEditAfterLogin";
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerID = Session[SessionKeys.OWNERID].ToString();
                var mdl = adbc.OwnersFamilies.Where(o => o.OwnerId == OwnerID).FirstOrDefault();
                bool IsProfilePictureExists = true;
                bool IsNIDFileExists = true;
                bool IsTINFileExists = true;
                //if (string.IsNullOrEmpty(mdl.PicturePath))
                //{
                //    IsProfilePictureExists = false;
                //}

                //if (string.IsNullOrEmpty(mdl.NIDFilePath))
                //{
                //    IsNIDFileExists = false;
                //}

                //if (string.IsNullOrEmpty(mdl.TINFilePath))
                //{
                //    IsTINFileExists = false;
                //}
                OwnersInfoView model = new OwnersInfoView()
                {
                    FamilyInfo = mdl,
                    IsProfilePictureExists = IsProfilePictureExists,
                    //IsNIDFileExists = IsNIDFileExists,
                    //IsTINFileExists = IsTINFileExists
                };
                return View("OwnersFamilyInfoEdit", model);
            }
        }
        [HttpPost]
        public ActionResult OwnersFamilyEditAfterLogin(OwnersInfoView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            ViewBag.Current = "OwnersInfoIncomeViewOrEditAfterLogin";

            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnersInformationDB =
                    adbc.OwnersFamilies.Where(o => o.OwnerId == mdl.FamilyInfo.OwnerId).FirstOrDefault();
                var nameOfOwner = adbc.OwnersInfos.Where(o => o.OwenerId == mdl.FamilyInfo.OwnerId).Select(o => o.Name)
                  .FirstOrDefault();
                OwnersInformationDB.SpouseName = mdl.FamilyInfo.SpouseName;
                OwnersInformationDB.ChildName5 = mdl.FamilyInfo.ChildName5;
                OwnersInformationDB.ChildName = mdl.FamilyInfo.ChildName;
                OwnersInformationDB.ChildName2 = mdl.FamilyInfo.ChildName2;
                OwnersInformationDB.ChildName3 = mdl.FamilyInfo.ChildName3;
                OwnersInformationDB.ChildName4 = mdl.FamilyInfo.ChildName4;
                //OwnersInformationDB.ChildPicture = mdl.Child1file;
                //OwnersInformationDB.PresentAddress = mdl.OwnerInfo.PresentAddress;
                //OwnersInformationDB.OwnersLastEditTime = DateTime.Now;
                //OwnersInformationDB.IsEditApproved = mdl.OwnerInfo.IsEditApproved;
                //OwnersInformationDB.IsEditApproved = "NO";
                adbc.SaveChanges();
                bool IsProfilePictureExists = true;
                bool IsNIDFileExists = true;
                bool IsTINFileExists = true;
                //if (string.IsNullOrEmpty(OwnersInformationDB.PicturePath))
                //{
                //    //mdl.PicturePath = "~/Content/ownerprofilepictures/" + mdl.SlotId;
                //    IsProfilePictureExists = false;
                //}

                //if (string.IsNullOrEmpty(OwnersInformationDB.NIDFilePath))
                //{
                //    //mdl.NIDFilePath = "~/Content/ownernidfiles/" + mdl.SlotId + ".jpg";
                //    IsNIDFileExists = false;
                //}

                //if (string.IsNullOrEmpty(OwnersInformationDB.TINFilePath))
                //{
                //    //mdl.NIDFilePath = "~/Content/ownertinfiles/" + mdl.SlotId + ".jpg";
                //    IsTINFileExists = false;
                //}
                ////notification//////////
                //Notification
                //var NotificationList = adbc.Notifications.ToList();
                //var NotificationIdList = new List<int>();
                //foreach (var notification in NotificationList)
                //{
                //    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                //}

                //var notificationToken = NotificationList.Count();
                //if (notificationToken > 0)
                //{
                //    NotificationIdList.Sort();
                //    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                //}

                //notificationToken++;
                //var OwnersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.OwenerId == mdl.OwnerInfo.OwenerId);
                //Notification notificationModel = new Notification()
                //{
                //    isSeen = false,
                //    NotificationHeader = "EditInfo",
                //    NotificationText = OwnersInfo.SlotId + " has requested an edit " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                //    NotificationLink = "/OwnersInfo/OwnersInfoEdit?ownerId=" + mdl.OwnerInfo.OwenerId,
                //    NotificationTime = DateTime.Now,
                //    NotificationId = "NOT-" + notificationToken,
                //    IdForRecognition = mdl.OwnerInfo.OwenerId
                //};
                //adbc.Notifications.Add(notificationModel);



                var thisOwner = OwnersInformationDB;


                adbc.SaveChanges();
                ///

                //sending sms

                //Common.SMSSending(mobileNo, "Dear " + nameOfOwner + ", Your request is being processed, soon you will receive an email with details instructions.");
                //var mdl1 = adbc.OwnersInfos.Where(o => o.OwenerId == mdl.OwnerInfo.OwenerId).FirstOrDefault();
                //var Owner = mdl;
                //var mobileNumber = mdl1.Mobile;
                //var Email = mdl1.Email;
                //var isSMSSent = "Processing";
                //var txt = "Dear " + nameOfOwner +
                //          ", Your request of information updating is being processed, soon you will receive an sms and an email from coral reef.";
                //if (mobileNumber != null && mobileNumber != "")
                //{
                //    var returnStatus = Common.SMSSending(mobileNumber, txt);
                //    if (returnStatus == true)
                //    {
                //        isSMSSent = "Delivered";
                //    }
                //    else
                //    {
                //        isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                //    }
                //}
                //else
                //{
                //    isSMSSent = "Not Delivered, Check whether Mobile No for this owner is correct";
                //}
                //EmailandSMSReport emailsmsreport = new EmailandSMSReport()
                //{
                //    IsSMSSent = isSMSSent,
                //    Mobile = mobileNumber,
                //    Email = OwnersInformationDB.Email,
                //    SlotId = OwnersInformationDB.SlotId,
                //    Name = OwnersInformationDB.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "SMS",
                //    Text = txt,
                //    NotificationSerialNo = "AUTO GENERATED:- UPDATEINFO",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreport);


                //var admin = adbc.Admins.FirstOrDefault();
                //Delete Later
                //var isSMSSentAdmin = "Processing";
                //if (!String.IsNullOrEmpty(admin.Mobile))
                //{
                //    var returnStatus = Common.SMSSending(admin.Mobile, OwnersInformationDB.SlotId + " has requested for updating information, Please see to that.");
                //    if (returnStatus == true)
                //    {
                //        isSMSSentAdmin = "Delivered";
                //    }
                //    else
                //    {
                //        isSMSSentAdmin = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                //    }
                //}
                //else
                //{
                //    isSMSSentAdmin = "Not Delivered, Check whether Mobile No for this owner is correct";
                //}
                //EmailandSMSReport emailsmsreportForAdmin = new EmailandSMSReport()
                //{
                //    IsSMSSent = isSMSSent,
                //    Mobile = admin.Mobile,
                //    Email = admin.Email,
                //    SlotId = "",
                //    Name = admin.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "SMS",
                //    Text = mdl.OwnerInfo.OwenerId + " has requested for updating information, Please see to that.",
                //    NotificationSerialNo = "AUTO GENERATED:- UPDATEINFO",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreportForAdmin);


                ////sending email
                //var Message ="Dear " + nameOfOwner +", Your request is being processed, you will receive an sms and an email from coral reef.";
                //var isEmailSent = "Processing";
                ////Common.EmailSending(email, "Notification of Booking from Heritage BD", "Dear " + nameOfOwner + ", Your request is being processed, soon you will receive an email with details instructions.");
                //if (Email != null && Email != "")
                //{
                //    Common.EmailSending(Email, "Notification of Updating Your Info from Heritage BD", Message);
                //    isEmailSent = "Delivered";
                //}
                //else
                //{
                //    isEmailSent = "Failed";
                //}
                //EmailandSMSReport emailsmsreportForEmail = new EmailandSMSReport()
                //{
                //    IsSMSSent = isEmailSent,
                //    Mobile = mdl1.Mobile,
                //    Email = Email,
                //    SlotId = mdl1.SlotId,
                //    Name = mdl1.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "Email",
                //    //Text = model.EmailText,
                //    Text = Message,
                //    NotificationSerialNo = "AUTO GENERATED:- UPDATEINFO",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreportForEmail);

                //var messageAdmin = mdl1.SlotId + " has requested for updating information, Please see to that.";

                //var adminForEmail = adbc.Admins.FirstOrDefault();

                //var isEmailSentAdmin = "Processing";

                //if (Email != null && Email != "")
                //{
                //    Common.EmailSending(adminForEmail.Email, "Request for updating information", messageAdmin);
                //    isEmailSentAdmin = "Delivered";
                //}
                //else
                //{
                //    isEmailSentAdmin = "Failed";
                //}
                //EmailandSMSReport emailsmsreportForEmailAdmin = new EmailandSMSReport()
                //{
                //    IsSMSSent = isEmailSentAdmin,
                //    Mobile = adminForEmail.Mobile,
                //    Email = adminForEmail.Email,
                //    SlotId = "",
                //    Name = adminForEmail.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "Email",
                //    //Text = model.EmailText,
                //    Text = messageAdmin,
                //    NotificationSerialNo = "AUTO GENERATED:- UPDATEINFO",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreportForEmailAdmin);

                return RedirectToAction("OwnersFamilyInfoEdit", "Owner", new { ownerId = mdl.FamilyInfo.OwnerId });
            }
        }
        [HttpGet]
        public ActionResult OwnersNomineeInfoEdit()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            ViewBag.Current = "OwnersInfoIncomeViewOrEditAfterLogin";
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnerID = Session[SessionKeys.OWNERID].ToString();
                var mdl = adbc.OwnersNominees.Where(o => o.OwnerId == OwnerID).FirstOrDefault();

                OwnersInfoView model = new OwnersInfoView()
                {
                    NomineeInfo = mdl

                };
                return View("OwnersNomineeInfoEdit", model);
            }
        }

        [HttpPost]
        public ActionResult OwnersNomineeEditAfterLogin(OwnersInfoView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            ViewBag.Current = "OwnersInfoIncomeViewOrEditAfterLogin";

            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var OwnersInformationDB =
                    adbc.OwnersNominees.Where(o => o.OwnerId == mdl.NomineeInfo.OwnerId).FirstOrDefault();
                //var nameOfOwner = adbc.OwnersInfos.Where(o => o.OwenerId == mdl.FamilyInfo.OwnerId).Select(o => o.Name)
                //  .FirstOrDefault();
                OwnersInformationDB.NomineeName = mdl.NomineeInfo.NomineeName;
                OwnersInformationDB.OwnerRelation = mdl.NomineeInfo.OwnerRelation;
                OwnersInformationDB.NomineeMobile = mdl.NomineeInfo.NomineeMobile;
                OwnersInformationDB.NomineePermanentAddress = mdl.NomineeInfo.NomineePermanentAddress;
                OwnersInformationDB.NomineePresentAddress = mdl.NomineeInfo.NomineePresentAddress;
                OwnersInformationDB.NomineeNID = mdl.NomineeInfo.NomineeNID;
                //OwnersInformationDB.PresentAddress = mdl.OwnerInfo.PresentAddress;
                //OwnersInformationDB.OwnersLastEditTime = DateTime.Now;
                //OwnersInformationDB.IsEditApproved = mdl.OwnerInfo.IsEditApproved;
                //OwnersInformationDB.IsEditApproved = "NO";
                adbc.SaveChanges();
                bool IsProfilePictureExists = true;
                bool IsNIDFileExists = true;
                bool IsTINFileExists = true;



                var thisOwner = OwnersInformationDB;


                adbc.SaveChanges();
                ///



                return RedirectToAction("OwnersNomineeInfoEdit", "Owner", new { ownerId = mdl.NomineeInfo.OwnerId });
            }
        }
    }
}