﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class OwnerTransactionInfoViewModel
    {
        public OwnersInfo ownerInfo { get; set; }
        public List<OwnerTransactionHistory> ownerTransactionHistoryList { get; set; }
    }
}