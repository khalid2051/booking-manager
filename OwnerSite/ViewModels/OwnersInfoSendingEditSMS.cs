﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class OwnersInfoSendingEditSMS
    {
        public string ownerId { get; set; }
        public bool flag { get; set; }
        public string ownerMobileNumber { get; set; }
    }
}