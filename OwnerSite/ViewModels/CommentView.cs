﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class CommentView
    {
        public Comment Comment { get; set; }
        public string CommentorName { get; set; }
    }
}