﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace OwnerSite.ViewModels
{
    public class OwnersInfoView
    {
        public OwnersInfo OwnerInfo { get; set; }
        public OwnersFamily FamilyInfo { get; set; }
        public OwnersNominee NomineeInfo { get; set; }
        public Boolean IsEdited { get; set; }
        public bool IsProfilePictureExists { get; set; }
        public bool IsNIDFileExists { get; set; }
        public bool IsTINFileExists { get; set; }
        public List<DashboardNotice> AllNotices = new List<DashboardNotice>();
        public string AdminComment { get; set; }
        public string IsEditApproved { get; set; }
        public HttpPostedFileBase Child1file { get; set; }
        public HttpPostedFileBase Child2file { get; set; }
        public HttpPostedFileBase Child3file { get; set; }
        public HttpPostedFileBase Child4file { get; set; }
        public HttpPostedFileBase Child5file { get; set; }


    }
}