﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace OwnerSite.ViewModels
{
    public class OwnerPaymentInfoViewModel
    {
        public OwnerBasicInfo OwnerInfo { get; set; }
        public List<OwnersPayment> PaymentList { get; set; }
    }
}