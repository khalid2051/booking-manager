﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class PostingJsonClass
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}