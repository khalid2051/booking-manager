﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class CommentingJsonClass
    {
        public string PostId { get; set; }
        public string CommentText { get; set; }
    }
}