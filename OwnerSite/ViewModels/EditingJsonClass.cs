﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class EditingJsonClass
    {
        public string PostId { get; set; }
        public string PostTitle { get; set; }
        public string PostText { get; set; }
    }
}