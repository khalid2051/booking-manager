﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class searchedCommentResultViewModel
    {
        public string CommentId { get; set; }
        public string CommentText { get; set; }
        public string CommentorName { get; set; }
        public string CommentTime { get; set; }
        public string PostHeader { get; set; }
    }
}