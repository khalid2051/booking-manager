﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class AdminChatView
    {
        public List<Chat> Chats { get; set; }
        public string LastMessageTime { get; set; }
        public string CurrentMessage { get; set; }
        public string SlotId { get; set; }
        public int ID { get; set; }
    }
}