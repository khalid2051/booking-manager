﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class EditingCommentJsonClass
    {
        public string CommentId { get; set; }
        public string CommentText { get; set; }
    }
}