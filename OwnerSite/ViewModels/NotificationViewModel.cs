﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class NotificationViewModel
    {
        public List<Notification> NotificationList { get; set; }
        public int NoOfNotification { get; set; }
        public int index { get; set; }
    }
}