﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace OwnerSite.ViewModels
{
    public class WallPostingViewModel
    {
        public List<Post> Posts { get; set; }
        public List<string> Likes { get; set; }
        public List<CommentView> Comments { get; set; }
        public List<string> PostedByName { get; set; }
        public int NoOfPages { get; set; }
        
        public List<string> MyPostIds { get; set; }
        public List<string> MyCommentIds { get; set; }
        public bool SortByStatus { get; set; }
        public int NoOfWeeks { get; set; }
        public int index { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}