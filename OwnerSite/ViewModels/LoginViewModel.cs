﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class LoginViewModel
    {
        public string OwnerSmartId { get; set; }
        public string SlotId { get; set; }
        public string OwnerId { get; set; }
        public string OwnerLoginOTPCode { get; set; }

        public Boolean ErrorFlag { get; set; }
        public Boolean OTPErrorFlag { get; set; }

        public Boolean OTPForEmailorSMS { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string MobileEmail { get; set; }

    }
}