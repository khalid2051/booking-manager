﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class OwnerRoomSlotInfoViewModel
    {
        public OwnersInfo ownerInfo { get; set; }
        
        public List<AccommodationInfo> OwnerAccommodationInfoListBySlotId { get; set; }

        public double OwnerSquareFeet { get; set; }
        public double TotalSqFeet { get; set; }
    }
}