﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwnerSite.ViewModels
{
    public class searchedPostResultViewModel
    {
        public string PostId { get; set; }
        public string PostedBy { get; set; }
        public string PostHeader { get; set; }
        public string OwnerName { get; set; }
        public string Time { get; set; }
   
    }
}