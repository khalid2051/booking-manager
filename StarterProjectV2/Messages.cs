﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2
{
    public static class Messages
    {
        public const string StatusSuccess = "Success";
        public const string StatusFail = "Fail";
        public const string StatusWarning = "Warning";
        public const string BookingNotExists = "Booking not exists!";
        public const string RoomRateNotSetup = "Room rate not setup!";
        public const string InvoiceGeneratedSuccessfully = "Invoice generated successfully!";
        public const string PleaseGenerateInvoiceFirst = "Please generate invoice first!";
        public const string CheckOutDateNotMatchedWithSystemDate = "Check Out Date is not matched with System Date!";
        public const string SystemDateMustBeLessThanCurrentDate = "System Date must be less than current date!";
        public const string HotelAdminstrationAndManagementSystem = "Hotel Adminstration & Management System!";
        public const string WelcomeToHAMSProject = "Welcome to HAMS Project 1.0!";
        public const string LedgerGeneratedSuccessfully = "Ledger generated successfully!";
        public const string BookingCreatedSuccessfully = "Booking created successfully!";
        public const string BookingUpdatedSuccessfully = "Booking updated successfully!";
        
        public const string CheckedInSuccessfully = "Checked-In Successfully!";
        public const string CheckedOutSuccessfully = "Checked Out Successfully!";
        public const string NotificationCreatedSuccessfully = "Notification created successfully!";
        public const string BookingRequestSMSSentSuccessfully = "Booking Request SMS Sent Successfully!";
        public const string BookingConfirmationSMSSentSuccessfully = "Booking Confirmation SMS Sent Successfully!";
        public const string BookingConfirmationEmailSentSuccessfully = "Booking Confirmation Email Sent Successfully!";

        public const string TransationHistoryNotAssigned = "Transation History Not Assigned";
        public const string InsertSuccess = "Inserted Successfully!";
        public const string UpdateSuccess = "Updated Successfully!";

        public const string SlotIdExist =  "SlotId already exist!";
        public const string OwnerIdNotFound = "OwnerId not found!";

        public const string FileSaved = "File Saved!";
        public const string FileNotSaved = "File Not Saved!";
        public const string FileNotFound = "File Not Found!";


    }
}