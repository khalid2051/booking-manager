﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2
{
    public class SessionKeys
    {
        //Accounts Management
        public static string USERNAME = "LOGGED_IN_USER_947";
        public static string PASSWORD = "LOGGED_IN_USER_PASSWORD_947";
        public static string LOGIN_TIME = "USER_LOGGED_IN_TIME_947";
        public static string PC_NAME = "USER_PC_NAME_947";
        public static string EXTERNAL_LOGIN_IP_ADDRESS = "EXT_USER_LOGIN_IP_ADDRESS_947";
        public static string LOCAL_LOGIN_IP_ADDRESS = "LCL_USER_LOGIN_IP_ADDRESS_947";
        public static string MODULE_LIST = "MODULE_LIST_103";
        public static string DISPLAY_USER_NAME = "DISPLAY_USERNAME_103";

        public static string SYSTEM_DATE = "SYSTEM_DATE";


        //User
        public static string ROLE_LIST = "ROLE_LIST_103";
        public static string USER_PROFILE_PICTUREPATH = "USER_PROFILE_PICTUREPATH_103";
        public static string LAST_UPDATED = "LAST_UPDATED_103";
        public static string NO_OF_EMPLOYEES = "NO_OF_EMPLOYEES_103";
        public static string DIVIDENT_SELECT_BOOLEAN_LIST = "DIVIDENT_SELECT_BOOLEAN_LIST_103";
        public static string DIVIDENT_ALL_SELECT = "DIVIDENT_ALL_SELECT_103";
    }
}