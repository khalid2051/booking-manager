﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerRoomBookingViewForSearch
    {
        public string BookingToken { get; set; } //Can be Sent, Not Sent Due to Incorrect/Null Mobile No, Not Sent due to SMS Api failure, 
        public string SlotId { get; set; }
        public string GuestNameMain { get; set; }
        public string GuestMobile { get; set; }
        public string Email { get; set; }
        public int NoOfAdult { get; set; }
        public int NoOfChild { get; set; }
        public int NoOfRoomRequested { get; set; }
        public string Startdate { get; set; }
        public string Enddate { get; set; }
        public string IsBookingCompleted { get; set; }
        public string HasStayedOrNot { get; set; }
        public string OwnerRoomBookngId { get; set; }
        public string ApplicationDate { get; set; }
    }
}