﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class TickerManagementView
    {
        public List<Ticker> TickerList { get; set; }
        public string TaskRoles { get; set; }
        public int noOfPage { get; set; }

        public int index { get; set; }
        public string searchfield;
    }
}