﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class UserDetailsView
    {

        public string UserName { get; set; }
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
        
        public string UserDisplayName { get; set; }
        
        public string UserType { get; set; }
        
        public string UserEmail { get; set; }
        
        public string UserMobileNumber { get; set; }
        
        public string UserPicturePath { get; set; }

        public HttpPostedFileBase UserProfilePicture { get; set; }

        public List<string> UserTypeList { get; set; }
        //        public string Details { get; set; }
        public List<Tuple<Module, List<Task>>> GetAllModuleListWithTasks { get; set; }
        //public List<Tuple<string, List<Tuple<string,string>>>> GetSelectedModuleListWithTasks { get; set; }
        public List<PartialUserDetailsViewModel> PartialUserDetailsViewModels { get; set; }
        
        //public IEnumerable<string> dummy { get; set; }
        public List<Tuple<string, string>> TaskWithRoles { get; set; }
        public IEnumerable<Module> ModuleIdandParent { get; set; }
        public string selectedModuleId { get; set; }
    }
}