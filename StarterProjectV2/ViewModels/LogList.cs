﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class LogList
    {
        public IEnumerable<Log> Logs;
        public string TaskRoles;
        public int NoOfAudit;
        public int index;
        public string searchfield;
    }
}