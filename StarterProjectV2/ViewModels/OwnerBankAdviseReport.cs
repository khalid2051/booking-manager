﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;


namespace StarterProjectV2.ViewModels
{
    public class OwnerBankAdviseReport
    {
        public string SlotId { get; set; }
        public string ReceiverName { get; set; }

        public string ReceivingBank { get; set; }

        public decimal Amount { get; set; }

        //public string Remarks { get; set; }

        public DateTime PaymentDate { get; set; }
        //public string Name { get; set; }
        public string AccountNo     
        { 
           get; 
           set; 
        }


    }
}