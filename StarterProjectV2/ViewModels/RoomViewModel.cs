﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class RoomViewModel
    {
        public List<string> OwnershipCriteriaList { get; set; }
        public string RoomId { get; set; }
        public string RoomNo { get; set; }
        public string Floor { get; set; }
        public int RoomSize { get; set; }
        public string OwnershipCriteria { get; set; }
        public string RoomType { get; set; }
    }
}