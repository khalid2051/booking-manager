﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerSearchByIdJsonView
    {
        public string Id { get; set; }
    }
}