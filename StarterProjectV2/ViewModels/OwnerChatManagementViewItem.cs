﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerChatManagementViewItem
    {
        public string OwnersName { get; set; }
        public string LastMessage { get; set; }
        public string SlotId { get; set; }
        public string LastMessageTime { get; set; }
    }
}