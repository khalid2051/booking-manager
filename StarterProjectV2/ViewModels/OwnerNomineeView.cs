﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerNomineeView
    {

        public List<string> OwnerNomineeNameArray { get; set; }
        public List<string> OwnerNomineeMobileArray { get; set; }
        public List<string> OwnerNomineeEmailArray { get; set; }
        public List<string> OwnerNomineeDOBArray { get; set; }
        public List<string> OwnerNomineePermanentAddressArray { get; set; }
        public List<string> OwnerNomineePresentAddressArray { get; set; }
        public List<string> OwnerNomineeNIDArray { get; set; }
        public List<string> OwnerNomineePictureArray { get; set; }

        public string OwnerId { get; set; }
    }

    public class OwnerNomineeViewModel
    {
        public string OwnerId { get; set; }

        [Required]
        public HttpPostedFileBase PictureFile { get; set; }
        [Required]
        public HttpPostedFileBase NIDPictureFile { get; set; }

        [Required]
        public string NomineeName { get; set; }
        public string NomineeEmail { get; set; }
        [Required]
        public string NomineeMobile { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeNID { get; set; }
        public string NomineePermanentAddress { get; set; }
        public string NomineePresentAddress { get; set; }
        public string NomineePicture { get; set; }
        public string NomineeNIDPath { get; set; }
        public string OwnerRelation { get; set; }
    }

}