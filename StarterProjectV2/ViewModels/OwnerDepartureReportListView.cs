﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerDepartureReportListView
    {
        public IList<OwnerDepartureReport> OwnerDepartureReportList { get; set; }
        public string CheckOutDate { get; set; }
        public string CheckInDate { get; set; }
    }
}