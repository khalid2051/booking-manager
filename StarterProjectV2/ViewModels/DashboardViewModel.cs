﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class DashboardViewModel
    {
        

        public int RoomsOccupiedLastNight { get; set; }
        public int RoomsOccupiedThisMonth { get; set; }
        public IEnumerable<SystemDate> Date;
        public List<SystemDate> SystemDates = new List<SystemDate>();
        public int InventoryCount { get; set; }
        public int RoomsOccupiedThisYear { get; set; }
        public int NoOfFutureBookingThisMonth { get; set; }
        public int OccupiedRoomsToday { get; set; }
        public int AvailableRoomsToday { get; set; }
        public int NoOfFutureBookingThisYear { get; set; }
        public int TodayArrival { get; set; }
        public int TodayCheckOut { get; set; }
        public int TodayStayOver { get; set; }
        public string UserFlag { get; set; }
        public bool[] LinkPermissions { get; set; }

    }
}