﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class searchedCommentWithStringDates
    {
        public string CommentId { get; set; }
        public string PostId { get; set; }
        public string CommentorId { get; set; }
        public string OwnerName { get; set; }
        public string CommentText { get; set; }
        public bool Approval { get; set; }
        public string CommentTime { get; set; }
        public string ApprovalTime { get; set; }
    }
}