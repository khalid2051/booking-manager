﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class searchedPostWithStringDate
    {
        public string PostId { get; set; }
        public string OwnerName { get; set; }
        public string PostedBy { get; set; }
        public string Time { get; set; }
        public bool Approval { get; set; }
        public string ApprovalTime { get; set; }
    }
}