﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerBookingHistoryReport
    {
        //public string Startdate { get; set; }
        //public string Enddate { get; set; }

        //public string OwnerRoomBookngId { get; set; }
        //public string OBOwnerId { get; set; }
        public string SlotId { get; set; }
        public string GuestNameMain { get; set; }

        public string GuestAddress { get; set; }
   
        public string BookingToken { get; set; }
        public string GuestMobile { get; set; }
        public string Email { get; set; }
        //public string DateOfBirth { get; set; }
        public string HasStayedOrNot { get; set; }
     
       
        //public int NoOfDaysCanStay { get; set; }
      

        public string NoOfRoomAllotted { get; set; }
        //public int MonthlyAllotment { get; set; }
        //public int YearlyAllotment { get; set; }

        public String CheckInDate { get; set; }
        public String CheckOutDate { get; set; }
        public int NoOfDaySpent { get; set; }
    }
}