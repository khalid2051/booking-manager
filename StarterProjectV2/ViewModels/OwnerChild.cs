﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerChild
    {
        public List<string> ChildName { get; set; }

        public List<string> ChildPicture { get; set; }

        public List<string> ChildDOB { get; set; }
    }
}