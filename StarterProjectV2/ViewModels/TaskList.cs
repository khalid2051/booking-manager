﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class TaskList
    {

        public IEnumerable<Tuple<Task, string>> tasks { get; set; }
        public string TaskRoles { get; set; }
        public string SearchField { get; set; }
    }
}