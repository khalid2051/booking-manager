﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class RoomSlotManagementView
    {
        public List<RoomSlotAssignment> RoomSlotList { get; set; }
        public string TaskRoles { get; set; }
    }
}