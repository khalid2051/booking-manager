﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace StarterProjectV2.ViewModels
{
    public class OwnerDepartureReport
    {
        public string SlotId { get; set; }
        public string OwnerSegment { get; set; }
        public string GuestNameMain { get; set; }
        public string NoOfAdult { get; set; } 
        public string NoOfChild { get; set; } 
        public string BookingToken { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string GuestMobile { get; set; }
        public string Email { get; set; }
        public string IsBookingCompleted { get; set; }
        public string HasStayedOrNot { get; set; }

        public int NoOfDaySpent { get; set; }
       
    }
}