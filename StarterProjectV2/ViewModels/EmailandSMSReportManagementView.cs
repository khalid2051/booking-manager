﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class EmailandSMSReportManagementView
    {
        public List<EmailandSMSReport> EmailandSMSReportList { get; set; }
        public string TaskRoles { get; set; }
        public int NoOfEmailSMSReport{ get; set; }
        public int index { get; set; }
        public string searchfield { get; set; }
    }
}