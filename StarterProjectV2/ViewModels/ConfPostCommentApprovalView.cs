﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class ConfPostCommentApprovalView
    {
        public bool PostApproval { get; set; }
        public bool CommentApproval { get; set; }
        public string ModifiedAt{ get; set; }
        public string TaskRoles { get; set; }


    }
}