﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class TickerView
    {
        public string TickerId { get; set; }
        public string TickerType { get; set; }
      
        public string Title { get; set; }
        public string Details { get; set; }
    
        public string Link { get; set; }
        public HttpPostedFileBase PDFFile { get; set; }
        public Boolean IsEdited { get; set; }
        public bool IsNoticeFileExists { get; set; }
        public List<string> TickerTypeList { get; set; }
        public string TaskRoles { get; set; }
        public string TickerList { get; set; }
    }
}