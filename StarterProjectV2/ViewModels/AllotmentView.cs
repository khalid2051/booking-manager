﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class AllotmentView
    {
        public int ConfAllotmentID { get; set; }
        public string SlotId { get; set; }
        public int MonthlyAllotment { get; set; }
        public int YearlyAllotment { get; set; }
        public DateTime CreatedAt { get; set; }
        public decimal NoOfSlot { get; set; }
        

        public Boolean IsEdited { get; set; }

        public string TaskRoles { get; set; }
        public string AllotmentList { get; set; }
    }
}