﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;
namespace StarterProjectV2.ViewModels
{
    public class InventoryView
    {
        public int ConfInventoryRoomID { get; set; }
        public int InventoryCount { get; set; } //36
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public System.DateTime CreatedAt { get; set; }  //WHen the rule was created
        public Boolean IsArchived { get; set; }

        public Boolean IsEdited { get; set; }

        public string TaskRoles { get; set; }
        public string InventoryList { get; set; }
    }
}