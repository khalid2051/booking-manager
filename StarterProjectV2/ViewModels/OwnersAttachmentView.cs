﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnersAttachmentView
    {
        public string AttachmentId { get; set; }
        public string OwnerId { get; set; }
        public string OwnerNID { get; set; }
        public string OwnerTIN { get; set; }
        public string OwnerPassport { get; set; }
        public string OwnerUtilityBill { get; set; }
        public string OwnerDrivingLicense { get; set; }
        public string SlotId { get; set; }

        public HttpPostedFileBase NIDFile { get; set; }
        public HttpPostedFileBase TINFile { get; set; }
        public HttpPostedFileBase PassportFile { get; set; }
        public HttpPostedFileBase UtilityBillFile { get; set; }
        public HttpPostedFileBase DrivingLicenseFile { get; set; }

    }
}