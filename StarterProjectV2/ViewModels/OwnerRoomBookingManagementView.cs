﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class OwnerRoomBookingManagementView
    {
        public OwnerRoomBookingManagementView()
        {
            OwnerRoomBookingListCurrent = new List<OwnerRoomBooking>();
            OwnerRoomBookingListHistory = new List<OwnerRoomBooking>();
            OwnerRoomBookingListFuture = new List<OwnerRoomBooking>();
            OwnerRoomBookingListCancelled = new List<OwnerRoomBooking>();

            PendingBookingNoBookings = new List<OwnerRoomBooking>();
            PendingCheckInBookings = new List<OwnerRoomBooking>();
            PendingCheckOutBookings = new List<OwnerRoomBooking>();
        }

        public List<OwnerRoomBooking> PendingBookingNoBookings { get; set; }
        public List<OwnerRoomBooking> PendingCheckInBookings { get; set; }
        public List<OwnerRoomBooking> PendingCheckOutBookings { get; set; }

        public List<OwnerRoomBooking> OwnerRoomBookingListCurrent { get; set; } //List of the Ongoing Non - Cancellable Owner Room Bookings
        public List<OwnerRoomBooking> OwnerRoomBookingListHistory { get; set; } //List of the Completed Owner Room Bookings(History)
        public List<OwnerRoomBooking> OwnerRoomBookingListFuture { get; set; } //List of the Future and Cancellable Bookings
        public List<OwnerRoomBooking> OwnerRoomBookingListCancelled { get; set; } //List of the Cancelled Bookings
        public string TaskRoles { get; set; }
        public int NoOfBooking { get; set; }
        public int index { get; set; }
        public string BookingToken { get; set; }
        public string OwnerRoomBookngId { get; set; }
        public string HasStayedOrNot { get; set; }
        public string FolioNo { get; set; }


        public DateTime CheckOutDate { get; set; }
        public string HasCheckedOutOrNot { get; set; }
        public double TotalExpense { get; set; } = 0.00;

        public double ExtraCharges { get; set; } = 0.00;
        public string SearchField { get; set; }
        public string BookingComplete { get; set; }
        public string Type { get; set; }
        public string TokenAssigned { get; set; }
        public string CheckedIn { get; set; }
        public string CheckedOut { get; set; }
        public string Date { get; set; }
    }
}