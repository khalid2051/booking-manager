﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class DashboardNoticeView
    {
        public string NoticeId { get; set; }
        public string NoticeType { get; set; }
        public DateTime Date { get; set; }
        public string Day { get; set; }
        public string PDFFilePath { get; set; }
        public HttpPostedFileBase PDFFile { get; set; }
        public Boolean IsEdited { get; set; }
        public bool IsNoticeFileExists { get; set; }
        public List<string> NoticeTypeList { get; set; }
        public string FileDisplayName { get; set; }
    }
}