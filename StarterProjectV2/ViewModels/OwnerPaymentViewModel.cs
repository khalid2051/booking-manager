﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerPaymentViewModel
    {
        public int PaymentID { get; set; }

        public string SlotId { get; set; }
      
        public decimal Amount { get; set; }

        public string Remarks { get; set; }

        public string PaymentDate { get; set; }

        public string ReceiverName { get; set; }

        public string ReceivingBank { get; set; }

        public string FiscalPeriod { get; set; }

        public string FromMonth { get; set; }

        public string ToMonth { get; set; }

    }
}