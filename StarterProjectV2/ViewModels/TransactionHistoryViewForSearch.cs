﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class TransactionHistoryViewForSearch
    {
        public string SlotID { get; set; }
        public string SaleDate { get; set; }
        public string SalesValue { get; set; }
        public string TransactionID { get; set; }
    }
}