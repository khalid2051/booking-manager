﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class FinancialYearViewModel
    {
        public List<FinancialYear> FinancialYearList { get; set; }
        public string TaskRoles { get; set; }
    }
}