﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class PartialOwnerViewModelForEmail
    {
        public string EmailText { get; set; }
        public List<string> OwnerIdList { get; set; }
    }
}