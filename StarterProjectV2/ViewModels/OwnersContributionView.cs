﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnersContributionView
    {
        public int ConfOwnersContributionID { get; set; }

        public double Amount { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public System.DateTime CreatedAt { get; set; }

        public Boolean IsEdited { get; set; }

        public string TaskRoles { get; set; }
        public string OwnersContributionList { get; set; }
    }
}