﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class OwnerChatView
    {
        public List<Chat> Chats { get; set; }
        public string LastMessageTime { get; set; }
        public string CurrentMessage { get; set; }
        public string SlotId { get; set; }
        public int ID { get; set; }
    }
}
