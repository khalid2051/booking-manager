﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class RoomSlotView
    {

        //public RoomInfo RoomSelected { get; set; }
        //public SlotInfo SlotSelected { get; set; }

        //public List<RoomInfo> RoomList { get; set; }
        //public List<SlotInfo> SlotList { get; set; }

        public string RoomSelected { get; set; }
        public string SlotSelected { get; set; }

        public List<RoomInfo> RoomList { get; set; }
        public List<SlotInfo> SlotList { get; set; }
    }
}