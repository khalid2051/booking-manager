﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class OwnerLedgerViewModel
    {
        public OwnerLedgerViewModel()
        {
            OwnerBasicInfo = new OwnerBasicInfo();
            LedgerList = new List<LedgerModel>();
            LedgerSummaryList = new List<LedgerSummaryModel>();
        }
        public OwnerBasicInfo OwnerBasicInfo { get; set; }
        public List<LedgerModel> LedgerList { get; set; }
        public List<LedgerSummaryModel> LedgerSummaryList { get; set; }

    }

    public class LedgerModel
    {
        public string SlotId { get; set; }
        public string BookingId { get; set; }
        public string BookingToken { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int Nights { get; set; }
        public int NoofroomRequested { get; set; }
        public string InvoiceNo { get; set; }
        public decimal TotalCharge { get; set; }
        public decimal OwnerContribution { get; set; }
        public decimal HotelContribution { get; set; }

    }

    public class LedgerSummaryModel
    {
        public string FinancialYearId { get; set; }
        public string Year { get; set; }
        public string SlotId { get; set; }
        public int Nights { get; set; }
        public int Rooms { get; set; }
        public decimal TotalCharge { get; set; }
        public decimal OwnerContribution { get; set; }
        public decimal HotelContribution { get; set; }
    }

}