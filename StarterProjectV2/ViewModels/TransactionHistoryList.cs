﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class TransactionHistoryList
    {
        public List<OwnerTransactionHistory> TransactionHistories { get; set; }
        public int NoOfTransactionHistories { get; set; }
        public int index { get; set; }
        public string TaskRoles { get; set; }
        public string SearchField { get; set; }
    }
}