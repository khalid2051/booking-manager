﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class FolioDetail
    {
        public OwnerRoomBooking Booking { get; set; }
        public string[] ARR { get; set; }
        public int TotalCost { get; set; }
        public int NoOfDaysSpent { get; set; }
    }
}
