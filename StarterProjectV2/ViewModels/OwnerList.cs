﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class OwnerList
    {
        public OwnerList()
        {
            Owners = new List<OwnersInfo>();
        }
        public IEnumerable<OwnersInfo> Owners;
        public string TaskRoles;
        public int NoOfOwner;
        public int index;
        public string SearchField { get; set; }
    }
}