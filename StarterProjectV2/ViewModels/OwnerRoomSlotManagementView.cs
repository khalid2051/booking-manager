﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class OwnerRoomSlotManagementView
    {
        public List<OwnerRoomSlotAssignment> OwnerRoomSlotList { get; set; }
        public List<RoomInfo> Rooms { get; set; }
        public List<SlotInfo> Slots { get; set; }
        public List<OwnersInfo> Owners { get; set; }
        public string TaskRoles { get; set; }
        public string RoomId { get; set; }
        public string SlotId { get; set; }
        public float Rate { get; set; }
        public string OwnerId { get; set; }
    }
}