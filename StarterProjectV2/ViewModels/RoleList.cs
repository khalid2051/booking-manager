﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class RoleList
    {
        public IEnumerable<UserRole> Users { get; set; }
        public string TaskRoles { get; set; }
        public string flg { get; set; }
        public IEnumerable<UserRole> userDetailsList { get; set; }

        public int noOfPage { get; set; }

        public int index { get; set; }
    }
}