﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class OwnersAdditionalInfoEditView
    {
        public OwnersAttachment OwnersAttachment { get; set; }
        public List<OwnersNominee> OwnersNomineeList { get; set; }
        public List<OwnersFamily> OwnersFamilyList { get; set; }
        public List<OwnersProject> OwnersProjectList { get; set; }
        public OwnerRoomSlotAssignment OwnerRoomSlotAssignment { get; set; }




        //By Imran for determining whether the files exists or not
        public bool IsNIDFileExists { get; set; }
        public bool IsPassportFileExists { get; set; }
        public bool IsUtilityBillFileExists { get; set; }
        public bool IsTINFileExists { get; set; }
        public bool IsDrivingLicenseFileExists { get; set; }


        public List<bool> IsNomineePictureExists { get; set; }
        public List<bool> IsNomineeNidFileExists { get; set; }


        public List<bool> IsChildPictureExists { get; set; }

        
    }
}