﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class SelectedDividendViewModel
    {
        public List<string> DividendIdList { get; set; }
    }
}