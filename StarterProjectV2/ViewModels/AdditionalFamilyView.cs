﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class AdditionalFamilyView
    {
        //public OwnerFamilyView OwnersFamilies;
        public string OwnersFamilyId { get; set; }

        public string OFOwnerId { get; set; }
        public string OFSpouseName { get; set; }
        public string OFSpouseMobile { get; set; }
        public string OFSpouseEmail { get; set; }
        public string OFSpouseDOB { get; set; }
        public string OFSpouseNID { get; set; }
        public string OFSpousePicture { get; set; }
        public string OFChildName { get; set; }
        public string OFChildPicture { get; set; }
        public string OFChildDOB { get; set; }
        //public string SpouseName { get; set; }
        //public string SpouseMobile { get; set; }
        //public string SpouseEmail { get; set; }
        //public string SpouseDOB { get; set; }
        //public string SpouseNID { get; set; }
        //public string SpousePicture { get; set; }
        //public List<string> ChildNameArray { get; set; }
        //public List<string> ChildDOBArray { get; set; }
        //public List<string> ChildPictureArray { get; set; }
        //public string SpouseNIDPath { get; set; }
        //public string SlotID { get; set; }


        public bool IsFamilyPresent { get; set; }
        public bool IsNomineePresent { get; set; }
        public bool IsProjectPresent { get; set; }
        public bool IsAttachmentPresent { get; set; }
    }
}