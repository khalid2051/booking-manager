﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class PostList
    {
        public IEnumerable<Post> Posts;
        public IEnumerable<string> OwnersName;
        public string TaskRoles;
        public int NoOfPosts;
        public int index;
        public string SearchField;
    }
}