﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class CommentList
    {
        public IEnumerable<Comment> Comments;
        public IEnumerable<string> OwnersName;
        public string TaskRoles;
        public int NoOfComments;
        public int index;
        public string SearchField;
    }
}