﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnersInfoOTPSMS
    {
        public string OwnerId { get; set; }
        public string InputSMSCode { get; set; }
    }
}