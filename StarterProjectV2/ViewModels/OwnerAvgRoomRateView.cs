﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerAvgRoomRateView
    {
        public int ConfOwnerAvgRoomRateID { get; set; }
        
        public int AverageRoomRate { get; set; }

        public DateTime ApplicableDate { get; set; }
      
        public Boolean IsEdited { get; set; }
   
        public string TaskRoles { get; set; }
        
        public string AverageRoomRateList { get; set; }
    }
}