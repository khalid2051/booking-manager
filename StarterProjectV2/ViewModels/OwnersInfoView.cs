﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnersInfoView
    {
        public OwnersInfoView()
        {
            SlotIdList = new List<string>();
            LedgerViewModel = new OwnerLedgerViewModel();
        }

        public OwnerLedgerViewModel LedgerViewModel { get; set; }

        [DisplayName("Owner Id")]
        public string OwenerId { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Father's Name")]
        public string FatherName { get; set; }

        [DisplayName("Mother's Name")]
        public string MotherName { get; set; }

        [DisplayName("Date Of Birth")]
        public string DateOfBirth { get; set; }

        [DisplayName("Spouse Name")]
        public string SpouseName { get; set; }

        [DisplayName("Marriage Date")]
        public string MarriageDate { get; set; }

        [DisplayName("Present Address")]
        public string PresentAddress { get; set; }

        [DisplayName("Permanant Address")]
        public string PermanantAddress { get; set; }

        [DisplayName("NID")]
        public string NID { get; set; }

        [DisplayName("TIN")]
        public string TIN { get; set; }

        [DisplayName("Email Address")]
        public string Email { get; set; }

        [DisplayName("Mobile Number")]
        public string Mobile { get; set; }

        [DisplayName("SMS Validated")]
        public string isSMSValidated { get; set; }
        [DisplayName("Email Validated")]
        public string isEmailValidated { get; set; }
        [DisplayName("Picture")]

        public string Id { get; set; }

        public string SlotId { get; set; }
        public string NoofSlots { get; set; }
        public string AdditionalId { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string AdminComment { get; set; }
        public string BranchName { get; set; }
        public string Remarks { get; set; }
        public string AdditionalInformation { get; set; }
        public string UnknownColumn { get; set; }
        public string NIDFilePath { get; set; }
        public string TINFilePath { get; set; }
        //Added for the Picture Upload
        public string PicturePath { get; set; }
        public HttpPostedFileBase OwnerProfilePicture { get; set; }

        public string OwnerSmartId { get; set; }
        public Boolean IsEdited { get; set; }
        public bool IsProfilePictureExists { get; set; }
        public bool IsNIDFileExists { get; set; }
        public bool IsTINFileExists { get; set; }
        public string IsEditApproved { get; set; }
        public List<string> IsEditApprovedList { get; set; }
        public bool IsDataPresent { get; set; }
        public string SalesDate { get; set; }
        public List<string> SlotIdList { get; set; }
        public string FathersName { get; set; }
        public DateTime OwnersLastEditTime { get; set; }
        public string RoutingNo { get; set; }
    }
}