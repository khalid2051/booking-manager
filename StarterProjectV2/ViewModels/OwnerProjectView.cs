﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerProjectView
    {
        public string ProjectName { get; set; }

        public string NumberOfRoom { get; set; }

        public string NumberOfDays { get; set; }

        public string PurchaseDate { get; set; }

        public string SalesAmount { get; set; }

        public string PaidAmount { get; set; }

        public List<string> PeriodArray { get; set; }
        public List<string> DateArray { get; set; }
        public List<string> AmountArray { get; set; }

        public string OwnerId { get; set; }
    }
}