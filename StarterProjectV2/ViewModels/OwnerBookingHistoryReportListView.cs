﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerBookingHistoryReportListView
    {
        public IList<OwnerBookingHistoryReport> OwnerBookingHistoryReportList { get; set; }
        public string SlotId { get; set; }
    }
}