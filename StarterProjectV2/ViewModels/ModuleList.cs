﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class ModuleList
    {
        public IEnumerable<Module> modules { get; set; }
        public List<bool> delete { get; set; }
        public string TaskRoles { get; set; }
    }
}