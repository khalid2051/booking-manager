﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class DashboardNoticeManagementView
    {
        public List<DashboardNotice> DashboardNoticeList { get; set; }
        public string TaskRoles { get; set; }
        public int noOfPage { get; set; }

        public int index { get; set; }
    }
}