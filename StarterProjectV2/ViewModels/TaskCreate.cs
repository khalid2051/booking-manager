﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class TaskCreate
    {
        public string TaskId { get; set; }
        public string TaskName { get; set; }
        public int TaskOrder { get; set; }
        public string TaskController { get; set; }
        public string TaskModule { get; set; }
        public bool isTaskViewed { get; set; }
        public bool isTaskAdded { get; set; }
        public bool isTaskSaved { get; set; }
        public bool isTaskEdited { get; set; }
        public bool isTaskDeleted { get; set; }
        public bool isTaskPrinted { get; set; }
        public bool isTaskCancelled { get; set; }
        public bool isTaskReseted { get; set; }
        public bool isTaskFound { get; set; }
        public string selectedTaskId { get; set; }
        public string selectedModuleId { get; set; }
        public IEnumerable<Module> ModuleIdandParent { get; set; }
        public IEnumerable<Task> TaskIdandParent { get; set; }
        public string getTaskName { get; set; }
        public string getModuleName { get; set; }
    }
}