﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class NotificationViewModel
    {
        public List<Notification> NotificationList { get; set; }
        public List<string> NotificationHeaders { get; set; }

        public int NoOfNotification { get; set; }
        public int index { get; set; }
        public string searchfield { get; set; }
    }
}