﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;


namespace StarterProjectV2.ViewModels
{
    public class CalendarViewModel
    {
        public string[,] calendarMatrix;
        public string[,] calendarMatrixColor;
        public string[,] calendarMatrixBookingId;
        public List<string> colorlist1;
        public List<string> colorlist2;
        int point1 = 0, point2 = 0;
        public int Offset;
        public void GenerateColor()
        {
            colorlist1 = new List<string>();
            colorlist2 = new List<string>();
            string red = "#fa8c8c";
            string blue = "#99aef8";
            string green = "#aef876";
            string yellow = "#e7f72f";
            string pink = "#f263cf";
            string orange = "#fbbe6d";
            colorlist2.Add(red);
            colorlist2.Add(blue);
            colorlist2.Add(pink);

            colorlist1.Add(yellow);
            colorlist1.Add(green);
            colorlist1.Add(orange);
        }
        public string Next(int col)
        {
            if ((col & 1) == 0)
            {
                point1++;
                if (point1 == colorlist1.Count) point1 = 0;
                return colorlist1[point1];
            }
            else
            {
                point2++;
                if (point2 == colorlist2.Count) point2 = 0;
                return colorlist2[point2];
            }
        }
        public void setMatrix(int row, int col, string slotid, int days, int rooms, string bookingId)
        {
            string color ;
            if(col == 0) { color = "red";}
            else color = Next(col);
            for (int k = 0; k < days; k++)
            {
                if (col + k > 15) break;
                for (int k2 = 0; k2 < rooms; k2++)
                {
                    //CalendarViewmodel.setMatrix(j + k2, i + k, slotid, false);

                    calendarMatrix[row + k2, col + k] = slotid;
                    calendarMatrixColor[row + k2, col + k] = color;
                    calendarMatrixBookingId[row + k2, col + k] = bookingId;
                }
            }
            
            return;
        }
    }
}
