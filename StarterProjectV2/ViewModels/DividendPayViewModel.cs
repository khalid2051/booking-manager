﻿using System.Collections;
using System.Collections.Generic;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class DividendPayViewModel
    {
        public IEnumerable<Dividend> dividends { get; set; }
        public double totalSqFeet { get; set; }
        public string fromMonth { get; set; }
        public string toMonth { get; set; }
        public int divAmount { get; set; }
        public double divAmountPerSq { get; set; }
        public HttpPostedFileBase file { get; set; }
        public bool flg { get; set; }
        public int totalRows { get; set; }
        public int updatedRows { get; set; }
        public int index { get; set; }
        //public int NoOfDividends { get; set; }
        public int DeductionRateforStaying { get; set; }
        public List<FinancialYear> FinancialIdandParent { get; set; }
        public string selectFinancialId { get; set; }
        public string TaskRoles { get; set; }
        public int noOfPage { get; set; }
        public double totalHotelSize { get; set; }
    }
}