﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class EmailSMSReportOptionView
    {
        public string Month { get; set; }
        public DateTime MonthDateTime { get; set; }
        public string Group { get; set; }
        public string NotificationStatus { get; set; }
        public List<string> GroupList { get; set; }
        public List<string> NotificationStatusList { get; set; }
        public DateTime FirstDate { get; set; }
        public DateTime LastDate { get; set; }
    }
}