﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class OwnersPaymentList
    {
        public List<OwnersPayment> OwnerPayments { get; set; }
        public int NoOfOwnerPayments { get; set; }
        public int index { get; set; }
        public string TaskRoles { get; set; }
        public string SearchField { get; set; }
    }
}