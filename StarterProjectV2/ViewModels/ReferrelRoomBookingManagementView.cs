﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class ReferrelRoomBookingManagementView
    {
        public List<ReferralBooking> OwnerRoomBookingListCurrent { get; set; } //List of the Ongoing Non - Cancellable Owner Room Bookings
        public List<ReferralBooking> OwnerRoomBookingListHistory { get; set; } //List of the Completed Owner Room Bookings(History)
        public List<ReferralBooking> OwnerRoomBookingListFuture { get; set; } //List of the Future and Cancellable Bookings
        public string TaskRoles { get; set; }
        public string SlotId { get; set; }
        public int NoOfBooking { get; set; }
        public int index { get; set; }
        public string BookingToken { get; set; }
        public string ReferralBookingId { get; set; }
        public string HasStayedOrNot { get; set; }
        public string FolioNo { get; set; }
        public double TotalExpense { get; set; }
        public string IsBookingCompleted { get; set; }
        public string HasCheckedOutOrNot { get; set; }
        public System.DateTime CheckOutDate { get; set; }
        public double ExtraCharges { get; set; }
        public double ReferralPoint { get; set; }
        public string SearchField { get; set; }
        public string ViewType { get; set; }
        public List<string> ViewTypes { get; set; }
        public bool IsReferralPointApproved { get; set; }
        public System.DateTime CheckInDate { get; set; }
        public List<bool> IsReferralPointApprovedList { get; set; }

    }
}
