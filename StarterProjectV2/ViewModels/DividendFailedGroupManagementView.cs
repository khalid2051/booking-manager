﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class DividendFailedGroupManagementView
    {
        public List<DividendFailedGroup> DividendFailedGroupList { get; set; }
        public string TaskRoles { get; set; }
        //public int NoOfDividendFailedGroup { get; set; }
        //public int index { get; set; }
    }
}