﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class AccommodationManagementViewModel
    {
        public string AccommodationDate { get; set; }
        public List<string> RoomInfoLists { get; set; }
        public List<string> RoomCriteriaLists { get; set; }
        public double TotalSquareFeet { get; set; }
    }
}