﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class UserList
    {
        public UserList()
        {
            UserDetailsList = new List<User>();
        }
        public IEnumerable<User> Users { get; set; }
        public string TaskRoles { get; set; }
        public string flg { get; set; }
        public IEnumerable<User> UserDetailsList { get; set; }
       
        public int noOfPage { get; set; }

        public int index { get; set; }
    }
}