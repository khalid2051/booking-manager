﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace StarterProjectV2.ViewModels
{
    public class RoleDetailsView
    {
        public string RoleName { get; set; }
        public List<string> RoleList { get; set; }
        public List<Tuple<Module, List<Task>>> GetAllModuleListWithTasks { get; set; }
        //public List<Tuple<string, List<Tuple<string,string>>>> GetSelectedModuleListWithTasks { get; set; }
        public List<PartialUserDetailsViewModel> PartialUserDetailsViewModels { get; set; }

        //public IEnumerable<string> dummy { get; set; }
        public List<Tuple<string, string>> TaskWithRoles { get; set; }
        public IEnumerable<Module> ModuleIdandParent { get; set; }
        public string selectedModuleId { get; set; }
    }
}