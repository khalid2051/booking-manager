﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class SlotManagementView
    {
        public List<SlotInfo> SlotList { get; set; }
        public string TaskRoles { get; set; }
    }
}