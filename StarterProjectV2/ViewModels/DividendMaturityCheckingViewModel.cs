﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class DividendMaturityCheckingViewModel
    {
        public string dividendMaturityMonth { get; set; }

        public string finId { get; set; }

        public string FixedToMonth { get; set; }

        public string FixedFromMonth { get; set; }

        public string isAlreadyExists { get; set; }
    }
}