﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class DividendViewModelForSearch
    {
        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
        public string DateOfBirth { get; set; }
        public string MobileNumber { get; set; }
        public decimal DividendAmount { get; set; }
        public string DividendId { get; set; }
        public string status { get; set; }
        public int notification { get; set; }
        public string FromMonth { get; set; }
        public string ToMonth { get; set; }
        public string SlotId { get; set; }
        public bool IsSelected { get; set; }
        public int DeductionRateforStaying { get; set; }
        public int NoofDaysHasStayed { get; set; }
    }
}