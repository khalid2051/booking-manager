﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class BookingHistoryByFinancialYearView
    {
        public List<int> NoOfBookings { get; set; }
        public string Id { get; set; }
        public List<List<OwnerRoomBooking>> OwnerRoomBookingHistory { get; set; }
        public List<string> FinancialYearNameList { get; set; }
        public List<string> FinancialYearIDList { get; set; }
    }
}