﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class DividendSelectionViewModel
    {
        public string Id { get; set; }
        public string IsChecked { get; set; }
        public List<string> Ids { get; set; }
    }
}