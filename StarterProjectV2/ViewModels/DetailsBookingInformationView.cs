﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class DetailsBookingInformationView
    {
        public List<OwnerRoomBooking> SelectedRoomBookingList { get; set; }
        public string FinancialYear { get; set; }
    }
}