﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerChatManagementView
    {
        public List<OwnerChatManagementViewItem> OwnerChatManagementViewItems { get; set; }
        public int noOfPage { get; set; }
        public string TaskRoles { get; set; }
        public int index { get; set; }
        public string SearchField { get; set; }
    }
}