﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class AccommodationmanagementTableViewModel
    {
        public List<AccommodationInfo> AccommodationInfos = new List<AccommodationInfo>();
        public string TaskRoles { get; set; }
    }
}