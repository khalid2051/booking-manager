﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerFamilyView
    {
        public string OwnerId { get; set; }

        [DisplayName("Spouse's Name")]
        public string SpouseName { get; set; }

        [DisplayName("Spouse's Mobile Number")]
        public string SpouseMobile { get; set; }
        
        [DisplayName("Spouse's Email")]
        public string SpouseEmail { get; set; }
        [DisplayName("Spouse's Date Of Birth")]
        public string SpouseDOB { get; set; }
        
        [DisplayName("Spouse's NID")]
        public string SpouseNID { get; set; }
        
        [DisplayName("Spouse's Picture")]
        public string SpousePicture { get; set; }

        public string SpouseNIDPath { get; set; }

        public HttpPostedFileBase SpousePictureFile { get; set; }

        public HttpPostedFileBase SpouseNIDPictureFile { get; set; }
    }

    public class OwnerFamilyDetailView
    {
        public string OwnerId { get; set; }

        public string ChildName { get; set; }
        
        public string ChildDOB { get; set; }
        
        public string ChildPicture { get; set; }

        public HttpPostedFileBase ChildPictureFile { get; set; }

    }
}