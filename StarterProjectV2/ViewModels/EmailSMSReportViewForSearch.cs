﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class EmailSMSReportViewForSearch
    {
        public string IsSMSSent { get; set; } //Can be Sent, Not Sent Due to Incorrect/Null Mobile No, Not Sent due to SMS Api failure, 
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string SlotId { get; set; }
        public string TimeOfDelivery { get; set; }
        public string NotificationType { get; set; }
        public string EmailandSMSReportId { get; set; }
        public string Text { get; set; }
        public string NotificationSerialNo { get; set; }
    }
}