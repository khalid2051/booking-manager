﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class ReferralRoomBookingManagementView
    {
        public List<ReferralBooking> ReferralBookingListCurrent { get; set; } //List of the Ongoing Non - Cancellable Owner Room Bookings
        public List<ReferralBooking> ReferralBookingListHistory { get; set; } //List of the Completed Owner Room Bookings(History)
        public List<ReferralBooking> ReferralBookingListFuture { get; set; } //List of the Future and Cancellable Bookings
        public List<ReferralBooking> ReferralBookingListCancelled { get; set; } //List of the Cancelled Bookings
        public string TaskRoles { get; set; }
        public int NoOfBooking { get; set; }
        public int index { get; set; }
        public string BookingToken { get; set; }
        public string ReferralBookingId { get; set; }
        public string HasStayedOrNot { get; set; }
        public string FolioNo { get; set; }
        public System.DateTime CheckOutDate { get; set; }
        public string HasCheckedOutOrNot { get; set; }
        public double TotalExpense { get; set; } = 0.00;
        public bool IsReferralPointApproved { get; set; }
        public double ExtraCharges { get; set; } = 0.00;
        public string SearchField { get; set; }
        public double ReferralPoint { get; set; }
    }
}