﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class PostViewModel
    {
        public string PostId { get; set; }
        public string PostedBy { get; set; }
        public string PostText { get; set; }
        public DateTime Time { get; set; }
        public int NoOfLikes { get; set; }
        public bool Approval { get; set; }
        public string PostHeader { get; set; }
        public string OwnersName { get; set; }
        public bool FlagLike { get; set; }
        public DateTime ApproveTime { get; set; }
        public List<CommentViewFromAdminSite> Comments { get; set; }
        public int NoOfComments { get; set; }
    }
}