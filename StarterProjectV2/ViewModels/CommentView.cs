﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class CommentViewFromAdminSite
    {
        public Comment Comment { get; set; }
        public string CommentorName { get; set; }
    }
}