﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class CommentViewModel
    {
        public string PostedBy { get; set; }
        public string PostText { get; set; }
        public string PostHeader { get; set; }
        public string CommentId { get; set; }
        public string PostId { get; set; }
        public string CommentorId { get; set; }
        public string OwnersName { get; set; }
        public string CommentText { get; set; }
        public bool Approval { get; set; }
        public DateTime CommentTime { get; set; }
        public string PosterName { get; set; }
    }
}
