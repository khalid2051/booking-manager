﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class PartialUserDetailsViewModel
    {
        public string ModuleId { get; set; }
        public List<string> TaskIdList { get; set; }
        public List<string> TaskEventList { get; set; }
    }
}