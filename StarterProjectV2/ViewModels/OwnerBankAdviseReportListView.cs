﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class OwnerBankAdviseReportListView
    {
        public IList<OwnerBankAdviseReport> OwnerBankAdviseReportList { get; set; }
    }
}