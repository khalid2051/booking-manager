﻿using StarterProjectV2.Models;
using System.Collections.Generic;
using System.ComponentModel;

namespace StarterProjectV2.ViewModels
{
    public class OwnersInfoFamilyNomineeAttFilesView
    {
        public OwnersInfoFamilyNomineeAttFilesView()
        {
            TransactionHistory = new OwnerTransactionHistory();
            AttachmentView = new OwnersAttachmentView();
            OwnersFamily = new OwnerFamilyView();
            OwnersFamilyList = new List<OwnerFamilyDetailView>();
            OwnersNomineeList = new List<OwnerNomineeViewModel>();
        }

        public OwnerTransactionHistory TransactionHistory { get; set; }

        public OwnersAttachmentView AttachmentView { get; set; }

        public OwnerFamilyView OwnersFamily { get; set; }

        public List<OwnerFamilyDetailView> OwnersFamilyList { get; set; }

        public List<OwnerNomineeViewModel> OwnersNomineeList { get; set; }

        public string OwenerId { get; set; }

        public string OFOwnerId { get; set; }

        [DisplayName("Owner Id")]
        public string ONOwnerId { get; set; }

        [DisplayName("Owner Id")]
        public string OAOwnerId { get; set; }



        public string OPProjectName { get; set; }

        public string OPNumberOfRoom { get; set; }

        public string OPNumberOfDays { get; set; }

        public string OPPurchaseDate { get; set; }

        public string OPSalesAmount { get; set; }

        public string OPPaidAmount { get; set; }

        public string OPPeriodArray { get; set; }
        public string OPDateArray { get; set; }
        public string OPAmountArray { get; set; }

        public string OPOwnerId { get; set; }








        public string ORSAOwnerId { get; set; }

        public string RoomId { get; set; }
        public string SlotId { get; set; }
        public string RoomSlotId { get; set; }
        public string RoomSlotText { get; set; }
        public List<RoomInfo> Rooms { get; set; }
        public List<SlotInfo> Slots { get; set; }

        public float Rate { get; set; }

    }
}