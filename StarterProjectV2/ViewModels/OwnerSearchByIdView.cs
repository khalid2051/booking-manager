﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StarterProjectV2.Models;

namespace StarterProjectV2.ViewModels
{
    public class OwnerSearchByIdView
    {
        //public OwnersInfo OwnersInfo { get; set; }
        public int NoOfDaysCanStay { get; set; }
        public int NoOfDaysHasStayed { get; set; }
        public string SlotId { get; set; }
        public string Name { get; set; }
        public string PicturePath { get; set; }
        public string NID { get; set; }
        public string TIN { get; set; }
        public string FathersName { get; set; }
        public string DateOfBirth { get; set; }
        public string SalesDate { get; set; }


        public bool IsDataPresent { get; set; }
        public bool IsSearchResultFound { get; set; }
        public string Id { get; set; }
        public List<int> NoOfDaysCanStayList { get; set; }
        public List<int> NoOfDaysHasStayedList { get; set; }
        public List<string> FinancialYearNameList { get; set; }
        public List<string> FinancialYearIDList { get; set; }
    }
}