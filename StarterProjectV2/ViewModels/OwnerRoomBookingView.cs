﻿using System;
using System.Collections.Generic;

namespace StarterProjectV2.ViewModels
{
    public partial class OwnerRoomBookingView
    {
        public OwnerRoomBookingView()
        {
            RoomList = new List<string>();
            RoomTypeList = new List<string>();
            RelationEnumList = new List<string>();
        }

        public List<string> RoomList { get; set; }
        public string OwnerRoomBookingId { get; set; }
        public DateTime Startdate { get; set; }
        public DateTime Enddate { get; set; }
        public string Roomtype { get; set; }
        public string PreferredRoomNo { get; set; }
        public string OtherPreferences { get; set; }
        public string IsBookingCompleted { get; set; }
        public string OBOwnerId { get; set; }
        public bool IsError { get; set; }
        public string AllottedRoom { get; set; }
        public string Suggestion { get; set; }
        public string SlotId { get; set; }
        public List<string> RoomTypeList { get; set; }

        //added later
        public string GuestNameMain { get; set; }
        public string GuestNameTwo { get; set; }
        public string GuestNameThree { get; set; }
        public string GuestAddress { get; set; }
        public int NoOfAdult { get; set; }
        public int NoOfChild { get; set; }
        public string BookingNo { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string BookingToken { get; set; }
        public string GuestMobile { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public string HasStayedOrNot { get; set; }
        public int NoOfRoomRequested { get; set; }
        public int NoOfRoomAllotted { get; set; }
        public int NoOfDaySpent { get; set; }
        public int NoOfDaysCanStayMonthly { get; set; }
        public int NoOfDaysCanStay { get; set; }
        public string ApplicationDateString { get; set; }
        public string StartdateString { get; set; }
        public string EnddateString { get; set; }
        public bool IsRoomBookingTokenExists { get; set; }
        public string IsDayClosePageExists { get; set; }
        public double ExtraCharges { get; set; }
        public double OwnerContribution { get; set; }
        public double HotelContribution { get; set; }
        public double TotalExpense { get; set; }
        public DateTime CheckInDate { get; set; }

        public DateTime CheckOutDate { get; set; }
        public int MonthlyAllotment { get; set; }
        public int YearlyAllotment { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public int NoOfDaySpentMonthly { get; set; }
        public string ErrorMessage { get; set; }
        public bool isOwner { get; set; } = true;
        public string FolioNo { get; set; }
        public string HasCheckedOutOrNot { get; set; }
    }

    public partial class OwnerRoomBookingView
    {
        public string Relation { get; set; }
        public IEnumerable<string> RelationEnumList { get; set; }
    }
}