﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.ViewModels
{
    public class AccommodationInfoView
    {
        public string AccommodationInfoId { get; set; }

        public string AccommodationDate { get; set; }

        public string RoomNo { get; set; }
        public string NonPaymentReason { get; set; }
        public float RoomIncome { get; set; }
        public float RoomExpense { get; set; }

        public List<string> NonPaymentReasonList { get; set; }

    }
}