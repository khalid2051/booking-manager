﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2
{
    public static class SettingCode
    {
        public const string BookingRestrictionHours = "BookingRestrictionHours";
        public const string RemoteLocation = "RemoteLocation";
        public const string SendEmail = "SendEmail";
        public const string SendSMS = "SendSMS";
        
    }
}