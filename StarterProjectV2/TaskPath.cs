﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2
{
    public static class TaskPath
    {
        public const string Booking_OwnerRoomBookingManagement = "Booking\\OwnerRoomBookingManagement";
        public const string OwnersInfo_SinglePageOwnersInfoManagement = "OwnersInfo\\SinglePageOwnersInfoManagement";
        public const string UserDetails_FinancialYearManagement = "UserDetails\\FinancialYearManagement";
        public const string Ownersinfo_OwnerManagement = "Ownersinfo\\OwnerManagement";
        public const string OwnersInfo_DailyOperations = "OwnersInfo\\DailyOperations";
        public const string Configuration_AllotmentManagement = "Configuration\\AllotmentManagement";
        public const string Log_LogManagement = "Log\\LogManagement";
        public const string Configuration_OwnerAvgRoomRateManagement = "Configuration\\OwnerAvgRoomRateManagement";
        public const string TransactionHistory_TransactionHistoryManagement = "TransactionHistory\\TransactionHistoryManagement";
    }
}