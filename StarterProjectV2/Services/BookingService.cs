﻿using Newtonsoft.Json;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Services
{
    public class BookingService
    {
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();
        private readonly CommonService _commonService = new CommonService();
        private readonly OwnerInfoService _ownerInfoService = new OwnerInfoService();
        private readonly EnumDataService _enumDataService = new EnumDataService();
        private const string RelationshipEnumGroup = "Relationship";

        public OwnerLedgerViewModel GetBookingLedger(string slotId)
        {
            var ownerBasicInfo = _ownerInfoService.GetOwnerBasicInfo(slotId);

            var response = GenerateLedger(slotId);

            DataSet ds = GetLedgerData(slotId);

            string jsonBookingLedger = JsonConvert.SerializeObject(ds.Tables[0]);
            var ledgerList = JsonConvert.DeserializeObject<List<LedgerModel>>(jsonBookingLedger);

            string jsonBookingLedgerSummary = JsonConvert.SerializeObject(ds.Tables[1]);
            var ledgerSummaryList = JsonConvert.DeserializeObject<List<LedgerSummaryModel>>(jsonBookingLedgerSummary);

            OwnerLedgerViewModel model = new OwnerLedgerViewModel()
            {
                OwnerBasicInfo = ownerBasicInfo,
                LedgerList = ledgerList,
                LedgerSummaryList = ledgerSummaryList,
            };

            return model;
        }

        public ResponseModel GenerateLedger(string slotId)
        {
            ResponseModel model = new ResponseModel();

            string sqlText = @"
----DECLARE @slotId AS VARCHAR(100) ='c1001'

Delete LedgerDetails where slotid = @slotId

Insert Into LedgerDetails

SELECT ownerroombookngid bookingid,
       b.bookingtoken,
       CheckInDate,
       CheckOutDate,
	   DateDiff(day, CheckInDate, CheckOutDate) Nights,
       noofroomrequested,
	   SlotId,
       inv.invoiceno,
       Isnull(inv.totalcharge, 0)       totalcharge,
       Isnull(inv.ownerpayment, 0)      ownerContribution,
       Isnull(inv.hotelcontribution, 0) hotelcontribution
FROM   ownerroombookings b
       LEFT JOIN (SELECT inv.bookingid,
                         d.invoiceno,
                         Sum(CASE
                               WHEN Isnull(chargeamount, 0) > = 0 THEN Isnull(
                               chargeamount, 0)
                             END) TotalCharge,
                         Max(CASE
                               WHEN acc.accountcode = 'OwnerPayment' THEN
                               Isnull(paymentamount, 0)
                             END) OwnerPayment,
                         Max(CASE
                               WHEN acc.accountcode = 'HotelContribution' THEN
                               Isnull(paymentamount, 0)
                             END) HotelContribution
                  FROM   invoices inv
                         LEFT JOIN invoicedetails d
                                ON inv.invoiceno = d.invoiceno
                         LEFT JOIN accounts acc
                                ON acc.id = d.accountid
                  WHERE  inv.slotid = @slotId
                  GROUP  BY d.invoiceno,
                            inv.bookingid) AS inv
              ON b.ownerroombookngid = inv.bookingid
WHERE  slotid = @slotId
       AND ( 'Yes' = hasstayedornot )
       AND ( 'Yes' = hascheckedoutornot )
";

            var paramDictionary = new Dictionary<string, object>
            {
                { "slotId", slotId },
            };

            _commonService.ExecuteQuery(sqlText, paramDictionary);

            model.Status = Messages.StatusSuccess;
            model.Message = Messages.LedgerGeneratedSuccessfully;
            return model;
        }

        public DataSet GetLedgerData(string slotId)
        {
            string sqlText = @"
----DECLARE @slotId AS VARCHAR(100) ='C1323'

SELECT
  BookingId,
  BookingToken,
  CheckInDate,
  CheckOutDate,
  Nights,
  NoOfRoomRequested,
  SlotId,
  InvoiceNo,
  TotalCharge,
  OwnerContribution,
  HotelContribution
FROM LedgerDetails
WHERE slotid = @slotId
ORDER BY checkindate DESC


--------------------------------------------------SUMMARY--------------------------------------------------
----DECLARE @slotId AS varchar(100) = 'C1001'

SELECT
  FinancialYearId,
  Year,
  SlotId,
  SUM(Nights) Nights,
  SUM(Rooms) Rooms,
  SUM(totalcharge) totalcharge,
  SUM(ownerContribution) ownerContribution,
  SUM(hotelcontribution) hotelcontribution

FROM (
SELECT
  fy.FinId FinancialYearId,
  fy.Year,
  detail.SlotId,
  SUM(Nights) Nights,
  SUM(detail.noofroomrequested) Rooms,
  SUM(totalcharge) totalcharge,
  SUM(ownerContribution) ownerContribution,
  SUM(hotelcontribution) hotelcontribution
FROM FinancialYears fy
JOIN LedgerDetails AS detail
  ON detail.CheckInDate BETWEEN fy.FromMonth AND fy.ToMonth
WHERE SlotId = @slotId
GROUP BY fy.FinId,
         fy.Year,
         detail.SlotId

UNION

SELECT
  FinancialYearId,
  Year,
  SlotId,
  Nights,
  Rooms,
  totalcharge,
  ownerContribution,
  hotelcontribution
FROM OwnerStayArchive arc
WHERE SlotId = @slotId
) AS LedgerSummary

GROUP BY FinancialYearId,
         Year,
         SlotId
ORDER BY Year DESC
  
";

            var paramDictionary = new Dictionary<string, object>
            {
                { "slotId", slotId },
            };

            DataSet ds = _commonService.GetData(sqlText, paramDictionary);

            return ds;
        }

        public ResponseModel BookingInsert(OwnerRoomBooking booking, bool isOwnerSite = false)
        {
            ResponseModel response = new ResponseModel();

            var bookingId = "";
            var OwnerRoomBookingsList = _dbContext.OwnerRoomBookings;

            var lastBookingId = OwnerRoomBookingsList.OrderByDescending(x => x.Id).FirstOrDefault().OwnerRoomBookngId;

            bookingId = CommonHelper.GenerateID("ORB", lastBookingId);

            var allotment = _dbContext.ConfAllotments.FirstOrDefault(o => o.SlotId == booking.SlotId);

            if (allotment == null)
            {
                response.Message = "Allotment configuration is not set-up! Please contact with administrator!";
                return response;
            }

            if (booking.CheckInDate < DateTime.Today)
            {
                response.Message = "You cannot place a booking requested in previous date!";
                return response;
            }

            var allconfigrooms = _dbContext.ConfigInvRooms.ToList();
            var allBookingComplete = OwnerRoomBookingsList.Where(o => o.IsBookingCompleted == "Yes" || o.IsBookingCompleted == "No");
            var allBookedRoomsForOwner = allBookingComplete.Where(o => o.SlotId == booking.SlotId);


            //Are there enough rooms on all days requested.
            bool WindowClear = true;

            for (DateTime date = booking.CheckInDate; date < booking.CheckOutDate; date = date.AddDays(1))
            {
                var configRoomForDate = allconfigrooms.LastOrDefault(c => c.FromDate <= date && date < c.ToDate);
                if (configRoomForDate == null) continue;
                int inventoryCount = configRoomForDate.InventoryCount; //36
                                                                       //Calculate the Already Booked no of rooms for this date
                int bookedRoomForDate = allBookingComplete.Where(c => c.CheckInDate <= date && date < c.CheckOutDate).Sum(row => (int?)row.NoOfRoomRequested) ?? 0;

                if (inventoryCount < bookedRoomForDate + booking.NoOfRoomRequested)
                {
                    //V1: cannot allocate due to room unavailibility in hotel
                    response.Message = "There are not enough rooms available for your booking request!\n";
                    return response;

                }
            }

            GetMISCBookingInfo(booking, allBookedRoomsForOwner, out int monthlySpent, out int yearlySpent, out int asked, out int startMonth, out int endMonth, out int monthlySpent2, out int asked2);

            if (allotment.MonthlyAllotment < monthlySpent + asked || allotment.YearlyAllotment < yearlySpent + asked || (startMonth != endMonth && allotment.MonthlyAllotment < monthlySpent2 + asked2))
            {
                //cannot allocate due to allotment issue.
                //exact case can be found via 3 cases from condition.
                string error = "Error\n";
                string[] months = new string[] { "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                if (allotment.MonthlyAllotment < monthlySpent + asked) { error = "Not enough days allowed in the month " + months[startMonth]; }
                else if (allotment.YearlyAllotment < yearlySpent + asked) { error = "Not enough days allowed in the year"; }
                else if (startMonth != endMonth && allotment.MonthlyAllotment < monthlySpent2 + asked2) { error = "Not enough days allowed in the month " + months[endMonth]; }

                response.Message = error;

                return response;
            }


            if (booking.CheckInDate >= booking.CheckOutDate)
            {
                var bookingModel = new OwnerRoomBookingView()
                {
                    IsError = true,
                    RoomList = _dbContext.RoomInfos.Select(r => r.RoomNo).ToList(),
                    SlotId = booking.SlotId,
                    OBOwnerId = booking.OBOwnerId,
                    IsBookingCompleted = "No",
                    RoomTypeList = _dbContext.RoomInfos.Select(r => r.RoomType).Distinct().ToList(),
                };

                response.Message = "CheckOut Date must be greater than CheckIn Date!";
                return response;
            }


            string bookingCompleted;
            if (WindowClear) { bookingCompleted = "Yes"; }
            else { bookingCompleted = "No"; }

            OwnerRoomBooking model = new OwnerRoomBooking()
            {
                OwnerRoomBookngId = bookingId,
                Startdate = booking.Startdate,
                Enddate = booking.Enddate,
                Roomtype = booking.Roomtype,
                PreferredRoomNo = booking.PreferredRoomNo,
                OtherPreferences = booking.OtherPreferences,
                OBOwnerId = booking.OBOwnerId,
                IsBookingCompleted = bookingCompleted,
                CancelApproved = false,
                NoOfRoomRequested = booking.NoOfRoomRequested,
                NoOfRoomAllotted = booking.NoOfRoomRequested,

                SlotId = booking.SlotId,
                GuestNameMain = booking.GuestNameMain,
                GuestNameTwo = booking.GuestNameTwo,
                GuestNameThree = booking.GuestNameThree,
                GuestAddress = booking.GuestAddress,
                NoOfAdult = booking.NoOfAdult,
                NoOfChild = booking.NoOfChild,
                BookingNo = "",
                ApplicationDate = DateTime.Now,
                BookingToken = !string.IsNullOrWhiteSpace(booking.BookingToken) ? booking.BookingToken : "",
                GuestMobile = booking.GuestMobile,
                Email = booking.Email,
                DateOfBirth = booking.DateOfBirth,
                HasStayedOrNot = "",

                CheckInDate = booking.CheckInDate,
                CheckOutDate = booking.CheckOutDate,
                ExtraCharges = booking.ExtraCharges,
                OwnerContribution = booking.OwnerContribution,
                HotelContribution = booking.HotelContribution,
                TotalExpense = booking.TotalExpense,
                FolioNo = "",
                Relation = booking.Relation,
            };

            _dbContext.OwnerRoomBookings.Add(model);

            _dbContext.SaveChanges();

            //Notification will only be activated in case of owner request
            if (isOwnerSite)
            {
                NotificationInsert(bookingId, booking);

                var ownersInfo = _dbContext.OwnersInfos.FirstOrDefault(o => o.SlotId == booking.SlotId);

                SendBookingRequestSMS(ownersInfo);
            }

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.BookingCreatedSuccessfully;
            return response;
        }

        #region MISC Methods

        private static void GetMISCBookingInfo(OwnerRoomBooking booking, IQueryable<OwnerRoomBooking> allBookedRoomsForOwner, out int monthlySpent, out int yearlySpent, out int asked, out int startMonth, out int endMonth, out int monthlySpent2, out int asked2)
        {
            NewMethod(booking, allBookedRoomsForOwner, out monthlySpent, out yearlySpent, out asked, out startMonth, out endMonth, out IQueryable<OwnerRoomBooking> previousStays, out IQueryable<OwnerRoomBooking> previousPunishedStays, out int year, out DateTime startingFiscalDate, out DateTime endingFiscalDate);

            yearlySpent = NewMethod1(yearlySpent, previousStays, startingFiscalDate, endingFiscalDate);

            List<OwnerRoomBooking> nextStaysforMonth = NewMethod2(ref monthlySpent, ref yearlySpent, startMonth, previousStays, previousPunishedStays, startingFiscalDate, endingFiscalDate);

            monthlySpent = NewMethod3(monthlySpent, year, nextStaysforMonth, out monthlySpent2, out asked2);

            NewMethod4(booking, ref asked, startMonth, endMonth, previousStays, year, ref monthlySpent2, ref asked2);
        }

        private static void NewMethod4(OwnerRoomBooking booking, ref int asked, int startMonth, int endMonth, IQueryable<OwnerRoomBooking> previousStays, int year, ref int monthlySpent2, ref int asked2)
        {
            if (startMonth != endMonth)
            {
                asked = Convert.ToInt32((new DateTime(year, startMonth + 1, 1) - booking.CheckOutDate).TotalDays);
                var previousStaysforMonth2 = previousStays.Where(o => o.CheckOutDate.Month == endMonth && o.IsBookingCompleted != "Cancelled").ToList();
                foreach (var stay in previousStaysforMonth2)
                {
                    if (stay.CheckInDate.Month == stay.CheckOutDate.Month)
                    {
                        monthlySpent2 += Convert.ToInt32((stay.CheckOutDate - stay.CheckInDate).TotalDays);
                    }
                    else
                    {
                        monthlySpent2 += (Convert.ToInt32(stay.CheckOutDate.Day) - 1);
                    }
                }
                asked2 = (booking.CheckOutDate.Day) - 1;
            }
        }

        private static int NewMethod3(int monthlySpent, int year, List<OwnerRoomBooking> nextStaysforMonth, out int monthlySpent2, out int asked2)
        {
            foreach (var stay in nextStaysforMonth)
            {
                monthlySpent += Convert.ToInt32((new DateTime(year, DateTime.Today.Month + 1, 1) - stay.CheckInDate).TotalDays);
            }
            monthlySpent2 = 0;
            asked2 = 0;
            return monthlySpent;
        }

        private static List<OwnerRoomBooking> NewMethod2(ref int monthlySpent, ref int yearlySpent, int startMonth, IQueryable<OwnerRoomBooking> previousStays, IQueryable<OwnerRoomBooking> previousPunishedStays, DateTime startingFiscalDate, DateTime endingFiscalDate)
        {
            foreach (var days in previousPunishedStays)
            {
                if (startingFiscalDate <= days.CheckInDate && endingFiscalDate >= days.CheckInDate)
                {
                    yearlySpent++;
                }
            }

            // previousStaysforMonth : How many days owner has booked this month
            var previousStaysforMonth = previousStays.Where(o => o.CheckOutDate.Month == startMonth && o.IsBookingCompleted != "Cancelled");
            foreach (var stay in previousStaysforMonth)
            {
                if (stay.CheckOutDate.Month == stay.CheckInDate.Month)
                {
                    monthlySpent += Convert.ToInt32((stay.CheckOutDate - stay.CheckInDate).TotalDays) * stay.NoOfRoomRequested;
                }
                else
                {
                    //Two different logic in two seperate place! Examine the logics and apply the correct one!
                    //monthlySpent += (Convert.ToInt32(stay.Enddate.Day) - 1);
                    monthlySpent += Convert.ToInt32(stay.CheckOutDate.Day - 1) * stay.NoOfRoomRequested;
                }
            }
            var nextStaysforMonth = previousStays.Where(o => o.CheckInDate.Month == startMonth && o.CheckOutDate.Month == startMonth + 1 && o.IsBookingCompleted != "Cancelled").ToList();
            return nextStaysforMonth;
        }

        private static int NewMethod1(int yearlySpent, IQueryable<OwnerRoomBooking> previousStays, DateTime startingFiscalDate, DateTime endingFiscalDate)
        {
            foreach (var pStay in previousStays)
            {
                if (startingFiscalDate <= pStay.CheckInDate && endingFiscalDate >= pStay.CheckInDate)
                {
                    yearlySpent += pStay.NoOfRoomRequested * Convert.ToInt32((pStay.CheckOutDate - pStay.CheckInDate).TotalDays);
                }
            }

            return yearlySpent;
        }

        private static void NewMethod(OwnerRoomBooking booking, IQueryable<OwnerRoomBooking> allBookedRoomsForOwner, out int monthlySpent, out int yearlySpent, out int asked, out int startMonth, out int endMonth, out IQueryable<OwnerRoomBooking> previousStays, out IQueryable<OwnerRoomBooking> previousPunishedStays, out int year, out DateTime startingFiscalDate, out DateTime endingFiscalDate)
        {
            monthlySpent = 0;
            yearlySpent = 0;
            asked = Convert.ToInt32((booking.CheckOutDate - booking.CheckInDate).TotalDays);
            startMonth = booking.CheckInDate.Month;
            endMonth = booking.CheckOutDate.Month;
            var startYear = booking.CheckInDate.Year; var endYear = booking.CheckOutDate.Year;
            previousStays = allBookedRoomsForOwner.Where(o => o.HasStayedOrNot == "Yes");
            previousPunishedStays = allBookedRoomsForOwner.Where(o => o.HasStayedOrNot == "No" && o.CheckInDate <= DateTime.Today);
            var dateToMeasureFiscalYear = DateTime.Now;
            int month = dateToMeasureFiscalYear.Month;
            year = dateToMeasureFiscalYear.Year;
            startingFiscalDate = month > 6 ? new DateTime(year, 7, 1) : new DateTime(year - 1, 7, 1);
            endingFiscalDate = month > 6 ? new DateTime(year + 1, 6, 30) : new DateTime(year, 6, 30);
        }

        #endregion

        public ResponseModel BookingUpdate(OwnerRoomBookingView paramBooking)
        {
            ResponseModel response = new ResponseModel();

            var booking = _dbContext.OwnerRoomBookings.Where(r => r.OwnerRoomBookngId == paramBooking.OwnerRoomBookingId).FirstOrDefault();

            bool isBookingTokenChanged = !paramBooking.IsRoomBookingTokenExists || (paramBooking.BookingToken != booking.BookingToken);

            booking.GuestNameMain = paramBooking.GuestNameMain;
            booking.GuestAddress = paramBooking.GuestAddress;
            booking.GuestMobile = paramBooking.GuestMobile;
            booking.Email = paramBooking.Email;
            booking.Relation = paramBooking.Relation;
            booking.BookingToken = paramBooking.BookingToken;
            booking.HasStayedOrNot = paramBooking.HasStayedOrNot;
            booking.Roomtype = paramBooking.Roomtype;

            booking.NoOfRoomRequested = paramBooking.NoOfRoomRequested;
            booking.NoOfRoomAllotted = paramBooking.NoOfRoomAllotted;
            booking.PreferredRoomNo = paramBooking.PreferredRoomNo;

            booking.IsBookingCompleted = "Yes";
            booking.CheckInDate = paramBooking.CheckInDate;
            booking.CheckOutDate = paramBooking.CheckOutDate;
            booking.Startdate = paramBooking.Startdate;
            booking.Enddate = paramBooking.Enddate;
            booking.NoOfAdult = paramBooking.NoOfAdult;
            booking.NoOfChild = paramBooking.NoOfChild;

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.BookingUpdatedSuccessfully;

            if (!isBookingTokenChanged)
            {
                return response;
            }

            var owner = _dbContext.OwnersInfos.Where(o => o.SlotId == paramBooking.SlotId).FirstOrDefault();

            string messageBody = BookingConfirmationMessage(owner, paramBooking.BookingToken);

            SendBookingConfirmationSMS(owner, messageBody);

            SendBookingConfirmationEmail(owner, messageBody);

            return response;
        }

        #region Email, SMS, Notification

        public ResponseModel SendBookingConfirmationEmail(OwnersInfo owner, string messageBody)
        {
            ResponseModel response = new ResponseModel();

            var EmailHeader = "Regarding Room Reservation from Heritage BD";
            string isEmailSent = "Failed";

            if (!string.IsNullOrWhiteSpace(owner.Email))
            {
                Common.EmailSending(owner.Email, EmailHeader, messageBody);
                isEmailSent = "Delivered";
            }

            EmailandSMSReport emailsmsreportForEmail = new EmailandSMSReport()
            {
                IsSMSSent = isEmailSent,
                Mobile = owner.Mobile,
                Email = owner.Email,
                SlotId = owner.SlotId,
                Name = owner.Name,
                TimeOfDelivery = DateTime.Now,
                NotificationType = "Email",
                //Text = model.EmailText,
                Text = messageBody,
                NotificationSerialNo = "AUTO GENERATED:- BookingConfirmation",
            };

            Common.InsertEmailandSMSReports(emailsmsreportForEmail);

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.BookingConfirmationSMSSentSuccessfully;
            return response;
        }

        public static string BookingConfirmationMessage(OwnersInfo owner, string BookingToken)
        {
            return $@"Reservation Confirmation SMS{Environment.NewLine}{Environment.NewLine}Respected BW Heritage Owner {owner?.Name},{Environment.NewLine}Your reservation has been confirmed. Your booking no is {BookingToken}.";
        }

        public ResponseModel NotificationInsert(string bookingId, OwnerRoomBooking info)
        {
            ResponseModel response = new ResponseModel();
            //Notification
            var NotificationList = _dbContext.Notifications.AsEnumerable();

            var lastNotificationId = NotificationList.OrderByDescending(x => x.Id).FirstOrDefault().NotificationId;
            int notificationToken = int.Parse(lastNotificationId.Split('-')[1]);

            notificationToken++;
            var ownersInfo = _dbContext.OwnersInfos.FirstOrDefault(o => o.SlotId == info.SlotId);
            Notification notificationModel = new Notification()
            {
                isSeen = false,
                NotificationHeader = "Booking",
                NotificationText = ownersInfo.SlotId + " has requested " + info.NoOfRoomRequested + " room at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                NotificationLink = "/Booking/OwnerRoomBookingEdit?ownerRoomBookngId=" + bookingId,
                NotificationTime = DateTime.Now,
                NotificationId = "NOT-" + notificationToken,
                IdForRecognition = bookingId
            };
            _dbContext.Notifications.Add(notificationModel);

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.BookingCreatedSuccessfully;
            return response;
        }

        public ResponseModel SendBookingRequestSMS(OwnersInfo ownersInfo)
        {
            ResponseModel response = new ResponseModel();

            string messageBody =
            $@"Reservation request SMS{Environment.NewLine}{Environment.NewLine}Respected BW Heritage Owner {ownersInfo.Name}, {Environment.NewLine}Your requested reservation has been received. Reservation confirmation will be notified to you soon.{Environment.NewLine}Thanks.";

            var ownerMobile = ownersInfo.Mobile;

            string isSMSSent;
            if (ownerMobile != null && ownerMobile != "")
            {
                string res = ownerMobile.Substring(0, 1);
                if (res != "0")
                {
                    ownerMobile = '0' + ownerMobile;
                }
                Common.SMSSending(ownerMobile, messageBody);
                isSMSSent = "Delivered";
            }
            else
            {
                isSMSSent = "Failed";
            }

            EmailandSMSReport model = new EmailandSMSReport()
            {
                IsSMSSent = isSMSSent,
                Mobile = ownerMobile,
                Email = "",
                SlotId = ownersInfo.SlotId,
                Name = ownersInfo.Name,
                TimeOfDelivery = DateTime.Now,
                NotificationType = "SMS",
                Text = messageBody,
                NotificationSerialNo = "AUTO GENERATED:- BookingRequest",
            };

            Common.InsertEmailandSMSReports(model);

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.BookingRequestSMSSentSuccessfully;
            return response;
        }

        public ResponseModel SendBookingConfirmationSMS(OwnersInfo ownersInfo, string messageBody)
        {
            ResponseModel response = new ResponseModel();

            var ownerMobile = ownersInfo.Mobile;


            string isSMSSent;
            if (ownerMobile != null && ownerMobile != "")
            {
                string res = ownerMobile.Substring(0, 1);
                if (res != "0")
                {
                    ownerMobile = '0' + ownerMobile;
                }
                Common.SMSSending(ownerMobile, messageBody);
                isSMSSent = "Delivered";
            }
            else
            {
                isSMSSent = "Failed";
            }

            EmailandSMSReport model = new EmailandSMSReport()
            {
                IsSMSSent = isSMSSent,
                Mobile = ownerMobile,
                Email = "",
                SlotId = ownersInfo.SlotId,
                Name = ownersInfo.Name,
                TimeOfDelivery = DateTime.Now,
                NotificationType = "SMS",
                Text = messageBody,
                NotificationSerialNo = "AUTO GENERATED:- BookingConfirmation",
            };

            Common.InsertEmailandSMSReports(model);


            response.Status = Messages.StatusSuccess;
            response.Message = Messages.BookingConfirmationSMSSentSuccessfully;
            return response;
        }

        #endregion

        public OwnerRoomBookingView GetOwnerRoomBookingView(OwnerRoomBooking paramBooking, string errorMessage)
        {
            var allotment = _dbContext.ConfAllotments.Where(o => o.SlotId == paramBooking.SlotId).ToList().LastOrDefault();
            int monthlyAllotment = allotment?.MonthlyAllotment ?? 0;

            OwnerRoomBookingView viewmodel = new OwnerRoomBookingView()
            {
                NoOfDaysCanStay = paramBooking.NoOfDaysCanStay,
                NoOfDaySpent = paramBooking.NoOfDaySpent,
                NoOfDaysCanStayMonthly = monthlyAllotment,
                ErrorMessage = errorMessage,
            };

            return viewmodel;
        }

        public IQueryable<OwnerRoomBooking> GetBookings(BookingParameterVM paramVM)
        {
            var bookings = _dbContext.OwnerRoomBookings.AsQueryable();
            if (!string.IsNullOrWhiteSpace(paramVM.Searchfield))
            {
                bookings = bookings.Where(o => o.SlotId.Contains(paramVM.Searchfield) || o.GuestNameMain.Contains(paramVM.Searchfield) || o.BookingToken.Contains(paramVM.Searchfield));
            }

            if (!string.IsNullOrWhiteSpace(paramVM.BookingComplete))
            {
                bookings = bookings.Where(o => o.IsBookingCompleted == paramVM.BookingComplete);
            }

            if (!string.IsNullOrWhiteSpace(paramVM.CheckedIn))
            {
                bookings = bookings.Where(o => o.HasStayedOrNot == paramVM.CheckedIn);
            }

            if (!string.IsNullOrWhiteSpace(paramVM.CheckedOut))
            {
                bookings = bookings.Where(o => o.HasCheckedOutOrNot == paramVM.CheckedOut);
            }

            if (paramVM.TokenAssigned == "Yes") bookings = bookings.Where(o => o.BookingToken != "");
            else if (paramVM.TokenAssigned == "No") bookings = bookings.Where(o => o.BookingToken == "");

            if (!string.IsNullOrWhiteSpace(paramVM.FromDate))
            {
                DateTime dateFrom = Convert.ToDateTime(paramVM.FromDate);
                bookings = bookings.Where(o => o.CheckInDate >= dateFrom);
            }

            if (!string.IsNullOrWhiteSpace(paramVM.ToDate))
            {
                DateTime dateTo = Convert.ToDateTime(paramVM.ToDate);
                bookings = bookings.Where(o => o.CheckInDate <= dateTo);
            }

            bookings = bookings.OrderByDescending(x => x.ApplicationDate);

            return bookings;
        }

        public OwnerRoomBookingView PreBooking(string slotId)
        {
            var owner = _dbContext.OwnersInfos.FirstOrDefault(o => o.SlotId == slotId);

            int canStay = _dbContext.ConfAllotments.Where(o => o.SlotId == owner.SlotId).Select(r => r.YearlyAllotment).FirstOrDefault();
            int canStayMonthly = _dbContext.ConfAllotments.Where(o => o.SlotId == owner.SlotId).Select(r => r.MonthlyAllotment).ToList().LastOrDefault();

            GetFiscalDates(out int year, out DateTime startingFiscalDate, out DateTime endingFiscalDate);

            GetTotalSpent(owner, year, startingFiscalDate, endingFiscalDate, out int totalSpent, out int totalSpentMonthly);

            var model = new OwnerRoomBookingView()
            {
                IsError = false,
                RoomList = _dbContext.RoomInfos.Select(r => r.RoomNo).ToList(),
                OBOwnerId = owner.OwenerId,
                IsBookingCompleted = "No",
                RoomTypeList = _dbContext.RoomInfos.Select(r => r.RoomType).Distinct().ToList(),
                NoOfDaySpent = totalSpent,
                NoOfDaySpentMonthly = totalSpentMonthly,
                SlotId = (owner == null) ? "" : owner.SlotId,
                NoOfDaysCanStay = canStay,
                NoOfDaysCanStayMonthly = canStayMonthly,
                RelationEnumList = _enumDataService.GetEnumData(RelationshipEnumGroup),
            };

            return model;
        }

        private void GetTotalSpent(OwnersInfo owner, int year, DateTime startingFiscalDate, DateTime endingFiscalDate, out int totalSpent, out int totalSpentMonthly)
        {
            //Determining how many days the owner has spent in the hotel
            var allBookingsOwner = _dbContext.OwnerRoomBookings.Where(r => r.SlotId == owner.SlotId);
            var previousBookings = allBookingsOwner.Where(r => r.IsBookingCompleted == "Yes" || r.IsBookingCompleted == "No");

            totalSpent = 0;
            //measuring how many days he spent in the hotel actually
            foreach (var days in previousBookings)
            {
                if (startingFiscalDate <= days.Startdate && endingFiscalDate >= days.Startdate)
                {
                    var noOfDays = (days.Enddate - days.Startdate).TotalDays * days.NoOfRoomRequested;
                    totalSpent += Convert.ToInt16(noOfDays);
                }
            }

            //punishment for not arriving at hotel after completing booking online
            var previousBookingspunished = allBookingsOwner.Where(o => o.HasStayedOrNot == "No" && o.IsBookingCompleted == "Yes" && o.Startdate <= DateTime.Today).ToList();
            foreach (var days in previousBookingspunished)
            {
                if (startingFiscalDate <= days.Startdate && endingFiscalDate >= days.Startdate)
                {
                    totalSpent++;
                }
            }
            var previousStaysforMonth = previousBookings.Where(o => o.Enddate.Month == DateTime.Today.Month).ToList();

            totalSpentMonthly = 0;
            foreach (var stay in previousStaysforMonth)
            {
                if (stay.Startdate.Month == stay.Enddate.Month)
                {
                    totalSpentMonthly += Convert.ToInt32((stay.Enddate - stay.Startdate).TotalDays);

                }
                else
                {
                    totalSpentMonthly += (Convert.ToInt32(stay.Enddate.Day) - 1);
                }
            }
            var nextStaysforMonth = previousBookings.Where(o => o.Startdate.Month == DateTime.Today.Month && o.Enddate.Month != DateTime.Today.Month && o.IsBookingCompleted != "Cancelled");

            foreach (var stay in nextStaysforMonth)
            {
                totalSpentMonthly += Convert.ToInt32((new DateTime(year, DateTime.Today.Month + 1, 1) - stay.Startdate).TotalDays);
            }
        }

        private static void GetFiscalDates(out int year, out DateTime startingFiscalDate, out DateTime endingFiscalDate)
        {
            //for determining the fiscal year of Present Day
            //int canStay = 30; //initially 30 for every fiscal year
            var dateToMeasureFiscalYear = DateTime.Now;
            int month = dateToMeasureFiscalYear.Month;
            year = dateToMeasureFiscalYear.Year;
            startingFiscalDate = month > 6 ? new DateTime(year, 7, 1) : new DateTime(year - 1, 7, 1);
            endingFiscalDate = month > 6 ? new DateTime(year + 1, 6, 30) : new DateTime(year, 6, 30);
        }
    }
}