﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace StarterProjectV2.Services
{
    public class GlobalService
    {
        public void Custom_Application_Error<Controller>(string userId)
        {
            var ex = HttpContext.Current.Server.GetLastError();
            
            //log error
            LogException(ex, userId);

            var ctx = HttpContext.Current;

            ctx.Response.Clear();
            ctx.Server.ClearError();
            ctx.Response.TrySkipIisCustomErrors = true;

            // Call target Controller and pass the routeData.
            IController errorController = (IController)Activator.CreateInstance(typeof(Controller));

            var routeData = new RouteData();
            routeData.Values.Add("controller", "Home");
            routeData.Values.Add("action", "Error");
            routeData.Values.Add("errorMessage", ex?.InnerException?.Message ?? ex?.Message);

            errorController.Execute(new RequestContext(new HttpContextWrapper(HttpContext.Current), routeData));
        }

        protected void LogException(Exception exc, string userId)
        {
            Logger _logger = new Logger();

            _logger.InsertLog(exc?.InnerException?.Message ?? exc?.Message, exc.ToString(), userId);
        }
    }
}