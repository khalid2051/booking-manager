﻿using StarterProjectV2.Models;
using System.Linq;

namespace StarterProjectV2.Services
{
    public class ConfigurationService
    {
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();

        public SystemDate GetSystemDate()
        {
            return _dbContext.SystemDates.FirstOrDefault();
        }

        public string GetSettingValue(string settingCode)
        {
            var settingValue = _dbContext.Settings.FirstOrDefault(s => s.SettingCode == settingCode)?.SettingValue;
            
            return settingValue;
        }

    }
}