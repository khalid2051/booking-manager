﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Services
{
    public class EnumDataService
    {
        private readonly static ApplicationDbContext _dbContext = new ApplicationDbContext();

        public IEnumerable<string> GetEnumData(string enumGroup)
        {
            var list = _dbContext.EnumData.Where(c => c.EnumGroup.Equals(enumGroup)).Select(x => x.Name);

            return list;
        }

    }
}