﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Services
{
    public class Logger
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public IEnumerable<Log> GetAllLogs()
        {
            var query = _context.Logs;
            return query;
        }

        public Log GetLogById(int logId)
        {
            return _context.Logs.Find(logId);
        }

        public Log InsertLog(string shortMessage, string fullMessage, string userId)
        {
            Log log = new Log
            {
                ShortMessage = shortMessage,
                FullMessage = fullMessage,
                UserId = userId,
                CreatedOn = DateTime.Now,
            };

            _context.Logs.Add(log);
            _context.SaveChanges();

            return log;
        }
    }
}