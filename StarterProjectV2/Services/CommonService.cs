﻿using StarterProjectV2.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Services
{
    public class CommonService
    {
        private readonly static ApplicationDbContext _dbContext = new ApplicationDbContext();

        public DataSet GetData(string sqlText, Dictionary<string, object> paramDictionary)
        {
            DataSet ds = new DataSet();
            using (var conn = new SqlConnection(_dbContext.Database.Connection.ConnectionString))
            {
                using (var cmd = new SqlCommand(sqlText, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    conn.Open();

                    foreach (var item in paramDictionary)
                    {
                        cmd.Parameters.AddWithValue("@" + item.Key, item.Value);
                    }

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
            }
            return ds;
        }

        public int ExecuteQuery(string sqlText, Dictionary<string, object> paramDictionary)
        {
            int executionResult = 0;

            using (var conn = new SqlConnection(_dbContext.Database.Connection.ConnectionString))
            {
                using (var cmd = new SqlCommand(sqlText, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    conn.Open();

                    foreach (var item in paramDictionary)
                    {
                        cmd.Parameters.AddWithValue("@" + item.Key, item.Value);
                    }

                    executionResult = cmd.ExecuteNonQuery();
                }
            }

            return executionResult;
        }


        public string TaskRole(string taskPath)
        {
            string taskId = _dbContext.Tasks.Where(u => u.TaskPath == taskPath).FirstOrDefault()?.TaskId;
            var myRoleList = (List<UserRole>)HttpContext.Current.Session[SessionKeys.ROLE_LIST];

            string taskRoles = myRoleList?.Where(mr => mr.TaskId == taskId).FirstOrDefault().Details ?? "";

            return taskRoles;
        }


        public void TaskRole(string taskPath, out string taskRoles)
        {
            taskRoles = TaskRole(taskPath);

            if (string.IsNullOrWhiteSpace(taskRoles) || taskRoles[0] == '0')
            {
                ////return View("Error");
            }
        }

        public List<Select2Model> GetSlotIdList(string term)
        {
            var data = _dbContext.OwnersInfos.Where(o => o.SlotId.Contains(term)).Select(o => o.SlotId);
            List<Select2Model> list = ToSelect2List(data.ToList());

            return list;
        }

        public static List<Select2Model> ToSelect2List(List<string> sourceList)
        {
            List<Select2Model> list = sourceList.ConvertAll(s =>
            {
                return new Select2Model()
                {
                    id = s.ToString(),
                    text = s.ToString(),
                };
            });

            return list;
        }

    }
}