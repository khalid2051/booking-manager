﻿using StarterProjectV2.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Services
{
    public class FileService
    {
        private readonly ConfigurationService _configurationService = new ConfigurationService();
        private string RemoteDirectoryPath;

        public ResponseModel FileUpload(HttpPostedFileBase file, string relativePath, string fileName)
        {
            ResponseModel response = FileUpload(file, relativePath, fileName, false);

            return response;
        }

        public ResponseModel FileUpload(HttpPostedFileBase file, string relativePath, string fileName, bool isRemote)
        {
            string directoryPath = CommonHelper.GetServerPath(relativePath);

            ResponseModel response = new ResponseModel();

            if (file == null)
            {
                response.Message = Messages.FileNotFound;
                return response;
            }

            string filePath = Path.Combine(directoryPath, fileName);

            file.SaveAs(filePath);

            if (isRemote)
            {
                RemoteDirectoryPath = _configurationService.GetSettingValue(SettingCode.RemoteLocation);

                var remoteFilePath = Path.Combine(RemoteDirectoryPath, fileName);

                file.SaveAs(remoteFilePath);
            }

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.FileSaved;

            return response;
        }

        public string RenameFile(string prefix, string slotId, HttpPostedFileBase file)
        {
            return prefix + "_" + slotId + Path.GetExtension(file.FileName);
        }
    }
}