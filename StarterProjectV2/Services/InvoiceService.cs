﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using StarterProjectV2.Models;
using StarterProjectV2.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace StarterProjectV2.Services
{
    public class InvoiceService
    {
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();
        private readonly CommonService _commonService = new CommonService();

        public ResponseModel GenerateInvoice(string bookingId)
        {
            ResponseModel response = new ResponseModel();

            #region Check Point

            //Booking existence check
            var booking = _dbContext.OwnerRoomBookings.Find(bookingId);

            if (booking == null)
            {
                response.Message = Messages.BookingNotExists;
                return response;
            }

            int totalDays = (int)(booking.CheckOutDate - booking.CheckInDate).TotalDays;

            //Room rate setup check
            for (int i = 0; i < totalDays; i++)
            {
                var roomRate = _dbContext.ConfOwnerAvgRoomRates.Where(rr => rr.ApplicableDate == DbFunctions.AddDays(booking.CheckInDate, i)).FirstOrDefault();

                if (roomRate == null)
                {
                    response.Message = Messages.RoomRateNotSetup;
                    return response;
                }
            }

            #endregion

            #region Initializations

            int paymentFactor = totalDays * booking.NoOfRoomRequested;

            var invoice = _dbContext.Invoices.Where(inv => inv.BookingId == bookingId).FirstOrDefault();

            int newInvoiceNo = 1;
            if (invoice != null)
            {
                newInvoiceNo = invoice.InvoiceNo;
            }
            else
            {
                newInvoiceNo = (_dbContext.Invoices.Max(x => (int?)x.InvoiceNo) ?? 0) + 1;
            }

            #endregion

            string sqlText = @"

----DECLARE @bookingId AS nvarchar(100) = 'ORB-20'
----DECLARE @checkInDate AS datetime = '2022-02-19'
----DECLARE @invoiceNo AS int = 1
----DECLARE @paymentFactor AS int = 1
DECLARE @checkOutDate AS DATETIME = '2022-02-20'
DECLARE @slotId AS NVARCHAR(100) = 'C1323'

SELECT @checkOutDate = checkoutdate,
       @slotId = slotid
FROM   ownerroombookings
WHERE  ownerroombookngid = @bookingId

--------------------------------------------------
--------------------------------------------------
DELETE invoicedetails
WHERE  invoiceno = @invoiceNo

DELETE invoices
WHERE  invoiceno = @invoiceNo

--------------------------------------------------
--------------------------------------------------
INSERT INTO invoices
SELECT @invoiceNo InvoiceNo,
       @bookingId BookingId,
       @slotId    SlotId 
";

            sqlText = sqlText + @"

INSERT INTO InvoiceDetails

";

            for (int i = 0; i < totalDays; i++)
            {
                sqlText = sqlText + @"
----Prepare this query with for loop in case of multiple days
SELECT @invoiceNo invoiceno ,
       @checkInDate[i] [Date] ,
       acc.id                                 accountid ,
       (rr.averageroomrate*noofroomrequested) chargeamount ,
       0                                      paymentamount
FROM   ownerroombookings b,
       accounts acc,
       confowneravgroomrates rr
WHERE  ownerroombookngid=@bookingId
AND    acc.accountcode='RoomCharge'
AND    applicabledate = @checkInDate[i]
UNION ALL
";
                sqlText = sqlText.Replace("@checkInDate[i]", "@checkInDate" + i + "");
            }

            sqlText = sqlText + @"
SELECT @invoiceNo    InvoiceNo,
       @checkOutDate [Date],
       acc.id        AccountId,
       CASE
         WHEN acc.ispayment = 0 THEN Isnull(defaultamount, 0)
         ELSE 0
       END           Charge,
       CASE
         WHEN acc.ispayment = 1 THEN Isnull(defaultamount, 0) * @paymentFactor
         ELSE 0
       END           Payment
FROM   accounts acc
WHERE  accountcode != 'RoomCharge'

--------------------------------------------------
--------------------------------------------------
----Get HotelContribution AccountId
DECLARE @hotelContributionAccountId AS INT = 0

SELECT @hotelContributionAccountId = id
FROM   accounts
WHERE  accountcode = 'HotelContribution'

----SET HotelContribution Amount
DECLARE @hotelContribution AS DECIMAL(18, 2) = 0

SELECT @hotelContribution = ( Sum(inv.chargeamount) - Sum(inv.paymentamount) )
FROM   invoicedetails inv
WHERE  invoiceno = @invoiceNo
       AND inv.accountid <> @hotelContributionAccountId

--------------------------------------------------
--------------------------------------------------
UPDATE invoicedetails
SET    paymentamount = @hotelContribution
WHERE  invoiceno = @invoiceNo
       AND accountid = @hotelContributionAccountId 
";

            var paramDictionary = new Dictionary<string, object>
            {
                { "bookingId", bookingId },
                { "invoiceNo", newInvoiceNo },
                { "paymentFactor", paymentFactor },
            };

            for (int i = 0; i < totalDays; i++)
            {
                paramDictionary.Add("checkInDate" + i, booking.CheckInDate.AddDays(i));
            }

            _commonService.ExecuteQuery(sqlText, paramDictionary);

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.InvoiceGeneratedSuccessfully;
            return response;
        }

        public Stream GetInvoiceReport(string bookingId)
        {
            var invoice = _dbContext.Invoices.Where(inv => inv.BookingId == bookingId).FirstOrDefault();

            if (invoice == null)
            {
                var response = GenerateInvoice(bookingId);
                invoice = _dbContext.Invoices.Where(inv => inv.BookingId == bookingId).FirstOrDefault();
            }

            string sqlText = @"
----DECLARE @invoiceNo AS INT = 2

SELECT inv.invoiceno,
       o.NAME OwnerName,
       o.slotid,
       b.preferredroomno,
       b.checkindate,
       b.checkoutdate,
	   b.GuestNameMain GuestName,
	   b.BookingToken BookingNo,
	   b.Relation
FROM   invoices inv
       LEFT OUTER JOIN ownerroombookings b
                    ON inv.bookingid = b.ownerroombookngid
       LEFT OUTER JOIN ownersinfoes o
                    ON o.slotid = inv.slotid
WHERE  invoiceno = @invoiceNo

SELECT inv.invoiceno,
       inv.transactiondate,
       acc.accounthead
       + Isnull((Char(13)+Char(10)+acc.accountlabel), '') Details,
       chargeamount,
       paymentamount
FROM   invoicedetails inv
       LEFT OUTER JOIN accounts acc
                    ON inv.accountid = acc.id
WHERE  invoiceno = @invoiceNo
ORDER  BY acc.id,
          inv.transactiondate,
          inv.chargeamount DESC 
";

            var paramDictionary = new Dictionary<string, object>
            {
                { "invoiceNo", invoice.InvoiceNo },
            };

            DataSet ds = _commonService.GetData(sqlText, paramDictionary);

            //Setting table name
            ds.Tables[0].TableName = typeof(InvoiceHeaderModel).Name;
            ds.Tables[1].TableName = typeof(InvoiceDetailModel).Name;

            var rpt = (ReportClass)Activator.CreateInstance(typeof(InvoiceReport));
            rpt.Load();

            foreach (Table item in rpt.Database.Tables)
            {
                item.SetDataSource(ds.Tables[item.Name]);
            }

            var stream = rpt.ExportToStream(ExportFormatType.PortableDocFormat);
            //Disposing Report
            rpt.Close();
            rpt.Dispose();

            return stream;
        }


    }
}