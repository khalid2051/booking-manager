﻿using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace StarterProjectV2.Services
{

    public class OwnerInfoService
    {
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();
        private readonly FileService _fileService = new FileService();

        public OwnerBasicInfo GetOwnerBasicInfo(string slotId, string ownerId = null)
        {
            var query = _dbContext.OwnersInfos.Where(o => o.SlotId == slotId || slotId == null);
            query = query.Where(o => o.OwenerId == ownerId || ownerId == null);

            var ownerBasicInfo = query.Select(x => new OwnerBasicInfo
            {
                OwnerId = x.OwenerId,
                SlotId = x.SlotId,
                Name = x.Name,
            }).FirstOrDefault();

            return ownerBasicInfo;
        }

        public ResponseModel InsertOwnerInfo(OwnersInfoView info, out OwnersInfo model)
        {
            ResponseModel response = new ResponseModel();
            model = new OwnersInfo();

            //SlotId exist check
            var owner = GetOwnerBasicInfo(info.SlotId);
            if (owner != null)
            {
                throw new Exception(Messages.SlotIdExist);
            }

            var lastId = _dbContext.OwnersInfos.AsEnumerable().Select(x => int.Parse(x.OwenerId.ToString().Split('-')[1])).OrderByDescending(x => x).FirstOrDefault();
            lastId++;

            string OwenerId = "OWNER-" + lastId;

            #region Comments

            //if (info.OwnerProfilePicture == null || info.OwnerProfilePicture.ContentLength == 0)
            //{
            //    return Content("INVALID FILE");
            //}
            //if (info.OwnerProfilePicture.ContentLength > 0)
            //{
            //    var fileName = Path.GetFileName(OwenerId + ".jpg");
            //    var path = Path.Combine(Server.MapPath("~/Content/images/avatars/"), fileName);
            //    info.OwnerProfilePicture.SaveAs(path);
            //}

            #endregion

            model = new OwnersInfo()
            {
                DateOfBirth = info.DateOfBirth,
                Email = info.Email,
                FatherName = info.FatherName,
                MarriageDate = info.MarriageDate,
                Mobile = info.Mobile,
                MotherName = info.MotherName,
                Name = info.Name,
                NID = info.NID,
                OwenerId = OwenerId,
                PresentAddress = info.PresentAddress,
                PermanantAddress = info.PermanantAddress,
                TIN = info.TIN,
                SpouseName = info.SpouseName,
                //added later for uploading picture
                PicturePath = info.PicturePath,
                OwnerSmartId = info.OwnerSmartId,
                OwnersLastEditTime = DateTime.Now,
                SlotId = info.SlotId,
                NoofSlots = info.NoofSlots,
                AdditionalId = info.AdditionalId,
                AccountNo = info.AccountNo,
                BankName = info.BankName,
                BranchName = info.BranchName,
                Remarks = info.Remarks,
                AdditionalInformation = info.AdditionalInformation,
                UnknownColumn = info.UnknownColumn,
                RoutingNo = info.RoutingNo,
                AdminLastEditApprovedTime = DateTime.Now,
                IsEditApproved = info.IsEditApproved

            };
            _dbContext.OwnersInfos.Add(model);

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.UpdateSuccess;

            return response;
        }

        #region Family

        public OwnersFamily GetOwnersFamily(string ownerId)
        {
            var query = _dbContext.OwnersFamilies.Where(o => o.OwnerId == ownerId || ownerId == null);
            return query.FirstOrDefault();
        }

        public IQueryable<OwnersFamily> GetOwnersFamilyList(string ownerId)
        {
            var query = _dbContext.OwnersFamilies.Where(o => o.OwnerId == ownerId || ownerId == null);
            return query;
        }

        public OwnerFamilyView GetOwnerFamilyViewModel(string ownerId)
        {
            var query = GetOwnersFamily(ownerId);

            return ToOwnerFamilyView(query);
        }

        public OwnerFamilyView ToOwnerFamilyView(OwnersFamily family)
        {
            if (family == null)
            {
                return new OwnerFamilyView();
            }

            OwnerFamilyView model = new OwnerFamilyView
            {
                OwnerId = family.OwnerId,
                SpouseName = family.SpouseName,
                SpouseMobile = family.SpouseMobile,
                SpouseEmail = family.SpouseEmail,
                SpouseDOB = family.SpouseDOB,
                SpouseNID = family.SpouseNID,
                SpousePicture = family.SpousePicture,
                SpouseNIDPath = family.SpouseNIDPath,
            };

            return model;
        }

        public OwnerFamilyDetailView ToOwnerFamilyDetailView(OwnersFamily family)
        {
            if (family == null)
            {
                return new OwnerFamilyDetailView();
            }

            OwnerFamilyDetailView model = new OwnerFamilyDetailView
            {
                OwnerId = family.OwnerId,
                ChildName = family.ChildName,
                ChildDOB = family.ChildDOB,
                ChildPicture = family.ChildPicture,
            };

            return model;
        }


        public List<OwnerFamilyDetailView> GetOwnerFamilyViewList(string ownerId)
        {
            var list = GetOwnersFamilyList(ownerId);

            List<OwnerFamilyDetailView> targetList = new List<OwnerFamilyDetailView>();

            foreach (OwnersFamily model in list)
            {
                var targetVM = ToOwnerFamilyDetailView(model);
                targetList.Add(targetVM);
            }

            return targetList;
        }

        public ResponseModel InsertOwnerFamily(OwnerFamilyView model, List<OwnerFamilyDetailView> detailList)
        {
            ResponseModel response = new ResponseModel();

            var lastId = _dbContext.OwnersFamilies.AsEnumerable().Select(x => int.Parse(x.OwnersFamilyId.ToString().Split('-')[1])).OrderByDescending(x => x).FirstOrDefault();
            lastId++;

            //delete the previous family data of this owner
            var families = _dbContext.OwnersFamilies.Where(p => p.OwnerId == model.OwnerId);
            _dbContext.OwnersFamilies.RemoveRange(families);

            var relativePath = "~/Content/families/";

            var SpouseNIDFileName =  "SNIDPic_" + model.OwnerId + ".jpg";
            var SpousePictureFileName =  "SPic_" + model.OwnerId + ".jpg";

            if (model.SpousePictureFile != null)
            {
                response = _fileService.FileUpload(model.SpousePictureFile, relativePath, SpousePictureFileName);
            }

            if (model.SpouseNIDPictureFile != null)
            {
                response = _fileService.FileUpload(model.SpouseNIDPictureFile, relativePath, SpouseNIDFileName);
            }

            int index = 0;
            foreach (var vm in detailList)
            {
                var ChildPictureFileName = "CPic_" + index + "_" + model.OwnerId + ".jpg";

                if (vm.ChildPictureFile != null)
                {
                    response = _fileService.FileUpload(vm.ChildPictureFile, relativePath, ChildPictureFileName);
                }

                var mdl = new OwnersFamily()
                {
                    OwnersFamilyId = "OWF-" + lastId,
                    OwnerId = model.OwnerId,
                    SpouseName = model.SpouseName,
                    SpouseMobile = model.SpouseMobile,
                    SpouseEmail = model.SpouseEmail,
                    SpouseDOB = model.SpouseDOB,
                    SpouseNIDPath = relativePath + SpouseNIDFileName,
                    SpouseNID = model.SpouseNID,
                    SpousePicture = relativePath + SpousePictureFileName,
                    ChildName = vm.ChildName,
                    ChildDOB = vm.ChildDOB,
                    ChildPicture = relativePath + ChildPictureFileName,
                };

                _dbContext.OwnersFamilies.Add(mdl);
                lastId++;
                index++;
            }

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.InsertSuccess;

            return response;
        }


        #endregion

        #region Nominee

        public IQueryable<OwnersNominee> GetOwnersNomineeList(string ownerId)
        {
            var query = _dbContext.OwnersNominees.Where(o => o.OwnerId == ownerId || ownerId == null);
            return query;
        }

        public List<OwnerNomineeViewModel> GetOwnersNomineeViewModelList(string ownerId)
        {
            var list = GetOwnersNomineeList(ownerId);

            List<OwnerNomineeViewModel> targetList = new List<OwnerNomineeViewModel>();

            foreach (var model in list)
            {
                OwnerNomineeViewModel targetVM = new OwnerNomineeViewModel
                {
                    NomineeName = model.NomineeName,
                    NomineeEmail = model.NomineeEmail,
                    NomineeMobile = model.NomineeMobile,
                    NomineeDOB = model.NomineeDOB,
                    NomineeNID = model.NomineeNID,
                    NomineePermanentAddress = model.NomineePermanentAddress,
                    NomineePresentAddress = model.NomineePresentAddress,
                    NomineePicture = model.NomineePicture,
                    NomineeNIDPath = model.NomineeNIDPath,
                    OwnerRelation = model.OwnerRelation,
                };

                targetList.Add(targetVM);
            }

            return targetList;
        }

        public ResponseModel InsertOwnerNominee(List<OwnerNomineeViewModel> OwnersNomineeList)
        {
            ResponseModel response = new ResponseModel();

            var ownerId = OwnersNomineeList.FirstOrDefault().OwnerId;

            //delete the previous nominee data of this owner
            var nominees = _dbContext.OwnersNominees.Where(n => n.OwnerId == ownerId).ToList();
            foreach (var nominee in nominees)
            {
                _dbContext.OwnersNominees.Remove(nominee);
            }

            var lastId = _dbContext.OwnersNominees.AsEnumerable().Select(x => int.Parse(x.OwnersNomineeId.ToString().Split('-')[1])).OrderByDescending(x => x).FirstOrDefault();
            lastId++;

            var relativePath = "~/Content/nominees/";

            int index = 0;
            foreach (var vm in OwnersNomineeList)
            {
                var pictureFileName = "ON_Pic_" + index + "_" + ownerId + ".jpg";
                var nidPictureFileName = "ON_NIDPic_" + index + "_" + ownerId + ".jpg";

                if (vm.PictureFile != null)
                {
                    response = _fileService.FileUpload(vm.PictureFile, relativePath, pictureFileName);
                }

                if (vm.NIDPictureFile != null)
                {
                    response = _fileService.FileUpload(vm.NIDPictureFile, relativePath, nidPictureFileName);
                }

                var model = new OwnersNominee()
                {
                    OwnersNomineeId = "OWN-" + lastId,
                    OwnerId = ownerId,

                    NomineeName = vm.NomineeName,
                    NomineeMobile = vm.NomineeMobile,
                    NomineeEmail = vm.NomineeEmail,
                    NomineeDOB = vm.NomineeDOB,

                    NomineePicture = relativePath + pictureFileName,
                    NomineeNIDPath = relativePath + nidPictureFileName,
                    NomineePresentAddress = vm.NomineePresentAddress,
                    NomineePermanentAddress = vm.NomineePermanentAddress,
                    NomineeNID = vm.NomineeNID,
                };
                _dbContext.OwnersNominees.Add(model);

                lastId++;
                index++;
            }

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.InsertSuccess;

            return response;
        }


        #endregion

        #region Transaction History

        public OwnerTransactionHistory GetOwnerTransactionHistory(string slotId)
        {
            var query = _dbContext.OwnerTransationHistories.Where(o => o.SlotID == slotId || slotId == null);
            return query.FirstOrDefault();
        }

        public ResponseModel InsertUpdateTransactionHistory(OwnerTransactionHistory model)
        {
            ResponseModel response = new ResponseModel();

            var transactionHistory = _dbContext.OwnerTransationHistories.FirstOrDefault(x => x.SlotID == model.SlotID);

            if (transactionHistory == null)
            {
                InsertTransactionHistory(model);
            }
            else
            {
                UpdateTransactionHistory(model);
            }

            return response;
        }

        public ResponseModel InsertTransactionHistory(OwnerTransactionHistory model)
        {
            ResponseModel response = new ResponseModel();

            var lastTransationId = _dbContext.OwnerTransationHistories.AsEnumerable().Select(x => int.Parse(x.TransactionID)).OrderByDescending(x => x).FirstOrDefault();
            lastTransationId++;

            OwnerTransactionHistory transactionHistory = new OwnerTransactionHistory
            {
                TransactionID = lastTransationId.ToString(),
                SLNo = "TRN-" + lastTransationId,
                SlotID = model.SlotID,
                RegistrationStatus = model.RegistrationStatus,
                RegistrationBatch = model.RegistrationBatch,
                SlotSFT = model.SlotSFT,
                SFTperPerson = model.SFTperPerson,
                Floor = model.Floor,
                Type = model.Type,
                SaleDate = model.SaleDate,
                NoofSlot = model.NoofSlot,

            };

            _dbContext.OwnerTransationHistories.Add(transactionHistory);

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.InsertSuccess;

            return response;
        }

        public ResponseModel UpdateTransactionHistory(OwnerTransactionHistory model)
        {
            ResponseModel response = new ResponseModel();

            var transactionHistory = GetOwnerTransactionHistory(model.SlotID);

            transactionHistory.RegistrationStatus = model.RegistrationStatus;
            transactionHistory.RegistrationBatch = model.RegistrationBatch;
            transactionHistory.SlotSFT = model.SlotSFT;
            transactionHistory.SFTperPerson = model.SFTperPerson;
            transactionHistory.Floor = model.Floor;
            transactionHistory.Type = model.Type;
            transactionHistory.SaleDate = model.SaleDate;
            transactionHistory.NoofSlot = model.NoofSlot;

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.UpdateSuccess;

            return response;
        }

        #endregion

        #region Attachment

        public OwnersAttachment GetOwnersAttachment(string ownerId)
        {
            var query = _dbContext.OwnersAttachments.Where(o => o.OwnerId == ownerId || ownerId == null);
            return query.FirstOrDefault();
        }

        public OwnersAttachmentView GetOwnersAttachmentView(string ownerId)
        {
            var attachment = GetOwnersAttachment(ownerId);

            if (attachment == null)
            {
                return new OwnersAttachmentView();
            }

            var model = new OwnersAttachmentView()
            {
                AttachmentId = attachment.OwnersAttachmentId,
                OwnerId = attachment.OwnerId,
                OwnerNID = attachment.OwnerNID,
                OwnerDrivingLicense = attachment.OwnerDrivingLicense,
                OwnerPassport = attachment.OwnerPassport,
                OwnerTIN = attachment.OwnerTIN,
                OwnerUtilityBill = attachment.OwnerUtilityBill
            };

            return model;
        }

        public ResponseModel InsertUpdateAttachment(OwnersAttachmentView model)
        {
            ResponseModel response = new ResponseModel();

            var attachment = GetOwnersAttachment(model.OwnerId);

            if (attachment == null)
            {
                InsertAttachment(model);
            }
            else
            {
                UpdateAttachment(model);
            }

            return response;
        }


        public ResponseModel InsertAttachment(OwnersAttachmentView model)
        {
            ResponseModel response = new ResponseModel();

            model = SavingFiles(model);

            var lastId = _dbContext.OwnersAttachments.AsEnumerable().Select(x => int.Parse(x.OwnersAttachmentId.ToString().Split('-')[1])).OrderByDescending(x => x).FirstOrDefault();
            lastId++;

            var ownersAttachmentId = "OA-" + lastId;
            var mdl = new OwnersAttachment()
            {
                OwnersAttachmentId = ownersAttachmentId,
                OwnerId = model.OwnerId,
                OwnerNID = model.OwnerNID,
                OwnerDrivingLicense = model.OwnerDrivingLicense,
                OwnerPassport = model.OwnerPassport,
                OwnerTIN = model.OwnerTIN,
                OwnerUtilityBill = model.OwnerUtilityBill

            };
            _dbContext.OwnersAttachments.Add(mdl);

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.InsertSuccess;

            return response;
        }

        public ResponseModel UpdateAttachment(OwnersAttachmentView model)
        {
            ResponseModel response = new ResponseModel();

            var attachment = GetOwnersAttachment(model.OwnerId);

            model.OwnerNID = attachment.OwnerNID;
            model.OwnerTIN = attachment.OwnerTIN;
            model.OwnerPassport = attachment.OwnerPassport;
            model.OwnerDrivingLicense = attachment.OwnerDrivingLicense;
            model.OwnerUtilityBill = attachment.OwnerUtilityBill;

            model = SavingFiles(model);

            attachment.OwnerNID = model.OwnerNID;
            attachment.OwnerTIN = model.OwnerTIN;
            attachment.OwnerPassport = model.OwnerPassport;
            attachment.OwnerDrivingLicense = model.OwnerDrivingLicense;
            attachment.OwnerUtilityBill = model.OwnerUtilityBill;

            _dbContext.SaveChanges();

            response.Status = Messages.StatusSuccess;
            response.Message = Messages.UpdateSuccess;

            return response;
        }


        public OwnersAttachmentView SavingFiles(OwnersAttachmentView model)
        {
            ResponseModel response;

            string relativePath = "~/Content/attachments/";

            var slotId = GetOwnerBasicInfo(null, model.OwnerId).SlotId;

            #region Saving Files

            string newFileName;

            //NID
            if (model.NIDFile != null)
            {
                newFileName = _fileService.RenameFile("OANID", slotId, model.NIDFile);

                model.OwnerNID = Path.Combine(relativePath, newFileName);

                response = _fileService.FileUpload(model.NIDFile, relativePath, newFileName);
            }

            //TIN
            if (model.TINFile != null)
            {
                newFileName = _fileService.RenameFile("OATIN", slotId, model.TINFile);

                model.OwnerTIN = Path.Combine(relativePath, newFileName);

                response = _fileService.FileUpload(model.TINFile, relativePath, newFileName);
            }

            //Passport
            if (model.PassportFile != null)
            {
                newFileName = _fileService.RenameFile("OAPassport", slotId, model.PassportFile);

                model.OwnerPassport = Path.Combine(relativePath, newFileName);

                response = _fileService.FileUpload(model.PassportFile, relativePath, newFileName);
            }

            //DrivingLicense
            if (model.DrivingLicenseFile != null)
            {
                newFileName = _fileService.RenameFile("OADrivingLicense", slotId, model.DrivingLicenseFile);

                model.OwnerDrivingLicense = Path.Combine(relativePath, newFileName);

                response = _fileService.FileUpload(model.DrivingLicenseFile, relativePath, newFileName);
            }

            //Utility
            if (model.UtilityBillFile != null)
            {
                newFileName = _fileService.RenameFile("OAUtility", slotId, model.UtilityBillFile);

                model.OwnerUtilityBill = Path.Combine(relativePath, newFileName);

                response = _fileService.FileUpload(model.UtilityBillFile, relativePath, newFileName);
            }

            #endregion

            return model;
        }

        #endregion

        #region Room Info

        public List<RoomInfo> GetRooms()
        {
            return _dbContext.RoomInfos.ToList();
        }

        #endregion
    }

}