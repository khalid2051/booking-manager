﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace StarterProjectV2.Models
{
    public class Stock
    {
        [Key]
        public string StockId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Title { get; set; }
    }
}