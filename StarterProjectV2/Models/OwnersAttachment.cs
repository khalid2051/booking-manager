﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OfficeOpenXml.Utils;

namespace StarterProjectV2.Models
{
    public class OwnersAttachment
    {
        //Have to work as Foreign Key actually
        [Required]
        public string OwnerId { get; set; }


        //key of this table
        [Key]
        [Required]
        public string OwnersAttachmentId { get; set; }
        public string OwnerNID { get; set; }
        public string OwnerTIN { get; set; }
        public string OwnerPassport { get; set; }
        public string OwnerUtilityBill { get; set; }
        public string OwnerDrivingLicense { get; set; }
    }
}