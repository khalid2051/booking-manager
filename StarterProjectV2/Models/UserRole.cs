﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace StarterProjectV2.Models
{
    public class UserRole
    {
        [Key]
        [Required]
        public string RoleID { get; set; }
        [Required]
        public string RoleName { get; set; }
        [Required]
        public string ModuleName { get; set; }
        [Required]
        public string ModuleId { get; set; }
 
        [Required]
        public string TaskId { get; set; }
        [Required]
        public string TaskName { get; set; }
        [Required]
        public string Details { get; set; }
    }
}