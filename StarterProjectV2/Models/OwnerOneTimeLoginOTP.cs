﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class OwnerOneTimeLoginOTP
    {
        [Key]
        [Required]
        public string OwnerOneTimeLoginOTPKey { get; set; }

        public string OwnerSmartId { get; set; }

        public string OwnerSlotId { get; set; }

        public string OwnerLoginOTPCode { get; set; }

        public bool IsOTPUsed { get; set; }

    }
}