﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StarterProjectV2.Models
{
    public class ReferralBooking
    {
        [Key]
        public string ReferralBookingId { get; set; }
        [Required]
        public string SlotId { get; set; }
        public System.DateTime ApplicationDate { get; set; }
        public System.DateTime RequestedStartDate { get; set; }
        public System.DateTime RequestedEndDate { get; set; }
        public System.DateTime CheckInDate { get; set; }
        public System.DateTime CheckOutDate { get; set; }
        public string Remarks { get; set; }
        public string ReservationStatus { get; set; }

        public string OBOwnerId { get; set; }

        public string MainGuestName { get; set; }
        public string MainGuestAddress { get; set; }
        public int NoOfAdult { get; set; }
        public int NoOfChild { get; set; }

        public string BookingToken { get; set; }
        public string GuestMobile { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public string HasStayedOrNot { get; set; }
        public int NoOfRoomRequested { get; set; }
        public int NoOfRoomAllotted { get; set; }
        public double ExtraCharges { get; set; }
        public double ReferralPoint { get; set; }
        public bool IsReferralPointPaid{ get; set; }
        public string IsReferralPointApproved { get; set; }
        public int RoomRate { get; set; }
        public double TotalExpense {get; set;}
        public string FolioNo { get; set; }
        public string IsBookingCompleted { get; set; }
        public string HasCheckedOutOrNot { get; set; }
    }
}
