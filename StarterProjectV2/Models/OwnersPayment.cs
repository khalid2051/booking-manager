﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace StarterProjectV2.Models
{
    public class OwnersPayment
    {
        [Key]
        public int PaymentID { get; set; }

        public string SlotId { get; set; }

        public decimal Amount { get; set; }

        public string Status { get; set; }

        public DateTime? PaymentDate { get; set; }

        public string Remarks { get; set; }

        public string ReceiverName { get; set; }

        public string ReceivingBank { get; set; }

        public DateTime? FromMonth { get; set; }

        public DateTime? ToMonth { get; set; }

        public string FiscalPeriod { get; set; }

    }
}