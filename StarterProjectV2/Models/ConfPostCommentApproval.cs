﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class ConfPostCommentApproval
    {
        [Key]
        public int ConfPostCommetApprovalId { get; set; }
        public bool PostAutoApprovalEnabled { get; set; }
        public bool CommentAutoApprovalEnabled { get; set; }
        public System.DateTime ModifiedAt { get; set; }
    }
}