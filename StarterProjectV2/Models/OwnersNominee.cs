﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class OwnersNominee
    {
        [Key]
        [Required]
        public string OwnersNomineeId { get; set; }
        [Required]
        public string OwnerId { get; set; }
        [Required]
        public string NomineeName { get; set; }
        public string NomineeEmail { get; set; }
        [Required]
        public string NomineeMobile { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeNID { get; set; }
        public string NomineePermanentAddress { get; set; }
        public string NomineePresentAddress { get; set; }
        public string NomineePicture { get; set; }
        public string NomineeNIDPath { get; set; }
        public string OwnerRelation { get; set; }
    }
}