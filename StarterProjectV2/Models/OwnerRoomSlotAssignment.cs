﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class OwnerRoomSlotAssignment
    {
        [Key]
        [Required]
        public string OwnerRoomSlotAssignmentId { get; set; }
        [Required]
        public string OwnersInfoId { get; set; }
        [Required]
        public string RoomSlotAssigmentId { get; set; }
        public float Rate { get; set; }
    }
}