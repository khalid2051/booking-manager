﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class DividendFailedGroup
    {
        public string SlotIds { get; set; }
        public int NoOfFailedDividendRows { get; set; }
        public DateTime FromMonth { get; set; }
        public DateTime ToMonth { get; set; }
        [Key]
        [Required]
        public string DividendFailedGroupId { get; set; }
    }
}