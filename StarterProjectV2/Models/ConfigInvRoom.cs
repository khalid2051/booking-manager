﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StarterProjectV2.Models
{
    public class ConfigInvRoom
    {
        [Key]
        public int ConfInventoryRoomID { get; set; }
        [Required]
        public int InventoryCount { get; set; } //36
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public System.DateTime CreatedAt { get; set; }  //WHen the rule was created
        public Boolean IsArchived { get; set; }   // IsArchived  = True means the rule is not being used
    }
}