﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class RoomInfo
    {
        [Key]
        [Required]
        public string RoomId { get; set; }

        [Required]
        public string RoomNo { get; set; }

        public string Floor { get; set; }
        public int RoomSize { get; set; }
        public string OwnershipCriteria { get; set; }
        public string RoomType { get; set; }

    }
}