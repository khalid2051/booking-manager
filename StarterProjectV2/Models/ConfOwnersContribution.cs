using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StarterProjectV2.Models
{
    public class ConfOwnersContribution
    {
        [Key]
        public int ConfOwnersContributionID { get; set; }
        [Required]
        public double Amount { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public System.DateTime CreatedAt { get; set; }
    }
}