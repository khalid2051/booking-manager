﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class tblEmployee
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> AddedOn { get; set; }
    }
}