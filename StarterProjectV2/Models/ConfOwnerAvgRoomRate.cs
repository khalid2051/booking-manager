﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StarterProjectV2.Models
{
    public class ConfOwnerAvgRoomRate
    {
        [Key]
        public int ConfOwnerAvgRoomRateID { get; set; }
        public int AverageRoomRate { get; set; }//4500
        public DateTime ApplicableDate {get; set;}
    }
}