﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class OwnerTransactionHistory
    {
        public string SLNo { get; set; }
        [Key]
        public string TransactionID { get; set; }

        //        [Index("SlotIdIndex", IsUnique = true), MaxLength(128), Required]
        public string SlotID { get; set; }
        public string RegistrationStatus { get; set; }
        public string RegistrationBatch { get; set; }
        public double SlotSFT { get; set; }
        public double SFTperPerson { get; set; }
        public int Floor { get; set; }
        public string Type { get; set; }

        private DateTime _saleDate = DateTime.MinValue;

        [Display(Name = "Sale Date")]
        [DataType(DataType.DateTime)]
        public DateTime SaleDate
        {
            get
            {
                return (_saleDate == DateTime.MinValue) ? DateTime.MinValue.AddYears(1900) : _saleDate;
            }
            set { _saleDate = value; }
        }
        public double NoofSlot { get; set; }

        public int? SalesValue1 { get; set; }
        public int ReceivableIncreaseDecreaseValue { get; set; }
        public int ReceivableExcludingRandU { get; set; }
        public int ReceivableRegistration { get; set; }
        public int ReceivableUtility { get; set; }
        public int TotalReceivable { get; set; }
        public int? SalesValue { get; set; }
        public int ReceivedIncreaseDecreaseValue1 { get; set; }
        public int ReceivedExcludingRandU { get; set; }
        public int ReceivedRegistration { get; set; }
        public int ReceivedUtility { get; set; }
        public int TotalReceived { get; set; }
        public int TotalReceivableExcludingRandU { get; set; }
        public int NETReceivableAmountinTaka { get; set; }
        public int OperationalCostBankCashReceived_CRPL { get; set; }
        public int OperationalCostBankCashReceived_GHL { get; set; }
        public int OperationalCostBankCashReceived_CRPLandGHL { get; set; }
        public int OperationalCostAdjustment_CRPL { get; set; }
        public int OperationalCostAdjustwithLateFee { get; set; }
        public int TotalReceivedOperationalCost { get; set; }
        public int OperationCostReceivable { get; set; }
        public int NetReceivableincludingOPandMembershipFee { get; set; }

        private DateTime? _operationalCostReceivedDate = DateTime.MinValue;

        [Display(Name = "Sale Date")]
        [DataType(DataType.DateTime)]
        public DateTime? OperationalCostReceivedDate
        {
            get
            {
                return (_operationalCostReceivedDate == DateTime.MinValue) ? DateTime.MinValue.AddYears(1900) : _operationalCostReceivedDate;
            }
            set { _operationalCostReceivedDate = value; }
        }
        public int MembershipFeeReceivable { get; set; }
        public int MembershipFeeReceived { get; set; }
        public string Particulars { get; set; }
        public string AgreementwithSlotOwner { get; set; }
        public string Remarks { get; set; }
    }
}