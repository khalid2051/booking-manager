﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Log
    {
        public int Id { get; set; }

        public string ShortMessage { get; set; }

        public string FullMessage { get; set; }

        public string IpAddress { get; set; }

        public string UserId { get; set; }

        public string PageUrl { get; set; }

        public string ReferrerUrl { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}