﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class OwnerDetail
    {
        [Key]
        [Required]
        public int OwnerDetailsID { get; set; }
        [DisplayName("Slot ID")]
        public string SlotId { get; set; }
        [DisplayName("Sl No. ")]
        public string SlNo { get; set; }
        [DisplayName("Registration Status")]
        public string RegistrationStatus { get; set; }
        [DisplayName("Suit Size")]
        public int SuitSize { get; set; }
        [DisplayName("Ownership of Space")]
        public double OwnershipOfSpace{ get; set; }
        [DisplayName("Sale Date")]
        public string SaleDate { get; set; }
        [DisplayName("No. of Slot")]
        public string NoOfSlot { get; set; }
        [DisplayName("Bank Name")]
        public string BankName { get; set; }
        [DisplayName("Routing Number")]
        public string RoutingNumber { get; set; }
        [DisplayName("Bank Account Number")]
        public string AccountNumber { get; set; }
    }
}