﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StarterProjectV2.Models
{
    public class ReferralBookingRoomDetail
    {
        [Key]
        public int ReferralBookingRoomDetailsID { get; set; }
        [Required]
        public string ReferralBookingId { get; set; }
        [Required]
        public string RoomID { get; set; }
        public int RoomCost { get; set; }
    }
}