﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Like
    {
        [Key]
        public string LikeId { get; set; }
        public string PostId { get; set; }
        public string LikedOwnerId { get; set; }
    }
}