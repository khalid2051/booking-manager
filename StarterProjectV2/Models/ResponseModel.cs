﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class ResponseModel
    {
        public ResponseModel()
        {
            Status = Messages.StatusFail;
            Message = Messages.StatusFail;
        }
        public string Status { get; set; }
        public string Message { get; set; }
    }

    public class Select2Model
    {
        public string id { get; set; }
        public string text { get; set; }
    }
}