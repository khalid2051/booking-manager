﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace StarterProjectV2.Models
{
    public class FinancialYear
    {
        [Key]
        public string FinId { get; set; }
        public string Year { get; set; }
        public DateTime FromMonth { get; set; }
        public DateTime ToMonth { get; set; }
    }
}