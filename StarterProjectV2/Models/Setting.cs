﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Setting
    {
        public int Id { get; set; }
        public string SettingCode { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
        public string SettingType { get; set; }
        public string SettingGroup { get; set; }
        public string SettingDetail { get; set; }
    }
}