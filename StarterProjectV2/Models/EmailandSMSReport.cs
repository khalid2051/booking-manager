﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class EmailandSMSReport
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string IsSMSSent { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string SlotId { get; set; }
        public DateTime TimeOfDelivery { get; set; }
        public string NotificationType { get; set; }
        [Key]
        [Required]
        public string EmailandSMSReportId { get; set; }
        public string Text { get; set; }
        public string NotificationSerialNo { get; set; }
    }
}