﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Comment
    {
        [Key]
        public string CommentId { get; set; }
        [Required]
        public string PostId { get; set; }
        [Required]
        public string CommentorId { get; set; }
        public string CommentText { get; set; }
        public bool Approval { get; set; }
        public DateTime CommentTime { get; set; }
        public DateTime ApprovalTime { get; set; }
    }
}