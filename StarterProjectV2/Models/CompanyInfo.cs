﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class CompanyInfo
    {
        [Key]
        [Required]
        public string CompanyInfoId { get; set; }

        public string Name { get; set; }
        public string CompanyAddresss { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CopyrightText { get; set; }
        public string CompanyLogoPath { get; set; }
        public string AlternativeLogoPath { get; set; }
        public double TotalSquareFeet { get; set; }
        public string AdministratorEmail { get; set; }
    }
}