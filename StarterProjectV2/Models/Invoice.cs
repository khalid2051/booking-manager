﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public int InvoiceNo { get; set; }
        public string BookingId { get; set; }
        public string SlotId { get; set; }
    }

    public class InvoiceDetail
    {
        public int Id { get; set; }
        public int InvoiceNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public int AccountId { get; set; }
        public decimal ChargeAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        [NotMapped]
        public decimal Details { get; set; }
    }

    public class InvoiceHeaderModel
    {
        public string InvoiceNo { get; set; }
        public string OwnerName { get; set; }
        public string SlotId { get; set; }
        public string PreferredRoomNo { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public string GuestName { get; set; }
        public string BookingNo { get; set; }
        public string Relation { get; set; }

    }

    public class InvoiceDetailModel
    {
        public DateTime TransactionDate { get; set; }
        public string Details { get; set; }
        public decimal ChargeAmount { get; set; }
        public decimal PaymentAmount { get; set; }
    }

    public class InvoiceParameterModel
    {
        public DateTime     CheckInDate             { get; set; }
        public DateTime     CheckOutDate        { get; set; }
        public string       BookingId       { get; set; }
        public string       SlotId          { get; set; }
        public int          InvoiceNo       { get; set; }
    }
}