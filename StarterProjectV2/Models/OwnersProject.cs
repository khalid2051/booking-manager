﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class OwnersProject
    {
        public string ProjectName { get; set; }
        public string NumberOfRoom { get; set; }
        public string NumberOfDays { get; set; }
        public string PurchaseDate { get; set; }
        public string SalesAmount { get; set; }
        public string PaidAmount { get; set; }



        //these will be multiple for 1 Owner
        public string Period { get; set; }
        public string Date { get; set; }
        public string Amount { get; set; }
        [Key]
        [Required]
        public string OwnersProjectId { get; set; }
        [Required]
        public string OwnerId { get; set; }

    }
}