﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StarterProjectV2.Models
{
    public class OwnersInfo
    {
        [Key]
        public string OwenerId { get; set; }

        [Index("SlotIdIndex", IsUnique = true), MaxLength(128), Required]
        public string SlotId { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string DateOfBirth { get; set; }
        public string SpouseName { get; set; }
        public string MarriageDate { get; set; }
        public string PresentAddress { get; set; }
        public string PermanantAddress { get; set; }
        public string NID { get; set; }
        public string TIN { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

        public string OwnerSmartId { get; set; }
        public string PicturePath { get; set; }
        public string NoofSlots { get; set; }
        public string AdditionalId { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string Remarks { get; set; }
        public string AdditionalInformation { get; set; }
        public string UnknownColumn { get; set; }

        public string NIDFilePath { get; set; }
        public string TINFilePath { get; set; }
        public DateTime OwnersLastEditTime { get; set; }
        public string AdminComment { get; set; }
        public string Password { get; set; }
        public string OwnerSegment { get; set; }
        public string IsEditApproved { get; set; }
        public DateTime AdminLastEditApprovedTime { get; set; }
        public string RoutingNo { get; set; }
    }

    public class OwnerBasicInfo
    {
        public string OwnerId { get; set; }
        public string SlotId { get; set; }
        public string Name { get; set; }

    }
}