﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace StarterProjectV2.Models
{
    public class Ticker
    {
        [Key]
        [Required]
        public string TickerId { get; set; }

        public string TickerType { get; set; }
   
        public string Title { get; set; }
        public string Details { get; set; }
        public string Link { get; set; }
     
    }
}