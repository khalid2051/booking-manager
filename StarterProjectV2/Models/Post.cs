﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Post
    {
        [Key]
        public string PostId { get; set; }
        public string PostedBy { get; set; }
        public string PostText { get; set; }
        public DateTime Time { get; set; }
        public int NoOfLikes { get; set; }
        public bool Approval { get; set; }
        public DateTime ApprovalTime { get; set; }
        public string PostHeader { get; set; }
    }
}