﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class RoomSlotAssignment
    {
        [Required]
        [Key]
        public string RoomSlotAssignmentId { get; set; }
        [Required]
        public string RoomId { get; set; }
        [Required]
        public string SlotId { get; set; }
        public string RoomSlotText { get; set; }
        public string SoldStatus { get; set; }
    }
}