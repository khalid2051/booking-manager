﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
namespace StarterProjectV2.Models
{
    public class Audit2
    {
        public int Id { get; set; }
        public string TableName { get; set; }
        public string UserId { get; set; }
        public string Actions { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }
        public string TableIdValue { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
    }
}