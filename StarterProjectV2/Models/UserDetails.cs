﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class UserDetails
    {
        [Key]
        [Required]
        public string UserDetailsKey { get; set; }
        [Required]
        public  string UserName { get; set; }
        [Required]
        public  string ModuleName { get; set; }
        [Required]
        public string ModuleId { get; set; }
        [Required]
        public  string TaskName { get; set; }
        [Required]
        public string TaskId { get; set; }
        [Required]
        public string Details { get; set; }
    }
}