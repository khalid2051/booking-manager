﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using OfficeOpenXml.Utils;

namespace StarterProjectV2.Models
{
    public class OwnersFamily
    {
        [Key]
        [Required]
        public string OwnersFamilyId { get; set; }

        [Required]
        public string OwnerId { get; set; }
        
        public string SpouseName { get; set; }
        
        public string SpouseMobile { get; set; }
        
        public string SpouseEmail { get; set; }
        
        public string SpouseDOB { get; set; }
        
        public string SpouseNID { get; set; }
        
        public string SpousePicture { get; set; }
        public string SpouseNIDPath { get; set; }

        public string ChildName { get; set; }
        public string ChildDOB { get; set; }
        public string ChildPicture { get; set; }


        public string ChildName2 { get; set; }
        public string ChildPicture2 { get; set; }
        public string ChildName3 { get; set; }
        public string ChildPicture3 { get; set; }
        public string ChildName4 { get; set; }
        public string ChildPicture4 { get; set; }
        public string ChildName5 { get; set; }
        public string ChildPicture5 { get; set; }
    }
}