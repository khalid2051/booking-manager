﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Notification
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Key]
        public string NotificationId { get; set; }
        [Required]
        public string NotificationHeader { get; set; }
        [Required]
        public string NotificationText { get; set; }
        public DateTime NotificationTime { get; set; }
        public string NotificationLink { get; set; }
        public bool isSeen { get; set; }
        public string IdForRecognition { get; set; }
        public string DividendHolder { get; set; }
    }
}