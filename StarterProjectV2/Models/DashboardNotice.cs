﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class DashboardNotice
    {
        [Key]
        [Required]
        public string NoticeId { get; set; }
        [Required]
        public string NoticeType { get; set; }
        public DateTime Date { get; set; }
        public string Day { get; set; }
        public string PDFFilePath { get; set; }
        public string FileDisplayName { get; set; }
    }
}