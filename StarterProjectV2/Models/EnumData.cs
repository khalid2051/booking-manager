namespace StarterProjectV2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EnumData")]
    public partial class EnumData
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string EnumGroup { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }
    }
}
