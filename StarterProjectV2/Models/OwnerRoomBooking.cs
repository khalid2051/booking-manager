﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StarterProjectV2.Models
{
    public partial class OwnerRoomBooking
    {
        public OwnerRoomBooking()
        {
            Startdate = DateTime.Today;
            Enddate = DateTime.Today.AddDays(1);

            CheckInDate = DateTime.Today;
            CheckOutDate = DateTime.Today.AddDays(1);

            ApplicationDate = DateTime.Today;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime Startdate { get; set; }
        public DateTime Enddate { get; set; }
        public string Roomtype { get; set; }
        public string PreferredRoomNo { get; set; }
        public string OtherPreferences { get; set; }
        public string IsBookingCompleted { get; set; }
        [Key]
        public string OwnerRoomBookngId { get; set; }
        public string OBOwnerId { get; set; }
        public string SlotId { get; set; }
        public string GuestNameMain { get; set; }
        public string GuestNameTwo { get; set; }
        public string GuestNameThree { get; set; }
        public string GuestAddress { get; set; }
        public int NoOfAdult { get; set; } = 1;
        public int NoOfChild { get; set; } = 0;
        public string BookingNo { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string BookingToken { get; set; }
        public string GuestMobile { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public string HasStayedOrNot { get; set; }
        public int NoOfRoomRequested { get; set; } = 1;
        public int NoOfDaySpent { get; set; }
        public int NoOfDaysCanStay { get; set; }
        [DataType(DataType.Date)]
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public double ExtraCharges { get; set; } = 0;
        public double OwnerContribution { get; set; } = 0;
        public double HotelContribution { get; set; } = 0;
        public double TotalExpense { get; set; } = 0;
        public int NoOfRoomAllotted { get; set; } = 1;
        public bool CancelApproved { get; set; }
        public string FolioNo { get; set; }
        public string HasCheckedOutOrNot { get; set; }
    }

    public class BookingParameterVM
    {
        public BookingParameterVM()
        {
            Index = 1;
        }
        public string Searchfield { get; set; }
        public string BookingComplete { get; set; }
        public string CheckedIn { get; set; }
        public string CheckedOut { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int Index { get; set; }
        public string Type { get; set; }
        public string TokenAssigned { get; set; }

    }

    public partial class OwnerRoomBooking {
        public string Relation { get; set; }
    }

}