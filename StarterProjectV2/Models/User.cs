﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class User
    { 
        [Key]
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        public string UserDisplayName { get; set; }
        
        [Required]
        public string UserType { get; set; }

        [Required]
        public string UserEmail { get; set; }

        [Required]
        public string UserMobileNumber { get; set; }

        [Required]
        public string UserPicturePath { get; set; }

        public DateTime UserLastLoginTime { get; set; }

    }
}