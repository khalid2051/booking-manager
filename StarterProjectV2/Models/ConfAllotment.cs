using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StarterProjectV2.Models
{
    public class ConfAllotment
    {
        [Key]
        public int ConfAllotmentID { get; set; }
        [Required]
        public string SlotId { get; set; }
        public int MonthlyAllotment { get; set; }
        public int YearlyAllotment { get; set; }
        public DateTime CreatedAt { get; set; }
        public decimal NoOfSlot { get; set; }
    }
}