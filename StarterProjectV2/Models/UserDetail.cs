//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StarterProjectV2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserDetail
    {
        public string UserDetailsKey { get; set; }
        public string UserName { get; set; }
        public string ModuleName { get; set; }
        public string ModuleId { get; set; }
        public string TaskName { get; set; }
        public string TaskId { get; set; }
        public string Details { get; set; }
    }
}
