﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class OwnerOTPCode
    {
        [Key]
        [Required]
        public  string OwnerOTPCodeId { get; set; }

        [Required]
        public string OwnerId { get; set; }

        public string OwnerOTPSMSCode { get; set; }
        public string IsOTPSMSCodeValidated { get; set; }
        public string OwnerOTPEmailCode { get; set; }
        public string IsOTPEmailCodeValidated { get; set; }
    }
}