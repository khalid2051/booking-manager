﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class AccommodationInfo
    {
        [Key]
        [Required]
        public string AccommodationInfoId { get; set; }
        [Required]
        public string AccommodationDate { get; set; }
        public double MonthlyIncome { get; set; }
        public double MonthlyExpense { get; set; }
        public double MonthlyPerSquareFeetRate { get; set; }
        public int IsDayClosed { get; set; }
    }
}