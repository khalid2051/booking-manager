﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Audit
    {
        [Key]
        public string AuditId { get; set; }

        [Required]
        public string AuditDTTM { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string UserLoginDTTM { get; set; }

        [Required]
        public string UserLogoutDTTM { get; set; }

        [Required]
        public string ActionString { get; set; }

        [Required]
        public string TableName { get; set; }

        [Required]
        public string LogInIp { get; set; }

        [Required]
        public string TaskName { get; set; }

        [Required]
        public string ActionDetails { get; set; }
    }
}