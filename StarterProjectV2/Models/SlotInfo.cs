﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class SlotInfo
    {
        [Key]
        [Required]
        public string SlotId { get; set; }

        [Required]
        public string SlotName { get; set; }
        
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SlotOtherDefinitions { get; set; }
    }
}