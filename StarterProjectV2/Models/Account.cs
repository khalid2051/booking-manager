﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string AccountCode { get; set; }
        public string AccountHead { get; set; }
        public string AccountLabel { get; set; }
        public bool IsPayment { get; set; }
        public bool IsDefault { get; set; }
        public decimal DefaultAmount { get; set; }
        public bool IsEnabled { get; set; }
    }
}