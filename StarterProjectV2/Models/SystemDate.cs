﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StarterProjectV2.Models
{
    public class SystemDate
    {
        [Key]
        public int ID { get; set; }
        public DateTime Date { get; set; }
    }
}