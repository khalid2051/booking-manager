
/****** Object:  Table [dbo].[Settings] ******/
if Not Exists(select Id from Settings where SettingCode = 'RemoteLocation')
Insert into Settings (SettingCode, SettingName, SettingValue)
select 'RemoteLocation', 'RemoteLocation', 'F:\hams-heritage\OwnerSite\Content\notices\'

/****** Object:  Table [dbo].[OwnersPayments] ******/
IF COL_LENGTH(N'[dbo].[OwnersPayments]', 'FiscalPeriod') IS NULL
Alter Table OwnersPayments
Add FiscalPeriod nvarchar(50)

/****** Object:  Table [dbo].[OwnersPayments] ******/
alter table OwnersPayments
alter column PaymentDate datetime

alter table OwnersPayments
alter column FromMonth datetime

alter table OwnersPayments
alter column ToMonth datetime

/****** Object:  Table [dbo].[OwnersPayments] ******/
IF COL_LENGTH(N'[dbo].[OwnersPayments]', 'Id_new') IS NULL
Alter Table OwnersPayments
Add Id_new Int Identity(1, 1)

alter table OwnersPayments
drop constraint [PK_dbo.OwnersPayments]

Alter Table OwnersPayments Drop Column PaymentID

Exec sp_rename 'OwnersPayments.Id_new', 'PaymentID', 'Column'

alter table OwnersPayments
ADD CONSTRAINT [PK_dbo.OwnersPayments] PRIMARY KEY (PaymentID)

/****** Object:  Table [dbo].[OwnersPayments] ******/
IF COL_LENGTH(N'[dbo].[OwnersPayments]', 'Status') IS NULL

alter table OwnersPayments
add [Status] nvarchar(100)

/****** Object:  Table [dbo].[OwnerRoomBookings] ******/
IF COL_LENGTH(N'[dbo].[OwnerRoomBookings]', 'Relation') IS NULL

alter table OwnerRoomBookings
add Relation nvarchar(100)


/****** Object:  Table [dbo].[Accounts] ******/
update Accounts set AccountHead ='Privilege Enjoyed in Terms of Money', AccountLabel = ''
where AccountCode = 'HotelContribution'
