﻿using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using System.Text;
using System.IO;
using StarterProjectV2.Services;

namespace StarterProjectV2.Controllers
{
    public class ConfigurationController : Controller
    {
        private readonly CommonService _commonService = new CommonService();
        private readonly ConfigurationService _configurationService = new ConfigurationService();

        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        [HttpGet]
        public ActionResult AllotmentManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext _dbContext = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole(TaskPath.Configuration_AllotmentManagement);
                if (taskRoles == "" || taskRoles == "0")
                {
                    return View("Error");
                }

                var confAllotments = _dbContext.ConfAllotments.AsQueryable();
                if (!string.IsNullOrWhiteSpace(searchfield) && searchfield != "null")
                {
                    confAllotments = confAllotments.Where(l => l.SlotId.Contains(searchfield));
                }

                confAllotments = confAllotments.OrderBy(x => x.SlotId);

                int noOfPage = (confAllotments.Count() + 9) / 10;

                List<ConfAllotment> selectedAllotment = confAllotments.Skip((index - 1) * 10).Take(10).ToList();

                var model = new AllotmentManagementView()
                {

                    index = index,
                    noOfPage = noOfPage,
                    AllotmentList = selectedAllotment,
                    TaskRoles = taskRoles,
                    searchfield = searchfield
                };
                return View(model);
            }

        }
        [HttpGet]
        public ActionResult InventoryRoomManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                string taskId = db.Tasks.Where(u => u.TaskPath == "Configuration\\InventoryRoomManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "0")
                {
                    return View("Error");
                }

                ////////////////////////////
                var InventoryRoomList = db.ConfigInvRooms.ToList();
                List<ConfigInvRoom> InventoryObjList = new List<ConfigInvRoom>();
                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var room in InventoryRoomList)
                    {
                        int InventoryCount = 0;
                        if (room.InventoryCount != 0)
                        {
                            InventoryCount = room.InventoryCount;
                        }

                        DateTime Date = new DateTime();
                        if (room.ToDate != null)
                        {
                            Date = room.ToDate.Date;
                        }

                        DateTime UpdateDate = new DateTime();
                        if (room.FromDate != null)
                        {
                            UpdateDate = room.FromDate.Date;
                        }
                        string UpdateDate1 = UpdateDate.ToString();
                        var InventoryCount1 = InventoryCount.ToString();
                        var Date1 = Date.ToString();
                        //var AuditDTTM1 = AuditDTTM;

                        if (UpdateDate1.Contains(searchfield1) == true || InventoryCount1.Contains(searchfield1) == true || Date1.Contains(searchfield1) == true)
                        {
                            InventoryObjList.Add(room);
                        }
                    }
                }
                else
                {
                    InventoryObjList = InventoryRoomList;
                }
                List<ConfigInvRoom> selectedInventoryRoom = new List<ConfigInvRoom>();
                int noOfPage;
                if (InventoryObjList.Count >= 10)
                {
                    noOfPage = (InventoryObjList.Count < (index * 10)) ? InventoryObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < noOfPage; i++)
                    {
                        selectedInventoryRoom.Add(InventoryObjList.ElementAt(i));
                    }

                    noOfPage = (InventoryRoomList.Count + 9) / 10;
                }
                else
                {
                    selectedInventoryRoom = InventoryObjList;
                    noOfPage = 1;
                }
                var model = new InventoryRoomManagementView()
                {

                    index = index,
                    noOfPage = noOfPage,
                    RoomList = selectedInventoryRoom,
                    TaskRoles = taskRoles,
                    searchfield = searchfield

                };
                return View(model);

            }

        }
        [HttpGet]
        public ActionResult OwnersContributionManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                string taskId = db.Tasks.Where(u => u.TaskPath == "Configuration\\OwnersContributionManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "0")
                {
                    return View("Error");
                }

                ////////////////////////////
                var OwnersContributionList = db.ConfOwnersContributions.ToList();
                List<ConfOwnersContribution> ContributionObjList = new List<ConfOwnersContribution>();
                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var audit in OwnersContributionList)
                    {
                        DateTime StartDate = new DateTime();
                        if (audit.StartDate != null)
                        {
                            StartDate = (DateTime)audit.StartDate;
                        }


                        DateTime EndDate = new DateTime();
                        if (audit.EndDate != null)
                        {
                            EndDate = (DateTime)audit.EndDate;
                        }

;
                        var EndDate1 = EndDate;
                        string date1 = EndDate1.ToString();
                        var StartDate1 = StartDate;
                        string date = StartDate1.ToString();
                        //var AuditDTTM1 = AuditDTTM;

                        if (date1.Contains(searchfield1) == true || date.Contains(searchfield1) == true)
                        {
                            ContributionObjList.Add(audit);
                        }
                    }
                }
                else
                {
                    ContributionObjList = OwnersContributionList;
                }
                List<ConfOwnersContribution> selectedOwnersContribution = new List<ConfOwnersContribution>();

                int noOfPage;
                if (ContributionObjList.Count >= 10)
                {
                    noOfPage = (ContributionObjList.Count < (index * 10)) ? ContributionObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < noOfPage; i++)
                    {
                        selectedOwnersContribution.Add(ContributionObjList.ElementAt(i));
                    }

                    noOfPage = (OwnersContributionList.Count + 9) / 10;
                }
                else
                {
                    selectedOwnersContribution = ContributionObjList;
                    noOfPage = 1;
                }
                var model = new OwnersContributionManagementView()
                {

                    index = index,
                    noOfPage = noOfPage,
                    OwnersContributionLIST = selectedOwnersContribution,
                    TaskRoles = taskRoles,
                    searchfield = searchfield

                };
                return View(model);
            }

        }
        [HttpGet]
        public ActionResult OwnerAvgRoomRateManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext _dbContext = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole(TaskPath.Configuration_OwnerAvgRoomRateManagement);
                if (taskRoles == "0")
                {
                    return View("Error");
                }

                var confOwnerAvgRoomRates = _dbContext.ConfOwnerAvgRoomRates.AsQueryable();
                if (!string.IsNullOrWhiteSpace(searchfield))
                {
                    confOwnerAvgRoomRates = confOwnerAvgRoomRates.Where(l => l.ApplicableDate.ToString().Contains(searchfield));
                }

                confOwnerAvgRoomRates = confOwnerAvgRoomRates.OrderByDescending(x => x.ApplicableDate);

                int noOfPage = (confOwnerAvgRoomRates.Count() + 9) / 10;

                List<ConfOwnerAvgRoomRate> selectedOwnerAvgRoomRateList = confOwnerAvgRoomRates.Skip((index - 1) * 10).Take(10).ToList();

                var model = new OwnerAvgRoomRateManagementView()
                {

                    index = index,
                    noOfPage = noOfPage,
                    OwnerAvgRoomRateList = selectedOwnerAvgRoomRateList,
                    TaskRoles = taskRoles,
                    searchfield = searchfield
                };

                return View(model);
            }

        }
        [HttpGet]
        public ActionResult OwnerAvgRoomRateCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole("Configuration\\OwnerAvgRoomRateManagement");
                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var applicableDate = _configurationService.GetSystemDate().Date;
                var dataModel = adbc.ConfOwnerAvgRoomRates.Where(a => a.ApplicableDate == applicableDate).FirstOrDefault();

                var model = new OwnerAvgRoomRateView()
                {
                    ApplicableDate = dataModel?.ApplicableDate ?? applicableDate,
                    AverageRoomRate = dataModel?.AverageRoomRate ?? 0
                };

                return View("OwnerAvgRoomRateCreate", model);
            }
        }
        [HttpGet]
        public ActionResult OwnerAvgRoomRateView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            return RedirectToAction("OwnerAvgRoomRateManagement", "Configuration");
        }
        [HttpPost]
        public ActionResult OwnerAvgRoomRateAddorEdit(OwnerAvgRoomRateView info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var model = adbc.ConfOwnerAvgRoomRates.Where(a => a.ApplicableDate == info.ApplicableDate).FirstOrDefault();

                if (model == null)
                {
                    model = new ConfOwnerAvgRoomRate();
                    adbc.ConfOwnerAvgRoomRates.Add(model);
                }

                model.AverageRoomRate = info.AverageRoomRate;
                model.ApplicableDate = info.ApplicableDate;

                adbc.SaveChanges();
            };

            ////Redirection is required since this action is called from several views
            if (!string.IsNullOrWhiteSpace(TempData["Action"] as string))
            {
                return RedirectToAction(TempData["Action"] as string, TempData["Controller"] as string);
            }

            return RedirectToAction("OwnerAvgRoomRateManagement");
        }

        [HttpGet]
        public ActionResult OwnerAvgRoomRateEdit(int ConfOwnerAvgRoomRateID)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\OwnerAvgRoomRateManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }
                var avgroomrate = adbc.ConfOwnerAvgRoomRates.FirstOrDefault(c => c.ConfOwnerAvgRoomRateID == ConfOwnerAvgRoomRateID);
                var model = new OwnerAvgRoomRateView()
                {
                    ConfOwnerAvgRoomRateID = avgroomrate.ConfOwnerAvgRoomRateID,
                    AverageRoomRate = avgroomrate.AverageRoomRate,

                    ApplicableDate = avgroomrate.ApplicableDate,

                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult OwnerAvgRoomRateEdit(OwnerAvgRoomRateView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var OwnerAvgRoomRateObj = adbc.ConfOwnerAvgRoomRates.Where(a => a.ConfOwnerAvgRoomRateID == mdl.ConfOwnerAvgRoomRateID).FirstOrDefault();
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\OwnerAvgRoomRateManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                OwnerAvgRoomRateObj.AverageRoomRate = mdl.AverageRoomRate;
                OwnerAvgRoomRateObj.ApplicableDate = mdl.ApplicableDate;

                adbc.SaveChanges();

                //var TickerList = adbc.Tickers.ToList();


                return RedirectToAction("OwnerAvgRoomRateManagement", "Configuration");
            }
        }
        /////////////Allotment////////////////
        [HttpGet]
        public ActionResult AllotmentCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\AllotmentManagement").Select(u => u.TaskId)
                    .FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }

                var model = new AllotmentView()
                {
                    ConfAllotmentID = 0,

                };
                return View("AllotmentCreate", model);
            }
        }
        [HttpGet]
        public ActionResult AllotmentView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            return RedirectToAction("AllotmentManagement", "Configuration");
        }
        [HttpPost]
        public ActionResult AllotmentAddorEdit(AllotmentView info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var previousAllotment = adbc.ConfAllotments.Where(o => o.SlotId == info.SlotId).ToList();
                if (previousAllotment.Count == 0)
                {
                    ConfAllotment model = new ConfAllotment()
                    {
                        //ConfAllotmentID = info.ConfAllotmentID,
                        SlotId = info.SlotId,
                        NoOfSlot = info.NoOfSlot,
                        MonthlyAllotment = info.MonthlyAllotment,
                        YearlyAllotment = info.YearlyAllotment,
                        CreatedAt = DateTime.Now,
                    };
                    adbc.ConfAllotments.Add(model);

                    adbc.SaveChanges();

                    return RedirectToAction("AllotmentManagement");
                }
                else
                {
                    return RedirectToAction("AllotmentCreate");
                }

            }
        }
        [HttpGet]
        public ActionResult AllotmentEdit(int ConfAllotmentID)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\AllotmentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }
                var allotment = adbc.ConfAllotments.FirstOrDefault(a => a.ConfAllotmentID == ConfAllotmentID);
                var model = new AllotmentView()
                {
                    ConfAllotmentID = allotment.ConfAllotmentID,
                    SlotId = allotment.SlotId,
                    NoOfSlot = allotment.NoOfSlot,
                    MonthlyAllotment = allotment.MonthlyAllotment,
                    YearlyAllotment = allotment.YearlyAllotment,
                    CreatedAt = allotment.CreatedAt,
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult AllotmentEdit(AllotmentView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var AllotmentObj = adbc.ConfAllotments.Where(a => a.ConfAllotmentID == mdl.ConfAllotmentID).FirstOrDefault();
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\AllotmentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                AllotmentObj.SlotId = mdl.SlotId;
                AllotmentObj.NoOfSlot = mdl.NoOfSlot;
                AllotmentObj.MonthlyAllotment = mdl.MonthlyAllotment;
                AllotmentObj.YearlyAllotment = mdl.YearlyAllotment;
                AllotmentObj.CreatedAt = DateTime.Now;
                adbc.SaveChanges();



                return RedirectToAction("AllotmentManagement", "Configuration");
            }
        }


        ////////////Owner Contribution//////////////////
        [HttpGet]
        public ActionResult OwnersContributionCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\OwnersContributionManagement").Select(u => u.TaskId)
                    .FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }

                var model = new OwnersContributionView()
                {
                    ConfOwnersContributionID = 0,

                };
                return View("OwnersContributionCreate", model);
            }
        }
        [HttpGet]
        public ActionResult OwnersContributionView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            return RedirectToAction("OwnersContributionManagement", "Configuration");
        }
        [HttpPost]
        public ActionResult OwnersContributionAddorEdit(OwnersContributionView info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var AllotmentList = adbc.ConfOwnersContributions.ToList();

                ConfOwnersContribution model = new ConfOwnersContribution()
                {
                    ConfOwnersContributionID = info.ConfOwnersContributionID,
                    Amount = info.Amount,
                    StartDate = info.StartDate,
                    EndDate = info.EndDate,
                    CreatedAt = DateTime.Now,


                };
                adbc.ConfOwnersContributions.Add(model);

                adbc.SaveChanges();

                return RedirectToAction("OwnersContributionManagement");

            }
        }
        [HttpGet]
        public ActionResult OwnersContributionEdit(int ConfOwnersContributionID)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\OwnersContributionManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }
                var contribution = adbc.ConfOwnersContributions.FirstOrDefault(c => c.ConfOwnersContributionID == ConfOwnersContributionID);
                var model = new OwnersContributionView()
                {

                    ConfOwnersContributionID = contribution.ConfOwnersContributionID,
                    Amount = contribution.Amount,
                    StartDate = contribution.StartDate,
                    EndDate = contribution.EndDate,
                    CreatedAt = contribution.CreatedAt,

                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult OwnersContributionEdit(OwnersContributionView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var OwnersContributionObj = adbc.ConfOwnersContributions.Where(a => a.ConfOwnersContributionID == mdl.ConfOwnersContributionID).FirstOrDefault();
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\OwnersContributionManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                OwnersContributionObj.Amount = mdl.Amount;
                OwnersContributionObj.StartDate = mdl.StartDate;
                OwnersContributionObj.EndDate = mdl.EndDate;
                OwnersContributionObj.CreatedAt = DateTime.Now;
                adbc.SaveChanges();

                //var TickerList = adbc.Tickers.ToList();


                return RedirectToAction("OwnersContributionManagement", "Configuration");
            }
        }
        [HttpGet]
        public ActionResult BlogPageAutoApproval()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Configuration\\OwnerAvgRoomRateManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "0")
                {
                    return View("Error");
                }
                var firstRow = db.ConfPostCommentApprovals.FirstOrDefault();
                var confPostApproval = firstRow.PostAutoApprovalEnabled;
                var confCommentApproval = firstRow.CommentAutoApprovalEnabled;
                var modifiedAt = firstRow.ModifiedAt.ToString();
                var model = new ConfPostCommentApprovalView()
                {
                    PostApproval = confPostApproval,
                    CommentApproval = confCommentApproval,
                    ModifiedAt = modifiedAt,
                    TaskRoles = taskRoles
                };
                return View(model);
            }

        }
        [HttpPost]
        public ActionResult BlogPageAutoApprovalEdit(ConfPostCommentApprovalView model)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var selectedRow = adbc.ConfPostCommentApprovals.FirstOrDefault();
                selectedRow.PostAutoApprovalEnabled = model.PostApproval;
                selectedRow.CommentAutoApprovalEnabled = model.CommentApproval;
                selectedRow.ModifiedAt = DateTime.Now;
                adbc.SaveChanges();
            };
            return RedirectToAction("BlogPageAutoApproval");
        }

        [HttpGet]
        public ActionResult InventoryRoomCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\InventoryRoomManagement").Select(u => u.TaskId)
                    .FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }

                var model = new InventoryView()
                {
                    ConfInventoryRoomID = 0,

                };
                return View("InventoryRoomCreate", model);
            }
        }
        [HttpGet]
        public ActionResult InventoryRoomView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            return RedirectToAction("InventoryRoomManagement", "Configuration");
        }
        [HttpPost]
        public ActionResult InventoryAddorEdit(InventoryView info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var AllotmentList = adbc.ConfigInvRooms.ToList();

                ConfigInvRoom model = new ConfigInvRoom()
                {
                    ConfInventoryRoomID = info.ConfInventoryRoomID,
                    InventoryCount = info.InventoryCount,
                    FromDate = info.FromDate,
                    ToDate = info.ToDate,
                    CreatedAt = DateTime.Now,
                    IsArchived = info.IsArchived,

                };
                adbc.ConfigInvRooms.Add(model);

                adbc.SaveChanges();

                return RedirectToAction("InventoryRoomManagement");

            }
        }

        [HttpGet]
        public ActionResult InventoryRoomEdit(int ConfInventoryRoomID)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\InventoryRoomManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }
                var inventory = adbc.ConfigInvRooms.FirstOrDefault(a => a.ConfInventoryRoomID == ConfInventoryRoomID);
                var model = new InventoryView()
                {

                    ConfInventoryRoomID = inventory.ConfInventoryRoomID,
                    InventoryCount = inventory.InventoryCount,
                    FromDate = inventory.FromDate,
                    ToDate = inventory.ToDate,
                    CreatedAt = inventory.CreatedAt,
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult InventoryRoomEdit(InventoryView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var ConfigInvRoomObj = adbc.ConfigInvRooms.Where(a => a.ConfInventoryRoomID == mdl.ConfInventoryRoomID).FirstOrDefault();
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\InventoryRoomManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                ConfigInvRoomObj.InventoryCount = mdl.InventoryCount;
                ConfigInvRoomObj.FromDate = mdl.FromDate;
                ConfigInvRoomObj.ToDate = mdl.ToDate;
                ConfigInvRoomObj.CreatedAt = DateTime.Now;
                adbc.SaveChanges();
                return RedirectToAction("InventoryRoomManagement", "Configuration");
            }
        }
        //[HttpGet]
        //public ActionResult CompanyManagement(string searchfield = "", int index = 1)
        //{
        //    if (!IsUserLoggedIn())
        //        return RedirectToAction("Login", "Accounts");
        //    using (ApplicationDbContext db = new ApplicationDbContext())
        //    {

        //        string taskId = db.Tasks.Where(u => u.TaskPath == "Configuration\\AllotmentManagement").Select(u => u.TaskId).FirstOrDefault();
        //        var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

        //        string taskRoles = "";
        //        foreach (var tasks in myRoleList)
        //        {
        //            if (tasks.TaskId == taskId)
        //            {
        //                taskRoles = tasks.Details;
        //            }
        //        }
        //        if (taskRoles == "0")
        //        {
        //            return View("Error");
        //        }

        //        ////////////////////////////
        //        var AllotmentAllList = db.CompanyInfoes.ToList();
        //        List<CompanyInfoes> AllotmentObjList = new List<CompanyInfoes>();
        //        if (!String.IsNullOrEmpty(searchfield))
        //        {
        //            var searchfield1 = searchfield.ToUpper();
        //            foreach (var audit in AllotmentAllList)
        //            {
        //                var UserId = "";
        //                if (audit.SlotId != null)
        //                {
        //                    UserId = audit.SlotId.ToUpper();
        //                }


        //                DateTime UpdateDate = new DateTime();
        //                if (audit.CreatedAt != null)
        //                {
        //                    UpdateDate = (DateTime)audit.CreatedAt;
        //                }


        //                var UserId1 = UserId;

        //                var UpdateDate1 = UpdateDate;
        //                string date = UpdateDate1.ToString();
        //                //var AuditDTTM1 = AuditDTTM;

        //                if (UserId1.Contains(searchfield1) == true || date.Contains(searchfield1) == true)
        //                {
        //                    AllotmentObjList.Add(audit);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            AllotmentObjList = AllotmentAllList;
        //        }
        //        //Search Finished
        //        //Pagination Start
        //        List<ConfAllotment> selectedAllotment = new List<ConfAllotment>();
        //        int noOfPage;
        //        if (AllotmentObjList.Count >= 10)
        //        {
        //            noOfPage = (AllotmentObjList.Count < (index * 10)) ? AllotmentObjList.Count : (index * 10);
        //            for (int i = index * 10 - 10; i < noOfPage; i++)
        //            {
        //                selectedAllotment.Add(AllotmentObjList.ElementAt(i));
        //            }

        //            noOfPage = (AllotmentAllList.Count + 9) / 10;
        //        }
        //        else
        //        {
        //            selectedAllotment = AllotmentObjList;
        //            noOfPage = 1;
        //        }
        //        var model = new AllotmentManagementView()
        //        {

        //            index = index,
        //            noOfPage = noOfPage,
        //            AllotmentList = selectedAllotment,
        //            TaskRoles = taskRoles,
        //            searchfield = searchfield
        //        };
        //        return View(model);
        //    }

        //}
        //[HttpGet]
        //public ActionResult CompanyCreate()
        //{
        //    if (!IsUserLoggedIn())
        //        return RedirectToAction("Login", "Accounts");
        //    using (ApplicationDbContext adbc = new ApplicationDbContext())
        //    {
        //        string taskId = adbc.Tasks.Where(u => u.TaskPath == "Configuration\\OwnerAvgRoomRateManagement").Select(u => u.TaskId)
        //            .FirstOrDefault();
        //        var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

        //        string taskRoles = "";
        //        foreach (var tasks in myRoleList)
        //        {
        //            if (tasks.TaskId == taskId)
        //            {
        //                taskRoles = tasks.Details;
        //            }
        //        }
        //        if (taskRoles[1] == '0')
        //        {
        //            return View("Error");
        //        }

        //        var model = new OwnerAvgRoomRateView()
        //        {
        //            ConfOwnerAvgRoomRateID = 0,
        //            //TickerTypeList = TickerTypeList,
        //        };
        //        return View("OwnerAvgRoomRateCreate", model);
        //    }
        //}
    }
}