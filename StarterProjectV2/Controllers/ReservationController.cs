﻿using CrystalDecisions.CrystalReports.Engine;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Ajax.Utilities;
using OfficeOpenXml;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = iTextSharp.text.Image;

namespace StarterProjectV2.Controllers
{
    public class ReservationController : Controller
    {
        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        [HttpGet]
        //OwnerRoomBooking Management for  Current Bookings
        public ActionResult ReferralBookingManagement(string searchfield = "", int index = 1, string type="Current and Future")
        {
            if (!Common.IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Reservation\\ReferralBookingManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                        return View("Error");
                }
                else return View("Error");


                List<ReferralBooking> ReferralBookingsAll = db.ReferralBookings.ToList();
                List<ReferralBooking> ReferralBookingHistory =
                    ReferralBookingsAll.Where(r => r.RequestedEndDate <= DateTime.Today || r.IsBookingCompleted == "Cancelled" || (r.CheckInDate < DateTime.Today && r.IsBookingCompleted == "No")).ToList();
                var ReferralBookingListCurrentList = ReferralBookingHistory;
                if (type == "Current and Future")
                    ReferralBookingListCurrentList = ReferralBookingsAll.Except(ReferralBookingHistory).ToList();
                SortedDictionary<int, ReferralBooking> ReferralBookingCurrentDictionary = new SortedDictionary<int, ReferralBooking>();

                foreach (var referralBookingCurrent in ReferralBookingListCurrentList)
                {
                    int key = int.Parse(referralBookingCurrent.ReferralBookingId);
                    ReferralBookingCurrentDictionary.Add(key, referralBookingCurrent);
                }

                Dictionary<int, ReferralBooking> ReferralBookingCurrentDictionaryReverse = new Dictionary<int, ReferralBooking>();

                foreach (var referralKeyValuePair in ReferralBookingCurrentDictionary.Reverse())
                {
                    ReferralBookingCurrentDictionaryReverse.Add(referralKeyValuePair.Key, referralKeyValuePair.Value);
                }

                var BookingObjList = ReferralBookingCurrentDictionaryReverse.Values.ToList();

                List<ReferralBooking> selectedBookingsSearched = new List<ReferralBooking>();

                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var booking in BookingObjList)
                    {
                        var UserId = "";
                        if (booking.SlotId != null)
                        {
                            UserId = booking.SlotId.ToUpper();
                        }
                        var Name = "";
                        if (booking.MainGuestName != null)
                        {
                            Name = booking.MainGuestName.ToUpper();
                        }
                        var Token = "";
                        if (booking.BookingToken != null)
                        {
                            Token = booking.BookingToken.ToUpper();
                        }


                        if (UserId.Contains(searchfield1) == true || Name.Contains(searchfield1) == true || Token.Contains(searchfield1) == true)
                        {
                            selectedBookingsSearched.Add(booking);
                        }
                    }
                }
                else
                {
                    selectedBookingsSearched = BookingObjList;
                }

                List<ReferralBooking> selectedBookings = new List<ReferralBooking>();
                int noOfBooking;
                if (selectedBookingsSearched.Count >= 10)
                {
                    var pageNo = (selectedBookingsSearched.Count < (index * 10)) ? selectedBookingsSearched.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedBookings.Add(selectedBookingsSearched.ElementAt(i));
                    }
                    noOfBooking = ((selectedBookingsSearched.Count + 9) / 10);
                }
                else
                {
                    selectedBookings = selectedBookingsSearched;
                    noOfBooking = 1;
                }
                var ViewTypes = new List<string>();
                ViewTypes.Add("Coming");
                ViewTypes.Add("Past");
                var model = new ReferrelRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = 1,
                    SearchField = searchfield,
                    ViewTypes = ViewTypes,
                    ViewType = type
                };
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult ReferralBookingApprove(string ReferralBookingId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Reservation\\ReferralBookingManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[8] == '0')
                {
                    return View("Error");
                }

          

                var info = adbc.ReferralBookings.Where(p => p.ReferralBookingId == ReferralBookingId).SingleOrDefault();

               

                var model = new ReferrelRoomBookingManagementView()
                {
                    //OwenerId = info.OwenerId,
                    IsReferralPointApproved = false,
                     //IsReferralPointApprovedList = new List<bool>()

                 
                };
                return View(model);

            }
        }
        [HttpPost]
        public ActionResult ReferralBookingApprove(ReferralBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var referralRoomBooking = adbc.ReferralBookings.Where(o => o.ReferralBookingId == info.ReferralBookingId).FirstOrDefault();
                referralRoomBooking.ReferralPoint = info.ReferralPoint;
                referralRoomBooking.IsReferralPointApproved = info.IsReferralPointApproved;
      
                adbc.SaveChanges();
                return RedirectToAction("ReferralBookingManagement");
            }

        }
    }
}
