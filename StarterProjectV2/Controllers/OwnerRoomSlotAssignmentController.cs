﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;

namespace StarterProjectV2.Controllers
{
    public class OwnerRoomSlotAssignmentController : Controller
    {
        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        // GET: OwnerRoomSlotAssignment
        [HttpGet]
        public ActionResult OwnerRoomSlotManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnerRoomSlotAssignment\\OwnerRoomSlotManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }


                var model = new OwnerRoomSlotManagementView
                {
                    OwnerRoomSlotList = db.OwnerRoomSlotAssigments.ToList(),
                    //TaskRoles = taskRoles
                };
                return View(model);
            }
        }
  
        public PartialViewResult SearchRoomSlot(string searchfield)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var OwnerRoomSlotManagementView = db.OwnerRoomSlotAssigments.ToList();
                List<OwnerRoomSlotAssignment> searchedRoomSlot = new List<OwnerRoomSlotAssignment>();
                var searchfield1 = "";
                if (!String.IsNullOrEmpty(searchfield)){
                    searchfield1 = searchfield.ToUpper();
                }
                foreach (var slot in OwnerRoomSlotManagementView)
                {

                    var OwnerRoomSlotAssignmentId = "";
                    if (slot.OwnerRoomSlotAssignmentId != null)
                    {
                        OwnerRoomSlotAssignmentId = slot.OwnerRoomSlotAssignmentId.ToUpper();
                    }
                    var OwnersInfoId = "";
                    if (slot.OwnersInfoId != null)
                    {
                        OwnersInfoId = slot.OwnersInfoId.ToUpper();
                    }
                  
                    var RoomSlotAssigmentId = "";
                    if (slot.RoomSlotAssigmentId != null)
                    {
                        RoomSlotAssigmentId = slot.RoomSlotAssigmentId.ToUpper();
                    }
                    //var Date1 = Date;
                    var OwnerRoomSlotAssignmentId1 = OwnerRoomSlotAssignmentId;
                    var OwnersInfoId1 = OwnersInfoId;
                    var RoomSlotAssigmentId1 = RoomSlotAssigmentId;
                  

                    if (OwnerRoomSlotAssignmentId1.Contains(searchfield1) == true ||
                        OwnersInfoId1.Contains(searchfield1) == true || RoomSlotAssigmentId1.Contains(searchfield1) == true)

                    {
                        searchedRoomSlot.Add(slot);
                    }

                }
                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnerRoomSlotAssignment\\OwnerRoomSlotManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    //return View(Json("Error"));
                }
                return PartialView("GridView", searchedRoomSlot);

            }
        }
        [HttpGet]
        public ActionResult OwnerRoomSlotCreate(OwnerRoomSlotManagementView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var mdl = new OwnerRoomSlotManagementView
                {
                    Rooms = db.RoomInfos.ToList(),
                    Slots = db.SlotInfos.ToList(),
                    Owners = db.OwnersInfos.ToList()
                    //ORSAOwnerId = model.OwenerId
                };
                return View(mdl);
            }
        }
        [HttpPost]
        public ActionResult OwnersRoomSlotInfo(OwnerRoomSlotManagementView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                //string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\OwnersRoomSlotInfo").Select(u => u.TaskId).FirstOrDefault();
                //var roomslotid = db.RoomSlotAssigments.Select(r => r.RoomId == model.RoomId && r => r.RoomId == model.)
                var ownersRoomSlot = adbc.RoomSlotAssigments;
                var s = model.RoomId.ToString() + "," + model.SlotId.ToString();
                var roomslotindb = adbc.RoomSlotAssigments.SingleOrDefault(r => r.RoomSlotText.Contains(s));
                roomslotindb.SoldStatus = "Sold";
                //adbc.SaveChanges();





                var SingleOwnersRoomSlots = adbc.OwnerRoomSlotAssigments.ToList();
                //creating a new OwnerRoomSlotAssignment Row 
                var OwnerRoomSlotAssId = "";


                if (!string.IsNullOrEmpty(model.OwnerId))
                {
                    var ownerList = adbc.OwnerRoomSlotAssigments.ToList();
                    var ownerIdList = new List<int>();
                    foreach (var owners in ownerList)
                    {
                        ownerIdList.Add(int.Parse(owners.OwnerRoomSlotAssignmentId.ToString().Split('-')[1]));
                    }

                    var ownerToken = ownerList.Count();
                    if (ownerToken > 0)
                    {
                        ownerIdList.Sort();
                        ownerToken = ownerIdList[ownerIdList.Count - 1];
                    }

                    ownerToken++;


                    OwnerRoomSlotAssId = "ORSA-" + ownerToken;
                    OwnerRoomSlotAssignment mdl = new OwnerRoomSlotAssignment()
                    {
                        //mdl.RoomSlotAssignmentId = OwnerRoomSlotAssId;
                        OwnerRoomSlotAssignmentId = OwnerRoomSlotAssId,
                        OwnersInfoId = model.OwnerId,
                        //OwnersInfoId = "OWNER-6",
                        RoomSlotAssigmentId = roomslotindb.RoomSlotAssignmentId,
                        //Rate = (float)100000.0
                        Rate = (float)(model.Rate)
                    };
                    adbc.OwnerRoomSlotAssigments.Add(mdl);
                    adbc.SaveChanges();
                    return RedirectToAction("OwnerRoomSlotManagement", "OwnerRoomSlotAssignment");
                }


            }
            return RedirectToAction("OwnerRoomSlotManagement", "OwnerRoomSlotAssignment");
        }
        [HttpGet]
        public ActionResult OwnerRoomSlotView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            return View();
        }
        [HttpPost]
        public ActionResult OwnerRoomSlotDelete(string ownerroomslotId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var RoomSlotInDb = adbc.OwnerRoomSlotAssigments.Where(r => r.OwnerRoomSlotAssignmentId == ownerroomslotId)
                    .FirstOrDefault();
                adbc.OwnerRoomSlotAssigments.Remove(RoomSlotInDb);
                adbc.SaveChanges();

                return RedirectToAction("OwnerRoomSlotManagement");
            }
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult gettingSlotList(string roomId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var slotList = adbc.RoomSlotAssigments.Where(m => m.RoomId == roomId).ToList();
                List<SelectListItem> someArray = new List<SelectListItem>();
                foreach (var x in slotList)
                {
                    var nm = new SelectListItem()
                    {
                        Text = adbc.SlotInfos.Where(s => s.SlotId == x.SlotId).Select(s => s.SlotName).FirstOrDefault(),
                        Value = x.SlotId
                    };
                    someArray.Add(nm);
                }

                return Json(someArray, JsonRequestBehavior.AllowGet);
            }

        }


    }
}