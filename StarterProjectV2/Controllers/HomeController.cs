﻿//using SignalRDemo;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;

namespace StarterProjectV2.Controllers
{
    public class HomeController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        public ActionResult Hello()
        {
            return View();
        }

        public ActionResult Index()
        {
            if (IsUserLoggedIn())
            {
                //return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
                return RedirectToAction("About");
            }
            else
            {

            }
            return View();
        }
        private bool IsPermittedLink(string taskPath)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == taskPath).Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                //string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        if (tasks.Details[0] == '1')
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
        public ActionResult About()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            ViewBag.Message = "Your application description page.";

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                //DateTime checkindate = DateTime.Now;
                DateTime checkindate = adbc.SystemDates.Select(u => u.Date).FirstOrDefault();
                var month = checkindate.Month;
                var year = checkindate.Year;
                DateTime checkoutdate = DateTime.Now.AddDays(-1);

                var allOwnerRoomBooking = adbc.OwnerRoomBookings.ToList();
                var a = adbc.OwnerRoomBookings.ToList();
                //var list = list2.Where(b => b.CheckOutDate.Date == checkoutdate.Date).Select().Sum(w => (int)w["NoOfRoomAllotted"]);
                var list = a.Where(b => b.CheckInDate.Date <= checkoutdate.Date && b.CheckOutDate.Date >= checkoutdate.Date).Sum(b => b.NoOfRoomAllotted);
                var c = a.Where(b => b.CheckInDate.Date == checkindate.Date).ToList();
                int list5 = c.Count();
                var d = a.Where(b => b.CheckOutDate.Date == checkindate.Date).ToList();
                int list6 = d.Count();
                var e = a.Where(b => b.CheckInDate.Date <= checkindate.Date && b.CheckOutDate.Date > checkindate.Date).ToList();///today checkouts are excluded
                int list7 = e.Count();
                var todayoccupiedrooms = e.Sum(v => v.NoOfRoomAllotted);
                var list1 = a.Where(b => b.CheckInDate.Month <= month && b.CheckOutDate.Month >= month && b.CheckInDate.Year <= year && b.CheckOutDate.Year >= year).Sum(b => b.NoOfRoomAllotted);///today checkouts are included
                var list2 = a.Where(b => b.CheckInDate.Year <= year && b.CheckOutDate.Year >= year).Sum(b => b.NoOfRoomAllotted);
                var f = a.Where(b => b.CheckInDate.Date > checkindate.Date && b.CheckInDate.Year == year && b.CheckInDate.Month == month).ToList();///today checkouts are included
                int list3 = f.Count();
                var g = a.Where(b => b.CheckInDate.Date > checkindate.Date && b.CheckInDate.Year == year).ToList();///today checkouts are included
                int list4 = g.Count();
                var rooms = adbc.ConfigInvRooms.ToList();
                var inventorycount = rooms.Where(u => u.FromDate.Date <= checkindate.Date && u.ToDate.Date >= checkindate.Date).Select(u => u.InventoryCount).LastOrDefault();
                var roomsAvailableToday = inventorycount - todayoccupiedrooms;

                bool[] linkPermissions = new bool[10]; //Array instead of List
                for (int i = 0; i < 10; i++)
                {
                    linkPermissions[i] = false;
                }
                //First Flag = User Management

                linkPermissions[0] = IsPermittedLink("OwnersInfo\\SinglePageOwnersInfoManagement");
                linkPermissions[1] = IsPermittedLink("OwnersInfo\\CalendarView");
                linkPermissions[2] = IsPermittedLink("OwnersInfo\\OwnerManagement");
                linkPermissions[3] = IsPermittedLink("UserDetails\\UserdetailsManagement");
                linkPermissions[4] = IsPermittedLink("OwnersInfo\\DayCloseManagement");
                linkPermissions[5] = IsPermittedLink("OwnersInfo\\OwnerPostManagement");
                linkPermissions[6] = IsPermittedLink("Dividend\\PendingView");
                linkPermissions[7] = IsPermittedLink("Booking\\OwnerRoomBookingManagement");
                //linkPermissions[0] = IsPermittedLink("UserDetails\\UserDetailsManagement");
                //linkPermissions[1] = IsPermittedLink("Audit\\AuditManagement");
                DashboardViewModel model = new DashboardViewModel()
                {
                    RoomsOccupiedLastNight = list,
                    TodayArrival = list5,
                    TodayCheckOut = list6,
                    TodayStayOver = list7,
                    RoomsOccupiedThisMonth = list1,
                    RoomsOccupiedThisYear = list2,
                    NoOfFutureBookingThisMonth = list3,
                    NoOfFutureBookingThisYear = list4,
                    InventoryCount = inventorycount,
                    OccupiedRoomsToday = todayoccupiedrooms,
                    AvailableRoomsToday = roomsAvailableToday,
                    LinkPermissions = linkPermissions
                };
                return View(model);
            }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult GetNotifications()
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var list = adbc.Notifications.Where(n => n.isSeen == false).ToList();
                List<Notification> templist = new List<Notification>();
                foreach (Notification notification in list)
                {
                    if (notification.NotificationHeader != "Dividend" && notification.NotificationHeader != "Notice")
                    {
                        templist.Add(notification);
                    }
                }
                SortedDictionary<int, Notification> NotificationList = new SortedDictionary<int, Notification>();

                foreach (var notification in templist)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    NotificationList.Add(key, notification);
                }
                Dictionary<int, Notification> NotificationListReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in NotificationList.Reverse())
                {
                    NotificationListReverse.Add(notificationKeyValuePair.Key, notificationKeyValuePair.Value);
                }
                //adbc.SaveChanges();
                return Json(NotificationListReverse.Values.ToList());
            }
        }

        public JsonResult UpdateNotification(string notificationId)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var NotificationInDb =
                    adbc.Notifications.Where(n => n.NotificationId == notificationId).FirstOrDefault();
                NotificationInDb.isSeen = true;
                adbc.SaveChanges();
                return Json("nothing");
            }
        }


        ///Have to Handle Cancelled Booking Notifications
        [HttpGet]
        public ActionResult NotificationView(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                List<Notification> NotificationObjList = new List<Notification>();

                var UnseenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == false && n.NotificationHeader != "Dividend" && n.NotificationHeader != "Notice").ToList();

                SortedDictionary<int, Notification> UnseenNotificationsListForSorting = new SortedDictionary<int, Notification>();

                foreach (var notification in UnseenNotificationObjList)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    UnseenNotificationsListForSorting.Add(key, notification);
                }

                Dictionary<int, Notification> UnseenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in UnseenNotificationsListForSorting.Reverse())
                {
                    NotificationObjList.Add(notificationKeyValuePair.Value);
                }

                var SeenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == true && n.NotificationHeader != "Dividend" && n.NotificationHeader != "Notice").ToList();

                SortedDictionary<int, Notification> SeenNotificationsListForSorting = new SortedDictionary<int, Notification>();

                foreach (var notification in SeenNotificationObjList)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    SeenNotificationsListForSorting.Add(key, notification);
                }

                Dictionary<int, Notification> SeenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in SeenNotificationsListForSorting.Reverse())
                {
                    NotificationObjList.Add(notificationKeyValuePair.Value);
                }
                //var FullNotificationObjList = adbc.Notifications.ToList();
                List<Notification> NotificationList = new List<Notification>();
                //List<Notification> NotificationObjList = new List<Notification>();

                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var notification in NotificationObjList)
                    {


                        var NotificationHeader = "";
                        if (notification.NotificationHeader != null)
                        {
                            NotificationHeader = notification.NotificationHeader.ToUpper();
                        }
                        //DateTime UpdateDate = new DateTime();
                        //if (audit.UpdateDate != null)
                        //{
                        //    UpdateDate = (DateTime)audit.UpdateDate;
                        //}
                        var NotificationText = "";
                        if (notification.NotificationText != null)
                        {
                            NotificationText = notification.NotificationText.ToUpper();
                        }


                        var NotificationHeader1 = NotificationHeader;
                        var NotificationText1 = NotificationText;
                        //var TableName1 = TableName;
                        //var UpdateDate1 = UpdateDate;
                        //string date = UpdateDate1.ToString();
                        //var AuditDTTM1 = AuditDTTM;

                        if (NotificationHeader1 == searchfield1 || NotificationText1.Contains(searchfield1) == true)
                        {
                            NotificationList.Add(notification);
                        }
                    }
                }
                else
                {
                    NotificationList = NotificationObjList;
                }
                List<Notification> selectedNotifications = new List<Notification>();
                int noOfNotifications;
                if (NotificationList.Count >= 10)
                {

                    var pageNo = (NotificationList.Count < (index * 10)) ? NotificationList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedNotifications.Add(NotificationList.ElementAt(i));
                    }

                    noOfNotifications = (NotificationList.Count + 9) / 10;
                }
                else
                {
                    selectedNotifications = NotificationList;
                    noOfNotifications = 1;
                }
                List<string> notificationHeaders = adbc.Notifications.Select(n => n.NotificationHeader).Distinct().ToList();
                NotificationViewModel mdl = new NotificationViewModel()
                {
                    NotificationList = selectedNotifications,
                    NoOfNotification = noOfNotifications,
                    index = index,
                    searchfield = searchfield,
                    NotificationHeaders = notificationHeaders,
                };

                return View(mdl);
            }
        }

        //[HttpGet]
        //public ActionResult NotificationsPagination(int index)
        //{
        //    if (!IsUserLoggedIn())
        //        return RedirectToAction("Index", "Home");
        //    using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
        //    {
        //        List<Notification> NotificationObjList = new List<Notification>();

        //        var UnseenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == false && n.NotificationHeader != "D" && n.NotificationHeader != "N").ToList();

        //        SortedDictionary<int, Notification> UnseenNotificationsListForSorting = new SortedDictionary<int, Notification>();

        //        foreach (var notification in UnseenNotificationObjList)
        //        {
        //            int key = int.Parse(notification.NotificationId.Split('-')[1]);
        //            UnseenNotificationsListForSorting.Add(key, notification);
        //        }

        //        Dictionary<int, Notification> UnseenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

        //        foreach (var notificationKeyValuePair in UnseenNotificationsListForSorting.Reverse())
        //        {
        //            NotificationObjList.Add(notificationKeyValuePair.Value);
        //        }

        //        var SeenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == true && n.NotificationHeader != "D" && n.NotificationHeader != "N").ToList();

        //        SortedDictionary<int, Notification> SeenNotificationsListForSorting = new SortedDictionary<int, Notification>();

        //        foreach (var notification in SeenNotificationObjList)
        //        {
        //            int key = int.Parse(notification.NotificationId.Split('-')[1]);
        //            SeenNotificationsListForSorting.Add(key, notification);
        //        }

        //        Dictionary<int, Notification> SeenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

        //        foreach (var notificationKeyValuePair in SeenNotificationsListForSorting.Reverse())
        //        {
        //            NotificationObjList.Add(notificationKeyValuePair.Value);
        //        }

        //        List<Notification> selectedNotifications = new List<Notification>();
        //        int noOfNotifications;
        //        if (NotificationObjList.Count >= 10)
        //        {
        //            var pageNo = (NotificationObjList.Count < (index * 10)) ? NotificationObjList.Count : (index * 10);
        //            for (int i = index * 10 - 10; i < pageNo; i++)
        //            {
        //                selectedNotifications.Add(NotificationObjList.ElementAt(i));
        //            }
        //            int x = (NotificationObjList.Count % 10 == 0) ? 0 : 1;

        //            noOfNotifications = (NotificationObjList.Count / 10) + x;
        //        }
        //        else
        //        {
        //            selectedNotifications = NotificationObjList;
        //            noOfNotifications = 1;
        //        }

        //        NotificationViewModel mdl = new NotificationViewModel()
        //        {
        //            NotificationList = selectedNotifications,
        //            NoOfNotification = noOfNotifications,
        //            index = index
        //        };

        //        return View("NotificationView", mdl);
        //    }
        //}

        public ActionResult TaskManagement()
        {
            return View();
        }

        public ActionResult ModuleCreate()
        {
            return View();
        }

        public ActionResult TaskCreate()
        {
            return View();
        }

        public ActionResult OwnersInfo()
        {
            return View();
        }

        public ViewResult Error(string errorMessage)
        {
            ViewBag.Error = errorMessage;
            return View("Error");
        }
    }
}