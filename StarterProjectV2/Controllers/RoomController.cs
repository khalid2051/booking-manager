﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;

namespace StarterProjectV2.Controllers
{
    public class RoomController : Controller
    {
        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        // GET: RoomManagement
        [HttpGet]
        public ActionResult RoomManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");



            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                string taskId = db.Tasks.Where(u => u.TaskPath == "Room\\RoomManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                var model = new RoomManagementView
                {
                    RoomList = db.RoomInfos.ToList(),
                    TaskRoles = taskRoles
                };
                return View(model);
            }
        }
        [HttpPost]
        public ActionResult RoomManagementApprove(RoomManagementView model)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                //var RoomInDb = adbc.RoomInfos.Where(r => r.RoomId == roomId).FirstOrDefault();
                //RoomViewModel mdl = new RoomViewModel()
                //{
                //    Floor = RoomInDb.Floor,
                //    OwnershipCriteria = RoomInDb.OwnershipCriteria,
                //    RoomId = RoomInDb.RoomId,
                //    RoomNo = RoomInDb.RoomNo,
                //    RoomType = RoomInDb.RoomType,
                //    RoomSize = RoomInDb.RoomSize,
                //    OwnershipCriteriaList = basicOwnershipCriteriaList,
                //};
                //return View("RoomCreate", mdl);
                return View("RoomManagement");
            }

        }
        [HttpGet]
        public ActionResult RoomCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                List<string> basicOwnershipCriteriaList = new List<string>()
                {
                     "J.V.", "Coral Reef"   
                };
                RoomViewModel model = new RoomViewModel()
                {
                    OwnershipCriteriaList = basicOwnershipCriteriaList
                };

                return View(model);
            }
        }


        [HttpGet]
        public ActionResult RoomEdit(string roomId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var RoomInDb = adbc.RoomInfos.Where(r => r.RoomId == roomId).FirstOrDefault();
                List<string> basicOwnershipCriteriaList = new List<string>()
                {
                    "J.V.", "Coral Reef"
                };
                RoomViewModel mdl = new RoomViewModel()
                {
                    Floor = RoomInDb.Floor,
                    OwnershipCriteria = RoomInDb.OwnershipCriteria,
                    RoomId = RoomInDb.RoomId,
                    RoomNo = RoomInDb.RoomNo,
                    RoomType = RoomInDb.RoomType,
                    RoomSize = RoomInDb.RoomSize,
                    OwnershipCriteriaList = basicOwnershipCriteriaList,
                };
                return View("RoomCreate", mdl);
            }
        }

        [HttpGet]
        public ActionResult RoomView(string roomId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var model = adbc.RoomInfos.Where(p => p.RoomId == roomId).SingleOrDefault();
                List<string> basicOwnershipCriteriaList = new List<string>()
                {
                    "J.V.", "Coral Reef"
                };
                RoomViewModel viewModel = new RoomViewModel()
                {
                    Floor = model.Floor,
                    RoomId = model.RoomId,
                    RoomType = model.RoomType,
                    RoomSize = model.RoomSize,
                    OwnershipCriteria = model.OwnershipCriteria,
                    OwnershipCriteriaList = basicOwnershipCriteriaList
                };

                return View("RoomView", viewModel);
            }
        }
        [HttpPost]
        public ActionResult RoomAddorEdit(RoomViewModel info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {


                var RoomId = "";
                //OwnersInfo info = model.Owners;
                //new Owner : Add Mode 
                if (string.IsNullOrEmpty(info.RoomId))
                {
                    var roomList = adbc.RoomInfos.ToList();
                    var roomIdIdList = new List<int>();
                    foreach (var rooms in roomList)
                    {
                        roomIdIdList.Add(int.Parse(rooms.RoomId.ToString().Split('-')[1]));
                    }

                    var roomToken = roomList.Count();
                    if (roomToken > 0)
                    {
                        roomIdIdList.Sort();
                        roomToken = roomIdIdList[roomIdIdList.Count - 1];
                    }

                    roomToken++;
                    RoomId = "ROOM-" + roomToken;


                    RoomInfo model = new RoomInfo()
                    {
                        RoomId = RoomId,
                        Floor = info.Floor,
                        OwnershipCriteria = info.OwnershipCriteria,
                        RoomNo = info.RoomNo,
                        RoomSize = info.RoomSize,
                        RoomType = info.RoomType
                    };
                    adbc.RoomInfos.Add(model);
                    adbc.SaveChanges();
                } //existing Owner: Edit Mode
                else
                {
                    var roomInDb = adbc.RoomInfos.SingleOrDefault(r => r.RoomId == info.RoomId);

                    RoomId = roomInDb.ToString();

                    roomInDb.OwnershipCriteria = info.OwnershipCriteria;
                    roomInDb.Floor = info.Floor;
                    roomInDb.RoomNo = info.RoomNo;
                    roomInDb.RoomSize = info.RoomSize;
                    roomInDb.RoomType = info.RoomType;
                }
                adbc.SaveChanges();
                return RedirectToAction("RoomManagement");

            }
        }

        

        [HttpPost]

        public ActionResult RoomDelete(string roomId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var RoomInDb = adbc.RoomInfos.Where(r => r.RoomId == roomId).FirstOrDefault();
                adbc.RoomInfos.Remove(RoomInDb);
                adbc.SaveChanges();
                return RedirectToAction("RoomManagement");
            }
        }

    }
    
}