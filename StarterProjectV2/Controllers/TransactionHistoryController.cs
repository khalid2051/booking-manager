﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using OfficeOpenXml;
using System.IO;
using System.Data.OleDb;
using System.Configuration;
using System.Data;
using StarterProjectV2.Services;

namespace StarterProjectV2.Controllers
{
    public class TransactionHistoryController : Controller
    {
        private readonly CommonService _commonService = new CommonService();
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        [HttpGet]
        public ActionResult TransactionHistoryManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            //SendEmailMailGun();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole(TaskPath.TransactionHistory_TransactionHistoryManagement);
                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }


                var transactionHistories = db.OwnerTransationHistories.AsQueryable();

                if (!string.IsNullOrEmpty(searchfield))
                    transactionHistories = transactionHistories.Where(o => o.SlotID.Contains(searchfield));

                transactionHistories = transactionHistories.OrderBy(o => o.SlotID);

                List<OwnerTransactionHistory> selectedTransactions = CommonHelper.PagedList(transactionHistories, index, out int noOfTransactionHistories);

                var model = new TransactionHistoryList
                {
                    TransactionHistories = selectedTransactions,
                    TaskRoles = taskRoles,
                    NoOfTransactionHistories = noOfTransactionHistories,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult TransactionHistoryPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            //SendEmailMailGun();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "TransactionHistory\\TransactionHistoryManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                var TransactionHistoriesObjList = db.OwnerTransationHistories.ToList().OrderBy(o => o.SlotID).ToList();
                List<OwnerTransactionHistory> selectedTransactionHistories = new List<OwnerTransactionHistory>();
                int noOfTransactionHistories;
                if (TransactionHistoriesObjList.Count >= 10)
                {
                    var pageNo = (TransactionHistoriesObjList.Count < (index * 10)) ? TransactionHistoriesObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedTransactionHistories.Add(TransactionHistoriesObjList.ElementAt(i));
                    }
                    int x = (TransactionHistoriesObjList.Count % 10 == 0) ? 0 : 1;

                    noOfTransactionHistories = (TransactionHistoriesObjList.Count / 10) + x;
                }
                else
                {
                    selectedTransactionHistories = TransactionHistoriesObjList;
                    noOfTransactionHistories = 1;
                }
                var model = new TransactionHistoryList
                {
                    TransactionHistories = selectedTransactionHistories,
                    TaskRoles = taskRoles,
                    NoOfTransactionHistories = noOfTransactionHistories,
                    index = index
                };
                return View("TransactionHistoryManagement", model);
            }
        }
        [HttpPost]
        public JsonResult gettingSearchResultForTransactionHistory(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var TransactionHistoryList = db.OwnerTransationHistories.ToList();
                List<TransactionHistoryViewForSearch> searchedTransactionHistory = new List<TransactionHistoryViewForSearch>();
                var searchfield1 = searchfield.ToUpper();

                foreach (var transactionHistory in TransactionHistoryList)
                {
                    var SlotId = "";
                    if (transactionHistory.SlotID != null)
                    {
                        SlotId = transactionHistory.SlotID.ToUpper();
                    }

                    var SaleDate = "";
                    if (transactionHistory.SaleDate != null)
                    {
                        SaleDate = transactionHistory.SaleDate.ToString("dd-MMM-yyyy").ToUpper();
                    }
                    var SalesValue = "";
                    //this code is erroneous. int is being checked against null. someone should correct it.
                    if (transactionHistory.SalesValue != null)
                    {
                        SalesValue = transactionHistory.SalesValue.ToString().ToUpper();
                    }
                    var SlotId1 = SlotId;
                    var SaleDate1 = SaleDate;
                    var SalesValue1 = SalesValue;

                    if (SlotId1.Contains(searchfield1) == true || SaleDate1.Contains(searchfield1) == true ||
                        SalesValue1.Contains(searchfield1) == true)
                    {
                        TransactionHistoryViewForSearch transactionHistoryToInsert = new TransactionHistoryViewForSearch()
                        {
                            TransactionID = transactionHistory.TransactionID,
                            SlotID = transactionHistory.SlotID,
                            SalesValue = transactionHistory.SalesValue.ToString(),
                            SaleDate = transactionHistory.SaleDate.ToString("dd-MMM-yyyy"),
                        };
                        searchedTransactionHistory.Add(transactionHistoryToInsert);
                    }

                }
                string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    //return View(Json("Error"));
                }
                //OwnerList mdl = new OwnerList()
                //{
                //    index = 1,
                //    NoOfOwner = 0,
                //    Owners = searchedOwner,
                //    TaskRoles = taskRoles
                //};
                return Json(searchedTransactionHistory);

            }
        }

        [HttpGet]
        public ActionResult TransactionHistoryView(string transactionHistoryId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var transactionHistory = adbc.OwnerTransationHistories
                    .Where(t => t.TransactionID == transactionHistoryId).FirstOrDefault();
                return View(transactionHistory);
            }
        }
        //Not Working expectedly, Maybe test and modify Later
        public ActionResult ExcelImport() => View();
        [HttpPost]
        public JsonResult SaveExcel()
        {
            List<ExcelViewModel> excelData = new List<ExcelViewModel>();
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        var newName = fname.Split('.');
                        fname = newName[0] + "_" + DateTime.Now.Ticks.ToString() + "." + newName[1];
                        var uploadRootFolderInput = AppDomain.CurrentDomain.BaseDirectory + "\\ExcelUploads";
                        Directory.CreateDirectory(uploadRootFolderInput);
                        var directoryFullPathInput = uploadRootFolderInput;
                        fname = Path.Combine(directoryFullPathInput, fname);
                        file.SaveAs(fname);
                        string xlsFile = fname;
                        excelData = readExcel(fname);
                    }
                    if (excelData.Count > 0)
                    {
                        return Json(excelData, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(false, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public List<ViewModels.ExcelViewModel> readExcel(string FilePath)
        {
            try
            {
                List<ExcelViewModel> excelData = new List<ExcelViewModel>();
                FileInfo existingFile = new FileInfo(FilePath);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    int rowCount = worksheet.Dimension.End.Row;
                    for (int row = 1; row <= rowCount; row++)
                    {
                        string[] getMonthYear = worksheet.Cells[row, 4].Value.ToString().Trim().Split(' ');
                        excelData.Add(new ExcelViewModel()
                        {
                            EnrollmentNo = worksheet.Cells[row, 2].Value.ToString().Trim(),
                            Semester = worksheet.Cells[row, 3].Value.ToString().Trim(),
                            Month = getMonthYear[0],
                            Year = getMonthYear[1],
                            StatementNo = worksheet.Cells[row, 5].Value.ToString().Trim()
                        });
                    }
                }
                return excelData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public DateTime cast(Object data)
        {
            DateTime value;
            try
            {
                value = (DateTime)data;
            }
            catch (Exception ex)
            {
                value = DateTime.MinValue;
            }
            return value;
        }

        //Working FIne
        [HttpGet]
        public ActionResult Importexcel(int errorline = 0, string status = "")
        {
            //IsLoggedIn
            ViewBag.ErrorLine = errorline;
            ViewBag.Status = status;
            return View();

        }
        [HttpPost]
        public ActionResult Importexcel(HttpPostedFileBase postedFile)
        {
            string filePath = string.Empty;
            if (postedFile != null)
            {
                string path = Server.MapPath("~/upload/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                string conString = string.Empty;
                switch (extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }

                DataTable dt = new DataTable();
                conString = string.Format(conString, filePath);

                using (OleDbConnection connExcel = new OleDbConnection(conString))
                {
                    using (OleDbCommand cmdExcel = new OleDbCommand())
                    {
                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                        {
                            cmdExcel.Connection = connExcel;

                            //Get the name of First Sheet.
                            connExcel.Open();
                            DataTable dtExcelSchema;
                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            connExcel.Close();

                            //Read Data from First Sheet.
                            connExcel.Open();
                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                            odaExcel.SelectCommand = cmdExcel;
                            odaExcel.Fill(dt);
                            connExcel.Close();
                        }
                    }
                }
                ApplicationDbContext adbc = new ApplicationDbContext();

                //validation and feedback to user
                int line = 1;
                string error = "";

                ////
                //Insert records to database table.
                foreach (DataRow row in dt.Rows)
                {
                    if (line == 29)
                    {
                        var tremp = row[2];
                    }
                    if (row[2] == null || row[2].ToString() == "")
                    {
                        break;
                    }
                    OwnerTransactionHistory ownerTransactionHistory = new OwnerTransactionHistory();
                    try
                    {
                        //ownerTransactionHistory.SLNo = row["SL No"].ToString();
                        //ownerTransactionHistory.TransactionID = row["Transaction ID"].ToString();
                        //ownerTransactionHistory.SlotID = row["Slot ID"].ToString();
                        //ownerTransactionHistory.RegistrationStatus = row["Registration Status"].ToString();
                        //ownerTransactionHistory.RegistrationBatch = row["Registration Batch"].ToString();
                        //ownerTransactionHistory.SlotSFT = (double)row["Slot SFT"];
                        //ownerTransactionHistory.SFTperPerson = (double)row["SFT per person"];
                        //ownerTransactionHistory.Floor = Convert.ToInt32(row["Floor"]);
                        //ownerTransactionHistory.Type = row["Type"].ToString();
                        //ownerTransactionHistory.SaleDate = (DateTime)row["Sale Date"];
                        //ownerTransactionHistory.NoofSlot = (double)row["No of Slot"];
                        //ownerTransactionHistory.SalesValue1 = Convert.ToInt32(row["Sales Value 1"]);
                        //ownerTransactionHistory.ReceivableIncreaseDecreaseValue = Convert.ToInt32(row[12]);
                        //ownerTransactionHistory.ReceivableExcludingRandU = Convert.ToInt32(row["Receivable Excluding R and U"].ToString());
                        //ownerTransactionHistory.ReceivableRegistration = Convert.ToInt32(row["Receivable Registration"].ToString());
                        //ownerTransactionHistory.ReceivableUtility = Convert.ToInt32(row["Receivable Utility"].ToString());
                        //ownerTransactionHistory.TotalReceivable = Convert.ToInt32(row["Total Receivable"].ToString());
                        //ownerTransactionHistory.SalesValue = Convert.ToInt32(row["Sales Value"].ToString());
                        //ownerTransactionHistory.ReceivedIncreaseDecreaseValue1 = Convert.ToInt32(row["Received Increase_Decrease Value 1"]);
                        //ownerTransactionHistory.ReceivedExcludingRandU = Convert.ToInt32(row["Received Excluding R and U"]);
                        //ownerTransactionHistory.ReceivedRegistration = Convert.ToInt32(row["Received Registration"]);
                        //ownerTransactionHistory.ReceivedUtility = Convert.ToInt32(row["Received Utility"]);
                        //ownerTransactionHistory.TotalReceived = Convert.ToInt32(row["Total Received"]);
                        //ownerTransactionHistory.TotalReceivableExcludingRandU = Convert.ToInt32(row["Total Receivable Excluding R and U"]);
                        //ownerTransactionHistory.NETReceivableAmountinTaka = Convert.ToInt32(row["NET Receivable Amount in Taka"]);
                        //ownerTransactionHistory.OperationalCostBankCashReceived_CRPL = Convert.ToInt32(row["Operational Cost Bank_Cash Received_CRPL"]);
                        //ownerTransactionHistory.OperationalCostBankCashReceived_GHL = Convert.ToInt32(row["Operational Cost Bank_Cash Received_GHL"]);
                        //ownerTransactionHistory.OperationalCostBankCashReceived_CRPLandGHL = Convert.ToInt32(row["Operational Cost Bank_Cash Received_CRPL and GHL"]);
                        //ownerTransactionHistory.OperationalCostAdjustment_CRPL = Convert.ToInt32(row["Operational Cost Adjustment_CRPL"]);
                        //ownerTransactionHistory.OperationalCostAdjustwithLateFee = Convert.ToInt32(row["Operational Cost Adjust with Late Fee"]);
                        //ownerTransactionHistory.TotalReceivedOperationalCost = Convert.ToInt32(row["Total Received Operational Cost"]);
                        //ownerTransactionHistory.OperationCostReceivable = Convert.ToInt32(row["Operation Cost Receivable"]);
                        //ownerTransactionHistory.NetReceivableincludingOPandMembershipFee = Convert.ToInt32(row["Net Receivable including OP and Membership Fee"]);
                        //ownerTransactionHistory.OperationalCostReceivedDate = (DateTime)row["Operational Cost Received Date"];
                        //ownerTransactionHistory.MembershipFeeReceivable = Convert.ToInt32(row["Membership Fee Receivable"]);
                        //ownerTransactionHistory.MembershipFeeReceived = Convert.ToInt32(row["Membership Fee Received"]);
                        //ownerTransactionHistory.Particulars = row["Particulars"].ToString();
                        //ownerTransactionHistory.AgreementwithSlotOwner = row["Agreement with Slot Owner"].ToString();
                        //ownerTransactionHistory.Remarks = row["Remarks"].ToString();


                        ownerTransactionHistory.SLNo = row[0].ToString();
                        ownerTransactionHistory.TransactionID = row[1].ToString();
                        ownerTransactionHistory.SlotID = row[2].ToString();
                        ownerTransactionHistory.RegistrationStatus = row[3].ToString();
                        ownerTransactionHistory.RegistrationBatch = row[4].ToString();
                        ownerTransactionHistory.SlotSFT = (double)row[5];
                        ownerTransactionHistory.SFTperPerson = (double)row[6];
                        ownerTransactionHistory.Floor = Convert.ToInt32(row[7]);
                        ownerTransactionHistory.Type = row[8].ToString();
                        ownerTransactionHistory.SaleDate = cast(row[9]);
                        ownerTransactionHistory.NoofSlot = (double)row[10];
                        ownerTransactionHistory.SalesValue1 = Convert.ToInt32(row[11]);
                        ownerTransactionHistory.ReceivableIncreaseDecreaseValue = Convert.ToInt32(row[12]);
                        ownerTransactionHistory.ReceivableExcludingRandU = Convert.ToInt32(row[13].ToString());
                        ownerTransactionHistory.ReceivableRegistration = Convert.ToInt32(row[14].ToString());
                        ownerTransactionHistory.ReceivableUtility = Convert.ToInt32(row[15].ToString());
                        ownerTransactionHistory.TotalReceivable = Convert.ToInt32(row[16].ToString());
                        ownerTransactionHistory.SalesValue = Convert.ToInt32(row[17].ToString());
                        ownerTransactionHistory.ReceivedIncreaseDecreaseValue1 = Convert.ToInt32(row[18]);
                        ownerTransactionHistory.ReceivedExcludingRandU = Convert.ToInt32(row[19]);
                        ownerTransactionHistory.ReceivedRegistration = Convert.ToInt32(row[20]);
                        ownerTransactionHistory.ReceivedUtility = Convert.ToInt32(row[21]);
                        ownerTransactionHistory.TotalReceived = Convert.ToInt32(row[22]);
                        ownerTransactionHistory.TotalReceivableExcludingRandU = Convert.ToInt32(row[23]);
                        ownerTransactionHistory.NETReceivableAmountinTaka = Convert.ToInt32(row[24]);
                        ownerTransactionHistory.OperationalCostBankCashReceived_CRPL = Convert.ToInt32(row[25]);
                        ownerTransactionHistory.OperationalCostBankCashReceived_GHL = Convert.ToInt32(row[26]);
                        ownerTransactionHistory.OperationalCostBankCashReceived_CRPLandGHL = Convert.ToInt32(row[27]);
                        ownerTransactionHistory.OperationalCostAdjustment_CRPL = Convert.ToInt32(row[28]);
                        ownerTransactionHistory.OperationalCostAdjustwithLateFee = Convert.ToInt32(row[29]);
                        ownerTransactionHistory.TotalReceivedOperationalCost = Convert.ToInt32(row[30]);
                        ownerTransactionHistory.OperationCostReceivable = Convert.ToInt32(row[31]);
                        ownerTransactionHistory.NetReceivableincludingOPandMembershipFee = Convert.ToInt32(row[32]);
                        ownerTransactionHistory.OperationalCostReceivedDate = cast(row[33]);
                        ownerTransactionHistory.MembershipFeeReceivable = Convert.ToInt32(row[34]);
                        ownerTransactionHistory.MembershipFeeReceived = Convert.ToInt32(row[35]);
                        ownerTransactionHistory.Particulars = row[36].ToString();
                        ownerTransactionHistory.AgreementwithSlotOwner = row[37].ToString();
                        ownerTransactionHistory.Remarks = row[38].ToString();
                    }
                    catch (Exception e)
                    {

                        return RedirectToAction("Importexcel", new { errorline = line, status = "Error" });
                    }
                    adbc.OwnerTransationHistories.Add(ownerTransactionHistory);
                    line++;
                }
                adbc.SaveChanges();
            }
            return RedirectToAction("Importexcel", new { errorline = 0, status = "OK" });
        }
    }
}