﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;

namespace StarterProjectV2.Controllers
{
    public class OwnerChatController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        ///////////////No TaskPath Right Now////////////////////
        [HttpGet]
        public ActionResult ChatManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "OwnerChat\\ChatManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");
                }


                List<OwnersInfo> owners = adbc.OwnersInfos.ToList();
                List<string> ownersNames = owners.Select(o => o.Name).ToList();
                List<string> slotIds = owners.Select(o => o.SlotId).ToList();
                //List<OwnerChatManagementViewItem> OwnerChatItems = new List<OwnerChatManagementViewItem>(slotIds.Count());
                var OwnerChatItems = new List<OwnerChatManagementViewItem>();
                var OwnerChatItemsNull = new List<OwnerChatManagementViewItem>();
                string lastMessage;
                string lastMessageTime;
                var chatsList = adbc.Chats.ToList();
                for (int i = 0; i < slotIds.Count(); i++)
                {
                    
                    var curslot = slotIds[i];
                    var selectedChat = chatsList.LastOrDefault(c => c.SlotId == curslot);
                    if (selectedChat != null)
                    {
                        lastMessage =  selectedChat.Text;
                        lastMessageTime = selectedChat.DeliveryTime.ToString();

                        //slotIds[i] = selectedChat.SlotId;
                        var temp = new OwnerChatManagementViewItem()
                        {
                            OwnersName = ownersNames[i],
                            SlotId = slotIds[i],
                            LastMessage = lastMessage,
                            LastMessageTime = lastMessageTime

                        };
                        OwnerChatItems.Add(temp);
                    }
                    else
                    {
                        {
                            lastMessage = "";
                            lastMessageTime = "";

                            //slotIds[i] = selectedChat.SlotId;
                            var temp = new OwnerChatManagementViewItem()
                            {
                                OwnersName = ownersNames[i],
                                SlotId = slotIds[i],
                                LastMessage = lastMessage,
                                LastMessageTime = lastMessageTime

                            };
                            OwnerChatItemsNull.Add(temp);
                        }
                    }
                }
                OwnerChatItems = OwnerChatItems.OrderByDescending(o => o.LastMessageTime).ToList();
                foreach (var item in OwnerChatItemsNull)
                {
                    OwnerChatItems.Add(item);
                }

                //Searching
                List<OwnerChatManagementViewItem> ChatObjList = new List<OwnerChatManagementViewItem>();

                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var chats in OwnerChatItems)
                    {
                        var UserId = "";
                        if (chats.SlotId != null)
                        {
                            UserId = chats.SlotId.ToUpper();
                        }
                        var Name = "";
                        if (chats.OwnersName != null)
                        {
                            Name = chats.OwnersName.ToUpper();
                        }

                        //var AuditDTTM = "";
                        //if (audit.AuditDTTM != null)
                        //{
                        //    AuditDTTM = audit.AuditDTTM.ToUpper();
                        //}

                        //var AuditDTTM1 = AuditDTTM;

                        if (UserId.Contains(searchfield1) == true || Name.Contains(searchfield1) == true)
                        {
                            ChatObjList.Add(chats);
                        }
                    }
                }
                else
                {
                    ChatObjList = OwnerChatItems;
                }
                //Search Finished
                //Pagination Start
                List<OwnerChatManagementViewItem> selectedChats = new List<OwnerChatManagementViewItem>();
                int noOfPage;
                if (ChatObjList.Count >= 10)
                {

                    var pageNo = (ChatObjList.Count < (index * 10)) ? ChatObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedChats.Add(ChatObjList.ElementAt(i));
                    }

                    noOfPage = (ChatObjList.Count + 9) / 10;
                }
                else
                {
                    selectedChats = ChatObjList;
                    noOfPage = 1;
                }

                var model = new OwnerChatManagementView()
                {
                    OwnerChatManagementViewItems = selectedChats,
                    noOfPage = noOfPage,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult ChatDetail(string SlotId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                
                var chatsOfThisSlotOwner = adbc.Chats.Where(c => c.SlotId == SlotId).ToList();
                //var chatsOfThisSlotOwnerOrdered = chatsOfThisSlotOwner.OrderByDescending(c => c.ID).ToList();
                var id = chatsOfThisSlotOwner.Count() > 0 ? chatsOfThisSlotOwner.Last().ID : 0;

                var ChatsList3 = chatsOfThisSlotOwner.Where(p => p.ID <= id).ToList();
                List<Chat> chatList2 = new List<Chat>();
                List<Chat> chatList = new List<Chat>();
                if (ChatsList3 != null)
                {
                    chatList2 = ChatsList3.OrderByDescending(o => o.ID).Take(5).ToList();
                    chatList = chatList2.OrderBy(o => o.ID).ToList();
                }
                if (chatList.Count() > 0 && chatList2.Count() > 0)
                {
                    var id2 = chatList2.Last().ID;
                    var modelWithData = new OwnerChatView
                    {
                        Chats = chatList,
                        ID = id2,
                        SlotId = SlotId
                    };
                    return View(modelWithData);
                }
                var model = new OwnerChatView
                {
                    Chats = null,
                    ID = -1,
                    SlotId = SlotId
                };
                return View(model);
            }
        }
        public JsonResult addMessage(OwnerChatView mdl)
        {
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                Chat chat = new Chat()
                {
                    SlotId = mdl.SlotId,
                    Text = mdl.CurrentMessage,
                    DeliveryTime = DateTime.Now,
                    Receiver = mdl.SlotId,
                    Sender = "Admin"
                };
                adbc.Chats.Add(chat);
                adbc.SaveChanges();
                Json("Success", JsonRequestBehavior.AllowGet);
            }
            return Json("Error", JsonRequestBehavior.DenyGet);
        }
        public JsonResult gettingChatWithAdmin(string slotId, int lastId)
        {
            if (!IsUserLoggedIn())
                return Json("error");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var SlotId = slotId;
                var chatsOfThisSlotOwner = adbc.Chats.Where(c => c.SlotId == SlotId).ToList();
                lastId = (chatsOfThisSlotOwner != null) ? lastId : 0;
                var chatsList3 = adbc.Chats.Where(p => p.SlotId == SlotId && p.ID < lastId).ToList();
                var a = chatsList3.Count();
                List<Chat> chatList2 = null;
                if (chatsList3.Count() >= 5)
                {
                    chatList2 = chatsList3.OrderByDescending(o => o.ID).Take(5).ToList();
                }
                else if (chatsList3.Count() > 0)
                {
                    chatList2 = chatsList3.OrderByDescending(o => o.ID).Take(5).ToList();
                }
                else if (chatsList3.Count() <= 0)
                {
                    chatList2 = null;
                    return Json("Error");
                }
                var LastID = chatList2.Last().ID;
                return Json(new { chatList = chatList2, LastID = LastID });
            }
        }
        public JsonResult gettingNewMessages(string slotId, int latestId)
        {
            if (!IsUserLoggedIn())
                return Json("error");
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {
                var SlotId = slotId;
                var chatsOfThisSlotOwner = adbc.Chats.Where(c => c.SlotId == SlotId).ToList();
                latestId = chatsOfThisSlotOwner != null ? latestId : 0;
                var chatList3 = chatsOfThisSlotOwner.Where(p=> p.ID > latestId).ToList();
                var a = chatList3.Count();
                var LatestID = 0;
                if (chatList3.Count() > 0)
                {
                    LatestID = chatList3.Last().ID;
                }
                else
                {
                    LatestID = latestId;
                }
                return Json(new { chatList = chatList3, LatestID = LatestID });
            }
        }
    }
}