﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;

namespace StarterProjectV2.Controllers
{
    public class SlotController : Controller
    {
        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult SlotManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            Common.EmailSending("omeca13@gmail.com", "Hello test", "Testing Webmail");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                string taskId = db.Tasks.Where(u => u.TaskPath == "Slot\\SlotManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                var model = new SlotManagementView
                {
                    SlotList = db.SlotInfos.ToList(),
                    TaskRoles = taskRoles
                };
                return View(model);
            }
        }
        public PartialViewResult SearchUsers(string searchText)
        {
            

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //List<SlotInfo> users = GetUsers();
               
                var users = db.SlotInfos.ToList();
                List<SlotInfo> searchedOwner = new List<SlotInfo>();
                var result = users;
            if (!string.IsNullOrEmpty(searchText))
            {
                result = users.Where(a => a.SlotName.ToLower().Contains(searchText.ToLower())).ToList();
                }
                return PartialView("_GridView", result);
            }
           
        }
        
        [HttpGet]
        public ActionResult SlotCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            return View();
        }

        [HttpGet]
        public ActionResult SlotEdit(string slotId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var slotInDb = adbc.SlotInfos.Where(r => r.SlotId == slotId).FirstOrDefault();


                //Modifying the from dates and todates before sending to the View
                var year = (int.Parse(slotInDb.FromDate.ToString().Split('-')[0]));
                var month = (int.Parse(slotInDb.FromDate.ToString().Split('-')[1]));
                var updated_month = month.ToString();
                if (month.ToString().Length == 1) updated_month = "0" + month.ToString();
                var newFromDate = year.ToString() + "-" + updated_month;

                var year2 = (int.Parse(slotInDb.ToDate.ToString().Split('-')[0]));
                var month2 = (int.Parse(slotInDb.ToDate.ToString().Split('-')[1]));
                var updated_month2 = month2.ToString();
                if (month2.ToString().Length == 1) updated_month2 = "0" + month2.ToString();
                var newToDate = year2.ToString() + "-" + updated_month2;

               
                SlotInfo mdl = new SlotInfo()
                {
                    SlotId = slotInDb.SlotId,
                    SlotName = slotInDb.SlotName,
                    SlotOtherDefinitions = slotInDb.SlotOtherDefinitions,
                    FromDate = newFromDate,
                    ToDate = newToDate
                    //(int.Parse(slotInDb.ToDate.ToString().Split('-')[0]) + int.Parse(slotInDb.ToDate.ToString().Split('-')[1])).ToString()
                };
                return View("SlotCreate", mdl);
            }
        }

        [HttpPost]
        public ActionResult SlotAddorEdit(SlotInfo info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var slotId = "";
                //OwnersInfo info = model.Owners;
                //new Owner : Add Mode 
                if (string.IsNullOrEmpty(info.SlotId))
                {
                    var slotList = adbc.SlotInfos.ToList();
                    var slotIdList = new List<int>();
                    foreach (var slots in slotList)
                    {
                        slotIdList.Add(int.Parse(slots.SlotId.ToString().Split('-')[1]));
                    }

                    var slotToken = slotList.Count();
                    if (slotToken > 0)
                    {
                        slotIdList.Sort();
                        slotToken = slotIdList[slotIdList.Count - 1];
                    }

                    slotToken++;
                    slotId = "SLOT-" + slotToken;


                    //Formatting the fromdate and todates
                    info.FromDate = info.FromDate + "-01";
                    var year = int.Parse(info.ToDate.ToString().Split('-')[0]);
                    var month = int.Parse(info.ToDate.ToString().Split('-')[1]);
                    var day = 0;
                    if (month == 2) day = 28;
                    else if (month % 2 == 1) day = 31;
                    else if (month % 2 == 0) day = 30;

                    if ((year % 4 == 0 && year % 100 != 0)||(year % 400 == 0))
                        day = day + 1;  //leap year
                    
                    info.ToDate = info.ToDate + "-" + day.ToString();





                    SlotInfo model = new SlotInfo()
                    {
                        SlotId = slotId,
                        SlotName = info.SlotName,
                        SlotOtherDefinitions = info.SlotOtherDefinitions,
                        ToDate = info.ToDate,
                        FromDate = info.FromDate
                    };
                    adbc.SlotInfos.Add(model);
                    adbc.SaveChanges();
                } //existing Owner: Edit Mode
                else
                {
                    var slotInDb = adbc.SlotInfos.SingleOrDefault(r => r.SlotId == info.SlotId);

                    slotId = slotInDb.ToString();



                    //Formatting the fromdate and todates
                    info.FromDate = info.FromDate + "-01";
                    var year = int.Parse(info.ToDate.ToString().Split('-')[0]);
                    var month = int.Parse(info.ToDate.ToString().Split('-')[1]);
                    var day = 0;
                    if (month == 2) day = 28;
                    else if (month % 2 == 1) day = 31;
                    else if (month % 2 == 0) day = 30;

                    if ((year % 4 == 0 && year % 100 != 0) ||(year % 400 == 0))
                        day = day + 1;  //leap year
                    
                    info.ToDate = info.ToDate + "-" + day.ToString();

                    


                    slotInDb.SlotName = info.SlotName;
                    slotInDb.FromDate = info.FromDate;
                    slotInDb.ToDate = info.ToDate;
                    slotInDb.SlotOtherDefinitions = info.SlotOtherDefinitions;
                }
                adbc.SaveChanges();
                return RedirectToAction("SlotManagement");
            }
        }



        [HttpGet]
        public ActionResult SlotView(string slotId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var model = adbc.SlotInfos.Where(p => p.SlotId == slotId).SingleOrDefault();
                SlotInfo slotModel = new SlotInfo()
                {
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    SlotOtherDefinitions = model.SlotOtherDefinitions,
                    SlotId = model.SlotId,
                    SlotName = model.SlotName
                };

                return View("SlotView", slotModel);
            }
        }


        [HttpPost]
        public ActionResult SlotDelete(string slotId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var SlotInDb = adbc.SlotInfos.Where(r => r.SlotId == slotId).FirstOrDefault();
                adbc.SlotInfos.Remove(SlotInDb);
                adbc.SaveChanges();
                return RedirectToAction("SlotManagement");
            }
        }
    }
}


