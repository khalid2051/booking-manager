﻿using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StarterProjectV2.Controllers
{
    public partial class OwnersInfoController : Controller
    {
        #region Day Close and Daily Operations

        [HttpGet]
        public ActionResult DayCloseManagement()
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole("OwnersInfo\\DayCloseManagement");

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var systemdate = _configurationService.GetSystemDate().Date;
                var date = systemdate.ToShortDateString();

                if (systemdate < DateTime.Now.Date)
                {
                    List<OwnerRoomBooking> BookingObjList = db.OwnerRoomBookings.Where(r =>
                    r.IsBookingCompleted == "Yes"
                    && string.IsNullOrEmpty(r.BookingToken)
                    && DbFunctions.TruncateTime(r.ApplicationDate) == systemdate
                    ).ToList();

                    List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();

                    int noOfBooking;
                    if (BookingObjList.Count >= 10)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            selectedBookings.Add(BookingObjList.ElementAt(i));
                        }
                        noOfBooking = (BookingObjList.Count + 9) / 10;
                    }
                    else
                    {
                        selectedBookings = BookingObjList;
                        noOfBooking = 1;
                    }

                    var model = new OwnerRoomBookingManagementView()
                    {
                        OwnerRoomBookingListCurrent = selectedBookings,
                        TaskRoles = taskRoles,
                        NoOfBooking = noOfBooking,
                        index = 1,
                        Date = date
                    };
                    return View(model);
                }
                else
                {
                    TempData["Status"] = Messages.StatusWarning;
                    TempData["Message"] = Messages.SystemDateMustBeLessThanCurrentDate;
                    return RedirectToAction("About", "Home");
                }
            }
        }

        [HttpGet]
        public ActionResult DailyOperations()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            string taskRoles = _commonService.TaskRole(TaskPath.OwnersInfo_DailyOperations);

            if (taskRoles == "" || taskRoles[0] == '0')
            {
                return View("Error");
            }

            var systemdate = _configurationService.GetSystemDate().Date;

            if (!(systemdate <= DateTime.Now.Date))
            {
                TempData["Status"] = "Warning";
                TempData["Message"] = Messages.SystemDateMustBeLessThanCurrentDate;
                return RedirectToAction("About", "Home");
            }

            var model = new OwnerRoomBookingManagementView()
            {
                PendingBookingNoBookings = GetPendingBookingNoBookings(systemdate),
                PendingCheckInBookings = GetPendingCheckInBookings(systemdate),
                PendingCheckOutBookings = GetPendingCheckOutBookings(systemdate),
                TaskRoles = taskRoles,
                index = 1,
                Date = systemdate.ToShortDateString(),
                CheckOutDate = systemdate,
            };
            return View(model);
        }

        public List<OwnerRoomBooking> GetPendingBookingNoBookings(DateTime systemdate)
        {
            var bookings = _dbContext.OwnerRoomBookings.Where(b =>
                    DbFunctions.TruncateTime(b.ApplicationDate) == systemdate
                    && b.IsBookingCompleted == "Yes"
                    && string.IsNullOrEmpty(b.BookingToken)
                   );

            return bookings.ToList();
        }

        public List<OwnerRoomBooking> GetPendingCheckInBookings(DateTime systemdate)
        {
            var bookings = _dbContext.OwnerRoomBookings.Where(b =>
                DbFunctions.TruncateTime(b.CheckInDate) == systemdate
                && b.IsBookingCompleted == "Yes"
                && !string.IsNullOrEmpty(b.BookingToken)
                && b.HasStayedOrNot != "Yes"
                );

            return bookings.ToList();
        }

        public List<OwnerRoomBooking> GetPendingCheckOutBookings(DateTime systemdate)
        {

            var bookings = _dbContext.OwnerRoomBookings.Where(b =>
                DbFunctions.TruncateTime(b.CheckOutDate) == systemdate
                && b.IsBookingCompleted == "Yes"
                && !string.IsNullOrEmpty(b.BookingToken)
                && b.HasStayedOrNot == "Yes"
                && b.HasCheckedOutOrNot != "Yes"
                );

            return bookings.ToList();
        }



        [HttpPost]
        public ActionResult BookingTokenAdd(OwnerRoomBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerRoomBooking = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == info.OwnerRoomBookngId).FirstOrDefault();

                //var ownerRoomBooking = adbc.OwnerRoomBookings.Select(o => o.OwnerRoomBookngId == info.OwnerRoomBookngId);
                ownerRoomBooking.BookingToken = info.BookingToken;
                ownerRoomBooking.IsBookingCompleted = "Yes";

                adbc.SaveChanges();
                return RedirectToAction("DailyOperations");
            }

        }

        [HttpGet]
        public ActionResult CheckRemainingCheckInManagementDayClose()
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole("OwnersInfo\\DayCloseManagement");

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var systemdate = _configurationService.GetSystemDate().Date;
                var date = systemdate.ToShortDateString();

                var BookingObjList = db.OwnerRoomBookings.Where(b =>
                DbFunctions.TruncateTime(b.CheckInDate) == systemdate
                && b.IsBookingCompleted == "Yes"
                && (string.IsNullOrEmpty(b.HasStayedOrNot) || b.HasStayedOrNot == "No")
                ).ToList();

                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();


                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }
                    noOfBooking = (BookingObjList.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }

                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = 1,
                    Date = date
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult HasCheckedInorNot(OwnerRoomBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            ResponseModel response = new ResponseModel();

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerRoomBooking = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == info.OwnerRoomBookngId).FirstOrDefault();

                ownerRoomBooking.HasStayedOrNot = info.HasStayedOrNot;
                ownerRoomBooking.FolioNo = info.FolioNo;
                ownerRoomBooking.PreferredRoomNo = info.PreferredRoomNo;

                adbc.SaveChanges();

                response.Status = Messages.StatusSuccess;
                response.Message = Messages.CheckedInSuccessfully;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult HasCheckedOutorNot(OwnerRoomBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            ResponseModel response = new ResponseModel();

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerRoomBooking = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == info.OwnerRoomBookngId).FirstOrDefault();

                var invoice = adbc.Invoices.Where(inv => inv.BookingId == info.OwnerRoomBookngId).FirstOrDefault();

                if (invoice == null)
                {
                    response.Message = Messages.PleaseGenerateInvoiceFirst;

                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                ownerRoomBooking.HasCheckedOutOrNot = info.HasCheckedOutOrNot;
                ownerRoomBooking.CheckOutDate = info.CheckOutDate;
                ownerRoomBooking.TotalExpense = info.TotalExpense;
                ownerRoomBooking.ExtraCharges = info.ExtraCharges;
                adbc.SaveChanges();

                response.Status = Messages.StatusSuccess;
                response.Message = Messages.CheckedOutSuccessfully;
                return Json(response, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult RemainingReferralBookingManagement()
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\DayCloseManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");

                }
                var systemdate = db.SystemDates.Select(x => x.Date).ToList().FirstOrDefault();
                var date = systemdate.ToShortDateString();
                List<ReferralBooking> BookingObjList = db.ReferralBookings.Where(b => String.IsNullOrEmpty(b.IsBookingCompleted) && b.ApplicationDate == systemdate).ToList();
                List<ReferralBooking> selectedBookings = new List<ReferralBooking>();


                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }
                    noOfBooking = (BookingObjList.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }

                var model = new ReferrelRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    //TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = 1

                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult RemainingReferralBookingManagementDayClose()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\DayCloseManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");

                }
                var systemdate = db.SystemDates.Select(x => x.Date).ToList().FirstOrDefault();
                List<ReferralBooking> BookingObjList = db.ReferralBookings.Where(b => b.ApplicationDate == systemdate).ToList();
                List<ReferralBooking> selectedBookings = new List<ReferralBooking>();


                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }
                    noOfBooking = (BookingObjList.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }
                //if (selectedBookings.Count == 0)
                //{
                //    return RedirectToAction("CheckRemainingOwnerCheckOutManagement");
                //}
                var model = new ReferrelRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    //TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = 1
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult ReferralBookingDetailsAdd(ReferralBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var referralBooking = adbc.ReferralBookings.Where(o => o.ReferralBookingId == info.ReferralBookingId).FirstOrDefault();
                //var ownerRoomBooking = adbc.OwnerRoomBookings.Select(o => o.OwnerRoomBookngId == info.OwnerRoomBookngId);
                referralBooking.HasStayedOrNot = info.HasStayedOrNot;
                referralBooking.BookingToken = info.BookingToken;
                referralBooking.FolioNo = info.FolioNo;
                referralBooking.IsBookingCompleted = info.IsBookingCompleted;
                adbc.SaveChanges();
                return RedirectToAction("RemainingReferralBookingManagement");
            }

        }

        [HttpGet]
        public ActionResult CheckRemainingOwnerCheckOutManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole("OwnersInfo\\DailyOperations");

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var systemdate = _configurationService.GetSystemDate().Date;
                var date = systemdate.ToShortDateString();

                var BookingObjList = db.OwnerRoomBookings.Where(b =>
                DbFunctions.TruncateTime(b.CheckOutDate) == systemdate
                && b.HasStayedOrNot == "Yes" && b.IsBookingCompleted == "Yes"
                && b.HasCheckedOutOrNot != "Yes"
                ).ToList();

                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();

                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }
                    noOfBooking = (BookingObjList.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }

                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = 1,
                    Date = date,
                    CheckOutDate = _configurationService.GetSystemDate().Date,
                };

                return View(model);
            }
        }

        [HttpGet]
        public ActionResult CheckRemainingOwnerCheckOutManagementDayClose()
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole("OwnersInfo\\DayCloseManagement");

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var systemdate = _configurationService.GetSystemDate().Date;
                var date = systemdate.ToShortDateString();

                var BookingObjList = db.OwnerRoomBookings.Where(b =>
                DbFunctions.TruncateTime(b.CheckOutDate) == systemdate
                && b.HasStayedOrNot == "Yes" && b.IsBookingCompleted == "Yes"
                && b.HasCheckedOutOrNot != "Yes"
                ).ToList();

                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();

                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }
                    noOfBooking = (BookingObjList.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }

                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = 1,
                    Date = date
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult CheckRemainingReferralCheckOutManagement()
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole("OwnersInfo\\DailyOperations");

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var systemdate = _configurationService.GetSystemDate().Date;
                var date = systemdate.ToShortDateString();

                var BookingObjList = db.ReferralBookings.Where(b =>
                DbFunctions.TruncateTime(b.CheckOutDate) == systemdate
                && b.HasStayedOrNot == "Yes" && b.IsBookingCompleted == "Yes"
                && b.HasCheckedOutOrNot != "Yes").ToList();

                List<ReferralBooking> selectedBookings = new List<ReferralBooking>();


                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }
                    noOfBooking = (BookingObjList.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }

                var model = new ReferrelRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    NoOfBooking = noOfBooking,
                    index = 1

                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult CheckRemainingReferralCheckOutManagementDayClose()
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole("OwnersInfo\\DayCloseManagement");

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var systemdate = _configurationService.GetSystemDate().Date;

                var BookingObjList = db.ReferralBookings.Where(b =>
                DbFunctions.TruncateTime(b.CheckOutDate) == systemdate
                && b.HasStayedOrNot == "Yes" && b.IsBookingCompleted == "Yes"
                ).ToList();

                List<ReferralBooking> selectedBookings = new List<ReferralBooking>();

                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }
                    noOfBooking = (BookingObjList.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }

                var model = new ReferrelRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    NoOfBooking = noOfBooking,
                    index = 1
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult ReferralHasCheckedOutorNotDayClose(ReferralBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var system = adbc.SystemDates.ToList().FirstOrDefault();
                var systemdate = adbc.SystemDates.Select(x => x.Date).ToList().FirstOrDefault();
                system.Date = systemdate.AddDays(+1);
                adbc.SaveChanges();
                return RedirectToAction("Logout", "Accounts");
            }

        }

        [HttpPost]
        public ActionResult ReferralHasCheckedOutorNot(ReferralBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerRoomBooking = adbc.ReferralBookings.Where(o => o.ReferralBookingId == info.ReferralBookingId).FirstOrDefault();

                ownerRoomBooking.HasCheckedOutOrNot = info.HasCheckedOutOrNot;
                ownerRoomBooking.CheckOutDate = info.CheckOutDate;
                ownerRoomBooking.TotalExpense = info.TotalExpense;
                ownerRoomBooking.ExtraCharges = info.ExtraCharges;
                ownerRoomBooking.ReferralPoint = info.ReferralPoint;
                adbc.SaveChanges();
                var systemdate = adbc.SystemDates.Select(x => x.Date).ToList().FirstOrDefault();
                systemdate = systemdate.AddDays(+1);
                adbc.SaveChanges();

                return RedirectToAction("CheckRemainingReferralCheckOutManagement");
            }

        }

        #endregion

        #region Single Page Owners Info

        [HttpGet]
        public ActionResult SinglePageOwnersInfoManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            string taskRoles = _commonService.TaskRole(TaskPath.OwnersInfo_SinglePageOwnersInfoManagement);

            if (taskRoles == "" || taskRoles[0] == '0')
            {
                return View("Error");
            }

            OwnersInfoView mdl = new OwnersInfoView()
            {
                IsDataPresent = false,
            };
            return View(mdl);
        }

        [HttpPost]
        public ActionResult SinglePageOwnersInfo(OwnersInfoView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var Id = model.Id;
                var ownersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.SlotId == Id || o.OwnerSmartId == Id);
                if (ownersInfo != null && !(string.IsNullOrEmpty(model.Id)))
                {
                    var slotId = ownersInfo.SlotId;

                    var transactionHistory = adbc.OwnerTransationHistories.FirstOrDefault(o => o.SlotID == slotId);

                    if (transactionHistory == null)
                    {
                        return RedirectToAction("Error", "Home", new { errorMessage = Messages.TransationHistoryNotAssigned });
                    }

                    var ownersSaleDate = adbc.OwnerTransationHistories.Where(t => t.SlotID == slotId).Min(o => o.SaleDate);

                    OwnersInfoView mdl = new OwnersInfoView()
                    {
                        DateOfBirth = ownersInfo.DateOfBirth,
                        FathersName = ownersInfo.FatherName,
                        Name = ownersInfo.Name,
                        NID = ownersInfo.NID,
                        AdminComment = ownersInfo.AdminComment,
                        TIN = ownersInfo.TIN,
                        Email = ownersInfo.Email,
                        PicturePath = ownersInfo.PicturePath,
                        SalesDate = ownersSaleDate.ToString("dd-MMM-yyyy"),
                        SlotId = ownersInfo.SlotId,
                        IsDataPresent = true,

                    };

                    mdl.LedgerViewModel = _bookingService.GetBookingLedger(slotId);

                    return View("SinglePageOwnersInfo", mdl);
                }
                else
                {
                    OwnerSearchByIdView mdl = new OwnerSearchByIdView()
                    {
                        IsDataPresent = true,
                        IsSearchResultFound = false
                    };
                    return View("SinglePageOwnersInfo", mdl);
                }
            }


        }

        public PartialViewResult _UserLog(string slotId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var users = db.Audit2.Where(a => a.TableName != "Audit" && a.TableName != "Notification").ToList();
                List<Audit2> searchedOwner = new List<Audit2>();
                var result = users;
                if (!string.IsNullOrEmpty(slotId))
                {
                    result = users.Where(a => a.UserId.ToLower().Contains(slotId.ToLower())).ToList();
                }
                return PartialView("_UserLogView", result);
            }

        }

        public PartialViewResult _ProfitCalculation(string slotId)
        {


            using (ApplicationDbContext db = new ApplicationDbContext())
            {


                var users = db.Dividends.ToList();
                List<Dividend> searchedOwner = new List<Dividend>();
                var result = users;
                if (!string.IsNullOrEmpty(slotId))
                {
                    result = users.Where(a => a.SlotId.ToLower().Contains(slotId.ToLower())).ToList();
                }
                return PartialView("_ProfitView", result);
            }

        }

        public PartialViewResult _Ledger(string slotId)
        {
            OwnerLedgerViewModel model = _bookingService.GetBookingLedger(slotId);

            return PartialView("_Ledger", model);
        }


        public PartialViewResult _ReservationHistory(string slotId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var users = db.OwnerRoomBookings;
                var result = users.AsQueryable();

                DateTime checkindate = DateTime.Now.Date;
                result = users.Where(a => a.SlotId == slotId && DbFunctions.TruncateTime(a.CheckInDate) <= checkindate);
                return PartialView("_ReservationHistoryView", result.ToList());
            }

        }

        public PartialViewResult _FutureReservation(string slotId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var users = db.OwnerRoomBookings.AsQueryable();
                var result = users;
                DateTime checkindate = DateTime.Now.Date;
                result = users.Where(a => a.SlotId == slotId && DbFunctions.TruncateTime(a.CheckInDate) > checkindate.Date);
                return PartialView("_FutureReservationView", result.ToList());
            }
        }

        #endregion

        #region Dashboard

        [HttpGet]
        public ActionResult DashboardNoticeCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Ownersinfo\\DashboardNoticeManagement").Select(u => u.TaskId)
                    .FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }

                List<string> noticeTypeList = new List<string>()
                {
                    "Report", "Notice","Audit Report"
                };
                var model = new DashboardNoticeView()
                {
                    NoticeId = "",
                    NoticeTypeList = noticeTypeList,
                };
                return View("DashboardNoticeCreate", model);
            }
        }

        [HttpGet]
        public ActionResult DashboardNoticeManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\DashboardNoticeManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "0")
                {
                    return View("Error");
                }

                ////////////////////////////
                var DashboardNoticeObjAllList = db.DashboardNotices.ToList();
                var DashboardNoticeObjList = DashboardNoticeObjAllList.OrderBy(p => p.Date).Reverse().ToList();

                List<DashboardNotice> selectedDashboardNotice = new List<DashboardNotice>();
                int noOfPage;
                if (DashboardNoticeObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedDashboardNotice.Add(DashboardNoticeObjList.ElementAt(i));
                    }
                    int x = (DashboardNoticeObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (DashboardNoticeObjList.Count / 10) + x;
                }
                else
                {
                    selectedDashboardNotice = DashboardNoticeObjList;
                    noOfPage = 1;
                }
                var model = new DashboardNoticeManagementView()
                {

                    index = 1,
                    noOfPage = noOfPage,
                    DashboardNoticeList = selectedDashboardNotice,
                    TaskRoles = taskRoles
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult DashboardNoticePagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo/DashboardnoticeManagement").Select(u => u.TaskId).FirstOrDefault();
                //var myRoleList = (List<UserDetails>)Session[SessionKeys.ROLE_LIST];

                //string taskRoles = "";
                //foreach (var tasks in myRoleList)
                //{
                //    if (tasks.TaskId == taskId)
                //    {
                //        taskRoles = tasks.Details;
                //    }
                //}
                //if (taskRoles == "")
                //{
                //    return View("Error");
                //}

                var DashboardNoticeObjAllList = db.DashboardNotices.ToList();
                var DashboardNoticeObjList = DashboardNoticeObjAllList.OrderBy(p => p.Date).Reverse().ToList();

                List<DashboardNotice> selectedDashboardNotice = new List<DashboardNotice>();
                int noOfPage;
                if (DashboardNoticeObjList.Count >= 10)
                {
                    var pageNo = (DashboardNoticeObjList.Count < (index * 10)) ? DashboardNoticeObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedDashboardNotice.Add(DashboardNoticeObjList.ElementAt(i));
                    }
                    int x = (DashboardNoticeObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (DashboardNoticeObjList.Count / 10) + x;
                }
                else
                {
                    selectedDashboardNotice = DashboardNoticeObjList;
                    noOfPage = 1;
                }
                var model = new DashboardNoticeManagementView()
                {
                    DashboardNoticeList = selectedDashboardNotice,
                    //TaskRoles = taskRoles,
                    noOfPage = noOfPage,
                    index = index
                };
                return View("DashboardNoticeManagement", model);
            }
        }

        [HttpPost]
        public ActionResult DashboardNoticeAddorEdit(DashboardNoticeView info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var NoticeId = "";
                if (string.IsNullOrEmpty(info.NoticeId))
                {
                    var dashboardNoticeList = adbc.DashboardNotices.ToList();
                    var NoticeIdList = new List<int>();
                    foreach (var dashboardNotice in dashboardNoticeList)
                    {
                        NoticeIdList.Add(int.Parse(dashboardNotice.NoticeId.ToString().Split('-')[1]));
                    }

                    var dashboardNoticeToken = dashboardNoticeList.Count();
                    if (dashboardNoticeToken > 0)
                    {
                        NoticeIdList.Sort();
                        dashboardNoticeToken = NoticeIdList[NoticeIdList.Count - 1];
                    }

                    dashboardNoticeToken++;
                    NoticeId = "NOTICE-" + dashboardNoticeToken;


                    ////default locale
                    //System.DateTime.Now.DayOfWeek.ToString();
                    ////localized version
                    DashboardNotice model = new DashboardNotice()
                    {
                        NoticeId = NoticeId,
                        NoticeType = info.NoticeType,
                        PDFFilePath = info.PDFFilePath,
                        //PDFFilePath = "~/Content/ownerprofilepictures/" + info.PDFFilePath ,
                        //PDFFilePath = Path.Combine(@"../shared_resources/", "hello.pdf"),
                        //PDFFilePath = Server.MapPath("/shared"),
                        Date = info.Date,
                        //Day = info.Day,
                        Day = info.Date.ToString("dddd"),
                        FileDisplayName = !string.IsNullOrEmpty(info.FileDisplayName) ? info.FileDisplayName : info.PDFFilePath,
                    };
                    adbc.DashboardNotices.Add(model);
                    var NotificationList = adbc.Notifications.ToList();
                    var NotificationIdList = new List<int>();
                    foreach (var notification in NotificationList)
                    {
                        NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                    }

                    var notificationToken = NotificationList.Count();
                    if (notificationToken > 0)
                    {
                        NotificationIdList.Sort();
                        notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                    }
                    notificationToken++;
                    Notification notificationModel = new Notification()
                    {
                        isSeen = false,
                        NotificationHeader = "Notice",
                        NotificationText = "A notice file has been uploaded at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                        NotificationLink = info.PDFFilePath,
                        NotificationTime = DateTime.Now,
                        NotificationId = "NOT-" + notificationToken,
                        IdForRecognition = NoticeId,
                        DividendHolder = "",
                    };
                    adbc.Notifications.Add(notificationModel);
                    adbc.SaveChanges();
                    return RedirectToAction("DashboardNoticeManagement");
                }
                else
                {
                    var dashboardNoticeInDb = adbc.DashboardNotices.SingleOrDefault(r => r.NoticeId == info.NoticeId);
                    NoticeId = dashboardNoticeInDb.ToString();

                    dashboardNoticeInDb.Date = info.Date;
                    dashboardNoticeInDb.Day = info.Day;
                    dashboardNoticeInDb.NoticeType = info.NoticeType;
                    dashboardNoticeInDb.PDFFilePath = info.PDFFilePath;
                    dashboardNoticeInDb.FileDisplayName = info.FileDisplayName;
                    return RedirectToAction("DashboardNoticeManagement");
                }
            }
        }

        [HttpGet]
        public ActionResult DashboardNoticeView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            return RedirectToAction("DashboardNoticeManagement", "OwnersInfo");
        }

        [HttpPost]
        public ActionResult DashboardNoticeDelete(string NoticeId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var dashboardNoticeInDb = adbc.DashboardNotices.Where(r => r.NoticeId == NoticeId).FirstOrDefault();
                var deletedNoticeId = dashboardNoticeInDb.NoticeId;
                adbc.DashboardNotices.Remove(dashboardNoticeInDb);

                var notificationInDb = adbc.Notifications.Where(n => n.IdForRecognition == deletedNoticeId)
                    .FirstOrDefault();
                adbc.Notifications.Remove(notificationInDb);

                adbc.SaveChanges();
                return RedirectToAction("DashboardNoticeManagement");
            }
        }

        #endregion

        #region Email and SMS

        [HttpPost]
        public JsonResult gettingOwnersInfoForSendingSMS(PartialOwnerViewModel model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string val = "";
                if (model.OwnerIdList == null)
                {
                    val = "no_owner";
                    return Json(val);
                }
                ////Generate Id for EmailSMSNReportTable
                //var emailSMSReportId = "";
                //var emailSMSReportList = adbc.EmailandSmsReports.ToList();
                //var emailSMSReportIdList = new List<int>();
                //foreach (var emailSMSReport in emailSMSReportList)
                //{
                //    emailSMSReportIdList.Add(int.Parse(emailSMSReport.EmailandSMSReportId.ToString().Split('-')[1]));
                //}

                //var emailSMSReportToken = emailSMSReportList.Count();
                //if (emailSMSReportToken > 0)
                //{
                //    emailSMSReportIdList.Sort();
                //    emailSMSReportToken = emailSMSReportIdList[emailSMSReportIdList.Count - 1];
                //}



                foreach (var OwnerId in model.OwnerIdList)
                {
                    var Owner = adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).FirstOrDefault();
                    var mobileNumber = Owner.Mobile;
                    //var serialString = adbc.EmailandSmsReports.Max(e => e.NotificationSerialNo);
                    //var serial = Int16.Parse(serialString);

                    /*To Send an Actual Text Please Uncomment the Following Code!!!*/
                    var isSMSSent = "Processing";
                    if (mobileNumber != null && mobileNumber != "")
                    {
                        var returnStatus = Common.SMSSending(Owner.Mobile, model.SMSText);
                        if (returnStatus == true)
                        {
                            isSMSSent = "Delivered";
                        }
                        else
                        {
                            isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                        }
                    }
                    else
                    {
                        isSMSSent = "Not Delivered, Check whether Mobile No for this owner is correct";
                    }
                    //emailSMSReportToken++;
                    EmailandSMSReport emailsmsreport = new EmailandSMSReport()
                    {
                        IsSMSSent = isSMSSent,
                        Mobile = mobileNumber,
                        Email = Owner.Email,
                        SlotId = Owner.SlotId,
                        Name = Owner.Name,
                        TimeOfDelivery = DateTime.Now,
                        NotificationType = "SMS",
                        //EmailandSMSReportId = "ESR-" + emailSMSReportToken,
                        Text = model.SMSText,
                        //NotificationSerialNo = (serial++).ToString(),
                        NotificationSerialNo = "MANUAL SMS",
                    };
                    //adbc.EmailandSmsReports.Add(emailsmsreport);
                    Common.InsertEmailandSMSReports(emailsmsreport);
                    adbc.SaveChanges();
                }
                adbc.Dispose();
                //                string val = "";
                val = !string.IsNullOrEmpty(model.SMSText) ? "true" : "false";
                return Json(val);
            }
        }

        [HttpPost]
        public async Task<JsonResult> gettingOwnersInfoForSendingEmailAsync(PartialOwnerViewModelForEmail model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string val = "";
                if (model.OwnerIdList == null)
                {
                    val = "no_owner";
                    return Json(val);
                }
                List<string> emailReceiver = new List<string>();
                foreach (var OwnerId in model.OwnerIdList)
                {
                    var ownerEmail = adbc.OwnersInfos.FirstOrDefault(o => o.OwenerId == OwnerId).Email;
                    if (!String.IsNullOrEmpty(ownerEmail))
                    {
                        bool isValidEmail = Regex.IsMatch(ownerEmail, @"^([\w-\.+]+)@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$");
                        if (isValidEmail)
                            emailReceiver.Add(ownerEmail);
                    }
                }
                var EmailHeader = "Notice From Heritage BD Ltd.";
                bool isSent = await Common.EmailSending_Bulk(emailReceiver, EmailHeader, model.EmailText);
                adbc.Dispose();
                return Json(val);
            }
        }

        [HttpGet]
        public ActionResult EmailandSMSReportSearchOption()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<string> groupList = new List<string>();
                groupList.Add("All");
                List<string> notificationStatusList = new List<string>();
                notificationStatusList.Add("All");
                //groupList = db.EmailandSmsReports.DistinctBy(g => g.NotificationSerialNo)
                //.Select(g => g.NotificationSerialNo).ToList();
                List<string> notificationStatusListTemp = new List<string>();
                notificationStatusListTemp = db.EmailandSmsReports.DistinctBy(g => g.IsSMSSent)
                    .Select(g => g.IsSMSSent).ToList();
                foreach (var notificationStatus in notificationStatusListTemp)
                {
                    notificationStatusList.Add(notificationStatus);
                }

                List<string> groupListTemp = new List<string>();
                groupListTemp = db.EmailandSmsReports.DistinctBy(g => g.NotificationSerialNo)
                    .Select(g => g.NotificationSerialNo).ToList();
                foreach (var group in groupListTemp)
                {
                    groupList.Add(group);
                }

                var model = new EmailSMSReportOptionView()
                {
                    NotificationStatusList = notificationStatusList,
                    GroupList = groupList
                };
                return View(model);
            }
        }
        //It is gettingEmailSMSReportSearchResult function actually
        [HttpPost]
        public JsonResult EmailandSMSReportSearch(EmailSMSReportOptionView info)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            //return RedirectToAction("EmailandSMSReportManagement", "OwnersInfo", info);
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<EmailSMSReportViewForSearch> EmailSMSReportViewForSearchList = new List<EmailSMSReportViewForSearch>();
                //for skip option in SearchOption Page
                if (info == null || info.Group == null)
                {
                    var EmailandSMSReportList = db.EmailandSmsReports.ToList();
                    EmailandSMSReportList = EmailandSMSReportList.OrderBy(e => e.TimeOfDelivery).ToList();
                    foreach (var emailandSmsReport in EmailandSMSReportList)
                    {
                        EmailSMSReportViewForSearch model = new EmailSMSReportViewForSearch();
                        model.SlotId = emailandSmsReport.SlotId;
                        model.Email = emailandSmsReport.Email;
                        model.EmailandSMSReportId = emailandSmsReport.EmailandSMSReportId;
                        model.IsSMSSent = emailandSmsReport.IsSMSSent;
                        model.Mobile = emailandSmsReport.Mobile;
                        model.Name = emailandSmsReport.Name;
                        model.NotificationSerialNo = emailandSmsReport.NotificationSerialNo;
                        model.Text = (emailandSmsReport.Text.Length > 20) ? emailandSmsReport.Text.Substring(0, 5) + "..." : emailandSmsReport.Text;
                        model.NotificationType = emailandSmsReport.NotificationType;
                        model.TimeOfDelivery = emailandSmsReport.TimeOfDelivery.ToString("dd-MMM-yyyy hh:mm tt");
                        EmailSMSReportViewForSearchList.Add(model);
                    }
                    return Json(EmailSMSReportViewForSearchList);
                }
                //var selectedMonth = info.Month;
                var selectedFirstDate = info.FirstDate;
                var selectedLastDate = info.LastDate;
                var selectedGroup = info.Group;
                var selectedNotificationStatus = info.NotificationStatus;
                var resultingList = new List<EmailandSMSReport>();

                resultingList = db.EmailandSmsReports.ToList();
                if (selectedGroup == "All" && selectedNotificationStatus != "All")
                {
                    resultingList = resultingList.Where(r => r.IsSMSSent == selectedNotificationStatus && r.TimeOfDelivery >= selectedFirstDate && r.TimeOfDelivery <= selectedLastDate).ToList();
                }
                else if (selectedGroup != "All" && selectedNotificationStatus == "All")
                {
                    resultingList = resultingList.Where(r => r.NotificationSerialNo == selectedGroup && r.TimeOfDelivery >= selectedFirstDate && r.TimeOfDelivery <= selectedLastDate).ToList();
                }
                else if (selectedGroup == "All" && selectedNotificationStatus == "All")
                {

                    resultingList = resultingList.Where(r => r.TimeOfDelivery >= selectedFirstDate && r.TimeOfDelivery <= selectedLastDate).ToList();
                }
                else
                {
                    resultingList = resultingList.Where(r =>
                        r.NotificationSerialNo == selectedGroup && r.IsSMSSent == selectedNotificationStatus && r.TimeOfDelivery >= selectedFirstDate && r.TimeOfDelivery <= selectedLastDate).ToList();
                }
                var EmailandSMSReportList1 = resultingList;
                EmailandSMSReportList1 = EmailandSMSReportList1.OrderBy(e => e.TimeOfDelivery).Reverse().ToList();
                foreach (var emailandSmsReport in EmailandSMSReportList1)
                {
                    EmailSMSReportViewForSearch model = new EmailSMSReportViewForSearch();
                    model.SlotId = emailandSmsReport.SlotId;
                    model.Email = emailandSmsReport.Email;
                    model.EmailandSMSReportId = emailandSmsReport.EmailandSMSReportId;
                    model.IsSMSSent = emailandSmsReport.IsSMSSent;
                    model.Mobile = emailandSmsReport.Mobile;
                    model.Name = emailandSmsReport.Name;
                    model.NotificationSerialNo = emailandSmsReport.NotificationSerialNo;
                    model.Text = (emailandSmsReport.Text.Length > 20) ? emailandSmsReport.Text.Substring(0, 5) + "..." : emailandSmsReport.Text;
                    model.NotificationType = emailandSmsReport.NotificationType;
                    model.TimeOfDelivery = emailandSmsReport.TimeOfDelivery.ToString("dd-MMM-yyyy hh:mm tt");
                    EmailSMSReportViewForSearchList.Add(model);
                }
                return Json(EmailSMSReportViewForSearchList);
            }
        }
        [HttpGet]
        public ActionResult EmailandSMSReportManagement(EmailSMSReportOptionView info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //task roles
                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\EmailandSMSReportSearchOption")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }

                //for skip option in SearchOption Page
                //if (info == null || info.Group == null)
                //{


                var EmailSMSObjAllList = db.EmailandSmsReports.ToList();
                var emailandSmsReportsObjList = EmailSMSObjAllList.OrderBy(p => p.TimeOfDelivery).Reverse().ToList();
                //var emailandSmsReportsObjList = db.EmailandSmsReports.OrderBy(e=>e.TimeOfDelivery).Reverse().ToList();
                List<EmailandSMSReport> selectedEmailSMSReports = new List<EmailandSMSReport>();
                int noOfEmailSMSReport;
                if (emailandSmsReportsObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedEmailSMSReports.Add(emailandSmsReportsObjList.ElementAt(i));
                    }
                    int x = (emailandSmsReportsObjList.Count % 10 == 0) ? 0 : 1;

                    noOfEmailSMSReport = (emailandSmsReportsObjList.Count / 10) + x;
                }
                else
                {
                    selectedEmailSMSReports = emailandSmsReportsObjList;
                    noOfEmailSMSReport = 1;
                }

                //Trimming the text
                List<EmailandSMSReport> resultingEmailandSMSReportList = new List<EmailandSMSReport>();
                var EmailandSMSReportList1 = selectedEmailSMSReports;

                foreach (var emailandSmsReport in EmailandSMSReportList1)
                {
                    EmailandSMSReport model = new EmailandSMSReport();
                    model.SlotId = emailandSmsReport.SlotId;
                    model.Email = emailandSmsReport.Email;
                    model.EmailandSMSReportId = emailandSmsReport.EmailandSMSReportId;
                    model.IsSMSSent = (emailandSmsReport.IsSMSSent.Length > 20) ? emailandSmsReport.IsSMSSent.Substring(0, 20) + "..." : emailandSmsReport.IsSMSSent;
                    model.Mobile = emailandSmsReport.Mobile;
                    model.Name = emailandSmsReport.Name;
                    model.NotificationSerialNo = emailandSmsReport.NotificationSerialNo;
                    model.Text = (emailandSmsReport.Text.Length > 20) ? emailandSmsReport.Text.Substring(0, 20) + "..." : emailandSmsReport.Text;
                    model.NotificationType = emailandSmsReport.NotificationType;
                    model.TimeOfDelivery = emailandSmsReport.TimeOfDelivery;

                    resultingEmailandSMSReportList.Add(model);
                }
                var model2 = new EmailandSMSReportManagementView()
                {
                    //EmailandSMSReportList = selectedEmailSMSReports,
                    EmailandSMSReportList = resultingEmailandSMSReportList,
                    TaskRoles = taskRoles,
                    NoOfEmailSMSReport = noOfEmailSMSReport,
                    index = 1
                };
                return View(model2);
            }

        }

        [HttpGet]
        public ActionResult EmailandSMSReportPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\EmailandSMSReportSearchOption").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }

                var EmailSMSObjAllList = db.EmailandSmsReports.ToList();
                var emailandSmsReportsObjList = EmailSMSObjAllList.OrderBy(p => p.TimeOfDelivery).Reverse().ToList();
                List<EmailandSMSReport> selectedEmailandSmsReports = new List<EmailandSMSReport>();
                int noOfEmailSMSReport;
                if (emailandSmsReportsObjList.Count >= 10)
                {
                    var pageNo = (emailandSmsReportsObjList.Count < (index * 10)) ? emailandSmsReportsObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedEmailandSmsReports.Add(emailandSmsReportsObjList.ElementAt(i));
                    }
                    int x = (emailandSmsReportsObjList.Count % 10 == 0) ? 0 : 1;

                    noOfEmailSMSReport = (emailandSmsReportsObjList.Count / 10) + x;
                }
                else
                {
                    selectedEmailandSmsReports = emailandSmsReportsObjList;
                    noOfEmailSMSReport = 1;
                }

                //Trimming the text
                List<EmailandSMSReport> resultingEmailandSMSReportList = new List<EmailandSMSReport>();
                var EmailandSMSReportList1 = selectedEmailandSmsReports;

                foreach (var emailandSmsReport in EmailandSMSReportList1)
                {
                    EmailandSMSReport model = new EmailandSMSReport();
                    model.SlotId = emailandSmsReport.SlotId;
                    model.Email = emailandSmsReport.Email;
                    model.EmailandSMSReportId = emailandSmsReport.EmailandSMSReportId;
                    model.IsSMSSent = emailandSmsReport.IsSMSSent;
                    model.Mobile = emailandSmsReport.Mobile;
                    model.Name = emailandSmsReport.Name;
                    model.NotificationSerialNo = emailandSmsReport.NotificationSerialNo;
                    model.Text = (emailandSmsReport.Text.Length > 20) ? emailandSmsReport.Text.Substring(0, 20) + "..." : emailandSmsReport.Text;
                    model.NotificationType = emailandSmsReport.NotificationType;
                    model.TimeOfDelivery = emailandSmsReport.TimeOfDelivery;

                    resultingEmailandSMSReportList.Add(model);
                }

                var model2 = new EmailandSMSReportManagementView()
                {
                    //EmailandSMSReportList = selectedEmailandSmsReports,
                    EmailandSMSReportList = resultingEmailandSMSReportList,
                    TaskRoles = taskRoles,
                    NoOfEmailSMSReport = noOfEmailSMSReport,
                    index = index
                };
                return View("EmailandSMSReportManagement", model2);
            }
        }

        [HttpPost]
        public JsonResult gettingSearchResultForEmailSMS(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\EmailandSMSReportSearchOption").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId) taskRoles = tasks.Details;
                }
                if (taskRoles == "")
                {
                    return Json("Error");
                }

                var EmailSMSList = db.EmailandSmsReports.ToList();
                EmailSMSList = EmailSMSList.OrderBy(e => e.TimeOfDelivery).Reverse().ToList();
                List<EmailSMSReportViewForSearch> searchedEmailSMS = new List<EmailSMSReportViewForSearch>();
                var searchfield1 = searchfield.ToUpper();

                foreach (var emailsms in EmailSMSList)
                {
                    var SlotId = "";
                    if (emailsms.SlotId != null)
                    {
                        SlotId = emailsms.SlotId.ToUpper();
                    }
                    var Email = "";
                    if (emailsms.Email != null)
                    {
                        Email = emailsms.Email.ToUpper();
                    }
                    var NotificationType = "";
                    if (emailsms.NotificationType != null)
                    {
                        NotificationType = emailsms.NotificationType.ToUpper();
                    }
                    var Mobile = "";
                    if (emailsms.Mobile != null)
                    {
                        Mobile = emailsms.Mobile.ToUpper();
                    }
                    var Name = "";
                    if (emailsms.Name != null)
                    {
                        Name = emailsms.Name.ToUpper();
                    }

                    var TimeOfDelivery = "";
                    if (emailsms.TimeOfDelivery != null)
                    {
                        TimeOfDelivery = emailsms.TimeOfDelivery.ToString("dd-MMM-yyyy hh:mm tt").ToUpper();
                    }

                    var EmailandSMSReportId = "";
                    if (emailsms.EmailandSMSReportId != null)
                    {
                        EmailandSMSReportId = emailsms.EmailandSMSReportId.ToUpper();
                    }

                    var IsSMSSent = "";
                    if (emailsms.IsSMSSent != null)
                    {
                        IsSMSSent = emailsms.IsSMSSent.ToUpper();
                    }
                    var NotificationSerialNo = "";
                    if (emailsms.NotificationSerialNo != null)
                    {
                        NotificationSerialNo = emailsms.NotificationSerialNo.ToUpper();
                    }
                    var SlotId1 = SlotId;
                    var Name1 = Name;
                    var Email1 = Email;
                    var NotificationType1 = NotificationType;
                    var IsSMSSent1 = IsSMSSent;
                    var TimeOfDelivery1 = TimeOfDelivery;

                    if (SlotId1.Contains(searchfield1) == true || Name1.Contains(searchfield1) == true ||
                        Email1.Contains(searchfield1) == true || NotificationType1.Contains(searchfield1) == true
                        || IsSMSSent1.Contains(searchfield1) == true || TimeOfDelivery1.Contains(searchfield1) == true)
                    {
                        EmailSMSReportViewForSearch emailsmsreportsearch = new EmailSMSReportViewForSearch();
                        emailsmsreportsearch.SlotId = emailsms.SlotId;
                        emailsmsreportsearch.Email = emailsms.Email;
                        emailsmsreportsearch.EmailandSMSReportId = emailsms.EmailandSMSReportId;
                        emailsmsreportsearch.IsSMSSent = (emailsms.IsSMSSent.Length > 20) ? emailsms.IsSMSSent.Substring(0, 20) + "..." : emailsms.IsSMSSent;
                        emailsmsreportsearch.Mobile = emailsms.Mobile;
                        emailsmsreportsearch.NotificationSerialNo = emailsms.NotificationSerialNo;
                        emailsmsreportsearch.NotificationType = emailsms.NotificationType;
                        emailsmsreportsearch.Text = (emailsms.Text.Length > 20) ? emailsms.Text.Substring(0, 20) + "..." : emailsms.Text;
                        emailsmsreportsearch.Name = emailsms.Name;
                        emailsmsreportsearch.TimeOfDelivery = emailsms.TimeOfDelivery.ToString("dd-MMM-yyyy hh:mm tt");

                        searchedEmailSMS.Add(emailsmsreportsearch);
                    }
                }
                return Json(searchedEmailSMS);

            }
        }

        [HttpGet]
        public ActionResult EmailAndSMSReportView(string auditId)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var emailSMSObj = adbc.EmailandSmsReports.Where(a => a.EmailandSMSReportId == auditId).FirstOrDefault();
                return View(emailSMSObj);
            }
        }

        #endregion

        [HttpGet]
        public ActionResult OwnersInfoNotification(string searchfield = "")
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\OwnersInfoNotification").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[4] == '0')
                {
                    return View("Error");
                }
                var FullNotificationObjList = db.OwnersInfos.ToList();
                //Searching
                List<OwnersInfo> NotificationObjList = new List<OwnersInfo>();

                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var notification in FullNotificationObjList)
                    {
                        var SlotId = "";
                        if (notification.SlotId != null)
                        {
                            SlotId = notification.SlotId.ToUpper();
                        }



                        var SlotId1 = SlotId;

                        if (SlotId1 == searchfield1)
                        {
                            NotificationObjList.Add(notification);
                        }
                    }
                }
                else
                {
                    NotificationObjList = FullNotificationObjList;
                }
                var model = new OwnerList
                {
                    Owners = NotificationObjList,
                    TaskRoles = taskRoles,
                    SearchField = searchfield
                };
                return View(model);
            }
        }

        [HttpPost]
        public JsonResult gettingNoticeSearchResult(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var NoticeList = db.DashboardNotices.ToList();
                List<DashboardNotice> searchedNotice = new List<DashboardNotice>();
                var searchfield1 = searchfield.ToUpper();

                foreach (var notice in NoticeList)
                {

                    var Day = "";
                    if (notice.Day != null)
                    {
                        Day = notice.Day.ToUpper();
                    }
                    var PDFFilePath = "";
                    if (notice.PDFFilePath != null)
                    {
                        PDFFilePath = notice.PDFFilePath.ToUpper();
                    }
                    var NoticeId = "";
                    if (notice.NoticeId != null)
                    {
                        NoticeId = notice.NoticeId.ToUpper();
                    }
                    var FileDisplayName = "";
                    if (notice.FileDisplayName != null)
                    {
                        FileDisplayName = notice.FileDisplayName.ToUpper();
                    }
                    //var Date1 = Date;
                    var Day1 = Day;
                    var PDFFilePath1 = PDFFilePath;
                    var NoticeId1 = NoticeId;
                    var FileDisplayName1 = FileDisplayName;

                    if (Day1.Contains(searchfield1) == true ||
                        PDFFilePath1.Contains(searchfield1) == true || NoticeId1.Contains(searchfield1) == true
                        || FileDisplayName1.Contains(searchfield1) == true)
                    {
                        searchedNotice.Add(notice);
                    }

                }
                string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    //return View(Json("Error"));
                }

                return Json(searchedNotice);

            }
        }
    }
}