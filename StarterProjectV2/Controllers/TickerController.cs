﻿using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using System.Text;
using System.IO;
namespace StarterProjectV2.Controllers
{
    public class TickerController : Controller
    {
        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        [HttpGet]
        public ActionResult TickerManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                string taskId = db.Tasks.Where(u => u.TaskPath == "Ticker\\TickerManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "0")
                {
                    return View("Error");
                }

                ////////////////////////////
                var TickerAllList = db.Tickers.ToList();

                List<Ticker> TickerObjList = new List<Ticker>();
                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var audit in TickerAllList)
                    {
                        //DateTime ApplicableDate = new DateTime();
                        //if (audit.ApplicableDate != null)
                        //{
                        //    ApplicableDate = (DateTime)audit.ApplicableDate;
                        //}
                        var TickerType = "";
                        if (audit.TickerType != null)
                        {
                            TickerType = audit.TickerType.ToUpper();
                        }
                        var Title = "";
                        if (audit.Title != null)
                        {
                            Title = audit.Title.ToUpper();
                        }
                        var Details = "";
                        if (audit.Details != null)
                        {
                            Details = audit.Details.ToUpper();
                        }

                        var TickerType1 = TickerType;
                        var Title1 = Title;
                        var Details1 = Details;
                        //string date1 = ApplicableDate1.ToString();


                        if (TickerType1.Contains(searchfield1) == true || Title1.Contains(searchfield1) == true || Details1.Contains(searchfield1) == true )
                        {
                            TickerObjList.Add(audit);
                        }
                    }
                }
                else
                {
                    TickerObjList = TickerAllList;
                }
                List<Ticker> selectedTicker = new List<Ticker>();
                int noOfPage;
                if (TickerObjList.Count >= 10)
                {
                    noOfPage = (TickerObjList.Count < (index * 10)) ? TickerObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < noOfPage; i++)
                    {
                        selectedTicker.Add(TickerObjList.ElementAt(i));
                    }
                    int x = (TickerObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (TickerObjList.Count / 10) + x;
                }
                else
                {
                    selectedTicker = TickerObjList;
                    noOfPage = 1;
                }
                var model = new TickerManagementView()
                {

                    index = 1,
                    noOfPage = noOfPage,
                    TickerList = selectedTicker,
                    TaskRoles = taskRoles,
                    searchfield = searchfield
                };
                return View(model);
            }

        }
        //[HttpGet]
        //public ActionResult TickerPagination(int index)
        //{
        //    if (!IsUserLoggedIn())
        //        return RedirectToAction("Login", "Accounts");

        //    using (ApplicationDbContext db = new ApplicationDbContext())
        //    {
        //        var TickerAllList = db.Tickers.ToList();


        //        List<Ticker> selectedTicker = new List<Ticker>();
        //        int noOfPage;
        //        if (TickerAllList.Count >= 10)
        //        {
        //            for (int i = 0; i < 10; i++)
        //            {
        //                selectedTicker.Add(TickerAllList.ElementAt(i));
        //            }
                    
        //            noOfPage = (TickerAllList.Count + 9) / 10;
        //        }
        //        else
        //        {
        //            selectedTicker = TickerAllList;
        //            noOfPage = 1;
        //        }
        //        var model = new TickerManagementView()
        //        {

        //            index = 1,
        //            noOfPage = noOfPage,
        //            TickerList = selectedTicker,
        //            //TaskRoles = taskRoles
        //        };
        //        return View("TickerManagement", model);
        //    }
        //}
        [HttpGet]
        public ActionResult TickerCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Ticker\\TickerManagement").Select(u => u.TaskId)
                    .FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }

             
                var model = new TickerView()
                {
                    TickerId = "",
                  
                };
                return View("TickerCreate", model);
            }
        }
        [HttpPost]
        public ActionResult TickerAddorEdit(TickerView info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var TickerId = "";
                if (string.IsNullOrEmpty(info.TickerId))
                {
                    var TickerList = adbc.Tickers.ToList();
                    var TickerIdList = new List<int>();
                    foreach (var Ticker in TickerList)
                    {
                        TickerIdList.Add(int.Parse(Ticker.TickerId.ToString().Split('-')[1]));
                    }

                    var TickerToken = TickerList.Count();
                    if (TickerToken > 0)
                    {
                        TickerIdList.Sort();
                        TickerToken = TickerIdList[TickerIdList.Count - 1];
                    }

                    TickerToken++;
                    TickerId = "Ticker-" + TickerToken;


               
                    Ticker model = new Ticker()
                    {
                        TickerId = TickerId,
                        TickerType = info.TickerType,
                        Title = info.Title,
                  
                        Details = info.Details,
                        Link = info.Link,
                        
                    };
                    adbc.Tickers.Add(model);
                    adbc.SaveChanges();
                    return RedirectToAction("TickerManagement");
                }
                else
                {
                    var TickerInDb = adbc.Tickers.SingleOrDefault(r => r.TickerId == info.TickerId);
                    TickerId = TickerInDb.ToString();

                    TickerInDb.TickerType = info.TickerType;
                    TickerInDb.Title = info.Title;
                    TickerInDb.Details = info.Details;
                    TickerInDb.Link = info.Link;
                    //TickerInDb.FileDisplayName = info.FileDisplayName;
                    return RedirectToAction("TickerManagement");
                }
            }
        }
        [HttpGet]
        public ActionResult TickerView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            return RedirectToAction("TickerManagement", "Ticker");
        }
        [HttpPost]
        public ActionResult TickerDelete(string TickerId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Ticker\\TickerManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[4] == '0')
                {
                    return View("Error");
                }
                var TickerInDb = adbc.Tickers.Where(r => r.TickerId == TickerId).FirstOrDefault();
                var deletedTickerId = TickerInDb.TickerId;
                adbc.Tickers.Remove(TickerInDb);


                adbc.SaveChanges();
                return RedirectToAction("TickerManagement");
            }
        }
        [HttpGet]
        public ActionResult TickerEdit(string TickerId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Ticker\\TickerManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }
                var model = new TickerView()
                {
                    TickerId = "",
                    //TickerTypeList = TickerTypeList,
                };
                return View( model);
            }
        }

        [HttpPost]
        public ActionResult TickerEdit(TickerView mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var TickerObj = adbc.Tickers.Where(a => a.TickerId == mdl.TickerId).FirstOrDefault();
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Ticker\\TickerManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                TickerObj.TickerType = mdl.TickerType;
                TickerObj.Title = mdl.Title;
                TickerObj.Details = mdl.Details;
                TickerObj.Link = mdl.Link;
                adbc.SaveChanges();

                var TickerList = adbc.Tickers.ToList();

                TickerView model = new TickerView()
                {
                    //TickerList = TickerList,
                    TaskRoles = taskRoles
                };
                return View("TickerManagement", model);
            }
        }

    }
}