﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
//using Rotativa;
namespace StarterProjectV2.Controllers
{
    public class AuditController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
   
        [HttpGet]
        public ActionResult AuditManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Audit\\AuditManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");
                }
                
                var FullAuditObjList = adbc.Audit2.ToList();
                //Searching
                List<Audit2> AuditObjList = new List<Audit2>();
                
                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var audit in FullAuditObjList)
                    {
                        var UserId = "";
                        if (audit.UserId != null)
                        {
                            UserId = audit.UserId.ToUpper();
                        }

                        var Actions = "";
                        if (audit.Actions != null)
                        {
                            Actions = audit.Actions.ToUpper();
                        }
                        DateTime UpdateDate = new DateTime();
                        if (audit.UpdateDate != null)
                        {
                            UpdateDate = (DateTime)audit.UpdateDate;
                        }
                        var TableName = "";
                        if (audit.TableName != null)
                        {
                            TableName = audit.TableName.ToUpper();
                        }
                     

                        var UserId1 = UserId;
                        var Actions1 = Actions;
                        var TableName1 = TableName;
                        var UpdateDate1 = UpdateDate;
                        string date = UpdateDate1.ToString();
                        //var AuditDTTM1 = AuditDTTM;

                        if (UserId1.Contains(searchfield1) == true || Actions1.Contains(searchfield1) == true || TableName1.Contains(searchfield1) == true || date.Contains(searchfield1) == true)
                        {
                            AuditObjList.Add(audit);
                        }
                    }
                }
                else
                {
                    AuditObjList = FullAuditObjList;
                }
                //Search Finished
                //Pagination Start
                List<Audit2> selectedAudits = new List<Audit2>();
                int noOfAudits;
                if (AuditObjList.Count >= 10)
                {

                    var pageNo = (AuditObjList.Count < (index * 10)) ? AuditObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedAudits.Add(AuditObjList.ElementAt(i));
                    }
                    
                    noOfAudits = (AuditObjList.Count+9) / 10;
                }
                else
                {
                    selectedAudits = AuditObjList;
                    noOfAudits = 1;
                }
                var model = new AuditList
                {
                    Audits = selectedAudits,
                    TaskRoles = taskRoles,
                    NoOfAudit = noOfAudits,
                    index = index,
                    searchfield = searchfield
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult AuditView(int Id)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var auditObj = adbc.Audit2.Where(a => a.Id == Id).FirstOrDefault();
                return View(auditObj);
            }
        }
    }
}