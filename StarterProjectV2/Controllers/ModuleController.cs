﻿using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace StarterProjectV2.Controllers
{
    public class ModuleController : Controller
    {
         
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult ModuleManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Module\\ModuleManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }


                var model = new ModuleList()
                {
                    modules = db.Modules.ToList(),
                    TaskRoles = taskRoles
                };
                return View(model);
            }

        }

        [HttpGet]
        public ActionResult ModuleCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Module\\ModuleManagement").Select(u => u.TaskId)
                    .FirstOrDefault();
                var myRoleList = (List<UserRole>) Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult ModuleCreate(Module newModule)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            //return Content(newModule.ModuleId+" "+newModule.ModuleName);
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext adbc = new ApplicationDbContext())
                {

                    if (string.IsNullOrEmpty(newModule.ModuleId))
                    {

                        var moduleList = adbc.Modules.ToList();
                        var moduleIdList = new List<int>();
                        foreach (var module in moduleList)
                        {
                            moduleIdList.Add(int.Parse(module.ModuleId.ToString().Split('-')[1]));
                        }
                        var moduleToken = moduleList.Count();
                        if (moduleToken > 0)
                        {
                            moduleIdList.Sort();
                            moduleToken = moduleIdList[moduleIdList.Count - 1];
                        }
                        moduleToken++;
                        newModule.ModuleId = "MDL-" + moduleToken;



                        adbc.Modules.Add(newModule);
                    }
                    else
                    {
                        var moduleInDb = adbc.Modules.SingleOrDefault(m => m.ModuleId == newModule.ModuleId);
                        moduleInDb.ModuleName = newModule.ModuleName;
                        moduleInDb.ModuleOrder = newModule.ModuleOrder;

                    }
                    adbc.SaveChanges();
                    Session[SessionKeys.MODULE_LIST] = Common.getMenuList(Session[SessionKeys.USERNAME].ToString());
                    var userName = Session[SessionKeys.USERNAME].ToString();
                    var userRole = adbc.Users.FirstOrDefault(u => u.UserName == userName).UserType.ToString();
                    Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(userRole);
                    //Session[SessionKeys.ROLE_LIST] = Common.getTasksWithRoleForUser(Session[SessionKeys.USERNAME].ToString());
                    return RedirectToAction("ModuleManagement");
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult ModuleDelete(string moduleId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    string taskId = db.Tasks.Where(u => u.TaskPath == "Module\\ModuleManagement").Select(u => u.TaskId)
                        .FirstOrDefault();
                    var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                    string taskRoles = "";
                    foreach (var tasks in myRoleList)
                    {
                        if (tasks.TaskId == taskId)
                        {
                            taskRoles = tasks.Details;
                        }
                    }

                    if (taskRoles[4] == '0')
                    {
                        return View("Error");
                    }
                }

                var delobj = adbc.Modules.Where(p => p.ModuleId == moduleId).SingleOrDefault();
                adbc.Modules.Remove(delobj);



                adbc.SaveChanges();
                Session[SessionKeys.MODULE_LIST] = Common.getMenuList(Session[SessionKeys.USERNAME].ToString());
                var userName = Session[SessionKeys.USERNAME].ToString();
                var userRole = adbc.Users.FirstOrDefault(u => u.UserName == userName).UserType.ToString();
                Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(userRole);
                return RedirectToAction("ModuleManagement");
            }
            //return Content(moduleId);
        }

        [HttpGet]
        public ActionResult ModuleEdit(string moduleId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    string taskId = db.Tasks.Where(u => u.TaskPath == "Module\\ModuleManagement").Select(u => u.TaskId)
                        .FirstOrDefault();
                    var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                    string taskRoles = "";
                    foreach (var tasks in myRoleList)
                    {
                        if (tasks.TaskId == taskId)
                        {
                            taskRoles = tasks.Details;
                        }
                    }

                    if (taskRoles[3] == '0')
                    {
                        return View("Error");
                    }
                }

                var model = adbc.Modules.Where(p => p.ModuleId == moduleId).SingleOrDefault();
                //adbc.Modules.Remove(delobj);
                //adbc.SaveChanges();
                return View("ModuleCreate", model);
            }
        }

        [HttpGet]
        public ActionResult ModuleView(string moduleId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var model = adbc.Modules.Where(p => p.ModuleId == moduleId).SingleOrDefault();
                //adbc.Modules.Remove(delobj);
                //adbc.SaveChanges();
                return View("ModuleView", model);
            }
        }

        [HttpGet]
        public ActionResult ModuleReport()
        {
            return Redirect("../Reports/ModuleReport.aspx");
        }


    }
}