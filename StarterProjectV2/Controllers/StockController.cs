﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StarterProjectV2.Controllers
{
    public class StockController : Controller
    {
        [HttpGet]
        public ActionResult index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AjaxMethod(Stock person)
        {
            string name = person.Name;
            string title = person.Title;
            System.Threading.Thread.Sleep(5000);
            return Json(person);
        }

    }
}