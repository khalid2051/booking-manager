﻿using StarterProjectV2.Models;
using StarterProjectV2.Services;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace StarterProjectV2.Controllers
{
    public class BookingController : Controller
    {
        private readonly ConfigurationService _configurationService = new ConfigurationService();
        private readonly CommonService _commonService = new CommonService();
        private readonly InvoiceService _invoiceService = new InvoiceService();
        private readonly BookingService _bookingService = new BookingService();
        private readonly EnumDataService _enumDataService = new EnumDataService();
        private const string RelationshipEnumGroup = "Relationship";

        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult BookingCreate()
        {
            if (!Common.IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            string taskRoles = _commonService.TaskRole(TaskPath.Booking_OwnerRoomBookingManagement);

            if (taskRoles == "" || taskRoles[0] == '0')
            {
                return View("Error");
            }

            var model = new OwnerRoomBookingView()
            {
                IsError = false,
                IsBookingCompleted = "Yes",
                IsDayClosePageExists = "Yes",
                RelationEnumList = _enumDataService.GetEnumData(RelationshipEnumGroup),
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult BookingAddorEdit(OwnerRoomBooking booking)
        {
            if (!Common.IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            ViewBag.Current = "Booking";

            booking.Startdate = booking.CheckInDate;
            booking.Enddate = booking.CheckOutDate;

            ResponseModel response = _bookingService.BookingInsert(booking);

            if (response.Status == Messages.StatusFail)
            {
                var model = new OwnerRoomBookingView();
                ViewBag.Status = response.Status;
                ViewBag.Message = response.Message;
                return View("BookingCreate", model);
            }

            return RedirectToAction("OwnerRoomBookingManagement");
        }

        [HttpGet]
        public ActionResult OwnerRoomBookingEdit(string ownerRoomBookngId)
        {
            if (!Common.IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            string taskRoles = _commonService.TaskRole("Booking\\OwnerRoomBookingManagement");

            if (taskRoles == "" || taskRoles[0] == '0')
            {
                return View("Error");
            }

            using (ApplicationDbContext _dbContext = new ApplicationDbContext())
            {
                var booking = _dbContext.OwnerRoomBookings.Where(r => r.OwnerRoomBookngId == ownerRoomBookngId).FirstOrDefault();

                //Determining How many Nights he can spend in a certain Fiscal Year.

                GetFiscalDates(out DateTime startingFiscalDate, out DateTime endingFiscalDate);

                ResponseModel response = new ResponseModel();

                int canStay = GetCanStayDays(_dbContext, booking, startingFiscalDate, endingFiscalDate, ref response);

                if (response.Status == Messages.StatusFail)
                {
                    return RedirectToAction("Error", "Home", new { errorMessage = response.Message });
                }

                int totalSpent = GetSpentDays(_dbContext, booking, startingFiscalDate, endingFiscalDate);

                bool isRoomBookingTokenExists = !string.IsNullOrEmpty(booking.BookingToken);

                OwnerRoomBookingView mdl = new OwnerRoomBookingView()
                {
                    OwnerRoomBookingId = booking.OwnerRoomBookngId,
                    Startdate = booking.Startdate,
                    Enddate = booking.Enddate,
                    CheckInDate = booking.CheckInDate,
                    CheckOutDate = booking.CheckOutDate,
                    NoOfRoomAllotted = booking.NoOfRoomAllotted,
                    ApplicationDateString = booking.ApplicationDate.ToString("dd-MMM-yyyy hh:mm tt"),
                    NoOfAdult = booking.NoOfAdult,
                    NoOfChild = booking.NoOfChild,
                    NoOfRoomRequested = booking.NoOfRoomRequested,
                    GuestMobile = booking.GuestMobile,
                    GuestNameThree = booking.GuestNameThree,
                    GuestNameTwo = booking.GuestNameTwo,
                    GuestNameMain = booking.GuestNameMain,
                    GuestAddress = booking.GuestAddress,
                    BookingToken = !string.IsNullOrEmpty(booking.BookingToken) ? booking.BookingToken : "",
                    Email = booking.Email,
                    DateOfBirth = Convert.ToDateTime(booking.DateOfBirth).ToString("dd-MMM-yyyy"),
                    HasStayedOrNot = booking.HasStayedOrNot,
                    OBOwnerId = booking.OBOwnerId,
                    IsBookingCompleted = booking.IsBookingCompleted,
                    SlotId = booking.SlotId,
                    NoOfDaysCanStay = canStay,
                    NoOfDaySpent = totalSpent,
                    IsRoomBookingTokenExists = isRoomBookingTokenExists,
                    Relation = booking.Relation,
                    RelationEnumList = _enumDataService.GetEnumData(RelationshipEnumGroup),
                    Roomtype = booking.Roomtype,
                    PreferredRoomNo = booking.PreferredRoomNo,
                };
                return View("OwnerRoomBookingEdit", mdl);
            }
        }

        private int GetCanStayDays(ApplicationDbContext _dbContext, OwnerRoomBooking booking, DateTime startingFiscalDate, DateTime endingFiscalDate, ref ResponseModel response)
        {
            //The first sale date of a slot owner is taken to determine when he/she became a slot owner and can stay at hotel
            var transactionHistory = _dbContext.OwnerTransationHistories.FirstOrDefault(o => o.SlotID == booking.SlotId);

            if (transactionHistory == null)
            {
                response.Status = Messages.StatusFail;
                response.Message = Messages.TransationHistoryNotAssigned;
                return 0;
            }

            var ownersSaleDate = _dbContext.OwnerTransationHistories.Where(o => o.SlotID == booking.SlotId).Min(o => o.SaleDate);

            int canStay;
            //determining the number of days an owner can stay at his debut fiscal year.
            var totalDays = 365;
            var ownersDays = 0;

            canStay = _dbContext.ConfAllotments.Where(b => b.SlotId == booking.SlotId).Select(u => u.YearlyAllotment).FirstOrDefault();

            if (ownersSaleDate >= startingFiscalDate && ownersSaleDate <= endingFiscalDate)
            {
                ownersDays = (endingFiscalDate - ownersSaleDate).Days;
                canStay = Convert.ToInt16(Math.Round((ownersDays * 1.0 / totalDays) * 30.0));
            }

            response.Status = Messages.StatusSuccess;

            return canStay;
        }

        private static void GetFiscalDates(out DateTime startingFiscalDate, out DateTime endingFiscalDate)
        {
            var dateToMeasureFiscalYear = DateTime.Now;
            int month = dateToMeasureFiscalYear.Month;
            int year = dateToMeasureFiscalYear.Year;
            startingFiscalDate = month > 6 ? new DateTime(year, 7, 1) : new DateTime(year - 1, 7, 1);
            int month2 = dateToMeasureFiscalYear.Month;
            int year2 = dateToMeasureFiscalYear.Year;
            endingFiscalDate = month2 > 6 ? new DateTime(year2 + 1, 6, 30) : new DateTime(year2, 6, 30);
        }

        private int GetSpentDays(ApplicationDbContext _dbContext, OwnerRoomBooking booking, DateTime startingFiscalDate, DateTime endingFiscalDate)
        {
            int totalSpent;
            //Determining how many days the owner has spent in the hotel
            var ownersNoOfDays = _dbContext.OwnerRoomBookings.Where(r => r.SlotId == booking.SlotId && (r.HasStayedOrNot == "Yes" || r.HasStayedOrNot == "true")).ToList();
            totalSpent = 0;

            //measuring how many days he spent in the hotel actually
            foreach (var days in ownersNoOfDays)
            {
                if (startingFiscalDate <= days.CheckInDate && endingFiscalDate >= days.CheckInDate)
                {
                    var noOfDays1 = Convert.ToDateTime(days.CheckInDate);
                    var noOfDays2 = Convert.ToDateTime(days.Enddate);
                    var noOfDays = (noOfDays2 - noOfDays1).TotalDays;
                    totalSpent += days.NoOfRoomRequested * Convert.ToInt16(noOfDays);
                }
            }

            //punishment for not arriving at hotel after completing booking online
            var ownersNoOfDaysCancelled = _dbContext.OwnerRoomBookings.Where(r => r.HasStayedOrNot == "No" && r.IsBookingCompleted == "Yes" && r.SlotId == booking.SlotId).ToList();
            foreach (var days in ownersNoOfDaysCancelled)
            {
                //total += days.NoOfRoomRequested * Convert.ToInt16(noOfDays);
                if (startingFiscalDate <= days.CheckInDate && endingFiscalDate >= days.CheckInDate)
                {
                    totalSpent += 1;
                }
            }

            return totalSpent;
        }

        [HttpPost]
        public ActionResult OwnerRoomBookingEdit(OwnerRoomBookingView booking)
        {
            if (!Common.IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            _bookingService.BookingUpdate(booking);

            return RedirectToAction("OwnerRoomBookingManagement", "Booking");
        }


        [HttpPost]
        public ActionResult OwnerRoomBookingDelete(string ownerRoomBookingId)
        {
            if (!Common.IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[4] == '0')
                {
                    return View("Error");
                }
                var OwnerRoomBookingInDb = adbc.OwnerRoomBookings.Where(r => r.OwnerRoomBookngId == ownerRoomBookingId).FirstOrDefault();
                OwnerRoomBookingInDb.IsBookingCompleted = "Cancelled";
                OwnerRoomBookingInDb.BookingToken = "";
                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }

                notificationToken++;
                Notification notificationModel = new Notification()
                {
                    isSeen = false,
                    NotificationHeader = "CancelledBooking",
                    //NotificationText = OwnerRoomBookingInDb.OBOwnerId + " has cancelled a room at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                    NotificationText = "Admin has Cancelled " + OwnerRoomBookingInDb.OBOwnerId + "'s Request for Room Booking at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                    NotificationLink = "#",
                    NotificationTime = DateTime.Now,
                    NotificationId = "NOT-" + notificationToken,
                    IdForRecognition = OwnerRoomBookingInDb.OwnerRoomBookngId
                };
                adbc.Notifications.Add(notificationModel);
                adbc.SaveChanges();



                //SMS to Owner
                var OwnerForNotification = adbc.OwnersInfos.FirstOrDefault(o => o.SlotId == OwnerRoomBookingInDb.SlotId);

                var messageOwner = "Admin has Cancelled " + OwnerRoomBookingInDb.SlotId + "'s Request for Room Booking at " + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".";
                var isSMSSentOwner = "Processing";
                if (OwnerForNotification.Mobile != null && OwnerForNotification.Mobile != "")
                {
                    var returnStatus = Common.SMSSending(OwnerForNotification.Mobile, messageOwner);
                    if (returnStatus == true)
                    {
                        isSMSSentOwner = "Delivered";
                    }
                    else
                    {
                        isSMSSentOwner = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                    }
                }
                else
                {
                    isSMSSentOwner = "Not Delivered, Check whether Mobile No for this owner is correct";
                }
                EmailandSMSReport emailsmsreportForSMSOwner = new EmailandSMSReport()
                {
                    IsSMSSent = isSMSSentOwner,
                    Mobile = OwnerForNotification.Mobile,
                    Email = "",
                    SlotId = OwnerForNotification.SlotId,
                    Name = OwnerForNotification.Name,
                    TimeOfDelivery = DateTime.Now,
                    NotificationType = "SMS",
                    Text = messageOwner,
                    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                };
                Common.InsertEmailandSMSReports(emailsmsreportForSMSOwner);

                //Email to Owner
                //var isEmailSentOwner = "Processing";
                //if (OwnerForNotification.Email != null && OwnerForNotification.Email != "")
                //{
                //    Common.EmailSending(OwnerForNotification.Email, "Hotel Reservation", messageOwner);
                //    isEmailSentOwner = "Delivered";
                //}
                //else
                //{
                //    isEmailSentOwner = "Failed";
                //}
                //EmailandSMSReport emailsmsreportForEmailOwner = new EmailandSMSReport()
                //{
                //    IsSMSSent = isEmailSentOwner,
                //    Mobile = "",
                //    Email = OwnerForNotification.Email,
                //    SlotId = OwnerForNotification.SlotId,
                //    Name = OwnerForNotification.Name,
                //    TimeOfDelivery = DateTime.Now,
                //    NotificationType = "Email",
                //    //Text = model.EmailText,
                //    Text = messageOwner,
                //    NotificationSerialNo = "AUTO GENERATED:- BOOKING",
                //};
                //Common.insertInEmailAndSMSTable(emailsmsreportForEmailOwner);
                return RedirectToAction("OwnerRoomBookingManagementHistory");
            }

        }
        
        [HttpGet]
        public ActionResult OwnerRoomBookingCurrentPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "")
                {
                    return View("Error");
                }

                //var BookingObjAllList = db.OwnerRoomBookings.ToList();

                //var BookingObjList = BookingObjAllList.OrderBy(p => p.Startdate).Reverse().ToList();
                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                List<OwnerRoomBooking> OwnerRoomBookingHistory =
                    db.OwnerRoomBookings.Where(r => r.CheckOutDate <= DateTime.Now || r.IsBookingCompleted == "Cancelled" || (r.CheckInDate < DateTime.Now && r.IsBookingCompleted.ToUpper() == "NO")).ToList();
                var ownerRoomBookingListCurrentList = OwnerRoomBookingsAll.Except(OwnerRoomBookingHistory).ToList();
                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingCurrent in ownerRoomBookingListCurrentList)
                {
                    int key = int.Parse(ownerRoomBookingCurrent.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingCurrentDictionary.Add(key, ownerRoomBookingCurrent);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingCurrentDictionary.Reverse())
                {
                    OwnerRoomBookingCurrentDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }



                var BookingObjList = OwnerRoomBookingCurrentDictionaryReverse.Values.ToList();
                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();
                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    var pageNo = (BookingObjList.Count < (index * 10)) ? BookingObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }

                    noOfBooking = ((BookingObjList.Count + 9) / 10);
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }

                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = index
                };
                return View("OwnerRoomBookingManagement", model);
            }
        }
        
        [HttpPost]
        public JsonResult gettingSearchResultForOwnerRoomBooking(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                List<OwnerRoomBooking> OwnerRoomBookingHistory =
                    db.OwnerRoomBookings.Where(r => r.CheckOutDate <= DateTime.Now || r.IsBookingCompleted == "Cancelled" || (r.CheckInDate < DateTime.Now && r.IsBookingCompleted.ToUpper() == "NO")).ToList();
                var ownerRoomBookingListCurrentList = OwnerRoomBookingsAll.Except(OwnerRoomBookingHistory).ToList();
                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingCurrent in ownerRoomBookingListCurrentList)
                {
                    int key = int.Parse(ownerRoomBookingCurrent.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingCurrentDictionary.Add(key, ownerRoomBookingCurrent);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingCurrentDictionary.Reverse())
                {
                    OwnerRoomBookingCurrentDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }
                var BookingObjList = OwnerRoomBookingCurrentDictionaryReverse.Values.ToList();


                var BookingList = BookingObjList;
                List<OwnerRoomBookingViewForSearch> searchedBookings = new List<OwnerRoomBookingViewForSearch>();
                var searchfield1 = searchfield.ToUpper();

                foreach (var booking in BookingList)
                {
                    var SlotId = "";
                    if (booking.SlotId != null)
                    {
                        SlotId = booking.SlotId.ToUpper();
                    }

                    var Email = "";
                    if (booking.Email != null)
                    {
                        Email = booking.Email.ToUpper();
                    }

                    var BookingToken = "";
                    if (booking.BookingToken != null && booking.BookingToken != "")
                    {
                        BookingToken = booking.BookingToken.ToUpper();
                    }
                    else if (booking.BookingToken == "")
                    {
                        BookingToken = ("Not Assigned").ToUpper();
                    }

                    var GuestMobile = "";
                    if (booking.GuestMobile != null)
                    {
                        GuestMobile = booking.GuestMobile.ToUpper();
                    }

                    var GuestNameMain = "";
                    if (booking.GuestNameMain != null)
                    {
                        GuestNameMain = booking.GuestNameMain.ToUpper();
                    }

                    var CheckInDate = "";
                    if (booking.CheckInDate != null)
                    {
                        CheckInDate = booking.CheckInDate.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var CheckOutDate = "";
                    if (booking.CheckOutDate != null)
                    {
                        CheckOutDate = booking.CheckOutDate.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var ApplicationDate = "";

                    if (booking.ApplicationDate != null)
                    {
                        ApplicationDate = booking.ApplicationDate.ToString("dd-MMM-yyyy hh:mm tt").ToUpper();
                    }


                    var IsBookingCompleted = "";
                    if (booking.IsBookingCompleted != null)
                    {
                        var bookingStatus = "";
                        if (booking.IsBookingCompleted == "Yes")
                        {
                            bookingStatus = "Approved";
                        }
                        else if (booking.IsBookingCompleted == "NO")
                        {
                            bookingStatus = "Pending";
                        }
                        else if (booking.IsBookingCompleted == "Cancelled")
                        {
                            bookingStatus = "Cancelled";
                        }

                        IsBookingCompleted = bookingStatus.ToUpper();
                    }

                    var HasStayedOrNot = "";
                    if (booking.HasStayedOrNot != null)
                    {
                        HasStayedOrNot = booking.HasStayedOrNot.ToUpper();
                    }

                    var SlotId1 = SlotId;
                    var GuestNameMain1 = GuestNameMain;
                    var Email1 = Email;
                    var IsBookingCompleted1 = IsBookingCompleted;
                    var CheckOutDate1 = CheckOutDate;
                    //var Startdate1 = Startdate;
                    //var CheckOutDate1 = CheckOutDate;
                    var CheckInDate1 = CheckInDate;
                    var ApplicationDate1 = ApplicationDate;
                    var BookingToken1 = BookingToken;
                    var GuestMobile1 = GuestMobile;
                    var HasStayedOrNot1 = HasStayedOrNot;

                    if (SlotId1.Contains(searchfield1) == true || GuestNameMain1.Contains(searchfield1) == true ||
                        Email1.Contains(searchfield1) == true || IsBookingCompleted1.Contains(searchfield1) == true
                        || BookingToken1.Contains(searchfield1) == true || GuestMobile1.Contains(searchfield1) == true
                        || HasStayedOrNot1.Contains(searchfield1) == true || CheckOutDate1.Contains(searchfield1)
                        || CheckInDate1.Contains(searchfield1) || ApplicationDate1.Contains(searchfield1))

                    {
                        OwnerRoomBookingViewForSearch bookingsearch = new OwnerRoomBookingViewForSearch();
                        bookingsearch.SlotId = booking.SlotId;
                        bookingsearch.Email = booking.Email;
                        bookingsearch.GuestMobile = booking.GuestMobile;
                        bookingsearch.GuestNameMain = booking.GuestNameMain;
                        bookingsearch.BookingToken = booking.BookingToken;
                        bookingsearch.Enddate = booking.Enddate.ToString("dd-MMM-yyyy");
                        bookingsearch.IsBookingCompleted = (booking.IsBookingCompleted == "Yes") ? "Approved" : (booking.IsBookingCompleted == "NO" ? "Pending" : "Cancelled");
                        bookingsearch.HasStayedOrNot = booking.HasStayedOrNot;
                        bookingsearch.Startdate = booking.Startdate.ToString("dd-MMM-yyyy");
                        bookingsearch.NoOfAdult = booking.NoOfAdult;
                        bookingsearch.NoOfChild = booking.NoOfAdult;
                        bookingsearch.NoOfRoomRequested = booking.NoOfRoomRequested;
                        bookingsearch.OwnerRoomBookngId = booking.OwnerRoomBookngId;
                        bookingsearch.ApplicationDate = booking.ApplicationDate.ToString("dd-MMM-yyyy hh:mm tt");
                        searchedBookings.Add(bookingsearch);
                    }
                }

                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId) taskRoles = tasks.Details;
                }
                if (taskRoles == "")
                {
                    return Json("Error");
                }

                return Json(searchedBookings);

            }
        }

        [HttpGet]
        public ActionResult OwnerRoomBookingManagement(BookingParameterVM paramVM)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            TempData["CalendarView"] = "";

            string taskRoles = _commonService.TaskRole(TaskPath.Booking_OwnerRoomBookingManagement);

            if (taskRoles == "" || taskRoles[0] == '0')
            {
                return View("Error");
            }

            var bookings = _bookingService.GetBookings(paramVM);

            int noOfBooking = (bookings.Count() + 9) / 10;

            List<OwnerRoomBooking> selectedBookings = bookings.Skip((paramVM.Index - 1) * 10).Take(10).ToList();

            var model = new OwnerRoomBookingManagementView()
            {
                OwnerRoomBookingListCurrent = selectedBookings,
                TaskRoles = taskRoles,
                NoOfBooking = noOfBooking,
                index = paramVM.Index,
                SearchField = paramVM.Searchfield,
                BookingComplete = paramVM.BookingComplete,
                CheckedIn = paramVM.CheckedIn,
                CheckedOut = paramVM.CheckedOut,
                Type = paramVM.Type,
                TokenAssigned = paramVM.TokenAssigned,
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult TokenForm(string bookingid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");
                }
                var tok = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == bookingid).First();
                tok.PreferredRoomNo = Request.UrlReferrer.ToString();

                return View(tok);
            }
        }

        [HttpPost]
        public ActionResult TokenForm(OwnerRoomBooking model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext _dbContext = new ApplicationDbContext())
            {
                var booking = _dbContext.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == model.OwnerRoomBookngId).FirstOrDefault();

                booking.BookingToken = model.BookingToken;
                booking.IsBookingCompleted = "Yes";

                _dbContext.SaveChanges();

                var owner = _dbContext.OwnersInfos.FirstOrDefault(o => o.SlotId == booking.SlotId);

                string messageBody = BookingService.BookingConfirmationMessage(owner, model.BookingToken);

                _bookingService.SendBookingConfirmationSMS(owner, messageBody);

                return RedirectToAction("OwnerRoomBookingManagement");
            }

        }

        [HttpGet]
        public ActionResult FolioDetail(string OwnerRoomBookngId)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                OwnerRoomBooking booking = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == OwnerRoomBookngId).ToList().FirstOrDefault();
                int daysSpent = (int)((booking.CheckOutDate - booking.CheckInDate).TotalDays);
                string[] ARRs = new string[daysSpent];
                int totalCost = 0;
                for (int i = 0; i < daysSpent; i++)
                {
                    var currentDay = booking.CheckInDate.AddDays(i);
                    var ARR = adbc.ConfOwnerAvgRoomRates.Where(o => o.ApplicableDate == currentDay).ToList().FirstOrDefault();
                    if (ARR != null)
                    {
                        ARRs[i] = Convert.ToString(ARR.AverageRoomRate);
                        totalCost += ARR.AverageRoomRate * booking.NoOfRoomAllotted;
                    }
                    else
                        ARRs[i] = "-----";

                }
                var model = new FolioDetail()
                {
                    Booking = booking,
                    ARR = ARRs,
                    NoOfDaysSpent = daysSpent,
                    TotalCost = totalCost,
                };
                return View(model);
            }

        }
        
        [HttpGet]
        public ActionResult CheckInForm(string bookingid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");
                }
                var checkin = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == bookingid).First();
                ////checkin.PreferredRoomNo = Request.UrlReferrer.ToString();

                return View(checkin);
            }
        }
        
        [HttpPost]
        public ActionResult CheckInForm(OwnerRoomBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerRoomBooking = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == info.OwnerRoomBookngId).FirstOrDefault();

                //If Checkin Date is not equals to System Date then do not allow checkin!
                if (!ownerRoomBooking.CheckInDate.Date.Equals(_configurationService.GetSystemDate().Date))
                {
                    TempData["Message"] = "Check In Date is not matched with System Date!";

                    //If request come from CalendarView rediret to CalendarView
                    if (TempData["CalendarView"] as string == "CalendarView")
                    {
                        return RedirectToAction("CalendarView", "OwnersInfo");
                    }
                    else
                    {
                        return RedirectToAction("OwnerRoomBookingManagement");
                    }
                }

                ownerRoomBooking.HasStayedOrNot = info.HasStayedOrNot;
                ownerRoomBooking.FolioNo = info.FolioNo;
                ownerRoomBooking.PreferredRoomNo = info.PreferredRoomNo;
                adbc.SaveChanges();

                //If request come from CalendarView rediret to CalendarView
                if (TempData["CalendarView"] as string == "CalendarView")
                {
                    return RedirectToAction("CalendarView", "OwnersInfo");
                }
                else
                {
                    return RedirectToAction("OwnerRoomBookingManagement");
                }
            }

        }
        
        [HttpGet]
        public ActionResult CheckOutForm(string bookingid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole("Booking\\OwnerRoomBookingManagement");

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var checkout = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == bookingid).First();

                return View(checkout);
            }
        }

        [HttpPost]
        public ActionResult CheckOutForm(OwnerRoomBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext _dbContext = new ApplicationDbContext())
            {
                var ownerRoomBooking = _dbContext.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == info.OwnerRoomBookngId).FirstOrDefault();

                //If Checkout Date is not equals to System Date then do not allow checkout!
                if (!ownerRoomBooking.CheckOutDate.Date.Equals(_configurationService.GetSystemDate().Date))
                {
                    TempData["Message"] = Messages.CheckOutDateNotMatchedWithSystemDate;
                    return RedirectToAction(nameof(OwnerRoomBookingManagement));
                }

                var invoice = _dbContext.Invoices.Where(inv => inv.BookingId == info.OwnerRoomBookngId).FirstOrDefault();

                if (invoice == null)
                {
                    TempData["Message"] = Messages.PleaseGenerateInvoiceFirst;
                    return RedirectToAction(nameof(OwnerRoomBookingManagement));
                }

                ownerRoomBooking.HasCheckedOutOrNot = info.HasCheckedOutOrNot;
                ownerRoomBooking.CheckOutDate = info.CheckOutDate;
                ownerRoomBooking.TotalExpense = info.TotalExpense;
                ownerRoomBooking.ExtraCharges = info.ExtraCharges;
                _dbContext.SaveChanges();
                return RedirectToAction(nameof(OwnerRoomBookingManagement));
            }
        }

        public ActionResult GenerateInvoice(string bookingId)
        {
            var response = _invoiceService.GenerateInvoice(bookingId);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InvoiceReport(string bookingId)
        {
            Stream fileStream = _invoiceService.GetInvoiceReport(bookingId);

            return File(fileStream, "application/pdf");
        }



        [HttpGet]
        public ActionResult OwnerRequestedTodayRoomBookingManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");
                }

                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                List<OwnerRoomBooking> ownerRoomBookingList = OwnerRoomBookingsAll;
                ownerRoomBookingList = ownerRoomBookingList.Where(o => o.ApplicationDate == DateTime.Today).ToList();


                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingCurrent in ownerRoomBookingList)
                {
                    int key = int.Parse(ownerRoomBookingCurrent.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingCurrentDictionary.Add(key, ownerRoomBookingCurrent);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingCurrentDictionary.Reverse())
                {
                    OwnerRoomBookingCurrentDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }

                //---------------------------------- real search from here

                var BookingObjList = OwnerRoomBookingCurrentDictionaryReverse.Values.ToList();

                List<OwnerRoomBooking> selectedBookingsSearched = new List<OwnerRoomBooking>();
                searchfield = searchfield.ToUpper();
                if (!String.IsNullOrEmpty(searchfield))
                    selectedBookingsSearched = BookingObjList.Where(o => o.SlotId.ToUpper().Contains(searchfield) || o.GuestNameMain.ToUpper().Contains(searchfield) || o.BookingToken.ToUpper().Contains(searchfield)).ToList();
                else
                    selectedBookingsSearched = BookingObjList;
                //---------------------------------- pagination from here
                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();
                int noOfBooking;
                if (selectedBookingsSearched.Count >= 10)
                {
                    var pageNo = (selectedBookingsSearched.Count < (index * 10)) ? selectedBookingsSearched.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedBookings.Add(selectedBookingsSearched.ElementAt(i));
                    }
                    noOfBooking = (selectedBookingsSearched.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = selectedBookingsSearched;
                    noOfBooking = 1;
                }
                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = index,
                    SearchField = searchfield,

                };
                return View(model);
            }
        }
        
        [HttpGet]
        public ActionResult OwnerInHouseManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");
                }

                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                List<OwnerRoomBooking> ownerRoomBookingList = OwnerRoomBookingsAll;
                ownerRoomBookingList = ownerRoomBookingList.Where(o => o.CheckInDate <= DateTime.Today && o.CheckOutDate > DateTime.Today && o.HasStayedOrNot == "Yes").ToList();


                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingCurrent in ownerRoomBookingList)
                {
                    int key = int.Parse(ownerRoomBookingCurrent.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingCurrentDictionary.Add(key, ownerRoomBookingCurrent);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingCurrentDictionary.Reverse())
                {
                    OwnerRoomBookingCurrentDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }

                //---------------------------------- real search from here

                var BookingObjList = OwnerRoomBookingCurrentDictionaryReverse.Values.ToList();

                List<OwnerRoomBooking> selectedBookingsSearched = new List<OwnerRoomBooking>();
                searchfield = searchfield.ToUpper();
                if (!String.IsNullOrEmpty(searchfield))
                    selectedBookingsSearched = BookingObjList.Where(o => o.SlotId.ToUpper().Contains(searchfield) || o.GuestNameMain.ToUpper().Contains(searchfield) || o.BookingToken.ToUpper().Contains(searchfield)).ToList();
                else
                    selectedBookingsSearched = BookingObjList;
                //---------------------------------- pagination from here
                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();
                int noOfBooking;
                if (selectedBookingsSearched.Count >= 10)
                {
                    var pageNo = (selectedBookingsSearched.Count < (index * 10)) ? selectedBookingsSearched.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedBookings.Add(selectedBookingsSearched.ElementAt(i));
                    }
                    noOfBooking = (selectedBookingsSearched.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = selectedBookingsSearched;
                    noOfBooking = 1;
                }
                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = index,
                    SearchField = searchfield,

                };
                return View(model);
            }
        }
        
        [HttpGet]
        public ActionResult OwnerCheckOutManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("Error");
                }

                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                List<OwnerRoomBooking> ownerRoomBookingList = OwnerRoomBookingsAll;



                ownerRoomBookingList = ownerRoomBookingList.Where(o => o.CheckOutDate == DateTime.Today && o.HasStayedOrNot == "Yes").ToList();


                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingCurrent in ownerRoomBookingList)
                {
                    int key = int.Parse(ownerRoomBookingCurrent.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingCurrentDictionary.Add(key, ownerRoomBookingCurrent);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingCurrentDictionary.Reverse())
                {
                    OwnerRoomBookingCurrentDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }

                //---------------------------------- real search from here

                var BookingObjList = OwnerRoomBookingCurrentDictionaryReverse.Values.ToList();

                List<OwnerRoomBooking> selectedBookingsSearched = new List<OwnerRoomBooking>();
                searchfield = searchfield.ToUpper();
                if (!String.IsNullOrEmpty(searchfield))
                    selectedBookingsSearched = BookingObjList.Where(o => o.SlotId.ToUpper().Contains(searchfield) || o.GuestNameMain.ToUpper().Contains(searchfield) || o.BookingToken.ToUpper().Contains(searchfield)).ToList();
                else
                    selectedBookingsSearched = BookingObjList;
                //---------------------------------- pagination from here
                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();
                int noOfBooking;
                if (selectedBookingsSearched.Count >= 10)
                {
                    var pageNo = (selectedBookingsSearched.Count < (index * 10)) ? selectedBookingsSearched.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedBookings.Add(selectedBookingsSearched.ElementAt(i));
                    }
                    noOfBooking = (selectedBookingsSearched.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = selectedBookingsSearched;
                    noOfBooking = 1;
                }
                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListCurrent = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = index,
                    SearchField = searchfield,

                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult OwnerRoomBookingManagementCancelled(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (StarterProjectV2.ApplicationDbContext db = new StarterProjectV2.ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }


                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                var ownerRoomBookingListCancelled = OwnerRoomBookingsAll.Where(o => o.IsBookingCompleted == "Cancelled" && o.CancelApproved == false);
                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingCurrent in ownerRoomBookingListCancelled)
                {
                    int key = int.Parse(ownerRoomBookingCurrent.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingCurrentDictionary.Add(key, ownerRoomBookingCurrent);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingCurrentDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingCurrentDictionary.Reverse())
                {
                    OwnerRoomBookingCurrentDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }



                var BookingObjList = OwnerRoomBookingCurrentDictionaryReverse.Values.ToList();
                List<OwnerRoomBooking> selectedSearched = new List<OwnerRoomBooking>();
                searchfield = searchfield.ToUpper();
                if (!String.IsNullOrEmpty(searchfield))
                    selectedSearched = BookingObjList.Where(o => o.SlotId.ToUpper().Contains(searchfield) || o.GuestNameMain.ToUpper().Contains(searchfield) || o.BookingToken.ToUpper().Contains(searchfield)).ToList();
                else
                    selectedSearched = BookingObjList;

                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();
                int noOfBooking;
                if (selectedSearched.Count >= 10)
                {
                    var pageNo = (selectedSearched.Count < (index * 10)) ? selectedSearched.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedBookings.Add(selectedSearched.ElementAt(i));
                    }
                    noOfBooking = ((selectedSearched.Count + 9) / 10);
                }
                else
                {
                    selectedBookings = selectedSearched;
                    noOfBooking = 1;
                }
                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListCancelled = selectedBookings,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }
        }
        
        [HttpGet]
        public ActionResult OwnerRoomBookingManagementHistory(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement")
                    .Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "")
                {
                    return View("Error");
                }

                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                List<OwnerRoomBooking> OwnerRoomBookingHistory =
                    db.OwnerRoomBookings.Where(r => r.CheckOutDate <= DateTime.Now || r.IsBookingCompleted == "Cancelled" || (r.Startdate < DateTime.Now && r.IsBookingCompleted.ToUpper() == "NO")).ToList();
                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingHistory in OwnerRoomBookingHistory)
                {
                    int key = int.Parse(ownerRoomBookingHistory.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingHistoryDictionary.Add(key, ownerRoomBookingHistory);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingHistoryDictionary.Reverse())
                {
                    OwnerRoomBookingHistoryDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }


                var BookingObjList = OwnerRoomBookingHistoryDictionaryReverse.Values.ToList();
                List<OwnerRoomBooking> searchedBookings = new List<OwnerRoomBooking>();
                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var booking in BookingObjList)
                    {
                        var UserId = "";
                        if (booking.SlotId != null)
                        {
                            UserId = booking.SlotId.ToUpper();
                        }
                        var Name = "";
                        if (booking.GuestNameMain != null)
                        {
                            Name = booking.GuestNameMain.ToUpper();
                        }

                        if (UserId.Contains(searchfield1) == true || Name.Contains(searchfield1) == true)
                        {
                            searchedBookings.Add(booking);
                        }
                    }
                }
                else
                {
                    searchedBookings = BookingObjList;
                }
                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();
                int noOfBooking;
                if (searchedBookings.Count >= 10)
                {
                    var pageNo = (searchedBookings.Count < (index * 10)) ? searchedBookings.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedBookings.Add(searchedBookings.ElementAt(i));
                    }
                    noOfBooking = (searchedBookings.Count + 9) / 10;
                }
                else
                {
                    selectedBookings = searchedBookings;
                    noOfBooking = 1;
                }

                List<OwnerRoomBooking> resultingBookingList = new List<OwnerRoomBooking>();
                var BookingList1 = selectedBookings;
                foreach (var booking in BookingList1)
                {
                    resultingBookingList.Add(booking);
                }
                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListHistory = resultingBookingList,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }
        }
        
        [HttpGet]
        public ActionResult OwnerRoomBookingHistoryPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "")
                {
                    return View("Error");
                }

                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                List<OwnerRoomBooking> OwnerRoomBookingHistory =
                    db.OwnerRoomBookings.Where(r => r.Enddate <= DateTime.Now || r.IsBookingCompleted == "Cancelled" || (r.Startdate < DateTime.Now && r.IsBookingCompleted.ToUpper() == "NO")).ToList();
                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingHistory in OwnerRoomBookingHistory)
                {
                    int key = int.Parse(ownerRoomBookingHistory.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingHistoryDictionary.Add(key, ownerRoomBookingHistory);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingHistoryDictionary.Reverse())
                {
                    OwnerRoomBookingHistoryDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }


                var BookingObjList = OwnerRoomBookingHistoryDictionaryReverse.Values.ToList();
                List<OwnerRoomBooking> selectedBookings = new List<OwnerRoomBooking>();
                int noOfBooking;
                if (BookingObjList.Count >= 10)
                {
                    var pageNo = (BookingObjList.Count < (index * 10)) ? BookingObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedBookings.Add(BookingObjList.ElementAt(i));
                    }

                    noOfBooking = ((BookingObjList.Count + 9) / 10);
                }
                else
                {
                    selectedBookings = BookingObjList;
                    noOfBooking = 1;
                }

                List<OwnerRoomBooking> resultingBookingList = new List<OwnerRoomBooking>();
                var BookingList1 = selectedBookings;
                foreach (var bookingReport in BookingList1)
                {
                    resultingBookingList.Add(bookingReport);
                }

                var model = new OwnerRoomBookingManagementView()
                {
                    OwnerRoomBookingListHistory = resultingBookingList,
                    TaskRoles = taskRoles,
                    NoOfBooking = noOfBooking,
                    index = index
                };
                return View("OwnerRoomBookingManagementHistory", model);
            }
        }

        [HttpPost]
        public JsonResult gettingSearchResultForOwnerRoomBookingHistory(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<OwnerRoomBooking> OwnerRoomBookingsAll = db.OwnerRoomBookings.ToList();
                List<OwnerRoomBooking> OwnerRoomBookingHistory =
                    db.OwnerRoomBookings.Where(r => r.Enddate <= DateTime.Now || r.IsBookingCompleted == "Cancelled" || (r.Startdate < DateTime.Now && r.IsBookingCompleted.ToUpper() == "NO")).ToList();
                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                foreach (var ownerRoomBookingHistory in OwnerRoomBookingHistory)
                {
                    int key = int.Parse(ownerRoomBookingHistory.OwnerRoomBookngId.Split('-')[1]);
                    OwnerRoomBookingHistoryDictionary.Add(key, ownerRoomBookingHistory);
                }

                Dictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionaryReverse = new Dictionary<int, OwnerRoomBooking>();

                foreach (var ownerKeyValuePair in OwnerRoomBookingHistoryDictionary.Reverse())
                {
                    OwnerRoomBookingHistoryDictionaryReverse.Add(ownerKeyValuePair.Key, ownerKeyValuePair.Value);
                }


                var BookingObjList = OwnerRoomBookingHistoryDictionaryReverse.Values.ToList();


                var BookingList = BookingObjList;
                List<OwnerRoomBookingViewForSearch> searchedBookings = new List<OwnerRoomBookingViewForSearch>();
                var searchfield1 = searchfield.ToUpper();

                foreach (var booking in BookingList)
                {
                    var SlotId = "";
                    if (booking.SlotId != null)
                    {
                        SlotId = booking.SlotId.ToUpper();
                    }
                    var Email = "";
                    if (booking.Email != null)
                    {
                        Email = booking.Email.ToUpper();
                    }
                    var BookingToken = "";
                    if (booking.BookingToken != null && booking.BookingToken != "")
                    {
                        BookingToken = booking.BookingToken.ToUpper();
                    }
                    else if (booking.BookingToken == "")
                    {
                        BookingToken = ("Not Assigned").ToUpper();
                    }
                    var GuestMobile = "";
                    if (booking.GuestMobile != null)
                    {
                        GuestMobile = booking.GuestMobile.ToUpper();
                    }
                    var GuestNameMain = "";
                    if (booking.GuestNameMain != null)
                    {
                        GuestNameMain = booking.GuestNameMain.ToUpper();
                    }

                    var Startdate = "";
                    if (booking.Startdate != null)
                    {
                        Startdate = booking.Startdate.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var Enddate = "";
                    if (booking.Enddate != null)
                    {
                        Enddate = booking.Enddate.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var ApplicationDate = "";

                    if (booking.ApplicationDate != null)
                    {
                        ApplicationDate = booking.ApplicationDate.ToString("dd-MMM-yyyy hh:mm tt").ToUpper();
                    }

                    var IsBookingCompleted = "";
                    if (booking.IsBookingCompleted != null)
                    {
                        var bookingStatus = "";
                        if (booking.IsBookingCompleted == "Yes")
                        {
                            bookingStatus = "Approved";
                        }
                        else if (booking.IsBookingCompleted == "NO")
                        {
                            bookingStatus = "Pending";
                        }
                        else if (booking.IsBookingCompleted == "Cancelled")
                        {
                            bookingStatus = "Cancelled";
                        }

                        IsBookingCompleted = bookingStatus.ToUpper();
                    }
                    var HasStayedOrNot = "";
                    if (booking.HasStayedOrNot != null)
                    {
                        HasStayedOrNot = booking.HasStayedOrNot.ToUpper();
                    }

                    var SlotId1 = SlotId;
                    var GuestNameMain1 = GuestNameMain;
                    var Email1 = Email;
                    var IsBookingCompleted1 = IsBookingCompleted;
                    var Enddate1 = Enddate;
                    var Startdate1 = Startdate;
                    var ApplicationDate1 = ApplicationDate;
                    var BookingToken1 = BookingToken;
                    var GuestMobile1 = GuestMobile;
                    var HasStayedOrNot1 = HasStayedOrNot;

                    if (SlotId1.Contains(searchfield1) == true || GuestNameMain1.Contains(searchfield1) == true ||
                        Email1.Contains(searchfield1) == true || IsBookingCompleted1.Contains(searchfield1) == true
                        || BookingToken1.Contains(searchfield1) == true || GuestMobile1.Contains(searchfield1) == true || HasStayedOrNot1.Contains(searchfield1) == true || Enddate1.Contains(searchfield1)
                        || Startdate1.Contains(searchfield1) || ApplicationDate1.Contains(searchfield1))
                    {
                        OwnerRoomBookingViewForSearch bookingsearch = new OwnerRoomBookingViewForSearch();
                        bookingsearch.SlotId = booking.SlotId;
                        bookingsearch.Email = booking.Email;
                        bookingsearch.GuestMobile = booking.GuestMobile;
                        bookingsearch.GuestNameMain = booking.GuestNameMain;
                        bookingsearch.BookingToken = booking.BookingToken;
                        bookingsearch.Enddate = booking.Enddate.ToString("dd-MMM-yyyy");
                        bookingsearch.IsBookingCompleted = (booking.IsBookingCompleted == "Yes") ? "Approved" : (booking.IsBookingCompleted == "NO" ? "Pending" : "Cancelled");
                        bookingsearch.HasStayedOrNot = booking.HasStayedOrNot;
                        bookingsearch.Startdate = booking.Startdate.ToString("dd-MMM-yyyy");
                        bookingsearch.NoOfAdult = booking.NoOfAdult;
                        bookingsearch.NoOfChild = booking.NoOfAdult;
                        bookingsearch.NoOfRoomRequested = booking.NoOfRoomRequested;
                        bookingsearch.OwnerRoomBookngId = booking.OwnerRoomBookngId;
                        bookingsearch.ApplicationDate = booking.ApplicationDate.ToString("dd-MMM-yyyy hh:mm tt");
                        searchedBookings.Add(bookingsearch);
                    }
                }

                string taskId = db.Tasks.Where(u => u.TaskPath == "Booking\\OwnerRoomBookingManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId) taskRoles = tasks.Details;
                }
                if (taskRoles == "")
                {
                    return Json("Error");
                }

                return Json(searchedBookings);

            }
        }

        [HttpPost]
        public ActionResult CancelBooking(OwnerRoomBooking info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Index", "Home");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerRoomBooking = adbc.OwnerRoomBookings.Where(o => o.OwnerRoomBookngId == info.OwnerRoomBookngId).FirstOrDefault();

                ownerRoomBooking.IsBookingCompleted = "Cancelled";
                ownerRoomBooking.CancelApproved = true;

                adbc.SaveChanges();
                return RedirectToAction("OwnerRoomBookingManagementCancelled");
            }

        }

        [HttpGet]
        public ActionResult BookingSearchBySlotId()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<string> slotIdList = db.OwnersInfos.Select(o => o.SlotId).ToList();
                var model = new BookingSearchOptionView()
                {
                    SlotIdList = slotIdList,
                };
                return View(model);
            }
        }

        //It is gettingEmailSMSReportSearchResult function actually
        [HttpPost]
        public JsonResult gettingBookingSearchBySlotId(BookingSearchOptionView info)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<OwnerRoomBookingView> OwnerRoomBookingSearchList = new List<OwnerRoomBookingView>();
                var BookingSearchedResult = db.OwnerRoomBookings.Where(r => r.SlotId == info.SlotId).ToList();
                BookingSearchedResult = BookingSearchedResult.OrderBy(b => b.ApplicationDate).ToList();
                BookingSearchedResult.Reverse();
                foreach (var bookingSearchedResult in BookingSearchedResult)
                {
                    OwnerRoomBookingView model = new OwnerRoomBookingView();
                    model.SlotId = bookingSearchedResult.SlotId;
                    model.OwnerRoomBookingId = bookingSearchedResult.OwnerRoomBookngId;
                    model.OBOwnerId = bookingSearchedResult.OBOwnerId;
                    model.Startdate = bookingSearchedResult.Startdate;
                    model.Enddate = bookingSearchedResult.Enddate;
                    model.GuestMobile = bookingSearchedResult.GuestMobile;
                    model.Email = bookingSearchedResult.Email;
                    model.GuestAddress = bookingSearchedResult.GuestAddress;
                    model.GuestNameMain = bookingSearchedResult.GuestNameMain;
                    model.GuestNameThree = bookingSearchedResult.GuestNameThree;
                    model.GuestNameTwo = bookingSearchedResult.GuestNameTwo;
                    model.ApplicationDate = bookingSearchedResult.ApplicationDate;
                    model.NoOfDaySpent = bookingSearchedResult.NoOfDaySpent;
                    model.HasStayedOrNot = bookingSearchedResult.HasStayedOrNot;
                    model.NoOfRoomRequested = bookingSearchedResult.NoOfRoomRequested;
                    model.DateOfBirth = bookingSearchedResult.DateOfBirth;
                    model.NoOfChild = bookingSearchedResult.NoOfChild;
                    model.NoOfAdult = bookingSearchedResult.NoOfAdult;
                    model.BookingToken = bookingSearchedResult.BookingToken;
                    model.IsBookingCompleted = bookingSearchedResult.IsBookingCompleted;
                    model.NoOfDaysCanStay = bookingSearchedResult.NoOfDaysCanStay;
                    model.ApplicationDateString = bookingSearchedResult.ApplicationDate.ToString("dd-MMM-yyyy");
                    model.StartdateString = bookingSearchedResult.Startdate.ToString("dd-MMM-yyyy");
                    model.EnddateString = bookingSearchedResult.Enddate.ToString("dd-MMM-yyyy");
                    OwnerRoomBookingSearchList.Add(model);
                }
                return Json(OwnerRoomBookingSearchList);
            }
        }

        [HttpGet]
        public ActionResult BookingSearchBySmartId()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            OwnerSearchByIdView mdl = new OwnerSearchByIdView()
            {
                IsDataPresent = false,
                IsSearchResultFound = false,
            };
            return View(mdl);
        }
        
        //It is gettingEmailSMSReportSearchResult function actually
        [HttpPost]
        public ActionResult gettingBookingSearchById(OwnerSearchByIdView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var Id = model.Id;
                var ownersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.SlotId == Id || o.OwnerSmartId == Id);
                if (ownersInfo != null && !(String.IsNullOrEmpty(model.Id)))
                {
                    var slotId = ownersInfo.SlotId;
                    var bookingList = adbc.OwnerRoomBookings.Where(b => b.SlotId == slotId).ToList();
                    var ownersSaleDate = adbc.OwnerTransationHistories.Where(o => o.SlotID == slotId).Min(o => o.SaleDate);//I have taken the first sale date of a slot owner to determine when he/she became a slot owner and can stay at hotel


                    var FinancialYears = adbc.FinancialYears.ToList().OrderBy(f => f.FromMonth).Reverse();
                    List<int> CanStayList = new List<int>();
                    List<int> HasSpentList = new List<int>();
                    List<string> FinancialYearNameList = new List<string>();
                    List<string> FinancialYearIDList = new List<string>();
                    foreach (FinancialYear financialYear in FinancialYears)
                    {
                        int canStay = 30; //initially 30 for every fiscal year
                        DateTime startingFiscalDate = financialYear.FromMonth;
                        DateTime endingFiscalDate = financialYear.ToMonth;
                        string financialYearName = financialYear.Year;
                        FinancialYearNameList.Add(financialYearName);
                        FinancialYearIDList.Add(financialYear.FinId);
                        var totalDays = 365;
                        var ownersDays = 0;
                        if (ownersSaleDate >= startingFiscalDate && ownersSaleDate <= endingFiscalDate)
                        {
                            ownersDays = (endingFiscalDate - ownersSaleDate).Days;
                            canStay = Convert.ToInt16(Math.Round((ownersDays * 1.0 / totalDays) * 30.0));
                        }
                        //Determining how many days the owner has spent in the hotel
                        var ownersNoOfDays = adbc.OwnerRoomBookings.Where(r => r.HasStayedOrNot == "Yes" && r.SlotId == slotId).ToList();
                        var totalSpent = 0;
                        //measuring how many days he spent in the hotel actually
                        foreach (var days in ownersNoOfDays)
                        {
                            if (startingFiscalDate <= days.Startdate && endingFiscalDate >= days.Startdate)
                            {
                                var noOfDays1 = Convert.ToDateTime(days.Startdate);
                                var noOfDays2 = Convert.ToDateTime(days.Enddate);
                                var noOfDays = (noOfDays2 - noOfDays1).TotalDays;
                                totalSpent += days.NoOfRoomRequested * Convert.ToInt16(noOfDays);
                            }
                        }

                        //punishment for not arriving at hotel after completing booking online
                        var ownersNoOfDaysCancelled = adbc.OwnerRoomBookings.Where(r => r.HasStayedOrNot == "No" && r.IsBookingCompleted == "Yes" && r.SlotId == slotId).ToList();
                        foreach (var days in ownersNoOfDaysCancelled)
                        {
                            //total += days.NoOfRoomRequested * Convert.ToInt16(noOfDays);
                            if (startingFiscalDate <= days.Startdate && endingFiscalDate >= days.Startdate)
                            {
                                totalSpent += 1;
                            }
                        }
                        HasSpentList.Add(totalSpent);
                        CanStayList.Add(canStay);
                    }

                    OwnerSearchByIdView mdl = new OwnerSearchByIdView()
                    {
                        //NoOfDaysCanStay = canStay,
                        //NoOfDaysHasStayed = totalSpent,
                        FinancialYearNameList = FinancialYearNameList,
                        NoOfDaysCanStayList = CanStayList,
                        FinancialYearIDList = FinancialYearIDList,
                        NoOfDaysHasStayedList = HasSpentList,
                        DateOfBirth = ownersInfo.DateOfBirth,
                        FathersName = ownersInfo.FatherName,
                        Name = ownersInfo.Name,
                        NID = ownersInfo.NID,
                        TIN = ownersInfo.TIN,
                        PicturePath = ownersInfo.PicturePath,
                        SalesDate = ownersSaleDate.ToString("dd-MMM-yyyy"),
                        SlotId = ownersInfo.SlotId,
                        IsDataPresent = true,
                        IsSearchResultFound = true
                    };
                    return View("BookingSearchBySmartId", mdl);
                }
                else
                {
                    OwnerSearchByIdView mdl = new OwnerSearchByIdView()
                    {
                        IsDataPresent = true,
                        IsSearchResultFound = false
                    };
                    return View("BookingSearchBySmartId", mdl);
                }
            }
        }

        [HttpGet]
        public ActionResult DetailsBookingInformation(string slotId, string finId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var OwneraRoomBookingListForThisOwner = adbc.OwnerRoomBookings.Where(r => r.SlotId == slotId).ToList();
                var financialYearObj = adbc.FinancialYears.Where(f => f.FinId == finId).FirstOrDefault();
                DateTime startFinancialDate = financialYearObj.FromMonth;
                DateTime endFinancialDate = financialYearObj.ToMonth;
                List<OwnerRoomBooking> selectedOwnerRoomBookings = new List<OwnerRoomBooking>();
                foreach (var ownerRoomBooking in OwneraRoomBookingListForThisOwner)
                {
                    if (startFinancialDate <= ownerRoomBooking.Startdate &&
                        endFinancialDate >= ownerRoomBooking.Startdate)
                    {
                        selectedOwnerRoomBookings.Add(ownerRoomBooking);
                    }
                }

                selectedOwnerRoomBookings = selectedOwnerRoomBookings.OrderBy(b => b.ApplicationDate).Reverse().ToList();
                DetailsBookingInformationView model = new DetailsBookingInformationView()
                {
                    SelectedRoomBookingList = selectedOwnerRoomBookings,
                    FinancialYear = financialYearObj.Year,
                };

                return View(model);
            }
        }
        
        public ActionResult Index()
        {
            return View();
        }
    }
}