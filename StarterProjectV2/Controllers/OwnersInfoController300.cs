﻿using CrystalDecisions.CrystalReports.Engine;
using OfficeOpenXml;
using StarterProjectV2.Models;
using StarterProjectV2.Services;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace StarterProjectV2.Controllers
{
    public partial class OwnersInfoController : Controller
    {
        #region Post Management

        [HttpGet]
        public ActionResult OwnerPostManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "OwnersInfo\\OwnerPostManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }


                List<Post> PostObjList = new List<Post>();

                var UnApprovedPostObjList = adbc.Posts.Where(p => p.Approval == false).ToList();

                SortedDictionary<int, Post> UnapprovedPostsList = new SortedDictionary<int, Post>();

                foreach (var post in UnApprovedPostObjList)
                {
                    int key = int.Parse(post.PostId.Split('-')[1]);
                    UnapprovedPostsList.Add(key, post);
                }

                Dictionary<int, Post> UnapprovedPostsListReverse = new Dictionary<int, Post>();

                foreach (var postKeyValuePair in UnapprovedPostsList.Reverse())
                {
                    PostObjList.Add(postKeyValuePair.Value);
                }

                var ApprovedPostObjList = adbc.Posts.Where(p => p.Approval == true).ToList();

                //                SortedDictionary<int, Post> ApprovedPostsList = new SortedDictionary<int, Post>();
                //
                //                foreach (var post in ApprovedPostObjList)
                //                {
                //                    int key = int.Parse(post.PostId.Split('-')[1]);
                //                    ApprovedPostsList.Add(key, post);
                //                }
                //
                //                Dictionary<int, Post> ApprovedPostsListReverse = new Dictionary<int, Post>();
                //
                //                foreach (var postKeyValuePair in ApprovedPostsList.Reverse())
                //                {
                //                    PostObjList.Add(postKeyValuePair.Value);
                //                }
                var SortedApprovedList = ApprovedPostObjList.OrderBy(p => p.ApprovalTime).ToList();

                for (var i = SortedApprovedList.Count - 1; i >= 0; i--)
                {
                    PostObjList.Add(SortedApprovedList.ElementAt(i));
                }

                List<Post> PostObjListSearched = new List<Post>();

                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var post in PostObjList)
                    {
                        var User = "";
                        if (post.PostedBy != null)
                        {
                            User = post.PostedBy.ToUpper();
                        }

                        //var AuditDTTM = "";
                        //if (audit.AuditDTTM != null)
                        //{
                        //    AuditDTTM = audit.AuditDTTM.ToUpper();
                        //}

                        //var AuditDTTM1 = AuditDTTM;

                        if (User.Contains(searchfield1) == true)
                        {
                            PostObjListSearched.Add(post);
                        }
                    }
                }
                else
                {
                    PostObjListSearched = PostObjList;
                }

                List<Post> selectedPostInfos = new List<Post>();
                List<string> selectedPosterNames = new List<string>();
                int noOfPosts;
                if (PostObjListSearched.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedPostInfos.Add(PostObjListSearched.ElementAt(i));
                    }

                    noOfPosts = (PostObjListSearched.Count + 9) / 10;
                }
                else
                {
                    selectedPostInfos = PostObjListSearched;
                    noOfPosts = 1;
                }


                var model = new PostList
                {
                    Posts = selectedPostInfos,
                    TaskRoles = taskRoles,
                    NoOfPosts = noOfPosts,
                    OwnersName = selectedPosterNames,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult PostsPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");



            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\OwnerPostManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                List<Post> PostObjList = new List<Post>();

                var UnApprovedPostObjList = db.Posts.Where(p => p.Approval == false).ToList();

                SortedDictionary<int, Post> UnapprovedPostsList = new SortedDictionary<int, Post>();

                foreach (var post in UnApprovedPostObjList)
                {
                    int key = int.Parse(post.PostId.Split('-')[1]);
                    UnapprovedPostsList.Add(key, post);
                }

                Dictionary<int, Post> UnapprovedPostsListReverse = new Dictionary<int, Post>();

                foreach (var postKeyValuePair in UnapprovedPostsList.Reverse())
                {
                    PostObjList.Add(postKeyValuePair.Value);
                }

                var ApprovedPostObjList = db.Posts.Where(p => p.Approval == true).ToList();

                //                SortedDictionary<int, Post> ApprovedPostsList = new SortedDictionary<int, Post>();
                //
                //                foreach (var post in ApprovedPostObjList)
                //                {
                //                    int key = int.Parse(post.PostId.Split('-')[1]);
                //                    ApprovedPostsList.Add(key, post);
                //                }
                //
                //                Dictionary<int, Post> ApprovedPostsListReverse = new Dictionary<int, Post>();
                //
                //                foreach (var postKeyValuePair in ApprovedPostsList.Reverse())
                //                {
                //                    PostObjList.Add(postKeyValuePair.Value);
                //                }
                var SortedApprovedList = ApprovedPostObjList.OrderBy(p => p.ApprovalTime).ToList();

                for (var i = SortedApprovedList.Count - 1; i >= 0; i--)
                {
                    PostObjList.Add(SortedApprovedList.ElementAt(i));
                }

                List<Post> selectedPostInfos = new List<Post>();
                List<string> selectedPosterNames = new List<string>();
                int noOfPosts;
                if (PostObjList.Count >= 10)
                {
                    var pageNo = (PostObjList.Count < (index * 10)) ? PostObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedPostInfos.Add(PostObjList.ElementAt(i));
                        var OwnerId = PostObjList.ElementAt(i).PostedBy;
                        selectedPosterNames.Add(db.OwnersInfos.Where(o => o.OwenerId == OwnerId).Select(o => o.Name).FirstOrDefault());
                    }
                    int x = (PostObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPosts = (PostObjList.Count / 10) + x;
                }
                else
                {
                    selectedPostInfos = PostObjList;
                    foreach (var Post in PostObjList)
                    {
                        selectedPosterNames.Add(db.OwnersInfos.Where(o => o.OwenerId == Post.PostedBy).Select(o => o.Name)
                            .FirstOrDefault());
                    }
                    noOfPosts = 1;
                }

                var model = new PostList
                {
                    Posts = selectedPostInfos,
                    TaskRoles = taskRoles,
                    NoOfPosts = noOfPosts,
                    OwnersName = selectedPosterNames,
                    index = index
                };
                return View("OwnerPostManagement", model);
            }
        }

        [HttpPost]
        public JsonResult gettingSearchResultForPost(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<Post> PostList = new List<Post>();

                var UnApprovedPostObjList = db.Posts.Where(p => p.Approval == false).ToList();

                SortedDictionary<int, Post> UnapprovedPostsList = new SortedDictionary<int, Post>();

                foreach (var post in UnApprovedPostObjList)
                {
                    int key = int.Parse(post.PostId.Split('-')[1]);
                    UnapprovedPostsList.Add(key, post);
                }

                Dictionary<int, Post> UnapprovedPostsListReverse = new Dictionary<int, Post>();

                foreach (var postKeyValuePair in UnapprovedPostsList.Reverse())
                {
                    PostList.Add(postKeyValuePair.Value);
                }

                var ApprovedPostObjList = db.Posts.Where(p => p.Approval == true).ToList();

                //                SortedDictionary<int, Post> ApprovedPostsList = new SortedDictionary<int, Post>();
                //
                //                foreach (var post in ApprovedPostObjList)
                //                {
                //                    int key = int.Parse(post.PostId.Split('-')[1]);
                //                    ApprovedPostsList.Add(key, post);
                //                }
                //
                //                Dictionary<int, Post> ApprovedPostsListReverse = new Dictionary<int, Post>();
                //
                //                foreach (var postKeyValuePair in ApprovedPostsList.Reverse())
                //                {
                //                    PostList.Add(postKeyValuePair.Value);
                //                }
                List<Post> SortedApprovedList = ApprovedPostObjList.OrderBy(p => p.ApprovalTime).ToList();

                for (var i = SortedApprovedList.Count - 1; i >= 0; i--)
                {
                    PostList.Add(SortedApprovedList.ElementAt(i));
                }
                List<Post> searchedPost = new List<Post>();
                var searchfield1 = searchfield.ToUpper();
                List<searchedPostWithStringDate> searchedPostWithStringDates = new List<searchedPostWithStringDate>();
                foreach (var post in PostList)
                {
                    var PostId = "";
                    if (post.PostId != null)
                    {
                        PostId = post.PostId.ToUpper();
                    }

                    var OwnerId = "";
                    if (post.PostedBy != null)
                    {
                        OwnerId = post.PostedBy.ToUpper();
                    }

                    var OwnerName = "";
                    var val = db.OwnersInfos.Where(o => o.OwenerId == post.PostedBy).Select(o => o.Name)
                        .FirstOrDefault();
                    if (val != null)
                    {
                        OwnerName = val.ToUpper();
                    }
                    var Time = "";
                    if (post.Time != null)
                    {
                        Time = Convert.ToString(post.Time).ToUpper();
                    }
                    var Approval = "";
                    if (Convert.ToString(post.Approval) != null)
                    {
                        Approval = Convert.ToString(post.Approval).ToUpper();
                    }

                    var ApprovalTime = "";
                    if (Convert.ToString(post.ApprovalTime) != null)
                    {
                        ApprovalTime = (post.ApprovalTime == Convert.ToDateTime("1900 - 01 - 01 00:00:00.000")) ? "NOT UPDATED YET" : Convert.ToString(post.ApprovalTime).ToUpper();
                    }
                    var OwnerId1 = OwnerId;
                    var OwnerName1 = OwnerName;
                    var PostId1 = PostId;
                    var Time1 = Time;
                    var Approval1 = Approval;
                    var ApprovalTime1 = ApprovalTime;

                    if (OwnerId1.Contains(searchfield1) == true || PostId1.Contains(searchfield1) == true ||
                        Time1.Contains(searchfield1) == true || Approval1.Contains(searchfield1) == true
                        || OwnerName1.Contains(searchfield1) == true || ApprovalTime1.Contains(searchfield1) == true)
                    {
                        //                        var obj = JsonConvert.SerializeObject(post.Time);
                        searchedPost.Add(post);
                        searchedPostWithStringDates.Add(new searchedPostWithStringDate()
                        {
                            Time = Convert.ToString(post.Time),
                            Approval = post.Approval,
                            PostId = post.PostId,
                            OwnerName = val,
                            PostedBy = post.PostedBy,
                            ApprovalTime = (post.ApprovalTime == Convert.ToDateTime("1900 - 01 - 01 00:00:00.000")) ? "Not Approved Yet" : Convert.ToString(post.ApprovalTime)
                        });
                    }

                }
                string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\OwnerPostManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    //return View(Json("Error"));
                }
                //OwnerList mdl = new OwnerList()
                //{
                //    index = 1,
                //    NoOfOwner = 0,
                //    Owners = searchedOwner,
                //    TaskRoles = taskRoles
                //};
                //                ;


                return Json(searchedPostWithStringDates);


            }
        }

        [HttpPost]
        public ActionResult PostDelete(string postId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Ownersinfo\\OwnerPostManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[4] == '0')
                {
                    return View("Error");
                }
                var PostObject = adbc.Posts.Where(p => p.PostId == postId).FirstOrDefault();
                var CommentObjectLists = adbc.Comments.Where(c => c.PostId == postId).ToList();
                foreach (var commentObject in CommentObjectLists)
                {
                    adbc.Comments.Remove(commentObject);
                }
                var LikeObjectLists = adbc.Likes.Where(c => c.PostId == postId).ToList();
                foreach (var likeObject in LikeObjectLists)
                {
                    adbc.Likes.Remove(likeObject);
                }
                adbc.Posts.Remove(PostObject);
                adbc.SaveChanges();
            }

            return RedirectToAction("OwnerPostManagement", "OwnersInfo");
        }

        [HttpGet]
        public ActionResult PostView(string postId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var model = adbc.Posts.Where(p => p.PostId == postId).SingleOrDefault();
                PostViewModel mdl = new PostViewModel()
                {
                    PostId = model.PostId,
                    Approval = model.Approval,
                    NoOfLikes = model.NoOfLikes,
                    OwnersName = adbc.OwnersInfos.Where(o => o.OwenerId == model.PostedBy).Select(o => o.Name).FirstOrDefault(),
                    PostedBy = model.PostedBy,
                    PostText = model.PostText,
                    PostHeader = model.PostHeader
                };
                //adbc.Modules.Remove(delobj);
                //adbc.SaveChanges();
                //                var notificationModelForPost =
                //                    adbc.Notifications.Where(n => n.IdForRecognition == postId).FirstOrDefault();
                //                if(notificationModelForPost!=null) notificationModelForPost.isSeen = true;
                adbc.SaveChanges();
                return View("PostView", mdl);
            }
        }

        [HttpGet]
        public ActionResult PostApprove(string postId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var postInDb = adbc.Posts.Where(p => p.PostId == postId).SingleOrDefault();
                postInDb.Approval = true;
                //adbc.Modules.Remove(delobj);
                postInDb.ApprovalTime = DateTime.Now;
                adbc.SaveChanges();
                return RedirectToAction("OwnerPostManagement", "OwnersInfo");
            }
        }

        #endregion

        #region Comment Management

        [HttpGet]
        public ActionResult OwnerCommentManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "OwnersInfo\\OwnerCommentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }


                List<Comment> CommentObjList = new List<Comment>();

                var UnApprovedCommentObjList = adbc.Comments.Where(c => c.Approval == false).ToList();

                SortedDictionary<int, Comment> UnapprovedCommentsList = new SortedDictionary<int, Comment>();

                foreach (var comment in UnApprovedCommentObjList)
                {
                    int key = int.Parse(comment.CommentId.Split('-')[1]);
                    UnapprovedCommentsList.Add(key, comment);
                }

                Dictionary<int, Comment> UnapprovedCommentsListReverse = new Dictionary<int, Comment>();

                foreach (var commentKeyValuePair in UnapprovedCommentsList.Reverse())
                {
                    CommentObjList.Add(commentKeyValuePair.Value);
                }

                var ApprovedCommentObjList = adbc.Comments.Where(p => p.Approval == true).ToList();

                var SortedApprovedList = ApprovedCommentObjList.OrderBy(c => c.ApprovalTime).ToList();

                for (var i = SortedApprovedList.Count - 1; i >= 0; i--)
                {
                    CommentObjList.Add(SortedApprovedList.ElementAt(i));
                }
                List<Comment> selectedCommentInfos = new List<Comment>();
                List<string> selectedCommenterNames = new List<string>();
                int noOfComments;
                if (CommentObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedCommentInfos.Add(CommentObjList.ElementAt(i));
                        var OwnerId = CommentObjList.ElementAt(i).CommentorId;
                        selectedCommenterNames.Add(adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).Select(o => o.Name).FirstOrDefault());
                    }
                    int x = (CommentObjList.Count % 10 == 0) ? 0 : 1;

                    noOfComments = (CommentObjList.Count / 10) + x;
                }
                else
                {
                    selectedCommentInfos = CommentObjList;
                    foreach (var Comment in CommentObjList)
                    {
                        selectedCommenterNames.Add(adbc.OwnersInfos.Where(o => o.OwenerId == Comment.CommentorId).Select(o => o.Name)
                            .FirstOrDefault());
                    }
                    noOfComments = 1;
                }


                var model = new CommentList
                {
                    Comments = selectedCommentInfos,
                    TaskRoles = taskRoles,
                    NoOfComments = noOfComments,
                    OwnersName = selectedCommenterNames,
                    index = 1
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult CommentsPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "OwnersInfo\\OwnerCommentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                List<Comment> CommentObjList = new List<Comment>();

                var UnApprovedCommentObjList = db.Comments.Where(c => c.Approval == false).ToList();

                SortedDictionary<int, Comment> UnapprovedCommentsList = new SortedDictionary<int, Comment>();

                foreach (var comment in UnApprovedCommentObjList)
                {
                    int key = int.Parse(comment.CommentId.Split('-')[1]);
                    UnapprovedCommentsList.Add(key, comment);
                }

                Dictionary<int, Comment> UnapprovedCommentsListReverse = new Dictionary<int, Comment>();

                foreach (var commentKeyValuePair in UnapprovedCommentsList.Reverse())
                {
                    CommentObjList.Add(commentKeyValuePair.Value);
                }

                var ApprovedCommentObjList = db.Comments.Where(p => p.Approval == true).ToList();

                var SortedApprovedList = ApprovedCommentObjList.OrderBy(c => c.ApprovalTime).ToList();

                for (var i = SortedApprovedList.Count - 1; i >= 0; i--)
                {
                    CommentObjList.Add(SortedApprovedList.ElementAt(i));
                }
                List<Comment> selectedCommentInfos = new List<Comment>();
                List<string> selectedCommenterNames = new List<string>();
                int noOfComments;
                if (CommentObjList.Count >= 10)
                {
                    var pageNo = (CommentObjList.Count < (index * 10)) ? CommentObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedCommentInfos.Add(CommentObjList.ElementAt(i));
                        var OwnerId = CommentObjList.ElementAt(i).CommentorId;
                        selectedCommenterNames.Add(db.OwnersInfos.Where(o => o.OwenerId == OwnerId).Select(o => o.Name).FirstOrDefault());
                    }
                    int x = (CommentObjList.Count % 10 == 0) ? 0 : 1;

                    noOfComments = (CommentObjList.Count / 10) + x;
                }
                else
                {
                    selectedCommentInfos = CommentObjList;
                    foreach (var Comment in CommentObjList)
                    {
                        selectedCommenterNames.Add(db.OwnersInfos.Where(o => o.OwenerId == Comment.CommentorId).Select(o => o.Name)
                            .FirstOrDefault());
                    }
                    noOfComments = 1;
                }

                var model = new CommentList
                {
                    Comments = selectedCommentInfos,
                    TaskRoles = taskRoles,
                    NoOfComments = noOfComments,
                    OwnersName = selectedCommenterNames,
                    index = index
                };
                return View("OwnerCommentManagement", model);
            }
        }

        [HttpPost]
        public JsonResult gettingSearchResultForComment(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<Comment> CommentList = new List<Comment>();

                var UnApprovedCommentObjList = db.Comments.Where(c => c.Approval == false).ToList();

                SortedDictionary<int, Comment> UnapprovedCommentsList = new SortedDictionary<int, Comment>();

                foreach (var comment in UnApprovedCommentObjList)
                {
                    int key = int.Parse(comment.CommentId.Split('-')[1]);
                    UnapprovedCommentsList.Add(key, comment);
                }

                Dictionary<int, Comment> UnapprovedCommentsListReverse = new Dictionary<int, Comment>();

                foreach (var commentKeyValuePair in UnapprovedCommentsList.Reverse())
                {
                    CommentList.Add(commentKeyValuePair.Value);
                }

                var ApprovedCommentObjList = db.Comments.Where(p => p.Approval == true).ToList();

                var SortedApprovedList = ApprovedCommentObjList.OrderBy(c => c.ApprovalTime).ToList();

                for (var i = SortedApprovedList.Count - 1; i >= 0; i--)
                {
                    CommentList.Add(SortedApprovedList.ElementAt(i));
                }

                List<Comment> searchedComment = new List<Comment>();
                var searchfield1 = searchfield.ToUpper();
                List<searchedCommentWithStringDates> searchedCommentWithStringDates = new List<searchedCommentWithStringDates>();
                foreach (var comment in CommentList)
                {
                    var CommentId = "";
                    if (comment.CommentId != null)
                    {
                        CommentId = comment.CommentId.ToUpper();
                    }

                    var PostId = "";
                    if (comment.PostId != null)
                    {
                        PostId = comment.PostId.ToUpper();
                    }

                    var OwnerId = "";
                    if (comment.CommentorId != null)
                    {
                        OwnerId = comment.CommentorId.ToUpper();
                    }

                    var OwnerName = "";
                    var val = db.OwnersInfos.Where(o => o.OwenerId == comment.CommentorId).Select(o => o.Name)
                        .FirstOrDefault();
                    if (val != null)
                    {
                        OwnerName = val.ToUpper();
                    }
                    var CommentTime = "";
                    if (comment.CommentTime != null)
                    {
                        CommentTime = Convert.ToString(comment.CommentTime).ToUpper();
                    }
                    var Approval = "";
                    if (Convert.ToString(comment.Approval) != null)
                    {
                        Approval = Convert.ToString(comment.Approval).ToUpper();
                    }

                    var ApprovalTime = "";
                    if (Convert.ToString(comment.ApprovalTime) != null)
                    {
                        ApprovalTime = (comment.ApprovalTime == Convert.ToDateTime("1900 - 01 - 01 00:00:00.000")) ? "NOT UPDATED YET" : Convert.ToString(comment.ApprovalTime).ToUpper();
                    }
                    var OwnerId1 = OwnerId;
                    var OwnerName1 = OwnerName;
                    var PostId1 = PostId;
                    var CommentTime1 = CommentTime;
                    var Approval1 = Approval;
                    var ApprovalTime1 = ApprovalTime;
                    var CommentId1 = CommentId;

                    if (OwnerId1.Contains(searchfield1) == true || PostId1.Contains(searchfield1) == true
                        || CommentTime1.Contains(searchfield1) == true || Approval1.Contains(searchfield1) == true
                        || OwnerName1.Contains(searchfield1) == true || ApprovalTime1.Contains(searchfield1) == true
                        || CommentId1.Contains(searchfield1) == true)
                    {
                        //                        var obj = JsonConvert.SerializeObject(post.Time);
                        searchedComment.Add(comment);
                        searchedCommentWithStringDates.Add(new searchedCommentWithStringDates()
                        {
                            CommentTime = Convert.ToString(comment.CommentTime),
                            Approval = comment.Approval,
                            PostId = comment.PostId,
                            OwnerName = val,
                            CommentorId = comment.CommentorId,
                            CommentId = comment.CommentId,
                            ApprovalTime = (comment.ApprovalTime == Convert.ToDateTime("1900 - 01 - 01 00:00:00.000")) ? "Not Approved Yet" : Convert.ToString(comment.ApprovalTime)
                        });
                    }

                }
                string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\OwnerCommentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    //return View(Json("Error"));
                }
                //OwnerList mdl = new OwnerList()
                //{
                //    index = 1,
                //    NoOfOwner = 0,
                //    Owners = searchedOwner,
                //    TaskRoles = taskRoles
                //};
                //                ;


                return Json(searchedCommentWithStringDates);


            }
        }

        [HttpPost]
        public ActionResult CommentDelete(string commentId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Ownersinfo\\OwnerCommentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[4] == '0')
                {
                    return View("Error");
                }
                var CommentObject = adbc.Comments.Where(c => c.CommentId == commentId).FirstOrDefault();
                adbc.Comments.Remove(CommentObject);
                adbc.SaveChanges();
            }

            return RedirectToAction("OwnerCommentManagement", "OwnersInfo");
        }

        [HttpGet]
        public ActionResult CommentView(string commentId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var model = adbc.Comments.Where(c => c.CommentId == commentId).SingleOrDefault();
                var postModel = adbc.Posts.Where(p => p.PostId == model.PostId).SingleOrDefault();
                CommentViewModel mdl = new CommentViewModel()
                {
                    PostId = model.PostId,
                    Approval = model.Approval,
                    CommentTime = model.CommentTime,
                    CommentId = model.CommentId,
                    CommentText = model.CommentText,
                    CommentorId = model.CommentorId,
                    OwnersName = adbc.OwnersInfos.Where(o => o.OwenerId == model.CommentorId).Select(o => o.Name).FirstOrDefault(),
                    PostedBy = postModel.PostedBy,
                    PostText = postModel.PostText,
                    PostHeader = postModel.PostHeader,
                    PosterName = adbc.OwnersInfos.Where(o => o.OwenerId == postModel.PostedBy).Select(o => o.Name).FirstOrDefault(),
                };
                //                var notificationModelForComment =
                //                    adbc.Notifications.Where(n => n.IdForRecognition == commentId).FirstOrDefault();
                //                if (notificationModelForComment != null) notificationModelForComment.isSeen = true;
                //adbc.Modules.Remove(delobj);
                adbc.SaveChanges();
                return View("CommentView", mdl);
            }
        }

        [HttpGet]
        public ActionResult CommentApprove(string commentId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var commentInDb = adbc.Comments.Where(c => c.CommentId == commentId).SingleOrDefault();
                commentInDb.Approval = true;
                //adbc.Modules.Remove(delobj);
                commentInDb.ApprovalTime = DateTime.Now;
                adbc.SaveChanges();
                return RedirectToAction("OwnerCommentManagement", "OwnersInfo");
            }
        }

        #endregion

        #region MISC Actions

        [HttpGet]
        public ActionResult Download_BankAdvisePDF(string slotId)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "CrystalReportOwnerPayments.rpt"));
            /////

            ///
            var selectedListOfBookings = db.OwnerPayments.Where(c => c.SlotId == slotId.ToString()).ToList();
            var selectedColums = selectedListOfBookings.Select(c => new
            {
                SlotId = c.SlotId,
                ReceiverName = c.ReceiverName,
                ReceivingBank = c.ReceivingBank,
                Amount = c.Amount
            }).ToList();

            rd.SetDataSource(selectedColums);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            //rd.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape;
            //rd.PrintOptions.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(5, 5, 5, 5));
            //rd.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA5;

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "OwnerPaymets.pdf");
        }

        public ActionResult Download_OwnerBookingHistoryPDF(string slotId)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "CrystalReportOwnerPayments.rpt"));

            //var selectedListOfBookings = db.OwnerPayments.Where(c => c.SlotId == slotId.ToString()).ToList();
            //var selectedColums = selectedListOfBookings.Select(c => new
            //{
            //    SlotId = c.SlotId,
            //    ReceiverName = c.ReceiverName,
            //    ReceivingBank = c.ReceivingBank,
            //    Amount = c.Amount
            //}).ToList();
            var bookingList2 = (from c in db.ConfAllotments
                                join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                                select new OwnerBookingHistoryReport
                                {
                                    SlotId = c.SlotId,
                                    BookingToken = b.BookingToken,
                                    GuestMobile = b.GuestMobile,
                                    Email = b.Email,

                                    //YearlyAllotment = c.YearlyAllotment
                                }).ToList();
            var bookingList = bookingList2.Where(c => c.SlotId == slotId).ToList();
            //return bookingList;

            rd.SetDataSource(bookingList);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            //rd.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape;
            //rd.PrintOptions.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(5, 5, 5, 5));
            //rd.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA5;

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "OwnerPaymets.pdf");
        }


        //Starting Owner's Booking Management
        [HttpGet]
        public ActionResult OwnerRoomBookingReport()
        {
            return Redirect("../Reports/OwnerRoomBookingReport.aspx");
        }

        #endregion

        #region MISC Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult gettingSlotList(string roomId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var slotList = adbc.RoomSlotAssigments.Where(m => m.RoomId == roomId).ToList();
                List<SelectListItem> someArray = new List<SelectListItem>();
                foreach (var x in slotList)
                {
                    var nm = new SelectListItem()
                    {
                        Text = adbc.SlotInfos.Where(s => s.SlotId == x.SlotId).Select(s => s.SlotName).FirstOrDefault(),
                        Value = x.SlotId
                    };
                    someArray.Add(nm);
                }

                //var allslot = adbc.SlotInfos.ToList();
                //List<SelectListItem> someArray2 = new List<SelectListItem>();
                //foreach (var x in allslot)
                //{
                //    if (someArray.Contains(x.SlotId))
                //    {
                //        someArray2.Add();
                //    }
                //}
                return Json(someArray, JsonRequestBehavior.AllowGet);
            }

        }

        //This is for OwnerRoomSlot Create, not for RoomSlot Create
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult gettingSlotList2(string roomId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var slotList = adbc.RoomSlotAssigments.Where(m => m.RoomId == roomId).ToList();
                List<SelectListItem> someArray = new List<SelectListItem>();
                foreach (var x in slotList)
                {
                    var slotidofNotSold = adbc.RoomSlotAssigments.Where(s => s.SlotId == x.SlotId && s.SoldStatus == "Not Sold")
                        .Select(s => s.SlotId).FirstOrDefault();
                    var nm = new SelectListItem()
                    {
                        //Text = adbc.SlotInfos.Where(s => s.SlotId == x.SlotId).Select(s => s.SlotName).FirstOrDefault(),
                        Text = adbc.SlotInfos.Where(s => s.SlotId == slotidofNotSold).Select(s => s.SlotName).FirstOrDefault(),
                        Value = x.SlotId
                    };
                    someArray.Add(nm);
                }
                return Json(someArray, JsonRequestBehavior.AllowGet);
            }

            //using (ApplicationDbContext db = new ApplicationDbContext())
            //{
            //    //string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\OwnersRoomSlotInfo").Select(u => u.TaskId).FirstOrDefault();

            //    var RoomList = db.RoomInfos.ToList();
            //    var RoomIdListRS =
            //        db.RoomSlotAssigments.Select(r => r.RoomId).ToList(); //RoomId List from RoomSlot combined
            //    foreach (var room in RoomList)
            //    {
            //        if (RoomIdListRS.Contains(room.RoomId))
            //        {
            //            var list = db.RoomSlotAssigments.Where(r => r.SoldStatus == "Not Sold").ToList();
            //            bool flag = false;
            //            for (int i = 0; i < list.Count; i++)
            //            {
            //                if (list[i].SoldStatus == "Not Sold")
            //                {
            //                    flag = true;
            //                }
            //            }

            //            if (flag == true)
            //            {

            //            }
            //        }
            //    }
            //    return Json( JsonRequestBehavior.AllowGet);
            //}
        }

        #endregion

        #region MISC Actions

        [HttpGet]
        public ActionResult ExportExcel()
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            Response.ClearContent();
            Response.BinaryWrite(GenerateExcel());
            Response.AppendHeader("content-disposition", "attachment; filename=OwnersInformationAndDividend.xlsx");
            Response.ContentType = "application/vdn.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Flush();
            Response.End();

            return RedirectToAction("OwnerManagement");
        }

        [HttpGet]
        private byte[] GenerateExcel()
        {
            ExcelPackage ep = new ExcelPackage();


            ep.Workbook.Worksheets.Add("OwnersList");


            ExcelWorksheet workSheet = ep.Workbook.Worksheets[1];

            workSheet.Cells["A1"].Value = "Owner Id";
            workSheet.Cells["B1"].Value = "Name";
            workSheet.Cells["C1"].Value = "Date of Birth";
            workSheet.Cells["D1"].Value = "Mobile Number";
            workSheet.Cells["E1"].Value = "Dividend Amount";


            workSheet.Column(1).AutoFit();
            workSheet.Column(2).AutoFit();
            workSheet.Column(3).AutoFit();
            workSheet.Column(4).AutoFit();
            workSheet.Column(5).AutoFit();

            workSheet.Protection.IsProtected = true;
            workSheet.Column(5).Style.Locked = false;


            //// Aplicar estilo al tipo de letra
            ////ew1.Cells[1, 1].Style.Font.Bold = true;
            //ew1.Cells["A1"].Style.Font.Bold = true;


            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var allOwners = db.OwnersInfos.ToList();
                for (int i = 0; i < allOwners.Count; i++)
                {
                    var own = allOwners[i];
                    DateTime enteredDate = DateTime.Parse(own.DateOfBirth.ToString());
                    workSheet.Cells[i + 2, 1].Value = own.OwenerId;
                    workSheet.Cells[i + 2, 2].Value = own.Name;
                    workSheet.Cells[i + 2, 3].Value = enteredDate.ToString("dd/MM/yyyy");
                    workSheet.Cells[i + 2, 4].Value = own.Mobile;
                }

            }

            ep.Save();
            return ep.GetAsByteArray();
        }

        [HttpPost]
        public JsonResult JSONDummy(TestView model)
        {
            return Json("Success");
        }

        [HttpGet]
        public ActionResult OwnersInfoReport()
        {
            return Redirect("../Reports/OwnersInfoReport.aspx");
        }

        #endregion

        #region MISC Actions

        [HttpPost]
        public void Upload_Test()
        {

            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var fileName = Path.GetFileName(file.FileName);
                //var fileName = "CPic_" + child + "_" + ownerId + ".jpg";

                var path = Path.Combine(Server.MapPath("~/Content/families/"), fileName);
                file.SaveAs(path);
            }
        }

        #endregion


        [HttpPost]
        public JsonResult gettingOwnerIdandCodeForDatabaseCheckingSMS(OwnersInfoOTPSMS model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var OwnerOTPCode = adbc.OwnerOtpCodes.Where(o => o.OwnerId == model.OwnerId).FirstOrDefault();
                if (OwnerOTPCode.OwnerOTPSMSCode == model.InputSMSCode)
                {
                    OwnerOTPCode.IsOTPSMSCodeValidated = "True";
                    adbc.SaveChanges();
                    return Json("Yes");
                }
                adbc.Dispose();
            }

            return Json("No");
        }

        [HttpPost]
        public JsonResult gettingOwnerIdForDatabasePoputlation(string OwnerId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerOtpCodeObj = adbc.OwnerOtpCodes.Where(o => o.OwnerId == OwnerId).FirstOrDefault();

                if (ownerOtpCodeObj == null)
                {
                    var ownerOtpCodesList = adbc.OwnerOtpCodes.ToList();
                    var ownerOtpCodesIdList = new List<int>();
                    foreach (var ownerOtpCodes in ownerOtpCodesList)
                    {
                        ownerOtpCodesIdList.Add(int.Parse(ownerOtpCodes.OwnerOTPCodeId.ToString().Split('-')[1]));
                    }
                    var ownerOtpCodesToken = ownerOtpCodesList.Count();
                    if (ownerOtpCodesToken > 0)
                    {
                        ownerOtpCodesIdList.Sort();
                        ownerOtpCodesToken = ownerOtpCodesIdList[ownerOtpCodesIdList.Count - 1];
                    }
                    ownerOtpCodesToken++;

                    string ownerOTPSMSCode = "";


                    string token = "";
                    bool checkPoint = false;
                    while (checkPoint == false)
                    {
                        Random r = new Random();
                        var x = r.Next(0, 1000000);
                        token = x.ToString("000000");

                        var OwnerOTPSMSCodeList = adbc.OwnerOtpCodes.Select(u => u.OwnerOTPSMSCode).ToList();
                        if (OwnerOTPSMSCodeList.Contains(token) == false)
                        {
                            ownerOTPSMSCode = token;
                            checkPoint = true;
                        }
                    }


                    var model = new OwnerOTPCode()
                    {
                        OwnerId = OwnerId,
                        OwnerOTPCodeId = "OTP-" + ownerOtpCodesToken,
                        OwnerOTPEmailCode = "",
                        OwnerOTPSMSCode = ownerOTPSMSCode,
                        IsOTPEmailCodeValidated = "False",
                        IsOTPSMSCodeValidated = "False"
                    };
                    adbc.OwnerOtpCodes.Add(model);

                    var mdl = adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).FirstOrDefault();

                    //Please Uncomment to Send Message
                    //Common.SMSSending(mdl.Mobile, "Your 6 digit OTP SMS Code : " + ownerOTPSMSCode);
                    var Owner = mdl;
                    var mobileNumber = mdl.Mobile;
                    var Email = mdl.Email;
                    var isSMSSent = "Processing";
                    var txt = "Your 6 digit OTP SMS Code : " + ownerOTPSMSCode;
                    if (mobileNumber != null && mobileNumber != "")
                    {
                        var returnStatus = Common.SMSSending(mobileNumber, txt);
                        if (returnStatus == true)
                        {
                            isSMSSent = "Delivered";
                        }
                        else
                        {
                            isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                        }
                    }
                    else
                    {
                        isSMSSent = "Not Delivered, Check whether Mobile No for this owner is correct";
                    }
                    EmailandSMSReport emailsmsreport = new EmailandSMSReport()
                    {
                        IsSMSSent = isSMSSent,
                        Mobile = mobileNumber,
                        Email = Owner.Email,
                        SlotId = Owner.SlotId,
                        Name = Owner.Name,
                        TimeOfDelivery = DateTime.Now,
                        NotificationType = "SMS",
                        Text = txt,
                        NotificationSerialNo = "AUTO GENERATED:- OTP",
                    };
                    Common.InsertEmailandSMSReports(emailsmsreport);

                    adbc.SaveChanges();
                }

                else
                {
                    string ownerOTPSMSCode = "";
                    string token = "";
                    bool checkPoint = false;
                    while (checkPoint == false)
                    {
                        Random r = new Random();
                        var x = r.Next(0, 1000000);
                        token = x.ToString("000000");

                        var OwnerOTPSMSCodeList = adbc.OwnerOtpCodes.Select(u => u.OwnerOTPSMSCode).ToList();
                        if (OwnerOTPSMSCodeList.Contains(token) == false)
                        {
                            ownerOTPSMSCode = token;
                            checkPoint = true;
                        }
                    }

                    var ownerOtpCode = adbc.OwnerOtpCodes.Where(o => o.OwnerId == OwnerId).FirstOrDefault();
                    ownerOtpCode.OwnerOTPSMSCode = ownerOTPSMSCode;

                    var mdl = adbc.OwnersInfos.Where(o => o.OwenerId == OwnerId).FirstOrDefault();
                    string message = "Your 6 digit OTP SMS Code : " + ownerOTPSMSCode;

                    //Please Uncomment to Send Message
                    //Common.SMSSending(mdl.Mobile, message);
                    var Owner = mdl;
                    var mobileNumber = mdl.Mobile;
                    var Email = mdl.Email;
                    var isSMSSent = "Processing";
                    var txt = message;
                    if (mobileNumber != null && mobileNumber != "")
                    {
                        var returnStatus = Common.SMSSending(mobileNumber, txt);
                        if (returnStatus == true)
                        {
                            isSMSSent = "Delivered";
                        }
                        else
                        {
                            isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                        }
                    }
                    else
                    {
                        isSMSSent = "Not Delivered, Check whether Mobile No for this owner is correct";
                    }
                    EmailandSMSReport emailsmsreport = new EmailandSMSReport()
                    {
                        IsSMSSent = isSMSSent,
                        Mobile = mobileNumber,
                        Email = Owner.Email,
                        SlotId = Owner.SlotId,
                        Name = Owner.Name,
                        TimeOfDelivery = DateTime.Now,
                        NotificationType = "SMS",
                        Text = txt,
                        NotificationSerialNo = "AUTO GENERATED:- OTP",
                    };
                    Common.InsertEmailandSMSReports(emailsmsreport);
                    adbc.SaveChanges();
                }
                adbc.Dispose();
            }
            return Json("sad");
        }
    }
}