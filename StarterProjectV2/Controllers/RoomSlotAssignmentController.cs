﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;

namespace StarterProjectV2.Controllers
{
    public class RoomSlotAssignmentController : Controller
    {
        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        // GET: RoomSlotAssignment

            
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult gettingSlotList(string roomId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allSlots = adbc.SlotInfos.ToList();
                var slotList = adbc.RoomSlotAssigments.Where(m => m.RoomId == roomId).ToList();
                var slotlist2 = adbc.RoomSlotAssigments.Where(m => m.RoomId == roomId).Select(m => m.SlotId).ToList();
                List<SelectListItem> someArray = new List<SelectListItem>();
                foreach (var x in allSlots)
                {
                    if (!(slotlist2.Contains(x.SlotId)))
                    {
                        var nm = new SelectListItem()
                        {
                            Text = adbc.SlotInfos.Where(s => s.SlotId == x.SlotId).Select(s => s.SlotName)
                                .FirstOrDefault(),
                            Value = x.SlotId
                        };
                        someArray.Add(nm);
                    }
                }
                return Json(someArray, JsonRequestBehavior.AllowGet);
                //previous version, working reverse (shows all the slots which are in roomslotassignment 
                //var slotList = adbc.RoomSlotAssigments.Where(m => m.RoomId == roomId).ToList();
                //List<SelectListItem> someArray = new List<SelectListItem>();
                //foreach (var x in slotList)
                //{
                //    var nm = new SelectListItem()
                //    {
                //        Text = adbc.SlotInfos.Where(s => s.SlotId == x.SlotId).Select(s => s.SlotName).FirstOrDefault(),
                //        Value = x.SlotId
                //    };
                //    someArray.Add(nm);
                //}





                //var allslot = adbc.SlotInfos.ToList();
                //List<SelectListItem> someArray2 = new List<SelectListItem>();
                //foreach (var x in allslot)
                //{
                //    if (someArray.Contains(x.SlotId))
                //    {
                //        someArray2.Add();
                //    }
                //}
                //return Json(someArray, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public ActionResult RoomSlotManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                string taskId = db.Tasks.Where(u => u.TaskPath == "RoomSlotAssignment\\RoomSlotManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                var model = new RoomSlotManagementView
                {
                    RoomSlotList = db.RoomSlotAssigments.ToList(),
                    TaskRoles = taskRoles
                };
                return View(model);
            }
        }


        [HttpGet]
        public ActionResult RoomSlotCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                
                string taskId = db.Tasks.Where(u => u.TaskPath == "Room\\RoomManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                
                string taskId2 = db.Tasks.Where(u => u.TaskPath == "Slot\\SlotManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList2 = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles2 = "";
                foreach (var tasks in myRoleList2)
                {
                    if (tasks.TaskId == taskId2)
                    {
                        taskRoles2 = tasks.Details;
                    }
                }
                if (taskRoles2 == "")
                {
                    return View("Error");
                }


                var model = new RoomSlotView
                {

                    RoomList = db.RoomInfos.ToList(),
                    SlotList = db.SlotInfos.ToList()
                };


                return View(model);
            }
        }

        [HttpGet]
        public ActionResult RoomSlotView(string roomslotId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                //var model = adbc.RoomInfos.Where(p => p.RoomId == roomId).SingleOrDefault();
                //List<string> basicOwnershipCriteriaList = new List<string>()
                //{
                //    "J.V.", "Coral Reef"
                //};
                //RoomViewModel viewModel = new RoomViewModel()
                //{
                //    Floor = model.Floor,
                //    RoomId = model.RoomId,
                //    RoomType = model.RoomType,
                //    RoomSize = model.RoomSize,
                //    OwnershipCriteria = model.OwnershipCriteria,
                //    OwnershipCriteriaList = basicOwnershipCriteriaList
                //};

                //return View("RoomView", viewModel);
                return View();
            }
        }

       

        //Only Has Add option, Will do the Edit option later, if possible
        [HttpPost]
        public ActionResult RoomSlotAddorEdit(RoomSlotView info)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {


                //var roomInDb = adbc.RoomInfos.SingleOrDefault(r => r.RoomNo == info.RoomSelected);

                //RoomId = roomInDb.ToString();
                var RoomId = "";
                var roomList = adbc.RoomSlotAssigments.ToList();
                var roomIdIdList = new List<int>();
                foreach (var rooms in roomList)
                {
                    roomIdIdList.Add(int.Parse(rooms.RoomSlotAssignmentId.ToString().Split('-')[1]));
                }

                var roomToken = roomList.Count();
                if (roomToken > 0)
                {
                    roomIdIdList.Sort();
                    roomToken = roomIdIdList[roomIdIdList.Count - 1];
                }

                roomToken++;
                RoomId = "RSA-" + roomToken;


                var roomNo = adbc.RoomInfos.Where(r => r.RoomId == info.RoomSelected).Select(r=> r.RoomNo).FirstOrDefault();
                var slotName = adbc.SlotInfos.Where(r => r.SlotId == info.SlotSelected).Select(r=> r.SlotName).FirstOrDefault();
                RoomSlotAssignment model = new RoomSlotAssignment()
                {
                    RoomSlotAssignmentId = RoomId,
                    RoomId = info.RoomSelected,
                    SlotId = info.SlotSelected,
                    RoomSlotText = info.RoomSelected + ","+ info.SlotSelected + "," + roomNo + "," + slotName,
                    SoldStatus = "Not Sold"
                };
                adbc.RoomSlotAssigments.Add(model);
                adbc.SaveChanges();
                return RedirectToAction("RoomSlotManagement");

            }
        }

        [HttpPost]
        public ActionResult RoomSlotDelete(string roomslotId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var RoomSlotInDb = adbc.RoomSlotAssigments.Where(r => r.RoomSlotAssignmentId == roomslotId).FirstOrDefault();
                adbc.RoomSlotAssigments.Remove(RoomSlotInDb);
                adbc.SaveChanges();
                return RedirectToAction("RoomSlotManagement");
            }
            
        }
    }
}