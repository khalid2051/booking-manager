﻿using CrystalDecisions.CrystalReports.Engine;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Ajax.Utilities;
using OfficeOpenXml;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = iTextSharp.text.Image;
namespace StarterProjectV2.Controllers
{
    public class ReportController : Controller
    {
        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        [HttpGet]
        public ActionResult OwnerBankAdviseReportPDF()
        {
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            //Top Heading
            PdfPCell cell = new PdfPCell();
            Image image = Image.GetInstance(Server.MapPath("~/Content/images/Combined-Logo2.png"));
            image.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

            image.ScaleAbsolute(50, 10);
            cell.AddElement(image);
            Paragraph para = new Paragraph();
            para.Add("Grand Heritage Ltd.\nBest Western Plus Heritage, Ground Floor\nKolatoli Circle, Cox's Bazar-4700. Phone : +88034152611-18");
            pdfDoc.Add(para);
            Chunk chunk = new Chunk("", FontFactory.GetFont("Arial", 20, Font.BOLDITALIC, BaseColor.RED));

            pdfDoc.Add(chunk);


            //Table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;

            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 30f;


            cell.Border = 0;

            //table.AddCell(cell);
            //Paragraph line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            //pdfDoc.Add(line);
            //Cell no 2



            table.AddCell(cell);

            //Add table to document
            pdfDoc.Add(table);

            //Horizontal Line

            //Table
            table = new PdfPTable(5);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 30f;

            //Cell
            cell = new PdfPCell();
            chunk = new Chunk("Owner Bank Advise Report");
            cell.AddElement(chunk);
            cell.Colspan = 5;
            cell.BackgroundColor = BaseColor.WHITE;
            table.AddCell(cell);

            table.AddCell("Slot Id");
            table.AddCell("Receiver Name");
            table.AddCell("Receiving A/C");
            table.AddCell("Receiving Bank");
            table.AddCell("Amount");
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerPayments on c.SlotId equals b.SlotId

                         select new OwnerBankAdviseReport
                         {
                             SlotId = c.SlotId,
                             ReceiverName = b.ReceiverName,
                             AccountNo = c.AccountNo,
                             ReceivingBank = b.ReceivingBank,
                             Amount = b.Amount,


                         }).ToList();

            if (List2.Count() == 0)
            {
                TempData["toastrData"] = "No Data Available";

                return RedirectToAction("OwnerBankAdviseReport");
            }
            List<OwnerBankAdviseReport> OwnerBankAdviseReportList = new List<OwnerBankAdviseReport>();
            foreach (var ownerBankAdviseReport in List2)
            {
                table.AddCell(ownerBankAdviseReport.SlotId);
                table.AddCell(ownerBankAdviseReport.ReceiverName);
                table.AddCell(ownerBankAdviseReport.AccountNo);
                table.AddCell(ownerBankAdviseReport.ReceivingBank);
                table.AddCell(Convert.ToString(ownerBankAdviseReport.Amount));
                //table.AddCell(ownerBankAdviseReport.Amount);


                //table.Addcell();
            }

            pdfDoc.Add(table);


            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=OwnerBankAdviseReport.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();

            return View();
        }
        [HttpGet]
        public ActionResult OwnerBookingHistoryReportPDF(string slotId)
        {
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            //Top Heading
            PdfPCell cell = new PdfPCell();
            Image image = Image.GetInstance(Server.MapPath("~/Content/images/Combined-Logo2.png"));
            //image.PaddingTop = 0;
            image.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            image.ScaleAbsolute(50, 10);
            cell.AddElement(image);
            Chunk chunk = new Chunk("Best Western Heritage\n", FontFactory.GetFont("Arial", 15, Font.BOLDITALIC, BaseColor.BLACK));

            pdfDoc.Add(chunk);


            //Table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            //0=Left, 1=Centre, 2=Right
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 30f;

            //Cell no 1
            //PdfPCell cell = new PdfPCell();
            cell.Border = 0;

            table.AddCell(cell);

            chunk = new Chunk("", FontFactory.GetFont("Arial", 15, Font.NORMAL, BaseColor.BLACK));
            cell = new PdfPCell();
            cell.Border = 0;
            cell.AddElement(chunk);
            table.AddCell(cell);

            //Add table to document
            pdfDoc.Add(table);


            //Table
            table = new PdfPTable(7);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 30f;

            //Cell
            cell = new PdfPCell();
            chunk = new Chunk("Owner Booking History Report");
            cell.AddElement(chunk);
            cell.Colspan = 7;
            cell.BackgroundColor = BaseColor.WHITE;
            table.AddCell(cell);

            table.AddCell("Slot Id");
            table.AddCell("Booking Token");
            table.AddCell("Mobile");
            table.AddCell("Email");
            table.AddCell("CheckIn Date");
            table.AddCell("CheckOut Date");
            table.AddCell("Stay Day");
            ApplicationDbContext db = new ApplicationDbContext();
            var bookingList2 = (from c in db.ConfAllotments
                                join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                                select new OwnerBookingHistoryReport
                                {
                                    SlotId = c.SlotId,
                                    BookingToken = b.BookingToken,
                                    GuestMobile = b.GuestMobile,
                                    Email = b.Email,
                                    CheckInDate = b.CheckInDate.ToString(),
                                    CheckOutDate = b.CheckOutDate.ToString(),
                                    HasStayedOrNot = b.HasStayedOrNot,
                                    NoOfDaySpent = b.NoOfDaySpent
                                }).ToList();
            var bookingList = bookingList2.Where(c => c.SlotId == slotId && c.HasStayedOrNot == "Yes").ToList();
            var sum = bookingList.Sum(b => b.NoOfDaySpent);
            if (bookingList.Count == 0)
            {
                //TempData["toastrData2"] = "No Data Available";
                TempData["toastrData"] = "No Data Available";
                //return View("OwnerBookingHistoryReportview");
                return RedirectToAction("OwnerBookingHistoryReport");
            }

            List<OwnerBookingHistoryReport> OwnerBookingHistoryReportList = new List<OwnerBookingHistoryReport>();
            foreach (var ownerBankAdviseReport in bookingList)
            {
                table.AddCell(ownerBankAdviseReport.SlotId);
                table.AddCell(ownerBankAdviseReport.BookingToken);
                table.AddCell(ownerBankAdviseReport.GuestMobile);
                table.AddCell(ownerBankAdviseReport.Email);
                table.AddCell(ownerBankAdviseReport.CheckInDate);
                table.AddCell(ownerBankAdviseReport.CheckOutDate);
                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfDaySpent));
                //table.AddCell(ownerBankAdviseReport.Amount);


                //table.Addcell();
            }
            table.AddCell("");
            table.AddCell("");
            table.AddCell("");
            table.AddCell("");
            table.AddCell("");
            table.AddCell("Total: ");
            table.AddCell(Convert.ToString(sum));
            pdfDoc.Add(table);



            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=OwnerBookingHistoryReport.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();

            return View();
        }
        [HttpGet]
        public ActionResult OwnerDepartureReportPDF(DateTime checkoutdate)
        {

            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            //Top Heading
            PdfPCell cell = new PdfPCell();
            Image image = Image.GetInstance(Server.MapPath("~/Content/images/Combined-Logo2.png"));
            //image.PaddingTop = 0;
            image.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            image.ScaleAbsolute(50, 10);
            cell.AddElement(image);
            Chunk chunk = new Chunk("Best Western Heritage\n", FontFactory.GetFont("Arial", 15, Font.BOLDITALIC, BaseColor.BLACK));

            pdfDoc.Add(chunk);


            //Table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            //0=Left, 1=Centre, 2=Right
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 30f;

            //Cell no 1
            //PdfPCell cell = new PdfPCell();
            cell.Border = 0;

            table.AddCell(cell);

            chunk = new Chunk("", FontFactory.GetFont("Arial", 15, Font.NORMAL, BaseColor.BLACK));
            cell = new PdfPCell();
            cell.Border = 0;
            cell.AddElement(chunk);
            table.AddCell(cell);
            //Add table to document
            pdfDoc.Add(table);

            //Horizontal Line

            //Table
            table = new PdfPTable(9);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 30f;

            //Cell
            cell = new PdfPCell();
            chunk = new Chunk("Owner Departure Report ", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.BLACK));
            cell.AddElement(chunk);
            cell.Colspan = 9;
            cell.BackgroundColor = BaseColor.WHITE;
            table.AddCell(cell);
            table.AddCell("Reservation No");
            table.AddCell("Guest Name");
            table.AddCell("Owner Id");
            table.AddCell("CheckIn Date");
            table.AddCell("CheckOut Date");
            table.AddCell("Stay Night");
            table.AddCell("No Of Adult");
            table.AddCell("No Of Child");


            table.AddCell("Owner Segment");
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                         select new OwnerDepartureReport
                         {
                             SlotId = c.SlotId,
                             BookingToken = b.BookingToken,
                             GuestNameMain = b.GuestNameMain,
                             NoOfAdult = b.NoOfAdult.ToString(),
                             NoOfChild = b.NoOfChild.ToString(),
                             HasStayedOrNot = b.HasStayedOrNot,
                             CheckInDate = b.CheckInDate.ToString(),
                             CheckOutDate = b.CheckOutDate.ToString(),
                             OwnerSegment = c.OwnerSegment,
                             NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                         }).ToList();
            //string CDate=db.OwnerRoomBookings.CheckOutDate.ToString().ToList();
            var List = List2.Where(b => Convert.ToDateTime(b.CheckOutDate).Date == checkoutdate.Date && b.HasStayedOrNot == "Yes").ToList();
            if (List.Count() == 0)
            {
                TempData["toastrData"] = "No Data Available";

                return RedirectToAction("OwnerDepartureReport");
            }
            //List<ReferralBooking> BookingObjList = db.ReferralBookings.Where(b => b.BookingToken == null || b.BookingToken == "").ToList();
            List<OwnerDepartureReport> OwnerBankAdviseReportList = new List<OwnerDepartureReport>();
            foreach (var ownerBankAdviseReport in List)
            {
                table.AddCell(ownerBankAdviseReport.BookingToken);
                table.AddCell(ownerBankAdviseReport.GuestNameMain);
                table.AddCell(ownerBankAdviseReport.SlotId);
                table.AddCell(ownerBankAdviseReport.CheckInDate);
                table.AddCell(ownerBankAdviseReport.CheckOutDate);
                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfDaySpent));

                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfAdult));
                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfChild));


                //table.AddCell(DateTime.Parse(ownerBankAdviseReport.CheckInDate));
                //table.AddCell(DateTime.Parse(Convert.ToString(ownerBankAdviseReport.CheckOutDate)));
                table.AddCell(ownerBankAdviseReport.OwnerSegment);
                //table.Addcell();
            }

            pdfDoc.Add(table);




            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=OwnerDepartureReportPDF.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();

            return View();
        }
        [HttpGet]
        public ActionResult OwnerInHouseReportPDF(string date)
        {

            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            //Top Heading
            PdfPCell cell = new PdfPCell();
            Image image = Image.GetInstance(Server.MapPath("~/Content/images/Combined-Logo2.png"));
            //image.PaddingTop = 0;
            image.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            image.ScaleAbsolute(50, 10);
            cell.AddElement(image);
            Chunk chunk = new Chunk("Best Western Heritage\n", FontFactory.GetFont("Arial", 15, Font.BOLDITALIC, BaseColor.BLACK));

            pdfDoc.Add(chunk);

            //Table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            //0=Left, 1=Centre, 2=Right
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 30f;

            //Cell no 1
            //PdfPCell cell = new PdfPCell();
            cell.Border = 0;

            table.AddCell(cell);

            chunk = new Chunk("", FontFactory.GetFont("Arial", 15, Font.NORMAL, BaseColor.BLACK));
            cell = new PdfPCell();
            cell.Border = 0;
            cell.AddElement(chunk);


            table.AddCell(cell);

            //Add table to document
            pdfDoc.Add(table);


            //Table
            table = new PdfPTable(8);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 20f;

            //Cell
            cell = new PdfPCell();
            chunk = new Chunk("Owner In House Report", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.BLACK));
            cell.AddElement(chunk);
            cell.Colspan = 9;
            cell.BackgroundColor = BaseColor.WHITE;
            table.AddCell(cell);
            table.AddCell("Reservation No");
            table.AddCell("Guest Name");
            table.AddCell("Owner Id");
            table.AddCell("CheckIn Date");
            table.AddCell("CheckOut Date");
            table.AddCell("Stay Night");
            table.AddCell("No Of Adult");
            table.AddCell("No Of Child");
            table.AddCell("Owner Segment");
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                         select new OwnerDepartureReport
                         {
                             SlotId = c.SlotId,
                             OwnerSegment = c.OwnerSegment,
                             GuestNameMain = b.GuestNameMain,
                             NoOfAdult = b.NoOfAdult.ToString(),
                             NoOfChild = b.NoOfChild.ToString(),
                             BookingToken = b.BookingToken,
                             CheckInDate = b.CheckInDate.ToString(),
                             CheckOutDate = b.CheckOutDate.ToString(),
                             NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                         }).ToList();
            var List = List2.Where(b => Convert.ToDateTime(b.CheckOutDate).Date > Convert.ToDateTime(date).Date && Convert.ToDateTime(b.CheckInDate).Date <= Convert.ToDateTime(date).Date).ToList();
            //Create the List<ViewModel>
            if (List.Count() == 0)
            {
                TempData["toastrData"] = "No Data Available";

                return RedirectToAction("OwnerInHouseReport");
            }
            List<OwnerDepartureReport> OwnerBankAdviseReportList = new List<OwnerDepartureReport>();
            foreach (var ownerBankAdviseReport in List)
            {
                table.AddCell(ownerBankAdviseReport.BookingToken);
                table.AddCell(ownerBankAdviseReport.GuestNameMain);
                table.AddCell(ownerBankAdviseReport.SlotId);
                table.AddCell(ownerBankAdviseReport.CheckInDate);
                table.AddCell(ownerBankAdviseReport.CheckOutDate);
                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfDaySpent));

                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfAdult));
                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfChild));
                table.AddCell(ownerBankAdviseReport.OwnerSegment);


                //table.AddCell(ownerBankAdviseReport.Amount);


                //table.Addcell();
            }

            pdfDoc.Add(table);

            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Owner In House Report.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();

            return View();
        }
        [HttpGet]
        public ActionResult OwnerOccupancyStatusReportPDF(string checkindate, string checkoutdate)
        {
            //    using (ApplicationDbContext db = new ApplicationDbContext())
            //    {
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            //Top Heading
            PdfPCell cell = new PdfPCell();
            Image image = Image.GetInstance(Server.MapPath("~/Content/images/Combined-Logo2.png"));
            //image.PaddingTop = 0;
            image.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            image.ScaleAbsolute(50, 10);
            cell.AddElement(image);
            Chunk chunk = new Chunk("Best Western Heritage\n", FontFactory.GetFont("Arial", 15, Font.BOLDITALIC, BaseColor.BLACK));

            pdfDoc.Add(chunk);


            //Table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 20f;

            cell.Border = 0;

            table.AddCell(cell);

            chunk = new Chunk("", FontFactory.GetFont("Arial", 15, Font.NORMAL, BaseColor.BLACK));
            cell = new PdfPCell();
            cell.Border = 0;
            cell.AddElement(chunk);


            table.AddCell(cell);

            //Add table to document
            pdfDoc.Add(table);


            //Table
            table = new PdfPTable(5);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 20f;

            //Cell
            cell = new PdfPCell();
            chunk = new Chunk("Owner Occupancy Status", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.BLACK));
            cell.AddElement(chunk);
            cell.Colspan = 5;
            cell.BackgroundColor = BaseColor.WHITE;
            table.AddCell(cell);

            table.AddCell("ID");

            table.AddCell("Description");
            table.AddCell("Mobile No");
            table.AddCell("Email");
            table.AddCell("Total");

            //table.AddCell("CheckInDate");
            //table.AddCell("CheckOutDate");
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                         select new OwnerDepartureReport
                         {
                             SlotId = c.SlotId,
                             GuestNameMain = b.GuestNameMain,
                             GuestMobile = b.GuestMobile,
                             Email = b.Email,
                             HasStayedOrNot = b.HasStayedOrNot,
                             NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                             CheckInDate = b.CheckInDate.ToString(),
                             CheckOutDate = b.CheckOutDate.ToString(),
                             //AccountNo = c.AccountNo
                         }).ToList();
            var List = List2.Where(b => Convert.ToDateTime(b.CheckInDate) <= Convert.ToDateTime(checkoutdate) && Convert.ToDateTime(b.CheckOutDate) >= Convert.ToDateTime(checkindate) && b.HasStayedOrNot == "Yes").ToList();
            //var a = db.OwnerRoomBookings.ToList();
            var sum = List.Sum(b => b.NoOfDaySpent);
            if (List.Count() == 0)
            {
                TempData["toastrData"] = "No Data Available";

                return RedirectToAction("OwnerOccupancyStatusReport");
            }
            List<OwnerDepartureReport> OwnerBankAdviseReportList = new List<OwnerDepartureReport>();
            foreach (var ownerBankAdviseReport in List)
            {
                table.AddCell(ownerBankAdviseReport.SlotId);
                table.AddCell(ownerBankAdviseReport.GuestNameMain);
                table.AddCell(ownerBankAdviseReport.GuestMobile);
                table.AddCell(ownerBankAdviseReport.Email);
                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfDaySpent));
                //table.AddCell(ownerBankAdviseReport.Email);
                //table.AddCell(ownerBankAdviseReport.CheckInDate.ToLongDateString());
                //table.AddCell(ownerBankAdviseReport.CheckOutDate.ToLongDateString());


                //table.Addcell();
            }
            //Adding Sum
            table.AddCell("");
            table.AddCell("");
            table.AddCell("");
            table.AddCell("Total: ");
            table.AddCell(Convert.ToString(sum));


            pdfDoc.Add(table);

            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Owner Occupancy Status Report.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();

            return View();
        }
        //}
        [HttpGet]
        public ActionResult OwnerCancellationReportPDF(string checkindate, string checkoutdate)
        {
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 25, 25, 25, 15);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();

            //Top Heading
            PdfPCell cell = new PdfPCell();
            Image image = Image.GetInstance(Server.MapPath("~/Content/images/Combined-Logo2.png"));
            //image.PaddingTop = 0;
            image.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
            image.ScaleAbsolute(50, 10);
            cell.AddElement(image);
            Chunk chunk = new Chunk("Best Western Heritage\n", FontFactory.GetFont("Arial", 15, Font.BOLDITALIC, BaseColor.BLACK));

            pdfDoc.Add(chunk);


            //Table
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 20f;

            cell.Border = 0;

            table.AddCell(cell);

            chunk = new Chunk("", FontFactory.GetFont("Arial", 15, Font.NORMAL, BaseColor.BLACK));
            cell = new PdfPCell();
            cell.Border = 0;
            cell.AddElement(chunk);


            table.AddCell(cell);

            //Add table to document
            pdfDoc.Add(table);


            //Table
            table = new PdfPTable(5);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            table.SpacingAfter = 20f;

            //Cell
            cell = new PdfPCell();
            chunk = new Chunk("Owner's Booking Cancellation", FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.BLACK));
            cell.AddElement(chunk);
            cell.Colspan = 5;
            cell.BackgroundColor = BaseColor.WHITE;
            table.AddCell(cell);

            table.AddCell("ID");

            table.AddCell("Description");
            table.AddCell("Mobile No");
            table.AddCell("Email");
            table.AddCell("Total Days");

            //table.AddCell("CheckInDate");
            //table.AddCell("CheckOutDate");
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                         select new OwnerDepartureReport
                         {
                             SlotId = c.SlotId,
                             GuestNameMain = b.GuestNameMain,
                             GuestMobile = b.GuestMobile,
                             Email = b.Email,

                             CheckInDate = b.CheckInDate.ToString(),
                             CheckOutDate = b.CheckOutDate.ToString(),
                             IsBookingCompleted = b.IsBookingCompleted,
                             NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                         }).ToList();
            var cancelList = List2.Where(b => Convert.ToDateTime(b.CheckInDate) <= Convert.ToDateTime(checkoutdate) && Convert.ToDateTime(b.CheckInDate) >= Convert.ToDateTime(checkindate) && b.IsBookingCompleted == "Cancelled").ToList();
            var sum = cancelList.Sum(b => b.NoOfDaySpent);
            List<OwnerDepartureReport> OwnerBankAdviseReportList = new List<OwnerDepartureReport>();
            if (cancelList.Count() == 0)
            {
                TempData["toastrData"] = "No Data Available";
                //ViewBag.toastrData = 
                return RedirectToAction("OwnerBookingCancellationReport");
            }
            foreach (var ownerBankAdviseReport in cancelList)
            {
                table.AddCell(ownerBankAdviseReport.SlotId);
                table.AddCell(ownerBankAdviseReport.GuestNameMain);
                table.AddCell(ownerBankAdviseReport.GuestMobile);
                table.AddCell(ownerBankAdviseReport.Email);
                table.AddCell(Convert.ToString(ownerBankAdviseReport.NoOfDaySpent));


            }
            table.AddCell("");
            table.AddCell("");
            table.AddCell("");
            table.AddCell("Total: ");
            table.AddCell(Convert.ToString(sum));

            pdfDoc.Add(table);

            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Owner's Booking Cancellation Report.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Write(pdfDoc);
            Response.End();

            return View();
        }
        public IList<OwnerBookingHistoryReport> GetBookingList(string slotId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var bookingList2 = (from c in db.ConfAllotments
                                join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                                select new OwnerBookingHistoryReport
                                {
                                    SlotId = c.SlotId,
                                    GuestNameMain = b.GuestNameMain,
                                    GuestMobile = b.GuestMobile,
                                    Email = b.Email,
                                    GuestAddress = b.GuestAddress,
                                    HasStayedOrNot = b.HasStayedOrNot,
                                    BookingToken = b.BookingToken,
                                    NoOfRoomAllotted = b.NoOfRoomAllotted.ToString(),
                                    CheckInDate = b.CheckInDate.ToString(),
                                    CheckOutDate = b.CheckOutDate.ToString(),
                                    NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                                }).ToList();
            var bookingList = bookingList2.Where(c => c.SlotId == slotId && c.HasStayedOrNot == "Yes").ToList();
            if (bookingList.Count() == 0)
            {
                return null;
            }

            var sum = bookingList2.Where(c => c.SlotId == slotId && c.HasStayedOrNot == "Yes").Sum(b => b.NoOfDaySpent);
            //var sum2 = bookingList2.Where(c => c.SlotId == slotId).Sum(b => b.NoOfRoomAllotted);
            /////

            var rowTotal = new OwnerBookingHistoryReport()
            {
                SlotId = "",
                GuestNameMain = "",
                GuestMobile = "",
                Email = "",
                //rowTotal.YearlyAllotment = "",
                BookingToken = "",
                NoOfRoomAllotted = "",
                CheckInDate = "",
                CheckOutDate = "Total",
                NoOfDaySpent = sum
            };
            bookingList.Add(rowTotal);
            return bookingList;


        }

        [HttpGet]
        public ActionResult ExportToExcel(string slotId)
        {
            var gv = new GridView();
            var bookingList = this.GetBookingList(slotId);
            if (bookingList == null)
            {
                TempData["toastrDataExcel"] = "No Data Available";
                return RedirectToAction("OwnerBookingHistoryReport");
            }
            gv.DataSource = this.GetBookingList(slotId);
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=BookingHistoryExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);


            var gvHeader = new GridView();

            gvHeader.DataSource = this.GetBookingList(slotId);
            gvHeader.DataBind();
            StringWriter objStringWriterHeader = new StringWriter();
            HtmlTextWriter objHtmlTextWriterHeader = new HtmlTextWriter(objStringWriterHeader);

            gvHeader.RenderControl(objHtmlTextWriterHeader);

            Response.Output.Write(objStringWriterHeader.ToString());

            Response.Flush();
            Response.End();
            return View();

        }
        public IList<OwnerBankAdviseReport> GetBankAdviseList()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerPayments on c.SlotId equals b.SlotId

                         select new OwnerBankAdviseReport
                         {
                             SlotId = c.SlotId,
                             ReceiverName = b.ReceiverName,
                             ReceivingBank = b.ReceivingBank,
                             Amount = b.Amount,
                             AccountNo = c.AccountNo
                         }).ToList();
            //var List = List2.Where(c => c.SlotId == slotId).ToList();
            if (List2.Count() == 0)
            {
                return null;
            }
            return List2;
        }

        [HttpGet]
        public ActionResult ExportToExcelBankAdvise()
        {
            var gv = new GridView();
            var List2 = this.GetBankAdviseList();
            if (List2 == null)
            {
                TempData["toastrDataExcel"] = "No Data Available";
                return RedirectToAction("OwnerBankAdviseReport");
            }

            gv.DataSource = this.GetBankAdviseList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=BankAdviseExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("", this.GetBankAdviseList());
            //return RedirectToAction("ReportExcelView","OwnersInfo", slotId);
        }
        public IList<OwnerDepartureReport> GetCancellationList(string checkindate, string checkoutdate)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                         select new OwnerDepartureReport
                         {
                             SlotId = c.SlotId,
                             GuestNameMain = b.GuestNameMain,
                             GuestMobile = b.GuestMobile,
                             Email = b.Email,
                             NoOfAdult = b.NoOfAdult.ToString(),
                             NoOfChild = b.NoOfChild.ToString(),
                             NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                             BookingToken = b.BookingToken,
                             OwnerSegment = c.OwnerSegment,
                             HasStayedOrNot = b.HasStayedOrNot,
                             CheckInDate = b.CheckInDate.ToString(),
                             CheckOutDate = b.CheckOutDate.ToString(),
                             IsBookingCompleted = b.IsBookingCompleted
                         }).ToList();
            var List = List2.Where(b => Convert.ToDateTime(b.CheckInDate) <= Convert.ToDateTime(checkoutdate) && Convert.ToDateTime(b.CheckInDate) >= Convert.ToDateTime(checkindate) && b.IsBookingCompleted == "Cancelled").ToList();
            if (List.Count() == 0)
            {
                return null;
            }

            return List;


        }
        [HttpGet]
        public ActionResult ExportToExcelCancellation(string checkindate, string checkoutdate)
        {
            var gv = new GridView();
            var List = this.GetCancellationList(checkindate, checkoutdate);
            if (List == null)
            {
                TempData["toastrDataExcel"] = "No Data Available";
                return RedirectToAction("OwnerBookingCancellationReport");
            }


            gv.DataSource = this.GetCancellationList(checkindate, checkoutdate);
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=CancellationExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("", this.GetCancellationList(checkindate, checkoutdate));
            //return RedirectToAction("ReportExcelView","OwnersInfo", slotId);
        }
        public IList<OwnerDepartureReport> GetDepartureList(string checkoutdate)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                         select new OwnerDepartureReport
                         {
                             SlotId = c.SlotId,
                             OwnerSegment = c.OwnerSegment,
                             GuestNameMain = b.GuestNameMain,
                             NoOfAdult = b.NoOfAdult.ToString(),
                             NoOfChild = b.NoOfChild.ToString(),
                             GuestMobile = b.GuestMobile,
                             NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                             Email = b.Email,
                             HasStayedOrNot = b.HasStayedOrNot,
                             IsBookingCompleted = b.IsBookingCompleted,
                             BookingToken = b.BookingToken,
                             CheckInDate = b.CheckInDate.ToString(),
                             CheckOutDate = b.CheckOutDate.ToString(),
                             //AccountNo = c.AccountNo
                         }).ToList();
            ////string CDate=db.OwnerRoomBookings.CheckOutDate.ToString().ToList();
            var List = List2.Where(b => Convert.ToDateTime(b.CheckOutDate).Date == Convert.ToDateTime(checkoutdate).Date && b.HasStayedOrNot == "Yes").ToList();
            if (List.Count() == 0)
            {
                return null;
            }

            return List;

        }

        [HttpGet]
        public ActionResult ExportToExcelDeparture(string checkoutdate)
        {
            var gv = new GridView();
            var List = this.GetDepartureList(checkoutdate);
            if (List == null)
            {
                TempData["toastrDataExcel"] = "No Data Available";
                return RedirectToAction("OwnerDepartureReport");
            }

            gv.DataSource = this.GetDepartureList(checkoutdate);
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DepartureExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("", this.GetDepartureList(checkoutdate));
            //return RedirectToAction("ReportExcelView","OwnersInfo", slotId);
        }

        public IList<OwnerDepartureReport> GetInHouseList(string date)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                         select new OwnerDepartureReport
                         {
                             SlotId = c.SlotId,
                             OwnerSegment = c.OwnerSegment,
                             GuestNameMain = b.GuestNameMain,
                             NoOfAdult = b.NoOfAdult.ToString(),
                             NoOfChild = b.NoOfChild.ToString(),
                             GuestMobile = b.GuestMobile,
                             NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                             Email = b.Email,
                             IsBookingCompleted = b.IsBookingCompleted,
                             HasStayedOrNot = b.HasStayedOrNot,

                             CheckInDate = b.CheckInDate.ToString(),
                             CheckOutDate = b.CheckOutDate.ToString(),
                             //AccountNo = c.AccountNo
                         }).ToList();
            var List = List2.Where(b => Convert.ToDateTime(b.CheckOutDate).Date > Convert.ToDateTime(date).Date && Convert.ToDateTime(b.CheckInDate).Date <= Convert.ToDateTime(date).Date && b.HasStayedOrNot == "Yes").ToList();

            if (List.Count() == 0)
            {
                return null;
            }

            return List;

        }


        [HttpGet]
        public ActionResult ExportToExcelInHouse(string date)
        {
            var gv = new GridView();
            var bookingList = this.GetInHouseList(date);
            if (bookingList == null)
            {
                TempData["toastrDataExcel"] = "No Data Available";
                return RedirectToAction("OwnerInHouseReport");
            }


            gv.DataSource = this.GetInHouseList(date);
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=OwnerInHouseExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("", this.GetInHouseList(date));
            //return RedirectToAction("ReportExcelView","OwnersInfo", slotId);
        }
        /// 
        public IList<OwnerDepartureReport> GetOccupancyList(string checkindate, string checkoutdate)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var List2 = (from c in db.OwnersInfos
                         join b in db.OwnerRoomBookings on c.SlotId equals b.SlotId

                         select new OwnerDepartureReport
                         {
                             SlotId = c.SlotId,
                             GuestNameMain = b.GuestNameMain,
                             GuestMobile = b.GuestMobile,
                             OwnerSegment = c.OwnerSegment,
                             Email = b.Email,
                             HasStayedOrNot = b.HasStayedOrNot,
                             BookingToken = b.BookingToken,
                             IsBookingCompleted = b.IsBookingCompleted,
                             NoOfAdult = b.NoOfAdult.ToString(),
                             NoOfChild = b.NoOfChild.ToString(),
                             CheckInDate = b.CheckInDate.ToString(),
                             CheckOutDate = b.CheckOutDate.ToString(),
                             NoOfDaySpent = (int)DbFunctions.DiffDays(b.CheckInDate, b.CheckOutDate),
                             //AccountNo = c.AccountNo
                         }).ToList();
            List<OwnerDepartureReport> occupancyList = List2.Where(b => Convert.ToDateTime(b.CheckInDate) <= Convert.ToDateTime(checkoutdate) && Convert.ToDateTime(b.CheckOutDate) >= Convert.ToDateTime(checkindate) && b.HasStayedOrNot == "Yes").ToList();
            if (occupancyList.Count() == 0)
            {
                return null;
            }
            var sum = occupancyList.Sum(b => b.NoOfDaySpent);
            var rowTotal = new OwnerDepartureReport()
            {
                SlotId = "",
                GuestNameMain = "",
                GuestMobile = "",
                Email = "",
                NoOfAdult = "",
                NoOfChild = "",
                CheckInDate = "",
                CheckOutDate = "",
                HasStayedOrNot = "Total",
                NoOfDaySpent = sum,
            };
            occupancyList.Add(rowTotal);
            return occupancyList;

        }
        [HttpGet]
        public ActionResult ExportToExcelOccupancy(string checkindate, string checkoutdate)
        {
            var gv = new GridView();
            var occupancyList = this.GetOccupancyList(checkindate, checkoutdate);
            if (occupancyList == null)
            {
                TempData["toastrDataExcel"] = "No Data Available";
                return RedirectToAction("OwnerOccupancyStatusReport");
            }
            gv.DataSource = occupancyList;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=OwnerOccupancyExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("", this.GetOccupancyList(checkindate, checkoutdate));
            //return RedirectToAction("ReportExcelView","OwnersInfo", slotId);
        }
        public ActionResult OwnerBookingHistoryReport()
        {
            return View("OwnerBookingHistoryReportview");
        }
        [HttpGet]
        public ActionResult OwnerBookingHistoryReportPreview(string slotId)
        {
            var model = new OwnerBookingHistoryReportListView()
            {
                OwnerBookingHistoryReportList = this.GetBookingList(slotId),
                SlotId = slotId
            };
            return View(model);
        }
        public ActionResult OwnerBankAdviseReport()
        {
            return View("OwnerBankAdviseReportview");
        }
        [HttpGet]
        public ActionResult OwnerBankAdviseReportPreview()
        {
            var model = new OwnerBankAdviseReportListView()
            {
                OwnerBankAdviseReportList = this.GetBankAdviseList()
            };
            return View(model);
        }
        public ActionResult OwnerDepartureReport()
        {
            return View("OwnerDepartureReportview");
        }
        [HttpGet]
        public ActionResult OwnerDepartureReportPreview(string checkoutdate)
        {
            var model = new OwnerDepartureReportListView()
            {
                OwnerDepartureReportList = this.GetDepartureList(checkoutdate),
                CheckOutDate = checkoutdate.ToString()
            };
            return View(model);
        }
        public ActionResult OwnerInHouseReport()
        {
            return View("OwnerInHouseReportview");
        }
        [HttpGet]
        public ActionResult OwnerInHouseReportPreview(string date)
        {
            var model = new OwnerDepartureReportListView()
            {
                OwnerDepartureReportList = this.GetInHouseList(date)
            };
            ViewBag.InH = date;
            return View(model);
        }
        public ActionResult OwnerOccupancyStatusReport()
        {
            return View("OwnerOccupancyStatusReportview");
        }
        [HttpGet]
        public ActionResult OwnerOccupancyReportPreview(string checkindate, string checkoutdate)
        {
            var model = new OwnerDepartureReportListView()
            {
                OwnerDepartureReportList = this.GetOccupancyList(checkindate, checkoutdate),
                CheckOutDate = checkoutdate.ToString(),
                CheckInDate = checkindate.ToString()
            };
            return View(model);
        }
        public ActionResult OwnerBookingCancellationReport()
        {
            return View("OwnerBookingCancellationReportview");
        }
        [HttpGet]
        public ActionResult OwnerCancellationReportPreview(string checkindate, string checkoutdate)
        {
            var model = new OwnerDepartureReportListView()
            {
                OwnerDepartureReportList = this.GetCancellationList(checkindate, checkoutdate),
                CheckOutDate = checkoutdate.ToString(),
                CheckInDate = checkindate.ToString()
            };
            return View(model);
        }
        public ActionResult ExportToPDF_test(string slotId)
        {
            var gv = new GridView();
            gv.DataSource = this.GetBookingList(slotId);
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.pdf");
            Response.ContentType = "application/pdf";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("", this.GetBookingList(slotId));
            //return RedirectToAction("ReportExcelView","OwnersInfo", slotId);
        }
        public ActionResult Report()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            return View("OwnerRoomBookingReports");
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}