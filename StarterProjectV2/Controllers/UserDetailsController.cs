﻿using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using System.Text;
using System.IO;
using StarterProjectV2.Services;

namespace StarterProjectV2.Controllers
{
    public class UserDetailsController : Controller
    {
        private readonly CommonService _commonService = new CommonService();
        private static readonly Random RNG = new Random();

        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpPost]
        public ActionResult UserDetailsCreate(UserDetailsView model)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var userList = adbc.Users.Select(u => u.UserName).ToList();
                    var RoleObject = adbc.UserRoles.Select(m => m.RoleName).ToList();
                if (userList.Contains(model.UserName) == true || model.Password != model.ConfirmPassword)
                {
                    return RedirectToAction("UserDetailsCreate");
                }
                else
                {
                    if (model.UserProfilePicture == null || model.UserProfilePicture.ContentLength == 0)
                    {
                        return Content("INVALID FILE");
                    }

                    if (model.UserProfilePicture.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(model.UserName + ".jpg");
                        var path = Path.Combine(Server.MapPath("~/Content/images/avatars/"), fileName);
                        model.UserProfilePicture.SaveAs(path);
                    }

                    string UserType = "";
                    if ((model.UserType) == "Employee")
                    {
                        while (true)
                        {
                            var builder = new StringBuilder();
                            while (builder.Length != 16)
                            {
                                builder.Append(RNG.Next(10).ToString());
                            }

                            string token = "EMP-" + builder.ToString();
                            var EmployeeCodeList = adbc.Users.Select(u => u.UserType).ToList();
                            if (EmployeeCodeList.Contains(token) == false)
                            {
                                UserType = token;
                                break;
                            }
                        }
                        //Probably need this to fix the problem with Random number if occurs like in ownersinfocontroller
                        //                        string UserType = "";
                        //                        bool checkPoint = false;
                        //                        while (checkPoint == false)
                        //                        {
                        //                            Random r = new Random();
                        //                            var x = r.Next(0, 1000000);
                        //                            token = x.ToString("000000");
                        //
                        //                            var OwnerOTPSMSCodeList = adbc.OwnerOtpCodes.Select(u => u.OwnerOTPSMSCode).ToList();
                        //                            if (OwnerOTPSMSCodeList.Contains(token) == false)
                        //                            {
                        //                                ownerOTPSMSCode = token;
                        //                                checkPoint = true;
                        //                            }
                        //                        }
                    }
                    else
                    {
                        while (true)
                        {
                            var builder = new StringBuilder();
                            while (builder.Length != 16)
                            {
                                builder.Append(RNG.Next(10).ToString());
                            }

                            string token = "GST-" + builder.ToString();
                            var EmployeeCodeList = adbc.Users.Select(u => u.UserType).ToList();
                            if (EmployeeCodeList.Contains(token) == false)
                            {
                                UserType = token;
                                break;
                            }
                        }
                    }
                    User mdl = new User()
                    {
                        UserName = model.UserName,
                        UserDisplayName = model.UserDisplayName,
                        UserType = UserType,
                        UserMobileNumber = model.UserMobileNumber,
                        UserEmail = model.UserEmail,
                        UserPicturePath = "~/Content/images/avatars/" + model.UserName + ".jpg",
                        Password = EncryptDecryptString.Encrypt(model.Password, "1234"),
                        UserLastLoginTime = Convert.ToDateTime("1900-01-01 00:00:00.000")
                    };

                    adbc.Users.Add(mdl);
                }

                adbc.SaveChanges();
                //Session[SessionKeys.ROLE_LIST] = (List<UserDetails>)Common.getTasksWithRoleForUser(Session[SessionKeys.USERNAME].ToString());
                return RedirectToAction("UserDetailsManagement");


                //                var TaskObject = adbc.Tasks.ToList();
                //                //                        var ModuleObject = db.Modules.Select(m=>m.ModuleName).ToList();
                //                var ModuleObject = adbc.Modules.ToList();
                //                var query = (from a in TaskObject
                //                    join b in ModuleObject
                //                        on a.moduleId equals b.ModuleId into f
                //                    from b in f.DefaultIfEmpty()
                //                    group new {a} by b.ModuleId
                //                    into g
                //                    select new
                //                    {
                //                        Module = adbc.Modules.Where(m => m.ModuleId == g.Key)
                //                            .FirstOrDefault(),
                //                        TaskList = g.ToList()
                //                    }).ToList();
                //
                //                List<Tuple<Module, List<Task>>> list =
                //                    new List<Tuple<Module, List<Task>>>();
                //
                //
                //                foreach (var q in query)
                //                {
                //                    List<Task> tempList = new List<Task>();
                //                    for (int i = 0; i < q.TaskList.Count; i++)
                //                    {
                //                        tempList.Add(q.TaskList[i].a);
                //                    }
                //
                //                    Tuple<Module, List<Task>> obj = Tuple.Create(q.Module, tempList);
                //                    list.Add(obj);
                //                }
                //
                //                var allModuleTaskList = list;
                //
                ////                adbc.UserDetailses.Where(u=>u.UserName==UserName)
                //
                //                var model = new UserDetailsView()
                //                {
                //                    GetAllModuleListWithTasks = allModuleTaskList,
                //
                //                };
                //                return View(model);
            }
        }


        [HttpPost]
        public JsonResult gettingUserDetailsModel(UserDetailsView model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                //HttpPost
                var userList = adbc.Users.Select(u => u.UserName).ToList();

                if (userList.Contains(model.UserName) == true || model.Password != model.ConfirmPassword)
                {
                    //return RedirectToAction("UserDetailsCreate");
                    return Json("Already Existing User");
                }
                else
                {
                    //if (model.UserProfilePicture == null || model.UserProfilePicture.ContentLength == 0)
                    //{
                    //    return Json("INVALID FILE");
                    //}

                    //if (model.UserProfilePicture.ContentLength > 0)
                    //{
                    var fileName = Path.GetFileName(model.UserName + ".jpg");
                    var path = Path.Combine(Server.MapPath("~/Content/images/avatars/"), fileName);
                    //model.UserProfilePicture.SaveAs(path);
                    //}

                    string UserType = "";
                    if ((model.UserType) == "Employee")
                    {
                        while (true)
                        {
                            var builder = new StringBuilder();
                            while (builder.Length != 16)
                            {
                                builder.Append(RNG.Next(10).ToString());
                            }

                            string token = "EMP-" + builder.ToString();
                            var EmployeeCodeList = adbc.Users.Select(u => u.UserType).ToList();
                            if (EmployeeCodeList.Contains(token) == false)
                            {
                                UserType = token;
                                break;
                            }
                        }

                    }
                    else
                    {
                        while (true)
                        {
                            var builder = new StringBuilder();
                            while (builder.Length != 16)
                            {
                                builder.Append(RNG.Next(10).ToString());
                            }

                            string token = "GST-" + builder.ToString();
                            var EmployeeCodeList = adbc.Users.Select(u => u.UserType).ToList();
                            if (EmployeeCodeList.Contains(token) == false)
                            {
                                UserType = token;
                                break;
                            }
                        }
                    }
                    User mdl = new User()
                    {
                        UserName = model.UserName,
                        UserDisplayName = model.UserDisplayName,
                        UserType = model.UserType,
                        UserMobileNumber = model.UserMobileNumber,
                        UserEmail = model.UserEmail,
                        UserPicturePath = "~/Content/images/avatars/" + model.UserName + ".jpg",
                        Password = EncryptDecryptString.Encrypt(model.Password, "1234"),
                        UserLastLoginTime = Convert.ToDateTime("1900-01-01 00:00:00.000")
                    };

                    adbc.Users.Add(mdl);
                }

                adbc.SaveChanges();




                //JSON
                var UserName = model.UserName;
                var UserPassword = model.Password;
                var UserObject = adbc.Users.Where(m => m.UserName == UserName && m.Password == UserPassword)
                    .FirstOrDefault();
                var UserWithRoles = adbc.UserDetailses.Select(u => u.UserName).ToList();
                //
                if (UserObject != null)
                {
                    return Json("Abcd");
                }

                else
                {
                    if (!String.IsNullOrEmpty(UserName) || !String.IsNullOrEmpty(UserPassword))
                    {
                        var userDetailsList = adbc.UserDetailses.ToList();
                        var userDetailsIdList = new List<int>();
                        foreach (var userDetails in userDetailsList)
                        {
                            userDetailsIdList.Add(int.Parse(userDetails.UserDetailsKey.ToString().Split('-')[1]));
                        }

                        var userDetailsToken = userDetailsList.Count();
                        if (userDetailsToken > 0)
                        {
                            userDetailsIdList.Sort();
                            userDetailsToken = userDetailsIdList[userDetailsIdList.Count - 1];
                        }

                        var ModuleWithTaskList = model.PartialUserDetailsViewModels;
                        if (ModuleWithTaskList == null)
                        {
                            return Json("MMMM");
                        }
                        foreach (var module in ModuleWithTaskList)
                        {
                            var moduleId = module.ModuleId;
                            var ModuleObject = adbc.Modules.Where(m => m.ModuleId == moduleId).FirstOrDefault();
                            for (var i = 0; i < module.TaskIdList.Count; i++)
                            {
                                var taskId = module.TaskIdList.ElementAt(i);
                                var TaskObject = adbc.Tasks.Where(m => m.TaskId == taskId).FirstOrDefault();


                                userDetailsToken++;



                                var Model = new UserDetails()
                                {
                                    UserDetailsKey = "UDS-" + userDetailsToken,
                                    UserName = UserName,
                                    ModuleId = ModuleObject.ModuleId,
                                    ModuleName = ModuleObject.ModuleName,
                                    TaskId = TaskObject.TaskId,
                                    TaskName = TaskObject.TaskName,
                                    Details = module.TaskEventList.ElementAt(i)
                                };
                                adbc.UserDetailses.Add(Model);
                            }
                        }

                        adbc.SaveChanges();
                    }
                }
                var sessionUserName = Session[SessionKeys.USERNAME];
                var roleName = adbc.Users.FirstOrDefault(u => u.UserName == (string)sessionUserName).UserType.ToString();
                Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(Session[SessionKeys.USERNAME].ToString());
                //return Json("Nothing");
                //return RedirectToAction("About", "Home", new { area = "" });
                adbc.SaveChanges();
                return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
            } //Do Stuff
        }

        //[HttpGet]
        //public ActionResult UserDetailsManagement()
        //{
        //    if (!IsUserLoggedIn())
        //    {
        //        return RedirectToAction("Login", "Accounts", new { });
        //    }
        //    using (ApplicationDbContext adbc = new ApplicationDbContext())
        //    {
        //        var UserObject = adbc.Users.ToList();
        //        var UsersWithCreatedRoles = adbc.UserDetailses.Select(u => u.UserName).ToList();
        //        var UserObjectWithRoles = new List<User>();
        //        foreach (var user in UserObject)
        //        {
        //            if (UsersWithCreatedRoles.Contains(user.UserName))
        //            {
        //                UserObjectWithRoles.Add(user);
        //            }
        //        }

        //        string taskId = adbc.Tasks.Where(u => u.TaskPath == "UserDetails\\UserDetailsManagement").Select(u => u.TaskId).FirstOrDefault();
        //        var myRoleList = (List<UserDetails>)Session[SessionKeys.ROLE_LIST];

        //        string taskRoles = "";
        //        foreach (var tasks in myRoleList)
        //        {
        //            if (tasks.TaskId == taskId)
        //            {
        //                taskRoles = tasks.Details;
        //            }
        //        }

        //        //if (taskRoles[0] == '0')
        //        //{
        //        //    return View("Error");
        //        //}

        //        var model = new UserList()
        //        {
        //            Users = UserObject,
        //            //Users = UserObjectWithRoles,
        //            TaskRoles = taskRoles
        //        };
        //        return View(model);
        //    }

        //}

        [HttpGet]
        public ActionResult UserDetailsManagement(string flg)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var UserObject = adbc.Users.ToList();
                var UsersWithCreatedRoles = adbc.UserDetailses.Select(u => u.UserName).ToList();
                var UserObjectWithRoles = new List<User>();
                foreach (var user in UserObject)
                {
                    if (UsersWithCreatedRoles.Contains(user.UserName))
                    {
                        UserObjectWithRoles.Add(user);
                    }
                }

                string taskId = adbc.Tasks.Where(u => u.TaskPath == "UserDetails\\UserDetailsManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }


                var UserAllList = adbc.Users.ToList();
                var UserObjList = UserAllList.OrderBy(p => p.UserName).ToList();

                List<User> selectedUser = new List<User>();
                int noOfPage;
                if (UserObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedUser.Add(UserObjList.ElementAt(i));
                    }
                    int x = (UserObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (UserObjList.Count / 10) + x;
                }
                else
                {
                    selectedUser = UserObjList;
                    noOfPage = 1;
                }
                ///////////////
                var model = new UserList()
                {
                    index = 1,
                    noOfPage = noOfPage,
                    UserDetailsList = selectedUser,
                    //TaskRoles = taskRoles,
                    Users = UserObjectWithRoles,
                    flg = (flg == "false") ? "false" : "true"
                };
                return View(model);
            }

        }
        [HttpGet]
        public ActionResult UserDetailsPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var UserObject = adbc.Users.ToList();
                var UsersWithCreatedRoles = adbc.UserDetailses.Select(u => u.UserName).ToList();
                var UserObjectWithRoles = new List<User>();
                foreach (var user in UserObject)
                {
                    if (UsersWithCreatedRoles.Contains(user.UserName))
                    {
                        UserObjectWithRoles.Add(user);
                    }
                }
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "UserDetails\\UserDetailsManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                //string taskRoles = "";
                //foreach (var tasks in myRoleList)
                //{
                //    if (tasks.TaskId == taskId)
                //    {
                //        taskRoles = tasks.Details;
                //    }
                //}

                //if (taskRoles[0] == '0')
                //{
                //    return View("Error");
                //}

                var UserAllList = adbc.Users.ToList();
                var UserObjList = UserAllList.OrderBy(p => p.UserName).ToList();

                List<User> selectedUser = new List<User>();
                int noOfPage;
                if (UserObjList.Count >= 10)
                {
                    var pageNo = (UserObjList.Count < (index * 10)) ? UserObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedUser.Add(UserObjList.ElementAt(i));
                    }
                    int x = (UserObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (UserObjList.Count / 10) + x;
                }
                else
                {
                    selectedUser = UserObjList;
                    noOfPage = 1;
                }
                var model = new UserList()
                {
                    //TaskRoles = taskRoles,
                    UserDetailsList = selectedUser,
                    Users = UserObjectWithRoles,
                    noOfPage = noOfPage,
                    index = index
                };
                return View("UserDetailsManagement", model);
            }
        }

        [HttpGet]
        public ActionResult UserDetailsCreate()
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "UserDetails\\UserDetailsManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }
                var TaskObject = adbc.Tasks.ToList();

                //var RoleObject = adbc.UserRoles.Select(m => m.RoleName).ToList();
                var RoleObject = adbc.UserRoles.Select(m => m.RoleName).Distinct().ToList();
                //                        var ModuleObject = db.Modules.Select(m=>m.ModuleName).ToList();
                var ModuleObject = adbc.Modules.ToList();
                var query = (from a in TaskObject
                             join b in ModuleObject
                                 on a.moduleId equals b.ModuleId into f
                             from b in f.DefaultIfEmpty()
                             group new { a } by b.ModuleId
                    into g
                             select new
                             {
                                 Module = adbc.Modules.Where(m => m.ModuleId == g.Key)
                                     .FirstOrDefault(),
                                 TaskList = g.ToList()
                             }).ToList();

                List<Tuple<Module, List<Task>>> list =
                    new List<Tuple<Module, List<Task>>>();


                foreach (var q in query)
                {
                    List<Task> tempList = new List<Task>();
                    for (int i = 0; i < q.TaskList.Count; i++)
                    {
                        tempList.Add(q.TaskList[i].a);
                    }

                    Tuple<Module, List<Task>> obj = Tuple.Create(q.Module, tempList);
                    list.Add(obj);
                }

                var allModuleTaskList = list;

                var model = new UserDetailsView()
                {
                    GetAllModuleListWithTasks = allModuleTaskList,
                    UserTypeList = RoleObject
                    //UserTypeList = new List<string>() { "Employee", "Guest" }
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult UserDetailsEdit(string UserName)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "UserDetails\\UserDetailsManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }

                var UserDetailsList = adbc.UserDetailses.Where(u => u.UserName == UserName).ToList();
                var TaskObject = adbc.Tasks.ToList();
                //                        var ModuleObject = db.Modules.Select(m=>m.ModuleName).ToList();
                var ModuleObject = adbc.Modules.ToList();
                var query = (from a in TaskObject
                             join b in ModuleObject
                                 on a.moduleId equals b.ModuleId into f
                             from b in f.DefaultIfEmpty()
                             group new { a } by b.ModuleId
                    into g
                             select new
                             {
                                 Module = adbc.Modules.Where(m => m.ModuleId == g.Key)
                                     .FirstOrDefault(),
                                 TaskList = g.ToList()
                             }).ToList();

                List<Tuple<Module, List<Task>>> list =
                    new List<Tuple<Module, List<Task>>>();


                foreach (var q in query)
                {
                    List<Task> tempList = new List<Task>();
                    for (int i = 0; i < q.TaskList.Count; i++)
                    {
                        tempList.Add(q.TaskList[i].a);
                    }

                    Tuple<Module, List<Task>> obj = Tuple.Create(q.Module, tempList);
                    list.Add(obj);
                }

                var allModuleTaskList = list;


                List<Tuple<string, string>> ListofTasksWithRolesFromUserDetails = new List<Tuple<string, string>>();

                var listOfPrivileges = adbc.UserDetailses.Where(u => u.UserName == UserName).ToList();

                foreach (var UserDetail in listOfPrivileges)
                {
                    ListofTasksWithRolesFromUserDetails.Add(Tuple.Create(UserDetail.TaskId, UserDetail.Details));
                }

                var mdl = adbc.Users.Where(u => u.UserName == UserName).FirstOrDefault();

                //string picPath = adbc.Users.Where(u => u.UserName == UserName).Select(u => u.UserPicturePath).FirstOrDefault();

                var model = new UserDetailsView()
                {
                    UserName = UserName,
                    GetAllModuleListWithTasks = allModuleTaskList,
                    TaskWithRoles = ListofTasksWithRolesFromUserDetails,
                    UserPicturePath = mdl.UserPicturePath,
                    UserMobileNumber = mdl.UserMobileNumber,
                    Password = (mdl.Password != "1234") ? EncryptDecryptString.Decrypt(mdl.Password, "1234") : "1234",
                    UserEmail = mdl.UserEmail,
                    UserDisplayName = mdl.UserDisplayName
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult UserDetailsEdit(UserDetailsView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            UserList mdl = new UserList();
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var UserInDb = adbc.Users.Where(u => u.UserName == model.UserName).FirstOrDefault();
                UserInDb.Password = EncryptDecryptString.Encrypt(model.Password, "1234");
                UserInDb.UserDisplayName = model.UserDisplayName;
                UserInDb.UserMobileNumber = model.UserMobileNumber;
                UserInDb.UserEmail = model.UserEmail;


                string taskId = adbc.Tasks.Where(u => u.TaskPath == "UserDetails\\UserDetailsManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                var userName = (string)Session[SessionKeys.USERNAME];
                string taskRoles = "";
                if (model.UserName == userName)
                {
                    foreach (var tasks in myRoleList)
                    {
                        if (tasks.TaskId == taskId && taskRoles != tasks.Details)
                        {
                            
                        }
                    }
                }
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                mdl = new UserList()
                {
                    Users = adbc.Users.ToList(),
                    TaskRoles = taskRoles
                };

                adbc.SaveChanges();
                //if (IsEdited == true)
                //{
                //    return RedirectToAction("Logout", "Accounts");
                //}
                return View("UserDetailsManagement", mdl);
            }

            
        }
        public JsonResult UploadProfilePicture()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;

                string mimeType = file.ContentType;
                System.IO.Stream fileContent = file.InputStream;
                file.SaveAs(Server.MapPath(fileName)); //File will be saved in application root
            }

            return Json("Uploaded " + Request.Files.Count + " files");
        }
        public JsonResult Upload_Edit()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                //                using (ApplicationDbContext db = new ApplicationDbContext())
                //                {
                //                    var ownerIdList = db.OwnersFamilies.Select(o => o.OwnerId).ToList();
                //                    if (ownerIdList.Contains(ownerIdToken[0]))
                //                    {
                //                        return Json("Duplicate");
                //                    }
                //                }
                string mimeType = file.ContentType;
                System.IO.Stream fileContent = file.InputStream;
                //To save file, use SaveAs method
                file.SaveAs(Server.MapPath("~/Content/images/avatars/") + fileName); //File will be saved in application root

            }


            return Json("Uploaded " + Request.Files.Count + " files");
        }

        [HttpGet]
        public ActionResult UserDetailsView(string UserName)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var UserDetailsList = adbc.UserDetailses.Where(u => u.UserName == UserName).ToList();
                var TaskObject = adbc.Tasks.ToList();
                //                        var ModuleObject = db.Modules.Select(m=>m.ModuleName).ToList();
                var ModuleObject = adbc.Modules.ToList();
                var query = (from a in TaskObject
                             join b in ModuleObject
                                 on a.moduleId equals b.ModuleId into f
                             from b in f.DefaultIfEmpty()
                             group new { a } by b.ModuleId
                    into g
                             select new
                             {
                                 Module = adbc.Modules.Where(m => m.ModuleId == g.Key)
                                     .FirstOrDefault(),
                                 TaskList = g.ToList()
                             }).ToList();

                List<Tuple<Module, List<Task>>> list =
                    new List<Tuple<Module, List<Task>>>();


                foreach (var q in query)
                {
                    List<Task> tempList = new List<Task>();
                    for (int i = 0; i < q.TaskList.Count; i++)
                    {
                        tempList.Add(q.TaskList[i].a);
                    }

                    Tuple<Module, List<Task>> obj = Tuple.Create(q.Module, tempList);
                    list.Add(obj);
                }

                var allModuleTaskList = list;


                List<Tuple<string, string>> ListofTasksWithRolesFromUserDetails = new List<Tuple<string, string>>();

                var listOfPrivileges = adbc.UserDetailses.Where(u => u.UserName == UserName).ToList();

                foreach (var UserDetail in listOfPrivileges)
                {
                    ListofTasksWithRolesFromUserDetails.Add(Tuple.Create(UserDetail.TaskId, UserDetail.Details));
                }

                var mdl = adbc.Users.Where(u => u.UserName == UserName).FirstOrDefault();

                var model = new UserDetailsView()
                {
                    UserName = UserName,
                    GetAllModuleListWithTasks = allModuleTaskList,
                    TaskWithRoles = ListofTasksWithRolesFromUserDetails,
                    UserPicturePath = mdl.UserPicturePath,
                    UserMobileNumber = mdl.UserMobileNumber,
                    UserType = mdl.UserType,
                    //Password = (mdl.Password != "1234") ? EncryptDecryptString.Decrypt(mdl.Password, "1234") : "1234",
                    UserEmail = mdl.UserEmail,
                    UserDisplayName = mdl.UserDisplayName
                };
                return View(model);
            }
        }


        [HttpPost]
        public JsonResult gettingUserDetailsModelForEdit(UserDetailsView model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var UserName = model.UserName;
                var tskIdList = adbc.UserDetailses.Where(u => u.UserName == UserName).Select(u => u.TaskId).ToList();

                if (model.PartialUserDetailsViewModels != null)
                {
                    List<string> taskIdListFromModel = new List<string>();
                    foreach (var tsk in model.PartialUserDetailsViewModels)
                    {
                        //t.ModuleId
                        //if (tsk.TaskIdList.Contains(t) == false)
                        //{
                        //    var UserDetails = adbc.UserDetailses.Where(u => u.TaskId == t & u.UserName == UserName)
                        //        .FirstOrDefault();
                        //    adbc.UserDetailses.Remove(UserDetails);
                        //    adbc.SaveChanges();
                        //}
                        foreach (var tskId in tsk.TaskIdList)
                        {
                            taskIdListFromModel.Add(tskId);
                        }

                    }

                    foreach (var t in tskIdList)
                    {
                        if (taskIdListFromModel.Contains(t) == false)
                        {
                            var UserDetails = adbc.UserDetailses.Where(u => u.TaskId == t & u.UserName == UserName)
                                .FirstOrDefault();
                            adbc.UserDetailses.Remove(UserDetails);
                            adbc.SaveChanges();
                        }
                    }
                }
                if ((model.PartialUserDetailsViewModels) != null)
                {
                    tskIdList = adbc.UserDetailses.Where(u => u.UserName == UserName).Select(u => u.TaskId).ToList();
                    foreach (var t in model.PartialUserDetailsViewModels)
                    {
                        //t.ModuleId
                        foreach (var tskId in t.TaskIdList)
                        {
                            if (tskIdList.Contains(tskId) == false)
                            {
                                var userDetailsList = adbc.UserDetailses.ToList();
                                var userDetailsIdList = new List<int>();
                                foreach (var userDetails in userDetailsList)
                                {
                                    userDetailsIdList.Add(int.Parse(userDetails.UserDetailsKey.ToString().Split('-')[1]));
                                }

                                var userDetailsToken = userDetailsList.Count();
                                if (userDetailsToken > 0)
                                {
                                    userDetailsIdList.Sort();
                                    userDetailsToken = userDetailsIdList[userDetailsIdList.Count - 1];
                                }

                                userDetailsToken++;

                                var ModuleObject = adbc.Modules.Where(m => m.ModuleId == t.ModuleId).FirstOrDefault();
                                var TaskObject = adbc.Tasks.Where(m => m.TaskId == tskId).FirstOrDefault();

                                var Model = new UserDetails()
                                {
                                    UserDetailsKey = "UDS-" + userDetailsToken,
                                    UserName = UserName,
                                    ModuleId = ModuleObject.ModuleId,
                                    ModuleName = ModuleObject.ModuleName,
                                    TaskId = TaskObject.TaskId,
                                    TaskName = TaskObject.TaskName,
                                    Details = t.TaskEventList.ElementAt(t.TaskIdList.IndexOf(tskId))
                                };
                                adbc.UserDetailses.Add(Model);


                                adbc.SaveChanges();

                            }
                            else
                            {
                                var UserDetails = adbc.UserDetailses.Where(u => u.TaskId == tskId & u.UserName == UserName)
                                    .FirstOrDefault();
                                UserDetails.Details = t.TaskEventList.ElementAt(t.TaskIdList.IndexOf(tskId));


                                adbc.SaveChanges();

                            }
                        }
                    }
                    var userName = Session[SessionKeys.USERNAME].ToString();
                    var userType = adbc.Users.FirstOrDefault(u => u.UserName == userName).UserType;
                    Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(userType);

                }

            } //Do Stuff
            return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UserDetailsDelete(string UserName)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "UserDetails\\UserDetailsManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[4] == '0')
                {
                    return View("Error");
                }

                var delObj = adbc.UserDetailses.Where(u => u.UserName == UserName).ToList();
                var delUser = adbc.Users.Where(u => u.UserName == UserName).FirstOrDefault();
                foreach (var userDetail in delObj)
                {
                    adbc.UserDetailses.Remove(userDetail);
                }

                if (delUser != null)
                {
                    adbc.Users.Remove(delUser);
                }


                adbc.SaveChanges();
                return RedirectToAction("UserDetailsManagement");
            }
        }

        [HttpGet]
        public ActionResult UserDetailsReport()
        {
            return Redirect("../Reports/UserDetailsReport.aspx");

        }

        [HttpGet]
        public ActionResult CreatingAFinancialYear()
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "UserDetails\\FinancialYearManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }
                var dateToMeasureFiscalYear = DateTime.Now;
                int month = dateToMeasureFiscalYear.Month;
                int year = dateToMeasureFiscalYear.Year;
                var startingFiscalDate = month > 6 ? new DateTime(year, 7, 1) : new DateTime(year - 1, 7, 1);
                int month2 = dateToMeasureFiscalYear.Month;
                int year2 = dateToMeasureFiscalYear.Year;
                var endingFiscalDate = month2 > 6 ? new DateTime(year2 + 1, 6, 30) : new DateTime(year2, 6, 30);
                FinancialYear mdl = new FinancialYear()
                {
                    FromMonth = startingFiscalDate,
                    ToMonth = endingFiscalDate
                };
                return View(mdl);
            }
        }

        [HttpPost]
        public ActionResult AddingAFinancialYear(FinancialYear mdl)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var financialYearsList = adbc.FinancialYears.ToList();
                var financialYearsIdList = new List<int>();

                foreach (var financialYears in financialYearsList)
                {
                    financialYearsIdList.Add(int.Parse(financialYears.FinId.ToString().Split('-')[1]));
                }


                var financialYearsToken = financialYearsList.Count();
                if (financialYearsToken > 0)
                {
                    financialYearsIdList.Sort();
                    financialYearsToken = financialYearsIdList[financialYearsIdList.Count - 1];
                }
                financialYearsToken++;
                FinancialYear model = new FinancialYear()
                {
                    FinId = "FY-"+financialYearsToken,
                    FromMonth = mdl.FromMonth,
                    ToMonth = mdl.ToMonth,
                    Year = mdl.Year
                };
                adbc.FinancialYears.Add(model);
                adbc.SaveChanges();
                return RedirectToAction("FinancialYearManagement");
            }
        }

        [HttpGet]
        public ActionResult FinancialYearManagement()
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole(TaskPath.UserDetails_FinancialYearManagement);

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var FinancialYearList = adbc.FinancialYears.OrderByDescending(x => x.Year).ToList();
                FinancialYearViewModel mdl = new FinancialYearViewModel()
                {
                    FinancialYearList = FinancialYearList,
                    TaskRoles = taskRoles
                };
                return View(mdl);
            }
        }

        [HttpGet]
        public ActionResult FinancialYearView(string finId)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var mdl = adbc.FinancialYears.FirstOrDefault(f => f.FinId == finId);
                return View(mdl);
            }
        }
    }
}