﻿using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using System.Text;
using System.IO;

namespace StarterProjectV2.Controllers
{
    public class RoleController : Controller
    {
        private static Random RNG = new Random();
        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

   [HttpGet]
        public ActionResult RoleManagement(string flg)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var UserObject = adbc.UserRoles.GroupBy(u => u.RoleName).Select(x => x.FirstOrDefault());
                var UsersWithCreatedRoles = adbc.UserRoles.Select(u => u.RoleName).Distinct().ToList();
                var UserObjectWithRoles = new List<UserRole>();
                foreach (var user in UserObject)
                {
                    if (UsersWithCreatedRoles.Contains(user.RoleName))
                    {
                        UserObjectWithRoles.Add(user);
                    }
                }

                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Role\\RoleManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }



                List<UserRole> selectedUser = new List<UserRole>();
            
                var model = new RoleList()
                {
                    index = 1,
                    //noOfPage = noOfPage,
                    userDetailsList = UserObjectWithRoles,
                    //TaskRoles = taskRoles,
                    //Users = UserObjectWithRoles,
                    flg = (flg == "false") ? "false" : "true"
                };
                return View(model);
            }

        }
        [HttpPost]
        public ActionResult RoleDelete(string RoleName)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Role\\RoleManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[4] == '0')
                {
                    return View("Error");
                }

                var delObj = adbc.UserRoles.Where(u => u.RoleName == RoleName).ToList();
                var delUser = adbc.UserRoles.Where(u => u.RoleName == RoleName).FirstOrDefault();
                foreach (var userDetail in delObj)
                {
                    adbc.UserRoles.Remove(userDetail);
                }

                if (delUser != null)
                {
                    adbc.UserRoles.Remove(delUser);
                }
                adbc.SaveChanges();
                return RedirectToAction("RoleManagement");
            }
        }


        [HttpGet]
        public ActionResult RoleDetailsView(string RoleName)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var UserDetailsList = adbc.UserRoles.Where(u => u.RoleName == RoleName).ToList();
                var TaskObject = adbc.Tasks.ToList();
                //                        var ModuleObject = db.Modules.Select(m=>m.ModuleName).ToList();
                var ModuleObject = adbc.Modules.ToList();
                var query = (from a in TaskObject
                             join b in ModuleObject
                                 on a.moduleId equals b.ModuleId into f
                             from b in f.DefaultIfEmpty()
                             group new { a } by b.ModuleId
                    into g
                             select new
                             {
                                 Module = adbc.Modules.Where(m => m.ModuleId == g.Key)
                                     .FirstOrDefault(),
                                 TaskList = g.ToList()
                             }).ToList();

                List<Tuple<Module, List<Task>>> list =
                    new List<Tuple<Module, List<Task>>>();


                foreach (var q in query)
                {
                    List<Task> tempList = new List<Task>();
                    for (int i = 0; i < q.TaskList.Count; i++)
                    {
                        tempList.Add(q.TaskList[i].a);
                    }

                    Tuple<Module, List<Task>> obj = Tuple.Create(q.Module, tempList);
                    list.Add(obj);
                }

                var allModuleTaskList = list;


                List<Tuple<string, string>> ListofTasksWithRolesFromUserDetails = new List<Tuple<string, string>>();

                var listOfPrivileges = adbc.UserRoles.Where(u => u.RoleName == RoleName).ToList();

                foreach (var UserDetail in listOfPrivileges)
                {
                    ListofTasksWithRolesFromUserDetails.Add(Tuple.Create(UserDetail.TaskId, UserDetail.Details));
                }

                var mdl = adbc.UserRoles.Where(u => u.RoleName == RoleName).FirstOrDefault();

                var model = new RoleDetailsView()
                {
                    RoleName = RoleName,
                    GetAllModuleListWithTasks = allModuleTaskList,
                    TaskWithRoles = ListofTasksWithRolesFromUserDetails
            
                    //Password = (mdl.Password != "1234") ? EncryptDecryptString.Decrypt(mdl.Password, "1234") : "1234",
        
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult RoleCreate()
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Role\\RoleManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles[1] == '0')
                {
                    return View("Error");
                }
                var TaskObject = adbc.Tasks.ToList();
                //                        var ModuleObject = db.Modules.Select(m=>m.ModuleName).ToList();
                var ModuleObject = adbc.Modules.ToList();
                var query = (from a in TaskObject
                             join b in ModuleObject
                                 on a.moduleId equals b.ModuleId into f
                             from b in f.DefaultIfEmpty()
                             group new { a } by b.ModuleId
                    into g
                             select new
                             {
                                 Module = adbc.Modules.Where(m => m.ModuleId == g.Key)
                                     .FirstOrDefault(),
                                 TaskList = g.ToList()
                             }).ToList();

                List<Tuple<Module, List<Task>>> list =
                    new List<Tuple<Module, List<Task>>>();


                foreach (var q in query)
                {
                    List<Task> tempList = new List<Task>();
                    for (int i = 0; i < q.TaskList.Count; i++)
                    {
                        tempList.Add(q.TaskList[i].a);
                    }

                    Tuple<Module, List<Task>> obj = Tuple.Create(q.Module, tempList);
                    list.Add(obj);
                }

                var allModuleTaskList = list;

                var model = new RoleDetailsView()
                {
                    GetAllModuleListWithTasks = allModuleTaskList,
                    //UserTypeList = new List<string>() { "Employee", "Guest" }
                };
                return View(model);
            }
        }
        [HttpPost]
        public JsonResult gettingUserDetailsModel(RoleDetailsView model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                //HttpPost
                var userList = adbc.UserRoles.Select(u => u.RoleName).ToList();

                if (userList.Contains(model.RoleName) == true)
                {
                    //return RedirectToAction("UserDetailsCreate");
                    return Json("Already Existing Role");
                }
                else
                {
                    var RoleName = model.RoleName;

                    var UserObject = adbc.UserRoles.Where(m => m.RoleName == RoleName)
                        .FirstOrDefault();
                    var UserWithRoles = adbc.UserRoles.Select(u => u.RoleName).ToList();
                    //
                    if (UserObject != null)
                    {
                        return Json("Abcd");
                    }

                    else
                    {
                        if (!String.IsNullOrEmpty(RoleName))
                        {
                            var userDetailsList = adbc.UserRoles.ToList();
                            var userDetailsIdList = new List<int>();
                            foreach (var userDetails in userDetailsList)
                            {
                                userDetailsIdList.Add(int.Parse(userDetails.RoleID.ToString().Split('-')[1]));
                            }

                            var userDetailsToken = userDetailsList.Count();
                            if (userDetailsToken > 0)
                            {
                                userDetailsIdList.Sort();
                                userDetailsToken = userDetailsIdList[userDetailsIdList.Count - 1];
                            }

                            var ModuleWithTaskList = model.PartialUserDetailsViewModels;
                            if (ModuleWithTaskList == null)
                            {
                                return Json("MMMM");
                            }
                            foreach (var module in ModuleWithTaskList)
                            {
                                var moduleId = module.ModuleId;
                                var ModuleObject = adbc.Modules.Where(m => m.ModuleId == moduleId).FirstOrDefault();
                                for (var i = 0; i < module.TaskIdList.Count; i++)
                                {
                                    var taskId = module.TaskIdList.ElementAt(i);
                                    var TaskObject = adbc.Tasks.Where(m => m.TaskId == taskId).FirstOrDefault();


                                    userDetailsToken++;



                                    var Model = new UserRole()
                                    {
                                        RoleID = "R-" + userDetailsToken,
                                        RoleName = RoleName,
                                        ModuleId = ModuleObject.ModuleId,
                                        ModuleName = ModuleObject.ModuleName,
                                        TaskId = TaskObject.TaskId,
                                        TaskName = TaskObject.TaskName,
                                        Details = module.TaskEventList.ElementAt(i)
                                    };
                                    adbc.UserRoles.Add(Model);
                                }
                            }

                            adbc.SaveChanges();
                        }
                    }




                 
                }

                adbc.SaveChanges();


                var userName = Session[SessionKeys.USERNAME].ToString();
                var userRole = adbc.Users.FirstOrDefault(u => u.UserName == userName).UserType.ToString();
                Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(userRole);

                adbc.SaveChanges();
                return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
            } //Do Stuff
        }

        [HttpGet]
        public ActionResult RoleEdit(string RoleName)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Role\\RoleManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }

                var UserDetailsList = adbc.UserRoles.Where(u => u.RoleName == RoleName).ToList();
                var TaskObject = adbc.Tasks.ToList();
                //                        var ModuleObject = db.Modules.Select(m=>m.ModuleName).ToList();
                var ModuleObject = adbc.Modules.ToList();
                var query = (from a in TaskObject
                             join b in ModuleObject
                                 on a.moduleId equals b.ModuleId into f
                             from b in f.DefaultIfEmpty()
                             group new { a } by b.ModuleId
                    into g
                             select new
                             {
                                 Module = adbc.Modules.Where(m => m.ModuleId == g.Key)
                                     .FirstOrDefault(),
                                 TaskList = g.ToList()
                             }).ToList();

                List<Tuple<Module, List<Task>>> list =
                    new List<Tuple<Module, List<Task>>>();


                foreach (var q in query)
                {
                    List<Task> tempList = new List<Task>();
                    for (int i = 0; i < q.TaskList.Count; i++)
                    {
                        tempList.Add(q.TaskList[i].a);
                    }

                    Tuple<Module, List<Task>> obj = Tuple.Create(q.Module, tempList);
                    list.Add(obj);
                }

                var allModuleTaskList = list;


                List<Tuple<string, string>> ListofTasksWithRolesFromUserDetails = new List<Tuple<string, string>>();

                var listOfPrivileges = adbc.UserRoles.Where(u => u.RoleName == RoleName).ToList();

                foreach (var UserDetail in listOfPrivileges)
                {
                    ListofTasksWithRolesFromUserDetails.Add(Tuple.Create(UserDetail.TaskId, UserDetail.Details));
                }

                var mdl = adbc.UserRoles.Where(u => u.RoleName == RoleName).FirstOrDefault();

                //string picPath = adbc.Users.Where(u => u.UserName == UserName).Select(u => u.UserPicturePath).FirstOrDefault();

                var model = new RoleDetailsView()
                {
                    RoleName = RoleName,
                    GetAllModuleListWithTasks = allModuleTaskList,
                    TaskWithRoles = ListofTasksWithRolesFromUserDetails,
                 
                };
                return View(model);
            }
        }


        [HttpPost]
        public JsonResult gettingRoleDetailsModelForEdit(RoleDetailsView model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var userList = adbc.UserRoles.Select(u => u.RoleName).ToList();
                var RoleName = model.RoleName;

                var UserObject = adbc.UserRoles.Where(m => m.RoleName == RoleName).ToList();
                var UserWithRoles = adbc.UserRoles.Select(u => u.RoleName).ToList();
                //
                if (UserObject != null)
                {
                    foreach (var userObject in UserObject)
                    {
                        adbc.UserRoles.Remove(userObject);
                    }
                    adbc.SaveChanges();
                    //return Json("Abcd");  //Do not return, continue to add the new entries in the database
                }
                if (!String.IsNullOrEmpty(RoleName))
                {
                    var userDetailsList = adbc.UserRoles.ToList();
                    var userDetailsIdList = new List<int>();
                    foreach (var userDetails in userDetailsList)
                    {
                        userDetailsIdList.Add(int.Parse(userDetails.RoleID.ToString().Split('-')[1]));
                    }

                    var userDetailsToken = userDetailsList.Count();
                    if (userDetailsToken > 0)
                    {
                        userDetailsIdList.Sort();
                        userDetailsToken = userDetailsIdList[userDetailsIdList.Count - 1];
                    }

                    var ModuleWithTaskList = model.PartialUserDetailsViewModels;
                    if (ModuleWithTaskList == null)
                    {
                        return Json("MMMM");
                    }
                    foreach (var module in ModuleWithTaskList)
                    {
                        var moduleId = module.ModuleId;
                        var ModuleObject = adbc.Modules.Where(m => m.ModuleId == moduleId).FirstOrDefault();
                        for (var i = 0; i < module.TaskIdList.Count; i++)
                        {
                            var taskId = module.TaskIdList.ElementAt(i);
                            var TaskObject = adbc.Tasks.Where(m => m.TaskId == taskId).FirstOrDefault();
                            userDetailsToken++;
                            var Model = new UserRole()
                            {
                                RoleID = "R-" + userDetailsToken,
                                RoleName = RoleName,
                                ModuleId = ModuleObject.ModuleId,
                                ModuleName = ModuleObject.ModuleName,
                                TaskId = TaskObject.TaskId,
                                TaskName = TaskObject.TaskName,
                                Details = module.TaskEventList.ElementAt(i)
                            };
                            adbc.UserRoles.Add(Model);
                        }
                    }
                    adbc.SaveChanges();
                }
                var userName = Session[SessionKeys.USERNAME].ToString();
                var userRole = adbc.Users.FirstOrDefault(u => u.UserName == userName).UserType.ToString();
                Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(userRole);
                adbc.SaveChanges();
                return Json(new EmptyResult(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}