﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using OfficeOpenXml;
using RestSharp;
using RestSharp.Authenticators;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using CrystalDecisions.CrystalReports.Engine;
using ClosedXML.Excel;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace StarterProjectV2.Controllers
{
    public class DividendController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        ///unnecessary now///
        public ActionResult Download_PDF(string slotId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            ApplicationDbContext db = new ApplicationDbContext();

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "OwnerBookingHistoryReport.rpt"));

            var selectedListOfBookings = db.OwnerPayments.Where(c => c.SlotId == slotId.ToString()).ToList();
            var selectedColums = selectedListOfBookings.Select(c => new
            {
                SlotId = c.SlotId,
                ReceiverName = c.ReceiverName,
                ReceivingBank = c.ReceivingBank,
                Amount = c.Amount
            }).ToList();

            rd.SetDataSource(selectedColums);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            //rd.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape;
            //rd.PrintOptions.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(5, 5, 5, 5));
            //rd.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA5;
            //rd.Refresh();

            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf", "OwnerPaymets.pdf");
        }
        [HttpGet]
        public ActionResult AddingDividendViaExcel()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var FinancialYearList = adbc.FinancialYears.OrderBy(f => f.FromMonth).ToList();
                FinancialYearList.Reverse();
                DividendViewModel model = new DividendViewModel()
                {
                    FinancialIdandParent = FinancialYearList,
                    totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault(),
                    flg = false,
                    totalRows = 0,
                    updatedRows = 0,
                };
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult AddingDividendsForAllOwners()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var FinancialYearList = adbc.FinancialYears.OrderBy(f => f.FromMonth).ToList();
                FinancialYearList.Reverse();
                DividendViewModel model = new DividendViewModel()
                {
                    FinancialIdandParent = FinancialYearList,
                    totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault(),
                    flg = false,
                    totalRows = 0,
                    updatedRows = 0,
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult DividendDistributionForAllOwners(DividendViewModel mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            var SlotIds = "";

            #region Check Point // Page Reload if validation failed

            if (mdl.fromMonth == null || mdl.toMonth == null)
            {
                DividendViewModel dividendObj = new DividendViewModel();

                using (ApplicationDbContext adbc = new ApplicationDbContext())
                {
                    var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();
                    //var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();

                    SortedDictionary<int, Dividend> PendingDividendList = new SortedDictionary<int, Dividend>();

                    foreach (var dividend in allDividends)
                    {
                        int key = int.Parse(dividend.DividendId.Split('-')[1]);
                        PendingDividendList.Add(key, dividend);
                    }

                    Dictionary<int, Dividend> PendingDividendListReverse = new Dictionary<int, Dividend>();

                    foreach (var dividendKeyValuePair in PendingDividendList.Reverse())
                    {
                        PendingDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                    }

                    SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();
                    var FinancialYearList = adbc.FinancialYears.OrderBy(f => f.FromMonth).ToList();
                    FinancialYearList.Reverse();
                    dividendObj.FinancialIdandParent = FinancialYearList;
                    dividendObj.dividends = PendingDividendListReverse.Values.ToList();
                    //                    model.dividends = allDividends;
                    dividendObj.totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault();
                    dividendObj.flg = true;
                    dividendObj.totalRows = 0;
                    dividendObj.updatedRows = 0;
                }
                return View("AddingDividendsForAllOwners", dividendObj);
            }

            #endregion

            var updatedRows = 0;
            var totalRows = 0;
            //var failedRows = 0;
            DateTime fromMonthConvert = DateTime.Now;
            DateTime toMonthConvert = DateTime.Now;
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                #region ID Generate

                var listDividends = adbc.Dividends.ToList();

                int Count = listDividends.Count;
                int last = Count;
                if (Count > 0)
                {
                    var listDivId = new List<int>();

                    foreach (var div in listDividends)
                    {
                        listDivId.Add(int.Parse(div.DividendId.ToString().Split('-')[1]));
                    }
                    listDivId.Sort();
                    last = listDivId[listDivId.Count - 1];
                }

                #endregion

                var Owners = adbc.OwnersInfos.ToList();
                totalRows = Owners.Count;

                #region Insert Divident

                foreach (OwnersInfo ownersinfo in Owners)
                {
                    last++;
                    var SlotId = ownersinfo.SlotId;
                    List<OwnerTransactionHistory> OwnerTransactionHistoryList = new List<OwnerTransactionHistory>();
                    bool flg = false;

                    if (SlotId != null)
                    {
                        var temp = adbc.OwnerTransationHistories.Where(o => o.SlotID == SlotId).ToList();
                        if (temp.Count != 0)
                        {
                            flg = true;
                            OwnerTransactionHistoryList = temp;
                        }
                    }

                    decimal totalSquareFeetPerPerson = 0;
                    foreach (var transaction in OwnerTransactionHistoryList)
                    {
                        totalSquareFeetPerPerson += Convert.ToDecimal(transaction.SFTperPerson);
                    }

                    totalSquareFeetPerPerson = Math.Round(totalSquareFeetPerPerson, 5);

                    decimal divAmountPerSq = Convert.ToDecimal(Math.Round(mdl.divAmountPerSq, 5));


                    if (flg == true)
                    {
                        updatedRows++;
                        DateTime toMonth = Convert.ToDateTime(mdl.toMonth);
                        DateTime LastDayOfTheToMonth = new DateTime(toMonth.Year, toMonth.Month, DateTime.DaysInMonth(toMonth.Year, toMonth.Month));
                        //                        var SlotId = workSheet.Cells[i, 1].Value.ToString().Trim();
                        var OwnersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.SlotId == SlotId);

                        DateTime FirstDayOfTheFromMonth = Convert.ToDateTime(mdl.fromMonth);
                        int timeperiod = ((LastDayOfTheToMonth.Year - FirstDayOfTheFromMonth.Year) * 12) + LastDayOfTheToMonth.Month - FirstDayOfTheFromMonth.Month + 1;
                        //int timeperiod = (13-FirstDayOfTheFromMonth.Month) + LastDayOfTheToMonth.Month;
                        //Determining how many days the owner has spent in the hotel
                        var ownersNoOfDays = adbc.OwnerRoomBookings.Where(r => r.SlotId == SlotId && (r.HasStayedOrNot == "Yes")).ToList(); //??

                        //var totalCost = 0;
                        var totalSpent = 0;
                        //var allARR = adbc.ConfOwnerAvgRoomRates.ToList();
                        var canStay = adbc.ConfAllotments.Where(r => r.SlotId == SlotId).Select(r => r.YearlyAllotment).FirstOrDefault();

                        if (canStay == 0)
                        {
                            continue;
                        }

                        var totaldayscanStay = canStay / 12 * timeperiod;
                        //measuring how many days he spent in the hotel actually
                        foreach (var booking in ownersNoOfDays)
                        {
                            if (FirstDayOfTheFromMonth <= booking.CheckInDate && LastDayOfTheToMonth >= booking.CheckInDate)
                            {
                                var noOfDays1 = booking.CheckInDate;
                                var noOfDays2 = booking.CheckOutDate;
                                var noOfDays = (noOfDays2 - noOfDays1).TotalDays;

                                totalSpent += booking.NoOfRoomAllotted * Convert.ToInt16(noOfDays);

                                //totalCost += allARR.Where(o => booking.CheckInDate <= o.ApplicableDate && o.ApplicableDate < booking.CheckOutDate).Select(r => r.AverageRoomRate).LastOrDefault() * Convert.ToInt16(noOfDays);
                            }
                        }
                        var profitEligibility = totaldayscanStay - totalSpent;

                        //punishment for not arriving at hotel after completing booking online
                        var ownersNoOfDaysCancelled = adbc.OwnerRoomBookings.Where(r => r.HasStayedOrNot == "No" && r.IsBookingCompleted == "Yes" && r.SlotId == SlotId).ToList();
                        foreach (var booking in ownersNoOfDaysCancelled)
                        {
                            if (FirstDayOfTheFromMonth <= booking.CheckInDate && LastDayOfTheToMonth >= booking.CheckInDate)
                            {
                                totalSpent++;
                                //totalCost += allARR.Where(o => booking.CheckInDate <= o.ApplicableDate && o.ApplicableDate < booking.CheckOutDate).Select(r => r.AverageRoomRate).LastOrDefault();
                            }
                        }

                        decimal dividendAmount = Math.Round(Convert.ToDecimal((divAmountPerSq * totalSquareFeetPerPerson) / totaldayscanStay * profitEligibility), 5);


                        var dividend = new Dividend()
                        {
                            DividendId = "DIVIDEND-" + last,
                            OwnerId = OwnersInfo.OwenerId,
                            SlotId = SlotId,
                            OwnerName = OwnersInfo.Name,
                            DateOfBirth = OwnersInfo.DateOfBirth,
                            MobileNumber = OwnersInfo.Mobile,
                            ToMonth = LastDayOfTheToMonth,
                            FromMonth = Convert.ToDateTime(mdl.fromMonth),
                            DividendAmount = dividendAmount,
                            notification = 0,
                            status = "PENDING",
                            IsSelected = false,
                            DeductionRateforStaying = mdl.DeductionRateforStaying,
                        };

                        adbc.Dividends.Add(dividend);

                        fromMonthConvert = Convert.ToDateTime(mdl.fromMonth);
                        toMonthConvert = LastDayOfTheToMonth;
                    }
                    else
                    {
                        SlotIds += SlotId + ", ";
                    }
                }
                adbc.SaveChanges();

                #endregion

                #region Generate Select List

                var DividendList = adbc.Dividends.ToList();
                Dictionary<string, bool> dividenDictionary = new Dictionary<string, bool>();
                foreach (var Dividend in DividendList)
                {
                    dividenDictionary.Add(Dividend.DividendId, false);
                }
                Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] =
                    (Dictionary<string, bool>)dividenDictionary;

                #endregion
            }

            DividendViewModel model = new DividendViewModel();

            #region Pending Dividend

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();

                SortedDictionary<int, Dividend> PendingDividendList = new SortedDictionary<int, Dividend>();

                foreach (var dividend in allDividends)
                {
                    int key = int.Parse(dividend.DividendId.Split('-')[1]);
                    PendingDividendList.Add(key, dividend);
                }

                Dictionary<int, Dividend> PendingDividendListReverse = new Dictionary<int, Dividend>();

                foreach (var dividendKeyValuePair in PendingDividendList.Reverse())
                {
                    PendingDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                }

                SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                model.dividends = PendingDividendListReverse.Values.ToList();
                model.totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault();
                model.flg = false;
                model.totalRows = totalRows;
                model.updatedRows = updatedRows;
            }

            #endregion

            #region Dividend Failed Group

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //Generate Primary KEY here
                var dividendFailedGroupId = "";
                var dividendFailedGroupList = db.DividendFailedGroups.ToList();
                var dividendFailedGroupIdList = new List<int>();
                foreach (var dividendFailedGroupTemp in dividendFailedGroupList)
                {
                    dividendFailedGroupIdList.Add(int.Parse(dividendFailedGroupTemp.DividendFailedGroupId.ToString().Split('-')[1]));
                }

                var dividendFailedGroupToken = dividendFailedGroupList.Count();
                if (dividendFailedGroupToken > 0)
                {
                    dividendFailedGroupIdList.Sort();
                    dividendFailedGroupToken = dividendFailedGroupIdList[dividendFailedGroupIdList.Count - 1];
                }

                dividendFailedGroupToken++;
                dividendFailedGroupId = "DFG-" + dividendFailedGroupToken;


                DividendFailedGroup dividendFailedGroup = new DividendFailedGroup()
                {
                    DividendFailedGroupId = dividendFailedGroupId,
                    NoOfFailedDividendRows = totalRows - updatedRows,
                    FromMonth = fromMonthConvert,
                    ToMonth = toMonthConvert,
                    SlotIds = SlotIds,
                };
                db.DividendFailedGroups.Add(dividendFailedGroup);
                db.SaveChanges();
            }

            #endregion

            return RedirectToAction("PendingViewAfterUpdate",
                new { totalRows = model.totalRows, updatedRows = model.updatedRows });
        }

        [HttpGet]
        public ActionResult PayToOwnerBulkEdit()
        {
            return View();
        }

        [HttpGet]
        public ActionResult DividendFailedGroupManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Dividend\\PendingView").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }

                List<DividendFailedGroup> DividendFailedGroups = adbc.DividendFailedGroups.ToList().OrderBy(d => d.FromMonth).ToList();
                DividendFailedGroupManagementView model = new DividendFailedGroupManagementView()
                {
                    DividendFailedGroupList = DividendFailedGroups,
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult DividendFailedGroupView(string dividendFailedGroupId)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Dividend\\PendingView").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }
                var dividendFailedGroupObj = adbc.DividendFailedGroups.FirstOrDefault(a => a.DividendFailedGroupId == dividendFailedGroupId);
                return View(dividendFailedGroupObj);
            }
        }
        [HttpPost]
        public JsonResult checkingDividendMaturityMonth(DividendMaturityCheckingViewModel mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                FinancialYear obj = adbc.FinancialYears.FirstOrDefault(f => f.FinId == mdl.finId);
                DateTime dividendMaturityMonth = Convert.ToDateTime(mdl.dividendMaturityMonth);
                DateTime LastDayOfdividendMaturityMonth = new DateTime(dividendMaturityMonth.Year, dividendMaturityMonth.Month, DateTime.DaysInMonth(dividendMaturityMonth.Year, dividendMaturityMonth.Month));
                var DividendList = adbc.Dividends.Where(d => d.FromMonth >= obj.FromMonth && d.ToMonth <= obj.ToMonth).ToList();
                var FixedFromMonth = DateTime.Now;
                if (DividendList.Count != 0)
                {
                    var tmpFixedFromMonth = DividendList.Max(d => d.ToMonth);
                    FixedFromMonth = tmpFixedFromMonth.AddDays(1);
                }
                else
                {
                    FixedFromMonth = obj.FromMonth;
                }
                var FixedToMonth = DateTime.Now;
                string isChangingOrNot = "n";
                if (FixedFromMonth >= LastDayOfdividendMaturityMonth)
                {
                    FixedToMonth = FixedFromMonth.AddDays(1);
                    isChangingOrNot = "y";
                }
                else if (FixedFromMonth < LastDayOfdividendMaturityMonth &&
                         LastDayOfdividendMaturityMonth <= obj.ToMonth)
                {
                    FixedToMonth = LastDayOfdividendMaturityMonth;
                }
                else if (LastDayOfdividendMaturityMonth > obj.ToMonth)
                {
                    FixedToMonth = obj.ToMonth;
                    isChangingOrNot = "y";
                }

                DividendMaturityCheckingViewModel model = new DividendMaturityCheckingViewModel();
                if (FixedFromMonth > obj.ToMonth)
                {
                    model.isAlreadyExists = "y";
                    model.FixedToMonth = "";
                    model.FixedFromMonth = "";
                }
                else
                {
                    model.isAlreadyExists = isChangingOrNot;
                    model.FixedFromMonth = FixedFromMonth.ToString("dd-MMM-yyyy hh:mm tt");
                    model.FixedToMonth = FixedToMonth.ToString("dd-MMM-yyyy hh:mm tt");
                }
                return Json(model);
            }
        }
        [HttpGet]
        public ActionResult PendingView(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            DividendViewModel model = new DividendViewModel();
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Dividend\\PendingView").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "0")
                {
                    return View("Error");
                }
                var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();


                SortedDictionary<int, Dividend> PendingDividendList = new SortedDictionary<int, Dividend>();

                foreach (var dividend in allDividends)
                {
                    int key = int.Parse(dividend.DividendId.Split('-')[1]);
                    PendingDividendList.Add(key, dividend);
                }

                Dictionary<int, Dividend> PendingDividendListReverse = new Dictionary<int, Dividend>();

                foreach (var dividendKeyValuePair in PendingDividendList.Reverse())
                {
                    PendingDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                }


                var DividendObjList = PendingDividendListReverse.Values.ToList();
                List<Dividend> DividendList = new List<Dividend>();

                if (!String.IsNullOrEmpty(searchfield))
                {
                    var searchfield1 = searchfield.ToUpper();
                    foreach (var dividend in DividendObjList)
                    {
                        var OwnerId = "";
                        if (dividend.OwnerId != null)
                        {
                            OwnerId = dividend.OwnerId.ToUpper();
                        }

                        var DividendId = "";
                        if (dividend.DividendId != null)
                        {
                            DividendId = dividend.DividendId.ToUpper();
                        }
                        DateTime ToMonth = new DateTime();
                        if (dividend.ToMonth != null)
                        {
                            ToMonth = (DateTime)dividend.ToMonth;
                        }
                        var SlotId = "";
                        if (dividend.SlotId != null)
                        {
                            SlotId = dividend.SlotId.ToUpper();
                        }


                        var OwnerId1 = OwnerId;
                        var DividendId1 = DividendId;
                        var SlotId1 = SlotId;
                        var ToMonth1 = ToMonth;
                        string date = ToMonth1.ToString();
                        //var AuditDTTM1 = AuditDTTM;

                        if (OwnerId1.Contains(searchfield1) == true || DividendId1.Contains(searchfield1) == true || SlotId1.Contains(searchfield1) == true || date.Contains(searchfield1) == true)
                        {
                            DividendList.Add(dividend);
                        }
                    }
                }
                else
                {
                    DividendList = DividendObjList;
                }
                List<Dividend> selectedDividendInfos = new List<Dividend>();
                int noOfPage;
                if (DividendList.Count >= 10)
                {

                    var pageNo = (DividendList.Count < (index * 10)) ? DividendList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedDividendInfos.Add(DividendList.ElementAt(i));
                    }

                    noOfPage = (DividendList.Count + 9) / 10;
                }
                else
                {
                    selectedDividendInfos = DividendList;
                    noOfPage = 1;
                }


                model.totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault();
                model.flg = false;
                model.totalRows = 0;
                model.updatedRows = 0;
                model.index = index;
                model.noOfPage = noOfPage;
                model.dividends = selectedDividendInfos;
                model.searchfield = searchfield;
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult PendingViewAfterUpdate(int totalRows, int updatedRows)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            DividendViewModel model = new DividendViewModel();
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();
                //                var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();

                SortedDictionary<int, Dividend> PendingDividendList = new SortedDictionary<int, Dividend>();

                foreach (var dividend in allDividends)
                {
                    int key = int.Parse(dividend.DividendId.Split('-')[1]);
                    PendingDividendList.Add(key, dividend);
                }

                Dictionary<int, Dividend> PendingDividendListReverse = new Dictionary<int, Dividend>();

                foreach (var dividendKeyValuePair in PendingDividendList.Reverse())
                {
                    PendingDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                }


                var DividendObjList = PendingDividendListReverse.Values.ToList();

                List<Dividend> selectedDividendInfos = new List<Dividend>();
                int noOfPage;
                if (DividendObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedDividendInfos.Add(DividendObjList.ElementAt(i));
                    }
                    int x = (DividendObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (DividendObjList.Count / 10) + x;
                }
                else
                {
                    selectedDividendInfos = DividendObjList;
                    noOfPage = 1;
                }


                model.totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault();
                model.flg = false;
                model.totalRows = totalRows;
                model.updatedRows = updatedRows;
                model.index = 1;
                model.noOfPage = noOfPage;
                model.dividends = selectedDividendInfos;
            }

            return View("PendingView", model);
        }
        //[HttpGet]
        //public ActionResult PendingViewPagination(int index)
        //{
        //    if (!IsUserLoggedIn())
        //        return RedirectToAction("Login", "Accounts");


        //    DividendViewModel model = new DividendViewModel();
        //    using (ApplicationDbContext adbc = new ApplicationDbContext())
        //    {
        //        var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();
        //        //                var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();

        //        SortedDictionary<int, Dividend> PendingDividendList = new SortedDictionary<int, Dividend>();

        //        foreach (var dividend in allDividends)
        //        {
        //            int key = int.Parse(dividend.DividendId.Split('-')[1]);
        //            PendingDividendList.Add(key, dividend);
        //        }

        //        Dictionary<int, Dividend> PendingDividendListReverse = new Dictionary<int, Dividend>();

        //        foreach (var dividendKeyValuePair in PendingDividendList.Reverse())
        //        {
        //            PendingDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
        //        }


        //        var DividendObjList = PendingDividendListReverse.Values.ToList();

        //        List<Dividend> selectedDividendInfos = new List<Dividend>();
        //        int noOfPage;
        //        if (DividendObjList.Count >= 10)
        //        {
        //            var pageNo = (DividendObjList.Count < (index * 10)) ? DividendObjList.Count : (index * 10);
        //            for (int i = index * 10 - 10; i < pageNo; i++)
        //            {
        //                selectedDividendInfos.Add(DividendObjList.ElementAt(i));
        //            }
        //            int x = (DividendObjList.Count % 10 == 0) ? 0 : 1;

        //            noOfPage = (DividendObjList.Count / 10) + x;
        //        }
        //        else
        //        {
        //            selectedDividendInfos = DividendObjList;
        //            noOfPage = 1;
        //        }


        //        model.totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault();
        //        model.flg = false;
        //        model.totalRows = 0;
        //        model.updatedRows = 0;
        //        model.index = index;
        //        model.noOfPage = noOfPage;
        //        model.dividends = selectedDividendInfos;
        //    }

        //    return View("PendingView", model);
        //}

        //[HttpPost]
        //public JsonResult gettingSearchResult(string searchfield)
        //{
        //    if (!IsUserLoggedIn())
        //    {
        //        return Json("Forbidden Http Request");
        //    }
        //    using (ApplicationDbContext db = new ApplicationDbContext())
        //    {
        //        var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
        //        var DividendList = db.Dividends.Where(d => d.status == "PENDING").ToList();
        //        List<DividendViewModelForSearch> searchedDividend = new List<DividendViewModelForSearch>();
        //        var searchfield1 = searchfield.ToUpper();

        //        foreach (var dividend in DividendList)
        //        {
        //            var DividendId = "";
        //            if (dividend.DividendId != null)
        //            {
        //                DividendId = dividend.DividendId.ToUpper();
        //            }


        //            var SlotId = "";
        //            if (dividend.SlotId != null)
        //            {
        //                SlotId = dividend.SlotId.ToUpper();
        //            }
        //            var Name = "";
        //            if (dividend.OwnerName != null)
        //            {
        //                Name = dividend.OwnerName.ToUpper();
        //            }
        //            var Mobile = "";
        //            if (dividend.MobileNumber != null)
        //            {
        //                Mobile = dividend.MobileNumber.ToUpper();
        //            }
        //            var DividendAmount = "";
        //            if (dividend.DividendAmount.ToString() != null)
        //            {
        //                DividendAmount = dividend.DividendAmount.ToString().ToUpper();
        //            }

        //            var FromMonth = "";
        //            if (dividend.FromMonth.ToString("dd-MMM-yyyy") != null)
        //            {
        //                FromMonth = dividend.FromMonth.ToString("dd-MMM-yyyy").ToUpper();
        //            }

        //            var ToMonth = "";
        //            if (dividend.ToMonth.ToString("dd-MMM-yyyy") != null)
        //            {
        //                ToMonth = dividend.ToMonth.ToString("dd-MMM-yyyy").ToUpper();
        //            }

        //            var DividendId1 = DividendId;
        //            var SlotId1 = SlotId;
        //            var Name1 = Name;
        //            var Mobile1 = Mobile;
        //            var DividendAmount1 = DividendAmount;
        //            var FromMonth1 = FromMonth;
        //            var ToMonth1 = ToMonth;

        //            if (DividendId1.Contains(searchfield1) == true || SlotId1.Contains(searchfield1) == true ||
        //                Name1.Contains(searchfield1) == true || FromMonth1.Contains(searchfield1) == true
        //                || ToMonth1.Contains(searchfield1) == true || Mobile1.Contains(searchfield1) == true
        //                || DividendAmount1.Contains(searchfield1) == true)
        //            {
        //                Dividend obj = dividend;
        //                obj.IsSelected = dividentDictionary[dividend.DividendId];
        //                DividendViewModelForSearch objDividendViewModelForSearch = new DividendViewModelForSearch()
        //                {
        //                    IsSelected = obj.IsSelected,
        //                    DateOfBirth = obj.DateOfBirth,
        //                    DividendId = obj.DividendId,
        //                    DividendAmount = obj.DividendAmount,
        //                    OwnerId = obj.OwnerId,
        //                    FromMonth = obj.FromMonth.ToString("dd-MMM-yyyy"),
        //                    ToMonth = obj.ToMonth.ToString("dd-MMM-yyyy"),
        //                    MobileNumber = obj.MobileNumber,
        //                    notification = obj.notification,
        //                    SlotId = obj.SlotId,
        //                    OwnerName = obj.OwnerName
        //                };
        //                searchedDividend.Add(objDividendViewModelForSearch);
        //            }

        //        }
        //        string taskId = db.Tasks.Where(u => u.TaskPath == "Dividend\\PendingView").Select(u => u.TaskId).FirstOrDefault();
        //        var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

        //        string taskRoles = "";
        //        foreach (var tasks in myRoleList)
        //        {
        //            if (tasks.TaskId == taskId)
        //            {
        //                taskRoles = tasks.Details;
        //            }
        //        }


        //        if (taskRoles == "")
        //        {
        //            //return View(Json("Error"));
        //        }
        //        //OwnerList mdl = new OwnerList()
        //        //{
        //        //    index = 1,
        //        //    NoOfOwner = 0,
        //        //    Owners = searchedOwner,
        //        //    TaskRoles = taskRoles
        //        //};
        //        return Json(searchedDividend);

        //    }
        //}

        [HttpPost]
        public JsonResult gettingSingleSelection(DividendSelectionViewModel mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string Id = mdl.Id;
                if (mdl.Id[0] == 'J')
                {
                    Id = mdl.Id.Substring(2, mdl.Id.Length - 2);
                }
                if (mdl.IsChecked == "t")
                {
                    var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                    dividentDictionary[Id] = true;
                    Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] = dividentDictionary;
                }
                else
                {
                    var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                    dividentDictionary[Id] = false;
                    Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] = dividentDictionary;
                }
            }

            return Json("okay");
        }

        [HttpPost]
        public JsonResult gettingAllSelection(DividendSelectionViewModel mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                if (mdl.IsChecked == "t")
                {
                    var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                    var DividendIds = dividentDictionary.Keys.ToList();
                    foreach (string dividendId in DividendIds)
                    {
                        dividentDictionary[dividendId] = true;
                    }
                    Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] = dividentDictionary;
                    Session[SessionKeys.DIVIDENT_ALL_SELECT] = ("t").ToString();
                }
                else
                {
                    var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                    var DividendIds = dividentDictionary.Keys.ToList();
                    foreach (string dividendId in DividendIds)
                    {
                        dividentDictionary[dividendId] = false;
                    }
                    Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] = dividentDictionary;
                    Session[SessionKeys.DIVIDENT_ALL_SELECT] = ("f").ToString();
                }
            }
            return Json("okay");
        }
        [HttpPost]
        public JsonResult gettingAllSelectionJs(DividendSelectionViewModel mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                if (mdl.IsChecked == "t")
                {
                    var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                    var DividendIds = mdl.Ids;
                    foreach (string dividendId in DividendIds)
                    {
                        var Id = dividendId.Substring(2, dividendId.Length - 2);
                        dividentDictionary[Id] = true;
                    }
                    Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] = dividentDictionary;
                    Session[SessionKeys.DIVIDENT_ALL_SELECT] = ("t").ToString();
                }
                else
                {
                    var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                    var DividendIds = dividentDictionary.Keys.ToList();
                    foreach (string dividendId in DividendIds)
                    {
                        dividentDictionary[dividendId] = false;
                    }
                    Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] = dividentDictionary;
                    Session[SessionKeys.DIVIDENT_ALL_SELECT] = ("f").ToString();
                }
            }
            return Json("okay");
        }
        [HttpPost]
        public ActionResult ApproveAll()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();
                foreach (var dividend in allDividends)
                {
                    dividend.status = "APPROVED";
                }
                adbc.SaveChanges();
            }

            return RedirectToAction("ApprovedView");
        }

        [HttpGet]
        public ActionResult ApprovedView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            DividendViewModel model = new DividendViewModel();
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Dividend\\PendingView").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles == "")
                {
                    return View("Error");
                }


                var allDividends = adbc.Dividends.Where(d => d.status == "APPROVED").ToList();
                SortedDictionary<int, Dividend> ApprovedDividendList = new SortedDictionary<int, Dividend>();

                foreach (var dividend in allDividends)
                {
                    int key = int.Parse(dividend.DividendId.Split('-')[1]);
                    ApprovedDividendList.Add(key, dividend);
                }

                Dictionary<int, Dividend> ApprovedDividendListReverse = new Dictionary<int, Dividend>();

                foreach (var dividendKeyValuePair in ApprovedDividendList.Reverse())
                {
                    ApprovedDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                }


                var DividendObjList = ApprovedDividendListReverse.Values.ToList();

                List<Dividend> selectedDividendInfos = new List<Dividend>();
                int noOfPage;
                if (DividendObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedDividendInfos.Add(DividendObjList.ElementAt(i));
                    }
                    int x = (DividendObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (DividendObjList.Count / 10) + x;
                }
                else
                {
                    selectedDividendInfos = DividendObjList;
                    noOfPage = 1;
                }

                var model1 = new DividendViewModel()
                {

                    index = 1,
                    noOfPage = noOfPage,
                    dividends = selectedDividendInfos,
                    //TaskRoles = taskRoles
                };
                return View(model1);



            }


        }

        [HttpGet]
        public ActionResult ApprovedViewPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            DividendViewModel model = new DividendViewModel();
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allDividends = adbc.Dividends.Where(d => d.status == "APPROVED").ToList();
                SortedDictionary<int, Dividend> ApprovedDividendList = new SortedDictionary<int, Dividend>();

                foreach (var dividend in allDividends)
                {
                    int key = int.Parse(dividend.DividendId.Split('-')[1]);
                    ApprovedDividendList.Add(key, dividend);
                }

                Dictionary<int, Dividend> ApprovedDividendListReverse = new Dictionary<int, Dividend>();

                foreach (var dividendKeyValuePair in ApprovedDividendList.Reverse())
                {
                    ApprovedDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                }


                var DividendObjList = ApprovedDividendListReverse.Values.ToList();

                List<Dividend> selectedDividendInfos = new List<Dividend>();
                int noOfPage;
                if (DividendObjList.Count >= 10)
                {
                    var pageNo = (DividendObjList.Count < (index * 10)) ? DividendObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedDividendInfos.Add(DividendObjList.ElementAt(i));
                    }
                    int x = (DividendObjList.Count % 10 == 0) ? 0 : 1;
                    noOfPage = (DividendObjList.Count / 10) + x;
                }
                else
                {
                    selectedDividendInfos = DividendObjList;
                    noOfPage = 1;
                }
                var model1 = new DividendViewModel()
                {

                    index = index,
                    noOfPage = noOfPage,
                    dividends = selectedDividendInfos,
                    //TaskRoles = taskRoles
                };

                return View("ApprovedView", model1);


            }


        }

        [HttpPost]
        public JsonResult gettingSearchResultForApprovedView(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                var DividendList = db.Dividends.Where(d => d.status == "APPROVED").ToList();
                List<DividendViewModelForSearch> searchedDividend = new List<DividendViewModelForSearch>();
                var searchfield1 = searchfield.ToUpper();

                foreach (var dividend in DividendList)
                {
                    var DividendId = "";
                    if (dividend.DividendId != null)
                    {
                        DividendId = dividend.DividendId.ToUpper();
                    }


                    var SlotId = "";
                    if (dividend.SlotId != null)
                    {
                        SlotId = dividend.SlotId.ToUpper();
                    }
                    var Name = "";
                    if (dividend.OwnerName != null)
                    {
                        Name = dividend.OwnerName.ToUpper();
                    }
                    var Mobile = "";
                    if (dividend.MobileNumber != null)
                    {
                        Mobile = dividend.MobileNumber.ToUpper();
                    }
                    var DividendAmount = "";
                    if (dividend.DividendAmount.ToString() != null)
                    {
                        DividendAmount = dividend.DividendAmount.ToString().ToUpper();
                    }

                    var FromMonth = "";
                    if (dividend.FromMonth.ToString("dd-MMM-yyyy") != null)
                    {
                        FromMonth = dividend.FromMonth.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var ToMonth = "";
                    if (dividend.ToMonth.ToString("dd-MMM-yyyy") != null)
                    {
                        ToMonth = dividend.ToMonth.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var DividendId1 = DividendId;
                    var SlotId1 = SlotId;
                    var Name1 = Name;
                    var Mobile1 = Mobile;
                    var DividendAmount1 = DividendAmount;
                    var FromMonth1 = FromMonth;
                    var ToMonth1 = ToMonth;
                    if (DividendId1.Contains(searchfield1) == true || SlotId1.Contains(searchfield1) == true ||
                        Name1.Contains(searchfield1) == true || FromMonth1.Contains(searchfield1) == true
                        || ToMonth1.Contains(searchfield1) == true || Mobile1.Contains(searchfield1) == true
                        || DividendAmount1.Contains(searchfield1) == true)
                    {
                        Dividend obj = dividend;
                        //obj.IsSelected = dividentDictionary[dividend.DividendId];
                        DividendViewModelForSearch objDividendViewModelForSearch = new DividendViewModelForSearch()
                        {
                            //IsSelected = obj.IsSelected,
                            DateOfBirth = obj.DateOfBirth,
                            DividendId = obj.DividendId,
                            DividendAmount = obj.DividendAmount,
                            OwnerId = obj.OwnerId,
                            FromMonth = obj.FromMonth.ToString("dd-MMM-yyyy"),
                            ToMonth = obj.ToMonth.ToString("dd-MMM-yyyy"),
                            MobileNumber = obj.MobileNumber,
                            notification = obj.notification,
                            SlotId = obj.SlotId,
                            OwnerName = obj.OwnerName
                        };
                        searchedDividend.Add(objDividendViewModelForSearch);
                    }

                }
                //OwnerList mdl = new OwnerList()
                //{
                //    index = 1,
                //    NoOfOwner = 0,
                //    Owners = searchedOwner,
                //    TaskRoles = taskRoles
                //};
                return Json(searchedDividend);

            }
        }

        [HttpGet]
        public ActionResult ArchivedView()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            DividendViewModel model = new DividendViewModel();
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allDividends = adbc.Dividends.Where(d => d.status == "ARCHIVED").ToList();
                SortedDictionary<int, Dividend> ArchivedDividendList = new SortedDictionary<int, Dividend>();

                foreach (var dividend in allDividends)
                {
                    int key = int.Parse(dividend.DividendId.Split('-')[1]);
                    ArchivedDividendList.Add(key, dividend);
                }

                Dictionary<int, Dividend> ArchivedDividendListReverse = new Dictionary<int, Dividend>();

                foreach (var dividendKeyValuePair in ArchivedDividendList.Reverse())
                {
                    ArchivedDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                }


                var DividendObjList = ArchivedDividendListReverse.Values.ToList();

                List<Dividend> selectedDividendInfos = new List<Dividend>();
                int noOfPage;
                if (DividendObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedDividendInfos.Add(DividendObjList.ElementAt(i));
                    }
                    int x = (DividendObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (DividendObjList.Count / 10) + x;
                }
                else
                {
                    selectedDividendInfos = DividendObjList;
                    noOfPage = 1;
                }
                model.dividends = allDividends;
                model.noOfPage = noOfPage;
                model.index = 1;
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult ArchivedViewPagination(int index)

        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            DividendViewModel model = new DividendViewModel();
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allDividends = adbc.Dividends.Where(d => d.status == "ARCHIVED").ToList();
                SortedDictionary<int, Dividend> ArchivedDividendList = new SortedDictionary<int, Dividend>();

                foreach (var dividend in allDividends)
                {
                    int key = int.Parse(dividend.DividendId.Split('-')[1]);
                    ArchivedDividendList.Add(key, dividend);
                }

                Dictionary<int, Dividend> ArchivedDividendListReverse = new Dictionary<int, Dividend>();

                foreach (var dividendKeyValuePair in ArchivedDividendList.Reverse())
                {
                    ArchivedDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                }


                var DividendObjList = ArchivedDividendListReverse.Values.ToList();

                List<Dividend> selectedDividendInfos = new List<Dividend>();
                int noOfPage;
                if (DividendObjList.Count >= 10)
                {
                    var pageNo = (DividendObjList.Count < (index * 10)) ? DividendObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedDividendInfos.Add(DividendObjList.ElementAt(i));
                    }
                    int x = (DividendObjList.Count % 10 == 0) ? 0 : 1;

                    noOfPage = (DividendObjList.Count / 10) + x;
                }
                else
                {
                    selectedDividendInfos = DividendObjList;
                    noOfPage = 1;
                }
                model.dividends = allDividends;
                model.noOfPage = noOfPage;
                model.index = index;
            }

            return View("ArchivedView", model);
        }

        [HttpPost]
        public JsonResult gettingSearchResultForArchivedView(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                var DividendList = db.Dividends.Where(d => d.status == "ARCHIVED").ToList();
                List<DividendViewModelForSearch> searchedDividend = new List<DividendViewModelForSearch>();
                var searchfield1 = searchfield.ToUpper();

                foreach (var dividend in DividendList)
                {
                    var DividendId = "";
                    if (dividend.DividendId != null)
                    {
                        DividendId = dividend.DividendId.ToUpper();
                    }


                    var SlotId = "";
                    if (dividend.SlotId != null)
                    {
                        SlotId = dividend.SlotId.ToUpper();
                    }
                    var Name = "";
                    if (dividend.OwnerName != null)
                    {
                        Name = dividend.OwnerName.ToUpper();
                    }
                    var Mobile = "";
                    if (dividend.MobileNumber != null)
                    {
                        Mobile = dividend.MobileNumber.ToUpper();
                    }
                    var DividendAmount = "";
                    if (dividend.DividendAmount.ToString() != null)
                    {
                        DividendAmount = dividend.DividendAmount.ToString().ToUpper();
                    }

                    var FromMonth = "";
                    if (dividend.FromMonth.ToString("dd-MMM-yyyy") != null)
                    {
                        FromMonth = dividend.FromMonth.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var ToMonth = "";
                    if (dividend.ToMonth.ToString("dd-MMM-yyyy") != null)
                    {
                        ToMonth = dividend.ToMonth.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var DividendId1 = DividendId;
                    var SlotId1 = SlotId;
                    var Name1 = Name;
                    var Mobile1 = Mobile;
                    var DividendAmount1 = DividendAmount;
                    var FromMonth1 = FromMonth;
                    var ToMonth1 = ToMonth;
                    if (DividendId1.Contains(searchfield1) == true || SlotId1.Contains(searchfield1) == true ||
                        Name1.Contains(searchfield1) == true || FromMonth1.Contains(searchfield1) == true
                        || ToMonth1.Contains(searchfield1) == true || Mobile1.Contains(searchfield1) == true
                        || DividendAmount1.Contains(searchfield1) == true)
                    {
                        Dividend obj = dividend;
                        //obj.IsSelected = dividentDictionary[dividend.DividendId];
                        DividendViewModelForSearch objDividendViewModelForSearch = new DividendViewModelForSearch()
                        {
                            //IsSelected = obj.IsSelected,
                            DateOfBirth = obj.DateOfBirth,
                            DividendId = obj.DividendId,
                            DividendAmount = obj.DividendAmount,
                            OwnerId = obj.OwnerId,
                            FromMonth = obj.FromMonth.ToString("dd-MMM-yyyy"),
                            ToMonth = obj.ToMonth.ToString("dd-MMM-yyyy"),
                            MobileNumber = obj.MobileNumber,
                            notification = obj.notification,
                            SlotId = obj.SlotId,
                            OwnerName = obj.OwnerName
                        };
                        searchedDividend.Add(objDividendViewModelForSearch);
                    }

                }
                //OwnerList mdl = new OwnerList()
                //{
                //    index = 1,
                //    NoOfOwner = 0,
                //    Owners = searchedOwner,
                //    TaskRoles = taskRoles
                //};
                return Json(searchedDividend);

            }
        }

        [HttpGet]
        public ActionResult DividendInfoView(string dividendid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var model = adbc.Dividends.SingleOrDefault(d => d.DividendId == dividendid);

                if (model == null)
                    return HttpNotFound();

                return View(model);
            }
        }


        [HttpGet]
        public ActionResult DividendInfoEdit(string dividendid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Dividend\\PendingView").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }
                var model = adbc.Dividends.SingleOrDefault(d => d.DividendId == dividendid);

                if (model == null)
                    return HttpNotFound();
                DateTime FirstDayOfTheFromMonth = model.FromMonth;
                DateTime LastDayOfTheToMonth = model.ToMonth;
                int noOfNightsSpent = 0;

                //Determining how many days the owner has spent in the hotel
                var ownersNoOfDays = adbc.OwnerRoomBookings.Where(r => r.SlotId == model.SlotId && (r.HasStayedOrNot == "Yes")).ToList();
                var totalSpent = 0;
                //measuring how many days he spent in the hotel actually
                foreach (var days in ownersNoOfDays)
                {
                    if (FirstDayOfTheFromMonth <= days.CheckInDate && LastDayOfTheToMonth >= days.CheckInDate)
                    {
                        var noOfDays1 = Convert.ToDateTime(days.CheckInDate);
                        var noOfDays2 = Convert.ToDateTime(days.CheckOutDate);
                        var noOfDays = (noOfDays2 - noOfDays1).TotalDays;
                        totalSpent += days.NoOfRoomAllotted * Convert.ToInt16(noOfDays);
                    }
                }
                //punishment for not arriving at hotel after completing booking online
                var ownersNoOfDaysCancelled = adbc.OwnerRoomBookings.Where(r => r.HasStayedOrNot == "No" && r.IsBookingCompleted == "Yes" && r.SlotId == model.SlotId).ToList();
                foreach (var days in ownersNoOfDaysCancelled)
                {
                    //total += days.NoOfRoomRequested * Convert.ToInt16(noOfDays);
                    if (FirstDayOfTheFromMonth <= days.CheckInDate && LastDayOfTheToMonth >= days.CheckInDate)
                    {
                        totalSpent += 1;
                    }
                }
                noOfNightsSpent = totalSpent;
                DividendViewModelForSearch mdl = new DividendViewModelForSearch()
                {
                    OwnerId = model.OwnerId,
                    OwnerName = model.OwnerName,
                    DateOfBirth = model.DateOfBirth,
                    MobileNumber = model.MobileNumber,
                    DividendAmount = model.DividendAmount,
                    DividendId = model.DividendId,
                    status = model.status,
                    notification = model.notification,
                    FromMonth = model.FromMonth.ToString("dd-MMM-yyyy"),
                    ToMonth = model.ToMonth.ToString("dd-MMM-yyyy"),
                    SlotId = model.SlotId,
                    IsSelected = model.IsSelected,
                    DeductionRateforStaying = model.DeductionRateforStaying,
                    NoofDaysHasStayed = noOfNightsSpent,
                };
                return View(mdl);
            }
        }

        [HttpPost]
        public ActionResult DividendInfoEdit(DividendViewModelForSearch editedDividend)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var model = adbc.Dividends.SingleOrDefault(d => d.DividendId == editedDividend.DividendId);

                if (model == null)
                    return HttpNotFound();

                model.DividendAmount = editedDividend.DividendAmount;
                model.DeductionRateforStaying = editedDividend.DeductionRateforStaying;
                adbc.SaveChanges();

                return RedirectToAction("PendingView");
            }
        }

        [HttpPost]
        public ActionResult DividendInfoDelete(string dividendid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var model = adbc.Dividends.SingleOrDefault(d => d.DividendId == dividendid);

                if (model == null)
                    return HttpNotFound();

                adbc.Dividends.Remove(model);
                adbc.SaveChanges();

                return RedirectToAction("PendingView");
            }
        }


        [HttpPost]
        public ActionResult TestForSms()
        {

            return View("TestForMail");

        }

        [HttpGet]
        public ActionResult TestForMail()
        {
            return View("TestForMail");
        }


        private bool SendMailTest()
        {
            //            const string username = "hams.system2019@gmail.com";
            //            //Set the email password - Change this as per your settings
            //            const string password = "Hamsnia...0";
            //            SmtpClient smtpclient = new SmtpClient();
            //            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            //            //Set the email from address of mail message -  Change this as per your settings
            //            MailAddress fromaddress = new MailAddress("hams.system2019@gmail.com");
            //            //Set the email smtp host -  Change this as per your settings
            //            smtpclient.Host = "smtp.gmail.com";
            //            //Set the email client port -  Change this as per your settings
            //            smtpclient.Port = 465;
            //            mail.From = fromaddress;
            //            //Adding email id of receiver link
            //            mail.To.Add("atiqishraq@gmail.com");
            //            //Set the email subject
            //            mail.Subject = ("Test Subject");
            //            mail.IsBodyHtml = true;
            //            //Set the email body - Change this as per your logic
            //            mail.Body = "This is test Mail";
            //            smtpclient.DeliveryMethod = SmtpDeliveryMethod.Network;
            //            smtpclient.Credentials = new System.Net.NetworkCredential(username, password);
            //            try
            //            {
            //                //Sending Email
            //                smtpclient.Send(mail);
            //                Response.Write("<B>Email Has been sent successfully.</B>");
            //            }
            //            catch (Exception ex)
            //            {
            //                //Catch if any exception occurs
            //                Content((ex.Message).ToString());
            //            }
            //
            //            return true;
            return true;
        }

        [HttpGet]
        public ActionResult TestForMail2()
        {
            SendEmailMailGun();
            //            try
            //            {
            //                SmtpClient client = new SmtpClient();
            ////                client.UseDefaultCredentials = false;
            ////                client.Host = "mail.blessingknitwear.com";
            ////                client.Port = 465;
            ////                client.EnableSsl = true;
            ////                //client.Timeout = 20000;
            ////                client.DeliveryMethod = SmtpDeliveryMethod.Network;
            ////                client.Credentials = new NetworkCredential("donotreply@blessingknitwear.com", "Room#114buet");
            //                MailMessage msg = new MailMessage("hams.system2019@gmail.com", "ishraqarnob@gmail.com");
            ////                msg.To.Add("ishraqarnob@gmail.com");
            ////                msg.From = new MailAddress("donotreply@blessingknitwear.com");
            //                msg.Subject = "Test Subject";
            //                msg.Body = "Test Body";
            //                client.Send(msg);
            //            }
            //            catch (Exception e)
            //            {
            //                return Content(e.Message.ToString());
            //            }
            return View("TestForMail");
        }

        private bool SendMail(OwnersInfo owner, Dividend dividend)
        {
            const string username = "hams.system2019@gmail.com";
            //Set the email password - Change this as per your settings
            const string password = "Hamsnia...0";
            SmtpClient smtpclient = new SmtpClient();
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            //Set the email from address of mail message -  Change this as per your settings
            MailAddress fromaddress = new MailAddress("hams.system2019@gmail.com");
            //Set the email smtp host -  Change this as per your settings
            smtpclient.Host = "smtp.gmail.com";
            //Set the email client port -  Change this as per your settings
            smtpclient.Port = 465;
            mail.From = fromaddress;
            //Adding email id of receiver link
            mail.To.Add("atiqishraq@gmail.com");
            //Set the email subject
            mail.Subject = ("Test Subject");
            mail.IsBodyHtml = true;
            //Set the email body - Change this as per your logic
            mail.Body = "This is test Mail";
            smtpclient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpclient.Credentials = new System.Net.NetworkCredential(username, password);
            try
            {
                //Sending Email
                smtpclient.Send(mail);
                Response.Write("<B>Email Has been sent successfully.</B>");
            }
            catch (Exception ex)
            {
                //Catch if any exception occurs
                Content((ex.Message).ToString());
            }
            return true;
        }

        private bool SendSMS(OwnersInfo owner, Dividend dividend)
        {
            //todo
            string Message = "Dear Mr/Ms " + owner.Name + ", We are happy to inform you that your dividend amount BDT " +
                dividend.DividendAmount + " has been pursed. Thanking you.";
            string MobileNumber = owner.Mobile;
            //Common.SMSSending(MobileNumber, Message);
            //Inserting to EmailSMS table
            var Owner = owner;
            var mobileNumber = Owner.Mobile;
            var isSMSSent = "Processing";
            if (mobileNumber != null && mobileNumber != "")
            {
                var returnStatus = Common.SMSSending(MobileNumber, Message);
                if (returnStatus == true)
                {
                    isSMSSent = "Delivered";
                }
                else
                {
                    isSMSSent = "Not Delivered, possible causes could be invalid mobile number or insufficient balance in SMS gateway";
                }
            }
            else
            {
                isSMSSent = "Not Delivered, Check whether Mobile No of this owner is correct";
            }
            EmailandSMSReport emailsmsreport = new EmailandSMSReport()
            {
                IsSMSSent = isSMSSent,
                Mobile = mobileNumber,
                Email = Owner.Email,
                SlotId = Owner.SlotId,
                Name = Owner.Name,
                TimeOfDelivery = DateTime.Now,
                NotificationType = "SMS",
                Text = Message,
                NotificationSerialNo = "AUTO GENERATED:- DIVIDEND",
            };
            Common.InsertEmailandSMSReports(emailsmsreport);
            return true;
        }



        [HttpPost]
        public ActionResult EmailNotificationSingle(string dividendid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var divModel = adbc.Dividends.SingleOrDefault(d => d.DividendId == dividendid && d.status == "APPROVED");

                if (divModel == null)
                    return HttpNotFound();

                var ownModel = adbc.OwnersInfos.SingleOrDefault(o => o.OwenerId == divModel.OwnerId);

                if (ownModel == null)
                    return HttpNotFound();

                string Message = "Dear Mr/Ms " + ownModel.Name + ", We are happy to inform you that your dividend amount BDT " +
                                 divModel.DividendAmount + " has been pursed. Thanking you.";

                //Common.EmailSending(ownModel.Email, "Regarding dividend amount", Message);
                var isEmailSent = "Processing";

                if (ownModel.Email != null && ownModel.Email != "")
                {
                    Common.EmailSending(ownModel.Email, "Regarding dividend amount", Message);
                    isEmailSent = "Delivered";
                }
                else
                {
                    isEmailSent = "Failed";
                }
                EmailandSMSReport emailsmsreportForEmail = new EmailandSMSReport()
                {
                    IsSMSSent = isEmailSent,
                    Mobile = ownModel.Mobile,
                    Email = ownModel.Email,
                    SlotId = ownModel.SlotId,
                    Name = ownModel.Name,
                    TimeOfDelivery = DateTime.Now,
                    NotificationType = "Email",
                    //Text = model.EmailText,
                    Text = Message,
                    NotificationSerialNo = "AUTO GENERATED:- DIVIDEND",
                };
                Common.InsertEmailandSMSReports(emailsmsreportForEmail);



                divModel.notification += 1;

                adbc.SaveChanges();

                return RedirectToAction("ApprovedView");
            }

        }


        [HttpPost]
        public ActionResult SmsNotificationSingle(string dividendid)
        {

            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var divModel = adbc.Dividends.SingleOrDefault(d => d.DividendId == dividendid && d.status == "APPROVED");

                if (divModel == null)
                    return HttpNotFound();

                var ownModel = adbc.OwnersInfos.SingleOrDefault(o => o.OwenerId == divModel.OwnerId);

                if (ownModel == null)
                    return HttpNotFound();

                if (SendSMS(ownModel, divModel))
                    divModel.notification += 1;


                adbc.SaveChanges();

                return RedirectToAction("ApprovedView");
            }
        }


        private bool SMSAll()
        {

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var divModel = adbc.Dividends.Where(d => d.status == "APPROVED").ToList();

                foreach (var dividend in divModel)
                {
                    var ownModel = adbc.OwnersInfos.SingleOrDefault(o => o.OwenerId == dividend.OwnerId);
                    if (SendSMS(ownModel, dividend))
                        dividend.notification += 1;
                }


                adbc.SaveChanges();
            }

            return true;
        }
        [HttpPost]
        public ActionResult SmsNotificationAll()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            SMSAll();
            return RedirectToAction("ApprovedView");

        }




        private bool EmailAll()
        {

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var divModel = adbc.Dividends.Where(d => d.status == "APPROVED").ToList();

                foreach (var dividend in divModel)
                {
                    var ownModel = adbc.OwnersInfos.SingleOrDefault(o => o.OwenerId == dividend.OwnerId);
                    string Message = "Dear Mr/Ms " + ownModel.Name + ", We are happy to inform you that your dividend amount BDT " +
                                     dividend.DividendAmount + " has been pursed. Thanking you Heritage BD.";

                    //Common.EmailSending(ownModel.Email, "Regarding dividend amount", Message);
                    var isEmailSent = "Processing";

                    if (ownModel.Email != null && ownModel.Email != "")
                    {
                        Common.EmailSending(ownModel.Email, "Regarding dividend amount", Message);
                        isEmailSent = "Delivered";
                    }
                    else
                    {
                        isEmailSent = "Failed";
                    }

                    EmailandSMSReport emailsmsreportForEmail = new EmailandSMSReport()
                    {
                        IsSMSSent = isEmailSent,
                        Mobile = ownModel.Mobile,
                        Email = ownModel.Email,
                        SlotId = ownModel.SlotId,
                        Name = ownModel.Name,
                        TimeOfDelivery = DateTime.Now,
                        NotificationType = "Email",
                        //Text = model.EmailText,
                        Text = Message,
                        NotificationSerialNo = "AUTO GENERATED:- DIVIDEND",
                    };
                    Common.InsertEmailandSMSReports(emailsmsreportForEmail);


                    dividend.notification += 1;
                }


                adbc.SaveChanges();
            }

            return true;
        }

        [HttpPost]
        public ActionResult EmailNotificationAll()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            EmailAll();
            return RedirectToAction("ApprovedView");
        }



        [HttpPost]
        public ActionResult EmailAndSmsNotificationAll()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            EmailAll();
            SMSAll();
            return RedirectToAction("ApprovedView");
        }




        [HttpPost]
        public ActionResult ArchiveAll()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allDividends = adbc.Dividends.Where(d => d.status == "APPROVED").ToList();
                foreach (var dividend in allDividends)
                {
                    dividend.status = "ARCHIVED";
                }


                adbc.SaveChanges();
            }

            return RedirectToAction("ApprovedView");
        }
        [HttpPost]
        public ActionResult ArchiveSingle(string dividendid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var dividend = adbc.Dividends.SingleOrDefault(d =>
                    (d.DividendId == dividendid && d.status == "APPROVED"));
                if (dividend == null)
                    return HttpNotFound();
                dividend.status = "ARCHIVED";
                adbc.SaveChanges();
            }

            return RedirectToAction("ApprovedView");
        }

        [HttpPost]
        public ActionResult RestoreToApproved(string dividendid)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var dividend = adbc.Dividends.SingleOrDefault(d =>
                    (d.DividendId == dividendid && d.status == "ARCHIVED"));
                if (dividend == null)
                    return HttpNotFound();
                dividend.status = "APPROVED";
                adbc.SaveChanges();
            }

            return RedirectToAction("ArchivedView");
        }


        [HttpPost]
        public ActionResult UploadXlsx(DividendViewModel mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            HttpPostedFileBase file = mdl.file;

            if (mdl.fromMonth == null || mdl.toMonth == null || file == null || file.ContentLength == 0)
            {
                DividendViewModel model = new DividendViewModel();
                using (ApplicationDbContext adbc = new ApplicationDbContext())
                {
                    var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();
                    //                    var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();

                    SortedDictionary<int, Dividend> PendingDividendList = new SortedDictionary<int, Dividend>();

                    foreach (var dividend in allDividends)
                    {
                        int key = int.Parse(dividend.DividendId.Split('-')[1]);
                        PendingDividendList.Add(key, dividend);
                    }

                    Dictionary<int, Dividend> PendingDividendListReverse = new Dictionary<int, Dividend>();

                    foreach (var dividendKeyValuePair in PendingDividendList.Reverse())
                    {
                        PendingDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                    }

                    SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();
                    var FinancialYearList = adbc.FinancialYears.OrderBy(f => f.FromMonth).ToList();
                    FinancialYearList.Reverse();
                    model.FinancialIdandParent = FinancialYearList;
                    model.dividends = PendingDividendListReverse.Values.ToList();
                    //                    model.dividends = allDividends;
                    model.totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault();
                    model.flg = true;
                    model.totalRows = 0;
                    model.updatedRows = 0;
                }
                return View("AddingDividendViaExcel", model);
            }

            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/dividendfiles"), fileName);
                string ext = Path.GetExtension(path);
                if (ext != ".xlsx")
                    return Content("File Type Not Supported: " + ext);
                file.SaveAs(path);



                using (FileStream fileStream = new FileStream(path, FileMode.Open))
                {
                    ExcelPackage excel = new ExcelPackage(fileStream);
                    var workSheet = excel.Workbook.Worksheets[1];

                    //                    var bb = workSheet.Cells["A1"].Value.ToString().ToString().Trim() + " " + workSheet.Cells["B1"].Value.ToString().ToString().Trim()
                    //                         + " " + workSheet.Cells["C1"].Value.ToString().ToString().Trim()
                    //                         + " " + workSheet.Cells["D1"].Value.ToString().ToString().Trim()
                    //                         + " " + workSheet.Cells["E1"].Value.ToString().ToString().Trim()
                    //                         + " " + workSheet.Dimension.Rows + " " + workSheet.Dimension.Columns;


                    //return Content(decimal.Parse(workSheet.Cells[2, 5].Value.ToString().Trim())+"");


                    if (workSheet.Cells["A1"].Value.ToString().Trim() == "Slot Id" &&
                        workSheet.Cells["B1"].Value.ToString().Trim() == "Dividend Amount")
                    {

                        int ROW_COUNT = workSheet.Dimension.Rows;
                        int updatedRows = 0;
                        int COL_COUNT = workSheet.Dimension.Columns;

                        using (ApplicationDbContext adbc = new ApplicationDbContext())
                        {

                            var listDividends = adbc.Dividends.ToList();

                            int Count = listDividends.Count;
                            int last = Count;
                            if (Count > 0)
                            {
                                var listDivId = new List<int>();

                                foreach (var div in listDividends)
                                {
                                    listDivId.Add(int.Parse(div.DividendId.ToString().Split('-')[1]));
                                }



                                listDivId.Sort();
                                last = listDivId[listDivId.Count - 1];
                            }


                            for (int i = 2; i <= ROW_COUNT; i++)
                            {
                                if (string.IsNullOrEmpty(workSheet.Cells[i, 2].Text))
                                {
                                    workSheet.Cells[i, 2].Value = "0.00";
                                }

                                last++;
                                //                                var OwnerIdForCheck = workSheet.Cells[i, 1].Value;
                                var SlotIdForCheck = workSheet.Cells[i, 1].Value;
                                if (SlotIdForCheck != null)
                                {

                                    List<OwnerTransactionHistory> OwnerTransactionHistoryList = new List<OwnerTransactionHistory>();
                                    bool flg = false;
                                    if (SlotIdForCheck != null)
                                    {
                                        var SlotId = workSheet.Cells[i, 1].Value.ToString().Trim();
                                        var temp =
                                            adbc.OwnerTransationHistories.Where(o => o.SlotID == SlotId).ToList();
                                        if (temp.Count != 0)
                                        {
                                            flg = true;
                                            OwnerTransactionHistoryList = temp;
                                        }
                                    }
                                    double totalSquareFeetPerPerson = 0;
                                    foreach (var transaction in OwnerTransactionHistoryList)
                                    {
                                        totalSquareFeetPerPerson += Convert.ToDouble(transaction.SFTperPerson);
                                    }

                                    //string OwnerId = workSheet.Cells[i, 1].Value.ToString().Trim();
                                    if (flg == true)
                                    {
                                        updatedRows++;
                                        DateTime toMonth = Convert.ToDateTime(mdl.toMonth);
                                        DateTime LastDayOfTheToMonth = new DateTime(toMonth.Year, toMonth.Month, DateTime.DaysInMonth(toMonth.Year, toMonth.Month));
                                        var SlotId = workSheet.Cells[i, 1].Value.ToString().Trim();
                                        var OwnersInfo = adbc.OwnersInfos.FirstOrDefault(o => o.SlotId == SlotId);
                                        int noOfNightsSpent = 0;
                                        DateTime FirstDayOfTheFromMonth = Convert.ToDateTime(mdl.fromMonth);

                                        //Determining how many days the owner has spent in the hotel
                                        var ownersNoOfDays = adbc.OwnerRoomBookings.Where(r => r.SlotId == SlotId && (r.HasStayedOrNot == "Yes")).ToList();
                                        var totalSpent = 0;
                                        //measuring how many days he spent in the hotel actually
                                        foreach (var days in ownersNoOfDays)
                                        {
                                            if (FirstDayOfTheFromMonth <= days.CheckInDate && LastDayOfTheToMonth >= days.CheckInDate)
                                            {
                                                var noOfDays1 = Convert.ToDateTime(days.CheckInDate);
                                                var noOfDays2 = Convert.ToDateTime(days.CheckOutDate);
                                                var noOfDays = (noOfDays2 - noOfDays1).TotalDays;
                                                totalSpent += days.NoOfRoomAllotted * Convert.ToInt16(noOfDays);
                                            }
                                        }
                                        //punishment for not arriving at hotel after completing booking online
                                        var ownersNoOfDaysCancelled = adbc.OwnerRoomBookings.Where(r => r.HasStayedOrNot == "No" && r.IsBookingCompleted == "Yes" && r.SlotId == SlotId).ToList();
                                        foreach (var days in ownersNoOfDaysCancelled)
                                        {
                                            //total += days.NoOfRoomRequested * Convert.ToInt16(noOfDays);
                                            if (FirstDayOfTheFromMonth <= days.CheckInDate && LastDayOfTheToMonth >= days.CheckInDate)
                                            {
                                                totalSpent += 1;
                                            }
                                        }
                                        noOfNightsSpent = totalSpent;
                                        adbc.Dividends.Add(new Dividend()
                                        {
                                            DividendId = "DIVIDEND-" + last,
                                            OwnerId = OwnersInfo.OwenerId,
                                            SlotId = (SlotIdForCheck != null) ? workSheet.Cells[i, 1].Value.ToString().Trim() : "",
                                            OwnerName = OwnersInfo.Name,
                                            DateOfBirth = OwnersInfo.DateOfBirth,
                                            MobileNumber = OwnersInfo.Mobile,
                                            //DividendAmount = decimal.Parse(workSheet.Cells[i, 5].Value.ToString().Trim()),
                                            ToMonth = LastDayOfTheToMonth,
                                            FromMonth = Convert.ToDateTime(mdl.fromMonth),
                                            DividendAmount = Convert.ToDecimal(mdl.divAmountPerSq * totalSquareFeetPerPerson - mdl.DeductionRateforStaying * noOfNightsSpent),
                                            notification = 0,
                                            status = "PENDING",
                                            IsSelected = false,
                                            DeductionRateforStaying = mdl.DeductionRateforStaying,
                                        });
                                    }
                                }
                            }
                            adbc.SaveChanges();
                            var DividendList = adbc.Dividends.ToList();
                            Dictionary<string, bool> dividenDictionary = new Dictionary<string, bool>();
                            foreach (var Dividend in DividendList)
                            {
                                dividenDictionary.Add(Dividend.DividendId, false);
                            }
                            Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] =
                                (Dictionary<string, bool>)dividenDictionary;
                        }

                        DividendViewModel model = new DividendViewModel();
                        using (ApplicationDbContext adbc = new ApplicationDbContext())
                        {
                            var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();

                            SortedDictionary<int, Dividend> PendingDividendList = new SortedDictionary<int, Dividend>();

                            foreach (var dividend in allDividends)
                            {
                                int key = int.Parse(dividend.DividendId.Split('-')[1]);
                                PendingDividendList.Add(key, dividend);
                            }

                            Dictionary<int, Dividend> PendingDividendListReverse = new Dictionary<int, Dividend>();

                            foreach (var dividendKeyValuePair in PendingDividendList.Reverse())
                            {
                                PendingDividendListReverse.Add(dividendKeyValuePair.Key, dividendKeyValuePair.Value);
                            }

                            SortedDictionary<int, OwnerRoomBooking> OwnerRoomBookingHistoryDictionary = new SortedDictionary<int, OwnerRoomBooking>();

                            model.dividends = PendingDividendListReverse.Values.ToList();
                            model.totalSqFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet).FirstOrDefault();
                            model.flg = false;
                            model.totalRows = ROW_COUNT;
                            model.updatedRows = updatedRows;
                        }

                        return RedirectToAction("PendingViewAfterUpdate",
                            new { totalRows = model.totalRows, updatedRows = model.updatedRows });

                    }

                }

            }

            return RedirectToAction("PendingView", "Dividend");

        }

        [HttpGet]
        public ActionResult DividendReport()
        {
            return Redirect("../Reports/DividendReport.aspx");
        }

        [HttpPost]
        public JsonResult approveSelected(string Dummy)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                //                var allDividends = adbc.Dividends.Where(d => d.status == "PENDING").ToList();
                var allDividends = new List<Dividend>();
                var dividentDictionary = (Dictionary<string, bool>)Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST];
                var DividendIds = dividentDictionary.Keys.ToList();
                foreach (var dividendId in DividendIds)
                {
                    Dividend DividendObj = adbc.Dividends.Where(d => d.DividendId == dividendId).FirstOrDefault();
                    if (DividendObj.status == "PENDING" && dividentDictionary[dividendId] == true)
                    {
                        allDividends.Add(DividendObj);
                    }
                }

                if (allDividends.Count == 0)
                {
                    return Json("no");
                }
                foreach (var dividend in allDividends)
                {
                    dividend.status = "APPROVED";
                }
                var OwnerPaymentsList = adbc.OwnerPayments.ToList();
                var OwnerPaymentsIdList = new List<int>();
                foreach (var ownerPayment in OwnerPaymentsList)
                {
                    OwnerPaymentsIdList.Add(int.Parse(ownerPayment.PaymentID.ToString().Split('-')[1]));
                }

                var ownerPaymentToken = OwnerPaymentsList.Count();
                if (ownerPaymentToken > 0)
                {
                    OwnerPaymentsIdList.Sort();
                    ownerPaymentToken = OwnerPaymentsIdList[OwnerPaymentsIdList.Count - 1];
                }


                foreach (Dividend dividend in allDividends)
                {
                    ownerPaymentToken++;
                    OwnersPayment ownerPaymentModel = new OwnersPayment()
                    {
                        Amount = dividend.DividendAmount,
                        FromMonth = dividend.FromMonth,
                        ToMonth = dividend.ToMonth,
                        PaymentDate = Convert.ToDateTime("1900-01-01 00:00:00.000"),
                        SlotId = dividend.SlotId,
                        ReceiverName = dividend.OwnerName,
                        PaymentID = ownerPaymentToken
                    };
                    adbc.OwnerPayments.Add(ownerPaymentModel);
                    adbc.SaveChanges();
                }
                var NotificationList = adbc.Notifications.ToList();
                var NotificationIdList = new List<int>();
                foreach (var notification in NotificationList)
                {
                    NotificationIdList.Add(int.Parse(notification.NotificationId.ToString().Split('-')[1]));
                }

                var notificationToken = NotificationList.Count();
                if (notificationToken > 0)
                {
                    NotificationIdList.Sort();
                    notificationToken = NotificationIdList[NotificationIdList.Count - 1];
                }
                foreach (Dividend dividend in allDividends)
                {
                    notificationToken++;
                    Notification notificationModel = new Notification()
                    {
                        isSeen = false,
                        NotificationHeader = "Dividend",
                        NotificationText = "Your New Dividend is approved at ." + DateTime.Now.ToString("hh:mm tt dd-MMM-yyyy") + ".",
                        NotificationLink = "/Owner/DividendView?dividendId=" + dividend.DividendId,
                        NotificationTime = DateTime.Now,
                        NotificationId = "NOT-" + notificationToken,
                        IdForRecognition = dividend.DividendId,
                        DividendHolder = (dividend.OwnerId == null || dividend.OwnerId == "") ? adbc.OwnersInfos.Where(o => o.SlotId == dividend.SlotId).Select(o => o.OwenerId).FirstOrDefault() : dividend.OwnerId
                    };
                    adbc.Notifications.Add(notificationModel);
                    adbc.SaveChanges();
                }
                adbc.SaveChanges();
            }

            return Json("yes");
        }

        [HttpPost]
        public ActionResult ApprovedView(string id)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            DividendViewModel model = new DividendViewModel();
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var allDividends = adbc.Dividends.Where(d => d.status == "APPROVED").ToList();
                model.dividends = allDividends;
            }

            return View(model);
        }
        public static RestResponse SendEmailMailGun()
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");

            client.Authenticator = new HttpBasicAuthenticator("api", "12028c5d0d04a8be1a06228b728b64e4-4a62b8e8-1d0fb753");
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "YOUR_DOMAIN_NAME", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            //request.AddParameter("from", "hams.system2019@gmail.com");
            request.AddParameter("from", "Excited User <mailgun@YOUR_DOMAIN_NAME>");
            request.AddParameter("to", "omeca13@gmail.com");
            request.AddParameter("to", "YOU@YOUR_DOMAIN_NAME");
            request.AddParameter("subject", "Hello");
            request.AddParameter("text", "Testing some Mailgun awesomness!");
            request.Method = Method.POST;
            return (RestResponse)client.Execute(request);
        }

        [HttpGet]
        public ActionResult OwnerPaymentManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Dividend\\OwnerPaymentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                var OwnerPaymentsObjList = db.OwnerPayments.ToList().OrderBy(o => o.FromMonth).ToList();
                OwnerPaymentsObjList.Reverse();
                List<OwnersPayment> selectedSearched = new List<OwnersPayment>();
                searchfield = searchfield.ToUpper();
                if (!String.IsNullOrEmpty(searchfield))
                    selectedSearched = OwnerPaymentsObjList.Where(o => o.SlotId.ToUpper().Contains(searchfield) || o.ReceiverName.ToUpper().Contains(searchfield)).ToList();
                else
                    selectedSearched = OwnerPaymentsObjList;

                List<OwnersPayment> selectedPayments = new List<OwnersPayment>();
                int noOfOwnerPayments;
                if (selectedSearched.Count >= 10)
                {
                    var pageNo = (selectedSearched.Count < (index * 10)) ? selectedSearched.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedPayments.Add(selectedSearched.ElementAt(i));
                    }
                    noOfOwnerPayments = ((selectedSearched.Count + 9) / 10);
                }
                else
                {
                    selectedPayments = selectedSearched;
                    noOfOwnerPayments = 1;
                }


                var model = new OwnersPaymentList
                {
                    OwnerPayments = selectedPayments,
                    TaskRoles = taskRoles,
                    NoOfOwnerPayments = noOfOwnerPayments,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult OwnerPaymentPagination(int index)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskId = db.Tasks.Where(u => u.TaskPath == "Dividend\\OwnerPaymentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    return View("Error");
                }

                var OwnerPaymentsObjList = db.OwnerPayments.ToList().OrderBy(o => o.FromMonth).ToList();
                OwnerPaymentsObjList.Reverse();
                List<OwnersPayment> selectedOwnerPaymentsInfos = new List<OwnersPayment>();
                int noOfOwnerPayments;
                if (OwnerPaymentsObjList.Count >= 10)
                {
                    var pageNo = (OwnerPaymentsObjList.Count < (index * 10)) ? OwnerPaymentsObjList.Count : (index * 10);
                    for (int i = index * 10 - 10; i < pageNo; i++)
                    {
                        selectedOwnerPaymentsInfos.Add(OwnerPaymentsObjList.ElementAt(i));
                    }
                    int x = (OwnerPaymentsObjList.Count % 10 == 0) ? 0 : 1;

                    noOfOwnerPayments = (OwnerPaymentsObjList.Count / 10) + x;
                }
                else
                {
                    selectedOwnerPaymentsInfos = OwnerPaymentsObjList;
                    noOfOwnerPayments = 1;
                }


                var model = new OwnersPaymentList
                {
                    OwnerPayments = selectedOwnerPaymentsInfos,
                    TaskRoles = taskRoles,
                    NoOfOwnerPayments = noOfOwnerPayments,
                    index = index
                };
                return View("OwnerPaymentManagement", model);
            }
        }

        [HttpPost]
        public JsonResult gettingSearchResultForOwnerPaymentHistory(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var OwnerPaymentList = db.OwnerPayments.OrderBy(o => o.FromMonth).ToList();
                OwnerPaymentList.Reverse();
                List<OwnerPaymentViewModel> searchedOwnerPayments = new List<OwnerPaymentViewModel>();
                var searchfield1 = searchfield.ToUpper();

                int PaymentID = 0;

                foreach (var ownerPayment in OwnerPaymentList)
                {
                    PaymentID = 0;
                    PaymentID = ownerPayment.PaymentID;

                    var SlotId = "";
                    if (ownerPayment.SlotId != null)
                    {
                        SlotId = ownerPayment.SlotId.ToUpper();
                    }
                    var ReceiverName = "";
                    if (ownerPayment.ReceiverName != null)
                    {
                        ReceiverName = ownerPayment.ReceiverName.ToUpper();
                    }
                    var ReceivingBank = "";
                    if (ownerPayment.ReceivingBank != null)
                    {
                        ReceivingBank = ownerPayment.ReceivingBank.ToUpper();
                    }
                    var Amount = "";
                    if (Convert.ToString(ownerPayment.Amount) != null)
                    {
                        Amount = Convert.ToString(ownerPayment.Amount).ToUpper();
                    }

                    var FromMonth = "";
                    if (ownerPayment.FromMonth != null)
                    {
                        FromMonth = ownerPayment.FromMonth?.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var ToMonth = "";
                    if (ownerPayment.ToMonth != null)
                    {
                        ToMonth = ownerPayment.ToMonth?.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var PaymentDate = "";
                    if (ownerPayment.PaymentDate != null)
                    {
                        PaymentDate = ownerPayment.PaymentDate?.ToString("dd-MMM-yyyy").ToUpper();
                    }

                    var PaymentID1 = PaymentID;
                    var SlotId1 = SlotId;
                    var ReceiverName1 = ReceiverName;
                    var ReceivingBank1 = (ReceivingBank == "") ? ("No Bank Data Available").ToUpper() : ReceivingBank;
                    var Amount1 = Amount;
                    var FromMonth1 = FromMonth;
                    var ToMonth1 = ToMonth;
                    var PaymentDate1 = (PaymentDate == "01-Jan-1900") ? ("No Payment Date Available").ToUpper() : PaymentDate;

                    if (PaymentID1.ToString().Contains(searchfield1) == true || SlotId1.Contains(searchfield1) == true ||
                        ReceiverName1.Contains(searchfield1) == true || ReceivingBank1.Contains(searchfield1) == true
                        || Amount1.Contains(searchfield1) == true || FromMonth1.Contains(searchfield1) == true
                        || ToMonth1.Contains(searchfield1) == true || PaymentDate1.Contains(searchfield1) == true)
                    {
                        OwnerPaymentViewModel ownerPaymentObj = new OwnerPaymentViewModel();
                        ownerPaymentObj.PaymentID = ownerPayment.PaymentID;
                        ownerPaymentObj.SlotId = ownerPayment.SlotId;
                        ownerPaymentObj.ReceiverName = ownerPayment.ReceiverName;
                        ownerPaymentObj.ReceivingBank = ownerPayment.ReceivingBank;
                        ownerPaymentObj.Amount = ownerPayment.Amount;
                        ownerPaymentObj.FromMonth = ownerPayment.FromMonth?.ToString("dd-MMM-yyyy");
                        ownerPaymentObj.ToMonth = ownerPayment.ToMonth?.ToString("dd-MMM-yyyy");
                        ownerPaymentObj.PaymentDate = ownerPayment.PaymentDate?.ToString("dd-MMM-yyyy");
                        searchedOwnerPayments.Add(ownerPaymentObj);
                    }

                }
                string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    //return View(Json("Error"));
                }
                //OwnerList mdl = new OwnerList()
                //{
                //    index = 1,
                //    NoOfOwner = 0,
                //    Owners = searchedOwner,
                //    TaskRoles = taskRoles
                //};
                return Json(searchedOwnerPayments);

            }
        }


        [HttpGet]
        public ActionResult OwnerPaymentView(int? paymentId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var mdl = adbc.OwnerPayments.FirstOrDefault(o => o.PaymentID == paymentId);
                return View(mdl);
            }
        }

        [HttpGet]
        public ActionResult OwnerPaymentEdit(int? paymentId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Dividend\\OwnerPaymentManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }
                var mdl = adbc.OwnerPayments.FirstOrDefault(o => o.PaymentID == paymentId);
                return View(mdl);
            }
        }

        [HttpPost]
        public ActionResult OwnerPaymentEdit(OwnersPayment mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var OwnersPaymentObjInDb = adbc.OwnerPayments.FirstOrDefault(o => o.PaymentID == mdl.PaymentID);
                if (mdl.PaymentDate != DateTime.MinValue)
                {
                    OwnersPaymentObjInDb.PaymentDate = mdl.PaymentDate;
                }
                else
                {
                    OwnersPaymentObjInDb.PaymentDate = Convert.ToDateTime("1900-01-01 00:00:00.000");
                }
                OwnersPaymentObjInDb.ReceivingBank = mdl.ReceivingBank;
                adbc.SaveChanges();
                return RedirectToAction("OwnerPaymentManagement");
            }
        }
    }
}