﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;

namespace StarterProjectV2.Controllers
{
    public class AccommodationController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult AccommodationManagementtable()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var AccommodationInfoList = adbc.AccomadationInfos.ToList();
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Accommodation\\AccommodationManagementtable").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }
                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }

                AccommodationmanagementTableViewModel mdl = new AccommodationmanagementTableViewModel()
                {
              
                    AccommodationInfos = AccommodationInfoList,
                    TaskRoles = taskRoles
                };
                return View(mdl);
            }
        }

        [HttpGet]
        public ActionResult AccommodationView(string AccommodationInfoId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var obj = adbc.AccomadationInfos.Where(a => a.AccommodationInfoId == AccommodationInfoId).FirstOrDefault();
                return View(obj);
            }
        }

        [HttpGet]
        public ActionResult AccommodationEdit(string AccommodationInfoId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Accommodation\\AccommodationManagementtable").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[3] == '0')
                {
                    return View("Error");
                }
                var obj = adbc.AccomadationInfos.Where(a => a.AccommodationInfoId == AccommodationInfoId).FirstOrDefault();
                return View(obj);
            }
        }

        [HttpPost]
        public ActionResult AccommodationEdit(AccommodationInfo mdl)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var AccommodationInfoObj = adbc.AccomadationInfos.Where(a=>a.AccommodationInfoId == mdl.AccommodationInfoId).FirstOrDefault();
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Accommodation\\AccommodationManagementtable").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                double TotalSquareFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1")
                    .Select(c => c.TotalSquareFeet).FirstOrDefault();

                AccommodationInfoObj.MonthlyExpense = mdl.MonthlyExpense;
                AccommodationInfoObj.MonthlyIncome = mdl.MonthlyIncome;
                AccommodationInfoObj.MonthlyPerSquareFeetRate =
                    (mdl.MonthlyIncome - mdl.MonthlyExpense) / TotalSquareFeet;

                adbc.SaveChanges();

                var AccommodationInfoList = adbc.AccomadationInfos.ToList();

                AccommodationmanagementTableViewModel model = new AccommodationmanagementTableViewModel()
                {
                    AccommodationInfos = AccommodationInfoList,
                    TaskRoles = taskRoles
                };
                return View("AccommodationManagementtable", model);
            }
        }

        [HttpGet]
        public ActionResult AccommodationManagement()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var RoomList = adbc.RoomInfos.Select(r => r.RoomNo).ToList();
                var RoomCriteriaLists = adbc.RoomInfos.Select(r => r.OwnershipCriteria).ToList();

                List<string> RoomInfoLists = new List<string>();

                int ListCount = RoomList.Count;

                for (int i = 0; i < ListCount; i++)
                {
                    string RoomInfo = RoomList[i] + " (" + RoomCriteriaLists[i] + ")";
                    RoomInfoLists.Add(RoomInfo);
                }

                double TotalSquareFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet)
                    .FirstOrDefault();
                AccommodationManagementViewModel mdl = new AccommodationManagementViewModel()
                {
                    RoomInfoLists = RoomList,
                    RoomCriteriaLists = RoomCriteriaLists,
                    TotalSquareFeet = TotalSquareFeet
                };
                return View(mdl);
            }
        }

        [HttpPost]
        public JsonResult gettingAccommodationDate(string acc_date)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var AccommodationList = adbc.AccomadationInfos.Where(a => a.AccommodationDate == acc_date).ToList();
                List<AccommodationInfo> AccObjList = new List<AccommodationInfo>();
                foreach (var acc in AccommodationList)
                {
                    AccommodationInfo obj = new AccommodationInfo()
                    {
                        IsDayClosed = acc.IsDayClosed,
                        AccommodationDate = acc.AccommodationDate,
                        AccommodationInfoId = acc.AccommodationInfoId,
                        MonthlyExpense = acc.MonthlyExpense,
                        MonthlyIncome = acc.MonthlyIncome,
                        MonthlyPerSquareFeetRate = acc.MonthlyPerSquareFeetRate
                    };
                    AccObjList.Add(obj);
                }

                return Json(AccObjList);
            }

        }

        [HttpPost]
        public JsonResult refreshingAccommodationList(AccommodationInfo mdl)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var accommodationInfosList = adbc.AccomadationInfos.ToList();

                List<int> accommodationInfosListIds = new List<int>();

                foreach (var accommodationInfo in accommodationInfosList)
                {
                    var id = int.Parse(accommodationInfo.AccommodationInfoId.Split('-')[1]);
                    accommodationInfosListIds.Add(id);
                }



                var accommodationInfosCount = accommodationInfosList.Count;

                if (accommodationInfosCount > 0)
                {
                    accommodationInfosListIds.Sort();

                    accommodationInfosCount = accommodationInfosListIds[accommodationInfosListIds.Count - 1];

                }

                accommodationInfosCount++;
                double TotalSquareFeet = adbc.CompanyInfoes.Where(c => c.CompanyInfoId == "CMP-1").Select(c => c.TotalSquareFeet)
                    .FirstOrDefault();

                AccommodationInfo obj = new AccommodationInfo()
                {
//                    IsDayClosed = 0,
                    AccommodationDate = mdl.AccommodationDate,
                    MonthlyExpense = mdl.MonthlyExpense,
                    MonthlyIncome = mdl.MonthlyIncome,
                    AccommodationInfoId = "ACC-" + accommodationInfosCount,
                    MonthlyPerSquareFeetRate = (mdl.MonthlyIncome - mdl.MonthlyExpense) / TotalSquareFeet
                };
                accommodationInfosList.Add(obj);
                adbc.AccomadationInfos.Add(obj);
                adbc.SaveChanges();

                List<AccommodationInfo> tempInfoList = new List<AccommodationInfo>()
                {
                    obj
                };
                

                return Json(tempInfoList);
            }
        }



        [HttpPost]
        public JsonResult gettingAccommodationDateforDayClosing(string acc_date)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var AccommodationList = adbc.AccomadationInfos.Where(a => a.AccommodationDate == acc_date).ToList();

                foreach (var accomodateInfo in AccommodationList)
                {
                    accomodateInfo.IsDayClosed = 1;
                }

                List<AccommodationInfo> AccObjList = new List<AccommodationInfo>();
                foreach (var acc in AccommodationList)
                {
                    AccommodationInfo obj = new AccommodationInfo()
                    {
                        IsDayClosed = acc.IsDayClosed,
                        AccommodationDate = acc.AccommodationDate,
                        AccommodationInfoId = acc.AccommodationInfoId,
                        //                        NonPaymentReason = acc.NonPaymentReason,
                        MonthlyExpense = acc.MonthlyExpense,
                        MonthlyIncome = acc.MonthlyIncome,
//                        RoomNo = adbc.RoomInfos.Where(r => r.RoomId == acc.RoomNo).Select(r => r.RoomNo)
//                            .FirstOrDefault(),
                    };
                    AccObjList.Add(obj);
                }

                

                adbc.SaveChanges();
                return Json(AccObjList);
            }
        }


        [HttpPost]

        public JsonResult gettingRoomListForAccommodationDate(string acc_date)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var AccommodationList = adbc.AccomadationInfos.Where(a => a.AccommodationDate == acc_date).ToList();

                List<AccommodationInfo> AccObjList = new List<AccommodationInfo>();
                foreach (var acc in AccommodationList)
                {
                    AccommodationInfo obj = new AccommodationInfo()
                    {
                        IsDayClosed = acc.IsDayClosed,
                        AccommodationDate = acc.AccommodationDate,
                        AccommodationInfoId = acc.AccommodationInfoId,
                        //                        NonPaymentReason = acc.NonPaymentReason,
                        MonthlyExpense = acc.MonthlyExpense,
                        MonthlyIncome = acc.MonthlyIncome,

                    };
                    AccObjList.Add(obj);
                }



                //adbc.SaveChanges();
                return Json(AccObjList);
            }
        }

    }
}