﻿using StarterProjectV2.Models;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StarterProjectV2.Controllers
{
    public class TaskController : Controller
    {
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult TaskManagement(string searchfield="")
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];
                string taskId = db.Tasks.Where(u=>u.TaskPath == "Task\\TaskManagement").Select(u=>u.TaskId).FirstOrDefault();


                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles != "")
                {
                    if (taskRoles[0] == '0')
                    {
                        return View("Error");
                    }


                }
                else
                {
                    return View("Error");

                }
               
                var TaskObject = db.Tasks.ToList();
                var ModuleObject = db.Modules.ToList();
                var query = (from a in TaskObject
                             join b in ModuleObject on a.moduleId equals b.ModuleId
                             select new { a, b.ModuleName }).ToList();
                
                searchfield = searchfield.ToUpper();
                List<Tuple<Task, string>> list = new List<Tuple<Task, string>>();
                foreach (var q in query)
                {
                    if (q.a.TaskName.ToUpper().Contains(searchfield))
                    {
                        Tuple<Task, string> obj = Tuple.Create(q.a, q.ModuleName);
                        list.Add(obj);
                    }
                }
                IEnumerable<Tuple<Task, string>> taskList = list.AsEnumerable();

                var model = new TaskList()
                {
                    tasks = taskList,
                    TaskRoles = taskRoles,
                    SearchField = searchfield,
                };

                return View(model);
            }

        }

  
        [HttpGet]
        public ActionResult TaskCreate()
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {


                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Task\\TaskManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles==""||taskRoles[1] == '0')
                {
                    return View("Error");
                }

                var module_obj = adbc.Modules.ToList();
                module_obj.Insert(0, new Module()
                {
                    ModuleName = "----",
                    ModuleId = "-100"
                });
                var task_obj = adbc.Tasks.ToList();
                var model = new TaskCreate()
                {
                    TaskModule = "",
                    TaskController = "",
                    TaskId = "",
                    TaskName = "",
                    TaskOrder = 0,
                    TaskIdandParent = task_obj,
                    isTaskFound = false,
                    isTaskEdited = false,
                    isTaskAdded = false,
                    isTaskDeleted = false,
                    isTaskPrinted = false,
                    isTaskViewed = false,
                    isTaskCancelled = false,
                    isTaskSaved = false,
                    isTaskReseted = false,
                    ModuleIdandParent = module_obj
                };
                //adbc.Modules.Remove(delobj);
                //adbc.SaveChanges();
                return View("TaskCreate", model);
            }
        }
        [HttpPost]
        public ActionResult TaskCreate(TaskCreate newTask)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }

            if (ModelState.IsValid)
            {
                using (ApplicationDbContext adbc = new ApplicationDbContext())
                {
                    string isTaskViewed = (newTask.isTaskViewed) ? "1" : "0";
                    string isTaskAdded = (newTask.isTaskAdded) ? "1" : "0";
                    string isTaskSaved = (newTask.isTaskSaved) ? "1" : "0";
                    string isTaskEdited = (newTask.isTaskEdited) ? "1" : "0";
                    string isTaskDeleted = (newTask.isTaskDeleted) ? "1" : "0";
                    string isTaskPrinted = (newTask.isTaskPrinted) ? "1" : "0";
                    string isTaskCancelled = (newTask.isTaskCancelled) ? "1" : "0";
                    string isTaskReseted = (newTask.isTaskReseted) ? "1" : "0";
                    string isTaskFound = (newTask.isTaskFound) ? "1" : "0";

                    if (string.IsNullOrEmpty(newTask.TaskId))
                    {
                        
                        var taskList = adbc.Tasks.ToList();
                        var taskIdList = new List<int>();

                        foreach (var task in taskList)
                        {
                            taskIdList.Add(int.Parse(task.TaskId.ToString().Split('-')[1]));
                        }


                        var taskToken = taskList.Count();
                        if (taskToken > 0)
                        {
                            taskIdList.Sort();
                            taskToken = taskIdList[taskIdList.Count - 1];
                        }
                        taskToken++;

                        var newATask = new Task()
                        {
                            TaskId = "TSK-" + taskToken,
                            TaskName = newTask.TaskName,
                            TaskPath = newTask.TaskController + "\\"+newTask.TaskModule,
                            TaskOrder = newTask.TaskOrder,
                            TaskParent = newTask.selectedTaskId,
                            moduleId = newTask.selectedModuleId,
                            TaskEvent = isTaskViewed + isTaskAdded + isTaskSaved + isTaskEdited + isTaskDeleted +
                                        isTaskPrinted + isTaskCancelled + isTaskReseted + isTaskFound + ""
                        };
                        adbc.Tasks.Add(newATask);
                    }
                    else
                    {
                        var taskInDb = adbc.Tasks.SingleOrDefault(m => m.TaskId == newTask.TaskId);
                        taskInDb.TaskName = newTask.TaskName;
                        taskInDb.TaskOrder = newTask.TaskOrder;
                        taskInDb.TaskParent = (newTask.selectedTaskId == null) ? "0" : newTask.selectedTaskId;
                        taskInDb.moduleId = newTask.selectedModuleId;
                        taskInDb.TaskEvent = isTaskViewed + isTaskAdded + isTaskSaved + isTaskEdited + isTaskDeleted +
                                             isTaskPrinted + isTaskCancelled + isTaskReseted + isTaskFound + "";
                        taskInDb.TaskPath = newTask.TaskController + "\\" + newTask.TaskModule;

                    }

                    adbc.SaveChanges();
                    Session[SessionKeys.MODULE_LIST] = Common.getMenuList(Session[SessionKeys.USERNAME].ToString());
                    var userName = Session[SessionKeys.USERNAME].ToString();
                    var userRole = adbc.Users.FirstOrDefault(u => u.UserName == userName).UserType.ToString();
                    Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(userRole);
                    //Session[SessionKeys.ROLE_LIST] = Common.getTasksWithRoleForUser(Session[SessionKeys.USERNAME].ToString());
                    return RedirectToAction("TaskManagement");
                }
            }

            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult gettingTaskList(string moduleId)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var taskList = adbc.Tasks.Where(m => m.moduleId == moduleId).ToList();
                List<SelectListItem> someArray = new List<SelectListItem>();
                foreach (var x in taskList)
                {
                    var nm = new SelectListItem()
                    {
                        Text = x.TaskName,
                        Value = x.TaskId
                    };
                    someArray.Add(nm);
                }

                return Json(someArray, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult TaskDelete(string taskId)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string tskId = adbc.Tasks.Where(u => u.TaskPath == "Task\\TaskManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == tskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles == "" ||  taskRoles[4] == '0')
                {
                    return View("Error");
                }

                var delobj = adbc.Tasks.Where(p => p.TaskId == taskId).SingleOrDefault();
                adbc.Tasks.Remove(delobj);
                adbc.SaveChanges();
                Session[SessionKeys.MODULE_LIST] = Common.getMenuList(Session[SessionKeys.USERNAME].ToString());
                var userName = Session[SessionKeys.USERNAME].ToString();
                var userRole = adbc.Users.FirstOrDefault(u => u.UserName == userName).UserType.ToString();
                Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(userRole);
                //Session[SessionKeys.ROLE_LIST] = Common.getTasksWithRoleForUser(Session[SessionKeys.USERNAME].ToString());
                return RedirectToAction("TaskManagement");
            }
            //return Content(moduleId);
        }

        [HttpGet]
        public ActionResult TaskEdit(string taskId)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string tskId = adbc.Tasks.Where(u => u.TaskPath == "Task\\TaskManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == tskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles=="" || taskRoles[3] == '0')
                {
                    return View("Error");
                }

                var obj = adbc.Tasks.Where(p => p.TaskId == taskId).SingleOrDefault();
                var module_obj = adbc.Modules.ToList();
                module_obj.Insert(0, new Module()
                {
                    ModuleName = "----",
                    ModuleId = "-100"
                });
                var task_obj = adbc.Tasks.ToList();
              
                var model = new TaskCreate()
                {
                    TaskId = obj.TaskId,
                    TaskName = obj.TaskName,
                    TaskController = obj.TaskPath.Split('\\')[0],
                    TaskModule = obj.TaskPath.Split('\\')[1],
                    TaskOrder = obj.TaskOrder,
                    isTaskViewed = (obj.TaskEvent[0] == '1') ? true : false,
                    isTaskAdded = (obj.TaskEvent[1] == '1') ? true : false,
                    isTaskSaved = (obj.TaskEvent[2] == '1') ? true : false,
                    isTaskEdited = (obj.TaskEvent[3] == '1') ? true : false,
                    isTaskDeleted = (obj.TaskEvent[4] == '1') ? true : false,
                    isTaskPrinted = (obj.TaskEvent[5] == '1') ? true : false,
                    isTaskCancelled = (obj.TaskEvent[6] == '1') ? true : false,
                    isTaskReseted = (obj.TaskEvent[7] == '1') ? true : false,
                    isTaskFound = (obj.TaskEvent[8] == '1') ? true : false,
                    selectedModuleId = obj.moduleId,
                    selectedTaskId = obj.TaskParent,
                    TaskIdandParent = task_obj,
                    ModuleIdandParent = module_obj
                };
                return View("TaskCreate", model);
            }
        }

        [HttpGet]
        public ActionResult TaskView(string taskId)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts", new { });
            }

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string tskId = adbc.Tasks.Where(u => u.TaskPath == "Task\\TaskManagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == tskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }

                if (taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var obj = adbc.Tasks.Where(p => p.TaskId == taskId).SingleOrDefault();
                var model = new TaskCreate()
                {
                    TaskId = obj.TaskId,
                    TaskName = obj.TaskName,
                    TaskController = obj.TaskPath.Split('\\')[0],
                    TaskModule = obj.TaskPath.Split('\\')[1],
                    TaskOrder = obj.TaskOrder,
                    isTaskViewed = (obj.TaskEvent[0] == '1') ? true : false,
                    isTaskAdded = (obj.TaskEvent[1] == '1') ? true : false,
                    isTaskSaved = (obj.TaskEvent[2] == '1') ? true : false,
                    isTaskEdited = (obj.TaskEvent[3] == '1') ? true : false,
                    isTaskDeleted = (obj.TaskEvent[4] == '1') ? true : false,
                    isTaskPrinted = (obj.TaskEvent[5] == '1') ? true : false,
                    isTaskCancelled = (obj.TaskEvent[6] == '1') ? true : false,
                    isTaskReseted = (obj.TaskEvent[7] == '1') ? true : false,
                    isTaskFound = (obj.TaskEvent[8] == '1') ? true : false,
                    getModuleName = adbc.Modules.Where(p => p.ModuleId == obj.moduleId).Select(p => p.ModuleName).FirstOrDefault(),
                    getTaskName = adbc.Tasks.Where(p => p.TaskId == obj.TaskParent).Select(p => p.TaskName).FirstOrDefault()
                };
                //adbc.Modules.Remove(delobj);
                //adbc.SaveChanges();
                return View("TaskView", model);
            }
            
        }



        [HttpGet]
        public ActionResult TaskReport()
        {
            return Redirect("../Reports/TaskReport.aspx");
        }

    }
}