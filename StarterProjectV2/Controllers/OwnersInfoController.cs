﻿using StarterProjectV2.Models;
using StarterProjectV2.Services;
using StarterProjectV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace StarterProjectV2.Controllers
{
    public partial class OwnersInfoController : Controller
    {
        private readonly BookingService _bookingService = new BookingService();
        private readonly ConfigurationService _configurationService = new ConfigurationService();
        private readonly CommonService _commonService = new CommonService();
        private readonly ApplicationDbContext _dbContext = new ApplicationDbContext();
        private readonly OwnerInfoService _ownerInfoService = new OwnerInfoService();
        private readonly FileService _fileService = new FileService();

        public bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetSlotIdList(string term)
        {
            List<Select2Model> list = _commonService.GetSlotIdList(term);

            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CalendarView(int offset = 0)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            TempData["CalendarView"] = "CalendarView";


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var today = DateTime.Today.AddDays(15 * offset);
                var finalDate = today.AddDays(15);

                ////Get Booking Information
                List<OwnerRoomBooking> selectedBookings = adbc.OwnerRoomBookings.Where(o => o.CheckOutDate >= today && o.CheckInDate < finalDate && o.IsBookingCompleted != "Cancelled").ToList();
                if (offset < 0)
                {
                    selectedBookings = selectedBookings.Where(o => o.HasStayedOrNot == "Yes").ToList();
                }
                else if (offset == 0)
                {
                    selectedBookings = selectedBookings.Where(o => o.HasStayedOrNot == "Yes" || ((o.HasStayedOrNot == "No" || String.IsNullOrEmpty(o.HasStayedOrNot)) && o.CheckInDate >= today)).ToList();
                }

                ////Initialize Matrix
                int i = 0; var date = today;
                String[,] matrix = new String[40, 16];
                string[,] matrixColor = new string[37, 16];
                string[,] matrixBooking = new string[37, 16];
                var CalendarViewmodel = new CalendarViewModel()
                {
                    calendarMatrix = matrix,
                    calendarMatrixColor = matrixColor,
                    calendarMatrixBookingId = matrixBooking,
                    Offset = offset
                };
                CalendarViewmodel.GenerateColor();
                for (i = 1; i < 16; i++)
                {
                    int j = 1;
                    for (; j <= 36; j++)
                    {
                        CalendarViewmodel.calendarMatrix[j, i] = "";
                        CalendarViewmodel.calendarMatrixColor[j, i] = "#FFFFFF";
                    }
                }

                ////Load Matrix
                {
                    ////Today bookings
                    List<OwnerRoomBooking> todayBookings = selectedBookings.Where(o => o.CheckOutDate == today).ToList();
                    int j = 1;
                    foreach (var booking in todayBookings)
                    {
                        var slotid = booking.SlotId;
                        var rooms = booking.NoOfRoomRequested;
                        var bookingId = booking.OwnerRoomBookngId;

                        CalendarViewmodel.setMatrix(j, 0, slotid, 1, rooms, bookingId);
                        j += rooms;
                    }

                    ////Previous bookings
                    List<OwnerRoomBooking> previousBookings = selectedBookings.Where(o => o.CheckOutDate >= today && o.CheckInDate < today).ToList();
                    j = 1;
                    foreach (var booking in previousBookings)
                    {
                        var days = Convert.ToInt16((booking.CheckOutDate - today).TotalDays);
                        var rooms = booking.NoOfRoomRequested; // booking.NoOfRoomAllotted
                        var slotid = booking.SlotId;
                        var bookingId = booking.OwnerRoomBookngId;
                        CalendarViewmodel.setMatrix(j, 1, slotid, days, rooms, bookingId);
                        j += rooms;
                    }
                }

                ////today to final day booking
                for (i = 1; i < 16; i++)
                {
                    date = today.AddDays(i - 1);
                    List<OwnerRoomBooking> todayBookings = selectedBookings.Where(o => o.CheckInDate == date).OrderBy(o => Convert.ToInt16((o.CheckOutDate - o.CheckInDate).TotalDays)).ToList();

                    foreach (var booking in todayBookings)
                    {
                        var days = Convert.ToInt16((booking.CheckOutDate - booking.CheckInDate).TotalDays);
                        var rooms = booking.NoOfRoomRequested;
                        var slotid = booking.SlotId;
                        var bookingId = booking.OwnerRoomBookngId;
                        int j = 1, k2;
                        for (; j < 36; j++)
                        {
                            for (k2 = 0; k2 < rooms; k2++)
                            {
                                if (CalendarViewmodel.calendarMatrix[j + k2, i] != "")
                                {
                                    break;
                                }
                            }
                            if (k2 == rooms) break;
                        }
                        CalendarViewmodel.setMatrix(j, i, slotid, days, rooms, bookingId);
                        j += rooms;
                        //i += days;
                    }
                    int roomsReserved = 0;
                    for (int j = 1; j <= 36; j++)
                    {
                        if (CalendarViewmodel.calendarMatrix[j, i] != "")
                            roomsReserved++;
                    }

                    CalendarViewmodel.calendarMatrix[36 + 2, i] = Convert.ToString(roomsReserved);


                    var temp = adbc.ConfigInvRooms.Where(o => o.FromDate <= date && date <= o.ToDate).ToList().LastOrDefault();
                    int inventorycount;
                    inventorycount = (temp != null) ? temp.InventoryCount : 36;
                    CalendarViewmodel.calendarMatrix[36 + 1, i] = Convert.ToString(inventorycount);

                    int roomsAvailable2 = inventorycount - roomsReserved;
                    CalendarViewmodel.calendarMatrix[36 + 3, i] = Convert.ToString(roomsAvailable2);

                }

                return View("CalendarView", CalendarViewmodel);

            }

        }

        public ActionResult CalendarViewReferral(int offset = 0)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var today = System.DateTime.Today.AddDays(15 * offset);
                var finalDate = today.AddDays(15);
                List<ReferralBooking> selectedBookings = adbc.ReferralBookings.Where(o => today <= o.CheckOutDate && o.CheckInDate < finalDate).ToList();
                int i = 0; var date = today;
                String[,] matrix = new String[39, 16];
                string[,] matrixColor = new string[37, 16];
                string[,] matrixBooking = new string[37, 16];
                var CalendarViewmodel = new CalendarViewModel()
                {
                    calendarMatrix = matrix,
                    calendarMatrixColor = matrixColor,
                    calendarMatrixBookingId = matrixBooking,
                    Offset = offset
                };
                CalendarViewmodel.GenerateColor();
                for (i = 1; i < 16; i++)
                {
                    int j = 1;
                    for (; j <= 36; j++)
                    {
                        CalendarViewmodel.calendarMatrix[j, i] = "";
                        CalendarViewmodel.calendarMatrixColor[j, i] = "#FFFFFF";
                    }
                }
                {
                    List<ReferralBooking> todayBookings = selectedBookings.Where(o => o.CheckOutDate == today).ToList();
                    int j = 1;
                    foreach (var booking in todayBookings)
                    {
                        var slotid = booking.SlotId;
                        var rooms = booking.NoOfRoomRequested;
                        var bookingId = booking.ReferralBookingId;

                        CalendarViewmodel.setMatrix(j, 0, slotid, 1, rooms, bookingId);
                        j += rooms;
                    }

                    List<ReferralBooking> previousBookings = selectedBookings.Where(o => o.CheckOutDate > today && o.CheckInDate <= today).ToList();
                    j = 1;
                    foreach (var booking in previousBookings)
                    {
                        var days = Convert.ToInt16((booking.CheckOutDate - today).TotalDays);
                        var rooms = booking.NoOfRoomRequested; // booking.NoOfRoomAllotted
                        var slotid = booking.SlotId;
                        var bookingId = booking.ReferralBookingId;
                        CalendarViewmodel.setMatrix(j, 1, slotid, days, rooms, bookingId);
                        j += rooms;
                    }
                }
                for (i = 1; i < 16; i++)
                {
                    date = today.AddDays(i - 1);
                    List<ReferralBooking> todayBookings = selectedBookings.Where(o => o.CheckInDate == date).OrderBy(o => Convert.ToInt16((o.CheckOutDate - o.CheckInDate).TotalDays)).ToList();


                    foreach (var booking in todayBookings)
                    {
                        var days = Convert.ToInt16((booking.CheckOutDate - booking.CheckInDate).TotalDays);
                        var rooms = booking.NoOfRoomRequested;
                        var slotid = booking.SlotId;
                        var bookingId = booking.ReferralBookingId;
                        int j = 1, k2;
                        for (; j < 36; j++)
                        {
                            for (k2 = 0; k2 < rooms; k2++)
                            {
                                if (CalendarViewmodel.calendarMatrix[j + k2, i] != "")
                                {
                                    break;
                                }
                            }
                            if (k2 == rooms) break;
                        }
                        CalendarViewmodel.setMatrix(j, i, slotid, days, rooms, bookingId);
                        j += rooms;
                        //i += days;
                    }
                    int roomsReserved = 0;
                    for (int j = 1; j <= 36; j++)
                    {
                        if (CalendarViewmodel.calendarMatrix[j, i] != "")
                            roomsReserved++;
                    }

                    CalendarViewmodel.calendarMatrix[36 + 1, i] = Convert.ToString(roomsReserved);


                    var temp = adbc.ConfigInvRooms.Where(o => o.FromDate <= date && date <= o.ToDate).ToList().LastOrDefault();
                    int roomsAvailable;
                    roomsAvailable = (temp != null) ? temp.InventoryCount : 36;
                    CalendarViewmodel.calendarMatrix[36 + 2, i] = Convert.ToString(roomsAvailable);

                }
                return View("CalendarView", CalendarViewmodel);
            }
        }

        #region Owners Info

        [HttpPost]
        public ActionResult OwnersRoomSlotInfo(OwnersInfoFamilyNomineeAttFilesView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                //string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\OwnersRoomSlotInfo").Select(u => u.TaskId).FirstOrDefault();
                //var roomslotid = db.RoomSlotAssigments.Select(r => r.RoomId == model.RoomId && r => r.RoomId == model.)
                var ownersRoomSlot = adbc.RoomSlotAssigments;
                var s = model.RoomId.ToString() + "," + model.SlotId.ToString();
                var roomslotindb = adbc.RoomSlotAssigments.SingleOrDefault(r => r.RoomSlotText.Contains(s));
                roomslotindb.SoldStatus = "Sold";
                adbc.SaveChanges();





                var SingleOwnersRoomSlots = adbc.OwnerRoomSlotAssigments.ToList();
                //creating a new OwnerRoomSlotAssignment Row 
                var OwnerRoomSlotAssId = "";


                if (string.IsNullOrEmpty(model.OwenerId))
                {
                    var ownerList = adbc.OwnerRoomSlotAssigments.ToList();
                    var ownerIdList = new List<int>();
                    foreach (var owners in ownerList)
                    {
                        ownerIdList.Add(int.Parse(owners.OwnerRoomSlotAssignmentId.ToString().Split('-')[1]));
                    }

                    var ownerToken = ownerList.Count();
                    if (ownerToken > 0)
                    {
                        ownerIdList.Sort();
                        ownerToken = ownerIdList[ownerIdList.Count - 1];
                    }

                    ownerToken++;


                    OwnerRoomSlotAssId = "ORSA-" + ownerToken;
                    OwnerRoomSlotAssignment mdl = new OwnerRoomSlotAssignment()
                    {
                        //mdl.RoomSlotAssignmentId = OwnerRoomSlotAssId;
                        OwnerRoomSlotAssignmentId = OwnerRoomSlotAssId,
                        OwnersInfoId = model.OwenerId,
                        //OwnersInfoId = "OWNER-6",
                        RoomSlotAssigmentId = roomslotindb.RoomSlotAssignmentId,
                        //Rate = (float)1000000.0
                        Rate = (float)(model.Rate)
                    };
                    adbc.OwnerRoomSlotAssigments.Add(mdl);
                    adbc.SaveChanges();
                    return RedirectToAction("RoomSlotManagement", "RoomSlotAssignment");
                }


            }
            return RedirectToAction("RoomSlotManagement", "RoomSlotAssignment");
        }

        [HttpGet]
        public ActionResult OwnerManagement(string searchfield = "", int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            //SendEmailMailGun();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                string taskRoles = _commonService.TaskRole(TaskPath.Ownersinfo_OwnerManagement);

                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }


                var owners = db.OwnersInfos.AsQueryable();

                if (!string.IsNullOrWhiteSpace(searchfield) && searchfield != "null")
                {
                    owners = owners.Where(o => o.SlotId.Contains(searchfield) || o.Name.Contains(searchfield));
                }
                owners = owners.OrderBy(x => x.SlotId);

                int noOfOwners = (owners.Count() + 9) / 10;

                List<OwnersInfo> selectedOwnersInfos = owners.Skip((index - 1) * 10).Take(10).ToList();

                var model = new OwnerList
                {
                    Owners = selectedOwnersInfos,
                    //TaskRoles = taskRoles,
                    NoOfOwner = noOfOwners,
                    index = index,
                    SearchField = searchfield
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult OwnersInfoCreate()
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            string taskRoles = _commonService.TaskRole(TaskPath.Ownersinfo_OwnerManagement);

            if (taskRoles[1] == '0')
            {
                return View("Error");
            }

            var model = new OwnersInfoView()
            {
                OwenerId = "",
                IsEditApprovedList = new List<string>() { "Yes", "No" }
            };
            return View("OwnersInfo", model);
        }

        [HttpGet]
        public ActionResult OwnersInfoView(string ownerId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");


            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var model = adbc.OwnersInfos.Where(p => p.OwenerId == ownerId).SingleOrDefault();

                return View("OwnersInfoView", model);
            }
        }

        [HttpGet]
        public ActionResult OwnersInfoEdit(string ownerId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            string taskRoles = _commonService.TaskRole(TaskPath.Ownersinfo_OwnerManagement);

            if (taskRoles[3] == '0')
            {
                return View("Error");
            }

            var info = _dbContext.OwnersInfos.Where(p => p.OwenerId == ownerId).SingleOrDefault();

            var ownerInfo = new OwnersInfoView()
            {
                OwenerId = info.OwenerId,
                DateOfBirth = info.DateOfBirth,
                Name = info.Name,
                FatherName = info.FatherName,
                MotherName = info.MotherName,
                SpouseName = info.SpouseName,
                MarriageDate = info.MarriageDate,
                PresentAddress = info.PresentAddress,
                PermanantAddress = info.PermanantAddress,
                NID = info.NID,
                TIN = info.TIN,
                Mobile = info.Mobile,
                Email = info.Email,
                NoofSlots = info.NoofSlots,
                AdditionalId = info.AdditionalId,
                AccountNo = info.AccountNo,
                BankName = info.BankName,
                BranchName = info.BranchName,
                RoutingNo = info.RoutingNo,
                AdditionalInformation = info.AdditionalInformation,
                AdminComment = info.AdminComment,
                SlotId = info.SlotId,
                OwnerSmartId = info.OwnerSmartId,
                PicturePath = info.PicturePath,
                IsEditApprovedList = new List<string>() { "Yes", "No" },
                Remarks = info.Remarks,
            };

            return View("OwnersInfo", ownerInfo);
        }

        [HttpPost]
        public ActionResult OwnersInfoDelete(string ownerId)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                string taskId = adbc.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles[4] == '0')
                {
                    return View("Error");
                }
                var delobj = adbc.OwnersInfos.Where(p => p.OwenerId == ownerId).SingleOrDefault();
                adbc.OwnersInfos.Remove(delobj);
                adbc.SaveChanges();
                return RedirectToAction("OwnerManagement");
            }
        }

        [HttpPost]
        public ActionResult OwnersInfoAddorEdit(OwnersInfoView info)
        {
            if (!IsUserLoggedIn())
            {
                return RedirectToAction("Login", "Accounts");
            }

            ResponseModel response = new ResponseModel();
            string ownerId = "";

            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                if (string.IsNullOrEmpty(info.OwenerId))
                {
                    _ownerInfoService.InsertOwnerInfo(info, out OwnersInfo model);
                    ownerId = model.OwenerId;

                    var modelAdditionalInformation = new OwnersInfoFamilyNomineeAttFilesView()
                    {
                        OFOwnerId = model.OwenerId,
                        OAOwnerId = model.OwenerId,
                        ONOwnerId = model.OwenerId,
                        OPOwnerId = model.OwenerId,
                        ORSAOwnerId = model.OwenerId,
                        OwenerId = model.OwenerId,
                        SlotId = model.SlotId,
                    };
                    return RedirectToAction("AdditionalInformation", modelAdditionalInformation);
                }
                else
                {
                    if (info.IsEditApproved == "Yes" || string.IsNullOrEmpty(info.IsEditApproved) == true)
                    {
                        var ownerInDb = adbc.OwnersInfos.FirstOrDefault(u => u.OwenerId == info.OwenerId);

                        var ownerOneTimeLoginOTPObject = adbc.OwnerOneTimeLoginOtps
                            .Where(o => o.OwnerSlotId == ownerInDb.SlotId).FirstOrDefault();

                        ownerId = ownerInDb.ToString();

                        ownerInDb.DateOfBirth = info.DateOfBirth;
                        ownerInDb.Email = info.Email;
                        ownerInDb.MarriageDate = info.MarriageDate;
                        ownerInDb.Mobile = info.Mobile;
                        ownerInDb.NID = info.NID;
                        ownerInDb.Name = info.Name;
                        ownerInDb.TIN = info.TIN;
                        ownerInDb.FatherName = info.FatherName;
                        ownerInDb.MotherName = info.MotherName;
                        ownerInDb.PermanantAddress = info.PermanantAddress;
                        ownerInDb.PresentAddress = info.PresentAddress;
                        ownerInDb.SpouseName = info.SpouseName;
                        ownerInDb.RoutingNo = info.RoutingNo;
                        ownerInDb.OwnerSmartId = info.OwnerSmartId;

                        ownerInDb.SlotId = info.SlotId;
                        ownerInDb.AccountNo = info.AccountNo;
                        ownerInDb.AdditionalId = info.AdditionalId;
                        ownerInDb.AdditionalInformation = info.AdditionalInformation;
                        ownerInDb.AdminComment = info.AdminComment;
                        ownerInDb.BankName = info.BankName;
                        ownerInDb.BranchName = info.BranchName;
                        ownerInDb.NoofSlots = info.NoofSlots;
                        ownerInDb.Remarks = info.Remarks;
                        ownerInDb.UnknownColumn = info.UnknownColumn;
                        ownerInDb.OwnersLastEditTime = DateTime.Now;
                        ownerInDb.IsEditApproved = info.IsEditApproved; // Approved 

                        adbc.SaveChanges();
                    }
                    else if (info.IsEditApproved == "No")
                    {
                        var auditList = adbc.Audit2.Where((u => u.TableIdValue == info.OwenerId)).ToList().OrderByDescending(o => o.Id);
                        var audit = auditList.FirstOrDefault();

                        var ownerInDb = adbc.OwnersInfos.SingleOrDefault(u => u.OwenerId == info.OwenerId);
                        var oldData = audit.OldData;
                        var oldDataList = oldData.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                        //Output:  oldDataList[0] = "Mobile=0134816348"
                        //oldDataList[1] = "Email=hasdjkfasdkj@sjfa.com"
                        string Mobile = info.Mobile, Email = info.Email, PresentAddress = info.PresentAddress, PermanantAddress = info.PermanantAddress;
                        for (int i = 0; i < oldDataList.Count(); i++)
                        {
                            var key = oldDataList[i].Split(new string[] { "=" }, StringSplitOptions.None)[0];
                            var value = oldDataList[i].Split(new string[] { "=" }, StringSplitOptions.None)[1];

                            if (key == "Mobile") Mobile = value;
                            else if (key == "Email") Email = value;
                            else if (key == "PresentAddress") PresentAddress = value;
                            else if (key == "PermanantAddress") PermanantAddress = value;
                        }

                        ownerInDb.DateOfBirth = info.DateOfBirth;
                        ownerInDb.Email = Email;
                        ownerInDb.MarriageDate = info.MarriageDate;
                        ownerInDb.Mobile = Mobile;
                        ownerInDb.NID = info.NID;
                        ownerInDb.Name = info.Name;
                        ownerInDb.TIN = info.TIN;
                        ownerInDb.FatherName = info.FatherName;
                        ownerInDb.MotherName = info.MotherName;
                        ownerInDb.PermanantAddress = PermanantAddress;
                        ownerInDb.PresentAddress = PresentAddress;
                        ownerInDb.SpouseName = info.SpouseName;

                        ownerInDb.OwnerSmartId = info.OwnerSmartId;
                        ownerInDb.RoutingNo = info.RoutingNo;
                        ownerInDb.SlotId = info.SlotId;
                        ownerInDb.AccountNo = info.AccountNo;
                        ownerInDb.AdditionalId = info.AdditionalId;
                        ownerInDb.BankName = info.BankName;
                        ownerInDb.BranchName = info.BranchName;
                        ownerInDb.NoofSlots = info.NoofSlots;
                        ownerInDb.Remarks = info.Remarks;
                        ownerInDb.UnknownColumn = info.UnknownColumn;
                        ownerInDb.OwnersLastEditTime = DateTime.Now;
                        ownerInDb.IsEditApproved = info.IsEditApproved; // Approved 

                        adbc.SaveChanges();
                    }
                }

                var ownerFamilyForEdit = new OwnersInfoFamilyNomineeAttFilesView()
                {
                    OFOwnerId = ownerId,
                    OAOwnerId = ownerId,
                    ONOwnerId = ownerId,
                    OPOwnerId = ownerId,
                    ORSAOwnerId = ownerId,
                    OwenerId = ownerId,
                    SlotId = info.SlotId,
                };

                return RedirectToAction("OwnerManagement");
            }
        }


        #endregion

        [HttpGet]
        public ActionResult AdditionalInformation(OwnersInfoFamilyNomineeAttFilesView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            if (string.IsNullOrWhiteSpace(model.OwenerId))
            {
                throw new Exception(Messages.OwnerIdNotFound);
            }

            if (string.IsNullOrWhiteSpace(model.SlotId))
            {
                var basicInfo = _ownerInfoService.GetOwnerBasicInfo(null, model.OwenerId);
                model.SlotId = basicInfo.SlotId;
            }

            if (!string.IsNullOrWhiteSpace(model.SlotId))
            {
                model.TransactionHistory = _ownerInfoService.GetOwnerTransactionHistory(model.SlotId);
            }
            if (!string.IsNullOrWhiteSpace(model.OwenerId))
            {
                #region Attachment

                model.AttachmentView = _ownerInfoService.GetOwnersAttachmentView(model.OwenerId);

                if (model.AttachmentView == null || model.AttachmentView.OwnerId == null)
                {
                    model.AttachmentView.OwnerId = model.OwenerId;
                }
                if (model.AttachmentView.SlotId == null)
                {
                    model.AttachmentView.SlotId = model.SlotId;
                }

                #endregion

                #region Family

                model.OwnersFamily = _ownerInfoService.GetOwnerFamilyViewModel(model.OwenerId);
                model.OwnersFamilyList = _ownerInfoService.GetOwnerFamilyViewList(model.OwenerId).ToList();

                #endregion

                #region Nominee

                model.OwnersNomineeList = _ownerInfoService.GetOwnersNomineeViewModelList(model.OwenerId).ToList();

                #endregion
            }

            model.Rooms = _ownerInfoService.GetRooms();

            return View(model);
        }

        #region Transaction History

        [HttpPost]
        public ActionResult TransactionHistoryAddorEdit(OwnersInfoFamilyNomineeAttFilesView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            ResponseModel response = _ownerInfoService.InsertUpdateTransactionHistory(model.TransactionHistory);

            return RedirectToAction("AdditionalInformation", new { OwenerId = model.OwenerId });
        }

        #endregion

        #region Project

        [HttpPost]
        public ActionResult OwnersProjectInfoAddorEdit(OwnersProject ownerProjectInfo)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            if (ModelState.IsValid)
            {
                using (ApplicationDbContext adbc = new ApplicationDbContext())
                {
                    //new Owner : Add Mode
                    if (string.IsNullOrEmpty(ownerProjectInfo.OwnersProjectId))
                    {
                        //existing Owner: Add Mode
                        var ownerProjectList = adbc.OwnersProjects.ToList();
                        var ownerProjectIdList = new List<int>();
                        foreach (var ownersProject in ownerProjectList)
                        {
                            ownerProjectIdList.Add(int.Parse(ownersProject.OwnersProjectId.Split('-')[1]));
                        }

                        var ownerProjectIdToken = ownerProjectList.Count();
                        if (ownerProjectIdToken > 0)
                        {
                            ownerProjectIdList.Sort();
                            ownerProjectIdToken = ownerProjectIdList[ownerProjectIdList.Count - 1];
                        }

                        ownerProjectIdToken++;
                        ownerProjectInfo.OwnersProjectId = "OWP-" + ownerProjectIdToken;
                        adbc.OwnersProjects.Add(ownerProjectInfo);

                        //Audit objectAudit = new Audit()
                        //{
                        //    AuditDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                        //    LogInIp = Session[SessionKeys.PC_NAME].ToString() + " " + Session[SessionKeys.EXTERNAL_LOGIN_IP_ADDRESS].ToString(),
                        //    TableName = "OwnersProjects",
                        //    UserLoginDTTM = "0",
                        //    UserLogoutDTTM = "0",
                        //    UserName = Session[SessionKeys.USERNAME].ToString(),
                        //    TaskName = "Adding owners project info",
                        //    ActionString = "AddingOwnerProjectInfo",
                        //    ActionDetails = "UserName: " + Session[SessionKeys.USERNAME].ToString() + " added owners project info on: " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm")
                        //};
                        //Common.insertInAuditTable(objectAudit);

                    }
                    //existing Owner: Edit Mode
                    else
                    {
                        //var ownerFamilyInDb =
                        //    adbc.OwnersFamilies.SingleOrDefault(u =>
                        //        u.OwnersFamilyId == ownerFamilyInfo.OwnersFamilyId);

                        //ownerFamilyInDb.ChildDOB = ownerFamilyInfo.ChildDOB;
                        //ownerFamilyInDb.ChildName = ownerFamilyInfo.ChildName;
                        //ownerFamilyInDb.ChildPicture = ownerFamilyInfo.ChildPicture;

                        //ownerFamilyInDb.SpouseName = ownerFamilyInfo.SpouseName;
                        //ownerFamilyInDb.SpouseDOB = ownerFamilyInfo.SpouseDOB;
                        //ownerFamilyInDb.SpouseEmail = ownerFamilyInfo.SpouseEmail;
                        //ownerFamilyInDb.SpouseMobile = ownerFamilyInfo.SpouseMobile;
                        //ownerFamilyInDb.SpouseNID = ownerFamilyInfo.SpouseNID;
                        //ownerFamilyInDb.SpousePicture = ownerFamilyInfo.SpousePicture;

                    }


                    adbc.SaveChanges();
                    //return RedirectToAction("OwnerManagement");
                    return RedirectToAction("AdditionalInformation", new { OwenerId = ownerProjectInfo.OwnerId });

                }

            }
            //return View("OwnersInfo");
            return RedirectToAction("AdditionalInformation", new { OwenerId = ownerProjectInfo.OwnerId });
        }

        [HttpPost]
        public JsonResult gettingOwnerProjectInfo(OwnerProjectView model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerProjectList = adbc.OwnersProjects.ToList();
                var OwnersProjectIdList = new List<int>();
                foreach (var ownersProject in ownerProjectList)
                {
                    OwnersProjectIdList.Add(int.Parse(ownersProject.OwnersProjectId.ToString().Split('-')[1]));
                }

                var ownersProjectToken = ownerProjectList.Count();
                if (ownersProjectToken > 0)
                {
                    OwnersProjectIdList.Sort();
                    ownersProjectToken = OwnersProjectIdList[OwnersProjectIdList.Count - 1];
                }

                ownersProjectToken++;

                for (var i = 0; i < model.AmountArray?.Count; i++)
                {
                    var mdl = new OwnersProject()
                    {
                        OwnersProjectId = "OWP-" + ownersProjectToken,
                        OwnerId = model.OwnerId,
                        NumberOfDays = model.NumberOfDays,
                        NumberOfRoom = model.NumberOfRoom,
                        ProjectName = model.ProjectName,
                        PurchaseDate = model.PurchaseDate,
                        SalesAmount = model.SalesAmount,
                        PaidAmount = model.PaidAmount,

                        Amount = model.AmountArray.ElementAt(i),
                        Period = model.PeriodArray.ElementAt(i),
                        Date = model.DateArray.ElementAt(i),
                    };
                    adbc.OwnersProjects.Add(mdl);
                    ownersProjectToken++;
                }
                adbc.SaveChanges();
            }
            return Json("balls");
        }

        [HttpPost]
        public JsonResult gettingOwnerProjectInfoEdit(OwnerProjectView model)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var ownerProjectList = adbc.OwnersProjects.ToList();
                var OwnersProjectIdList = new List<int>();
                foreach (var ownersProject in ownerProjectList)
                {
                    OwnersProjectIdList.Add(int.Parse(ownersProject.OwnersProjectId.ToString().Split('-')[1]));
                }

                var ownersProjectToken = ownerProjectList.Count();
                if (ownersProjectToken > 0)
                {
                    OwnersProjectIdList.Sort();
                    ownersProjectToken = OwnersProjectIdList[OwnersProjectIdList.Count - 1];
                }

                //delete the previous project data of this owner
                var projects = adbc.OwnersProjects.Where(p => p.OwnerId == model.OwnerId).ToList();
                foreach (var VARIABLE in projects)
                {
                    adbc.OwnersProjects.Remove(VARIABLE);
                }
                //delete the previous project data of this owner ended



                ownersProjectToken++;
                for (var i = 0; i < model.AmountArray?.Count; i++)
                {
                    var mdl = new OwnersProject()
                    {
                        OwnersProjectId = "OWP-" + ownersProjectToken,
                        OwnerId = model.OwnerId,
                        NumberOfDays = model.NumberOfDays,
                        NumberOfRoom = model.NumberOfRoom,
                        ProjectName = model.ProjectName,
                        PurchaseDate = model.PurchaseDate,
                        SalesAmount = model.SalesAmount,
                        PaidAmount = model.PaidAmount,

                        Amount = model.AmountArray.ElementAt(i),
                        Period = model.PeriodArray.ElementAt(i),
                        Date = model.DateArray.ElementAt(i),
                    };
                    adbc.OwnersProjects.Add(mdl);
                    ownersProjectToken++;
                }
                adbc.SaveChanges();
            }
            return Json("balls");
        }

        #endregion

        #region Attachment

        [HttpPost]
        public ActionResult OwnerAttachmentAddOrEdit(OwnersAttachmentView model)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            _ownerInfoService.InsertUpdateAttachment(model);

            return RedirectToAction("AdditionalInformation", new { OwenerId = model.OwnerId });
        }

        #endregion

        #region Family

        [HttpPost]
        public ActionResult FamilyAddorEdit(OwnerFamilyView OwnersFamily, List<OwnerFamilyDetailView> OwnersFamilyList)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            ResponseModel response = _ownerInfoService.InsertOwnerFamily(OwnersFamily, OwnersFamilyList);

            var redirectModel = new { OwenerId = OwnersFamily.OwnerId };

            return RedirectToAction("AdditionalInformation", redirectModel);
        }

        #endregion

        #region Nominee

        [HttpPost]
        public ActionResult NomineeAddOrEdit(List<OwnerNomineeViewModel> OwnersNomineeList)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            _ownerInfoService.InsertOwnerNominee(OwnersNomineeList);

            var redirectModel = new { OwenerId = OwnersNomineeList.FirstOrDefault().OwnerId };

            return RedirectToAction("AdditionalInformation", redirectModel);
        }

        #endregion

        [HttpPost]
        public JsonResult gettingSearchResult(string searchfield)
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var OwnerList = db.OwnersInfos.ToList();
                List<OwnersInfo> searchedOwner = new List<OwnersInfo>();
                var searchfield1 = searchfield.ToUpper();

                foreach (var owner in OwnerList)
                {
                    var OwnerId = "";
                    if (owner.OwenerId != null)
                    {
                        OwnerId = owner.OwenerId.ToUpper();
                    }

                    var SlotId = "";
                    if (owner.SlotId != null)
                    {
                        SlotId = owner.SlotId.ToUpper();
                    }
                    var Name = "";
                    if (owner.Name != null)
                    {
                        Name = owner.Name.ToUpper();
                    }
                    var Email = "";
                    if (owner.Email != null)
                    {
                        Email = owner.Email.ToUpper();
                    }
                    var Mobile = "";
                    if (owner.Mobile != null)
                    {
                        Mobile = owner.Mobile.ToUpper();
                    }
                    var OwnerId1 = OwnerId;
                    var SlotId1 = SlotId;
                    var Name1 = Name;
                    var Email1 = Email;
                    var Mobile1 = Mobile;

                    if (OwnerId1.Contains(searchfield1) == true || SlotId1.Contains(searchfield1) == true ||
                        Name1.Contains(searchfield1) == true || Email1.Contains(searchfield1) == true
                        || Mobile1.Contains(searchfield1) == true)
                    {
                        searchedOwner.Add(owner);
                    }

                }
                string taskId = db.Tasks.Where(u => u.TaskPath == "Ownersinfo\\ownermanagement").Select(u => u.TaskId).FirstOrDefault();
                var myRoleList = (List<UserRole>)Session[SessionKeys.ROLE_LIST];

                string taskRoles = "";
                foreach (var tasks in myRoleList)
                {
                    if (tasks.TaskId == taskId)
                    {
                        taskRoles = tasks.Details;
                    }
                }


                if (taskRoles == "")
                {
                    //return View(Json("Error"));
                }

                return Json(searchedOwner);

            }
        }

        #region File Upload

        public JsonResult Upload_PDFFile_Edit()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Forbidden Http Request");
            }

            string relativePath = "~/Content/ownersitenotices/";

            ResponseModel response = _fileService.FileUpload(Request.Files[0], relativePath, Request.Files[0].FileName, true);

            return Json("Uploaded " + Request.Files.Count + " files");
        }

        #endregion

    }
}
