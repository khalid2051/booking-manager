﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StarterProjectV2.Models;
using StarterProjectV2.Services;
using StarterProjectV2.ViewModels;

namespace StarterProjectV2.Controllers
{
    public class LogController : Controller
    {
        private readonly CommonService _commonService = new CommonService();

        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        [HttpGet]
        public ActionResult LogManagement(string searchfield, int index = 1)
        {
            if (!IsUserLoggedIn())
                return RedirectToAction("Login", "Accounts");

            using (ApplicationDbContext _dbContext = new ApplicationDbContext())
            {

                string taskRoles = _commonService.TaskRole(TaskPath.Log_LogManagement);
                if (taskRoles == "" || taskRoles[0] == '0')
                {
                    return View("Error");
                }

                var logs = _dbContext.Logs.AsQueryable();
                if (!string.IsNullOrWhiteSpace(searchfield))
                {
                    logs = logs.Where(l => l.ShortMessage.Contains(searchfield));
                }

                logs = logs.OrderByDescending(x => x.Id);
                
                List<Log> selectedLogs = CommonHelper.PagedList(logs, index, out int noOfLogs);

                var model = new LogList
                {
                    Logs = selectedLogs,
                    TaskRoles = taskRoles,
                    NoOfAudit = noOfLogs,
                    index = index,
                    searchfield = searchfield
                };
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult LogView(int Id)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var auditObj = adbc.Logs.Where(a => a.Id == Id).FirstOrDefault();
                return View(auditObj);
            }
        }
    }
}