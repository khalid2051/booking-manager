﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StarterProjectV2.Controllers
{
    public class NotificationController : AsyncController
    {
        // GET: Notification
        public ActionResult Index()
        {
            return View();
        }
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }
        [HttpPost]
        public JsonResult NotificationSending()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Index");
            }
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var no_of_new = Convert.ToString(adbc.Notifications.Where(n => n.isSeen == false && n.NotificationHeader!="Dividend" && n.NotificationHeader != "Notice").ToList().Count);
                var no_of_old = Convert.ToString(Session[SessionKeys.NO_OF_EMPLOYEES]);
                var n_new = adbc.Notifications.Where(n => n.isSeen == false).ToList().Count;
                var n_old = Convert.ToInt32(Session[SessionKeys.NO_OF_EMPLOYEES]);
                if (no_of_new != no_of_old)
                {
                    Session[SessionKeys.NO_OF_EMPLOYEES] = no_of_new;
                    return Json(no_of_new);
                }
                return Json(no_of_old);
            }
        }

        [HttpPost]
        public JsonResult GettingNotificationList()
        {
            if (!IsUserLoggedIn())
            {
                return Json("Index");
            }
            using (StarterProjectV2.ApplicationDbContext adbc = new StarterProjectV2.ApplicationDbContext())
            {

                List<Notification> NotificationObjList = new List<Notification>();

                var UnseenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == false && n.NotificationHeader != "Dividend" && n.NotificationHeader!="Notice").ToList();

                SortedDictionary<int, Notification> UnseenNotificationsListForSorting = new SortedDictionary<int, Notification>();

                foreach (var notification in UnseenNotificationObjList)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    UnseenNotificationsListForSorting.Add(key, notification);
                }

                Dictionary<int, Notification> UnseenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in UnseenNotificationsListForSorting.Reverse())
                {
                    NotificationObjList.Add(notificationKeyValuePair.Value);
                }

                var SeenNotificationObjList = adbc.Notifications.Where(n => n.isSeen == true && n.NotificationHeader != "Dividend").ToList();

                SortedDictionary<int, Notification> SeenNotificationsListForSorting = new SortedDictionary<int, Notification>();

                foreach (var notification in SeenNotificationObjList)
                {
                    int key = int.Parse(notification.NotificationId.Split('-')[1]);
                    SeenNotificationsListForSorting.Add(key, notification);
                }

                Dictionary<int, Notification> SeenNotificationsListForSortingReverse = new Dictionary<int, Notification>();

                foreach (var notificationKeyValuePair in SeenNotificationsListForSorting.Reverse())
                {
                    NotificationObjList.Add(notificationKeyValuePair.Value);
                }

                List<Notification> selectedNotifications = new List<Notification>();
                int noOfNotifications;
                if (NotificationObjList.Count >= 10)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        selectedNotifications.Add(NotificationObjList.ElementAt(i));
                    }
                    int x = (NotificationObjList.Count % 10 == 0) ? 0 : 1;

                    noOfNotifications = (NotificationObjList.Count / 10) + x;
                }
                else
                {
                    selectedNotifications = NotificationObjList;
                    noOfNotifications = 1;
                }

                return Json(selectedNotifications);
            }
        }

    }
}