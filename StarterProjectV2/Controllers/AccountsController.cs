﻿using License;
using StarterProjectV2.Models;
using StarterProjectV2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StarterProjectV2.Controllers
{
    public class AccountsController : Controller
    {
        ConfigurationService _configurationService = new ConfigurationService();
        public bool IsUserLoggedIn()
        {
            return !string.IsNullOrEmpty(Session[SessionKeys.USERNAME] as string);
        }

        // GET: Accounts
        public ActionResult Login()
        {
            string licenseMessage = LicenseService.LicenseCheck();

            if (!string.IsNullOrWhiteSpace(licenseMessage))
            {
                Session["LicenseMessage"] = licenseMessage;
            }

            if (IsUserLoggedIn())
            {
                return RedirectToAction("Index", "Home", new { });
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            LicenseService.LicenseCheck();

            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                var usr = db.Users.FirstOrDefault(u => u.UserName == user.UserName);
                var DividendList = db.Dividends.ToList();

                Dictionary<string, bool> dividenDictionary = new Dictionary<string, bool>();
                foreach (var Dividend in DividendList)
                {
                    dividenDictionary.Add(Dividend.DividendId, false);
                }

                string IsAllSelected = "f";
                if (usr != null)
                {
                    if (EncryptDecryptString.Decrypt(usr.Password, "1234") == user.Password)
                    {
                        //Console.WriteLine(Session);
                        Session[SessionKeys.USERNAME] = usr.UserName.ToString();
                        Session[SessionKeys.PASSWORD] = usr.Password.ToString();
                        Session[SessionKeys.DISPLAY_USER_NAME] = usr.UserDisplayName.ToString();
                        //Console.WriteLine(Session);

                        Session[SessionKeys.LOGIN_TIME] = DateTime.Now.ToString("dd-MMM-yyyy hh:mm");

                        Session[SessionKeys.DIVIDENT_ALL_SELECT] = IsAllSelected.ToString();
                        Tuple<string, string> hostDetails = Common.GetLocalHostDetails();
                        Session[SessionKeys.PC_NAME] = hostDetails.Item1;
                        Session[SessionKeys.LOCAL_LOGIN_IP_ADDRESS] = hostDetails.Item2;

                        Session[SessionKeys.EXTERNAL_LOGIN_IP_ADDRESS] = Common.GetExternalIP();
                        Session[SessionKeys.NO_OF_EMPLOYEES] = Convert.ToString(db.Notifications.Where(n => n.isSeen == false && n.NotificationHeader != "Dividend").ToList().Count);

                        IEnumerable<Tuple<string, List<Tuple<string, string>>>> taskList = Common.getMenuList(Session[SessionKeys.USERNAME].ToString());

                        var path = db.Users.FirstOrDefault(u => u.UserName == usr.UserName).UserPicturePath;

                        Session[SessionKeys.USER_PROFILE_PICTUREPATH] = path;

                        Session[SessionKeys.MODULE_LIST] = taskList;
                        var userName = Session[SessionKeys.USERNAME].ToString();
                        var userRole = db.Users.FirstOrDefault(u => u.UserName == userName).UserType.ToString();
                        Session[SessionKeys.ROLE_LIST] = (List<UserRole>)Common.GetTasksWithRoleForUser(userRole);
                        Session[SessionKeys.DIVIDENT_SELECT_BOOLEAN_LIST] = (Dictionary<string, bool>)dividenDictionary;

                        Session[SessionKeys.SYSTEM_DATE] = _configurationService.GetSystemDate().Date.ToString("dd/MM/yyyy");

                        TempData["Message"] = Messages.HotelAdminstrationAndManagementSystem;
                        return RedirectToAction("About", "Home", new { area = "" });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Username or Password is wrong");

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Username or Password is wrong");

                }
            }


            return View();
        }

        [HttpGet]
        public ActionResult Logout()
        {
            if (IsUserLoggedIn())
            {

                Audit objectAudit = new Audit()
                {
                    AuditDTTM = Session[SessionKeys.LOGIN_TIME].ToString(),
                    LogInIp = Session[SessionKeys.PC_NAME].ToString() + " " + Session[SessionKeys.EXTERNAL_LOGIN_IP_ADDRESS] as string,
                    TableName = "0",
                    UserLoginDTTM = Session[SessionKeys.LOGIN_TIME].ToString(),
                    UserLogoutDTTM = DateTime.Now.ToString("dd-MMM-yyyy hh:mm"),
                    UserName = Session[SessionKeys.USERNAME].ToString(),
                    TaskName = "0",
                    ActionString = "LogOut",
                    ActionDetails = "UserName: " + Session[SessionKeys.USERNAME].ToString() + " Logged Out: " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm")
                };
                using (ApplicationDbContext adbc = new ApplicationDbContext())
                {
                    var DividendObjList = adbc.Dividends.ToList();
                    foreach (Dividend dividend in DividendObjList)
                    {
                        dividend.IsSelected = false;
                    }

                    adbc.SaveChanges();
                }
                Common.InsertAudit(objectAudit);

                HttpContext.Response.Cache.SetNoStore();

                Session.Abandon();
            }

            return RedirectToAction("Login");
        }
    }
}