﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarterProjectV2
{
    public class CommonHelper
    {
        public static string decimalFormat = "#,##0.##";

        public static readonly Dictionary<string, string> dictBookingStatus = new Dictionary<string, string> {
                     { "All", "" },
                     { "Approved", "Yes" },
                     { "Waiting", "No" },
                     { "Cancelled", "Cancelled" },
                    };

        public static readonly Dictionary<string, string> dictBookingTime = new Dictionary<string, string> {
                     { "All", "" },
                     { "Past", "Past" },
                     { "Current", "Current" },
                     { "Future", "Future" },
                     { "Requested Today", "Requested Today" },
                    };

        public static readonly Dictionary<string, string> dictYesNo = new Dictionary<string, string> {
                     { "All", "" },
                     { "Yes", "Yes" },
                     { "No", "No" },
                    };

        public static List<Entity> PagedList<Entity>(IQueryable<Entity> entities, int index, out int noOfLogs)
        {
            noOfLogs = (entities.Count() + 9) / 10;
            return entities.Skip((index - 1) * 10).Take(10).ToList();
        }

        public static string GenerateID(string prefix, string lastId)
        {
            int id = int.Parse(lastId.Split('-')[1]);
            id++;

            return $"{ prefix } - { id }";
        }

        public static string GetServerPath(string relativePath)
        {
            return HttpContext.Current.Server.MapPath(relativePath);
        }
    }
}