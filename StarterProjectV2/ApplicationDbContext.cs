﻿using StarterProjectV2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;

namespace StarterProjectV2
{
    public class ApplicationDbContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ApplicationDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }

        public ApplicationDbContext()
            : base("name=DefaultConnection")
        {
        }

        public DbSet<EnumData> EnumData { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceDetail> InvoiceDetails { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Audit> Audits { get; set; }
        public DbSet<OwnersInfo> OwnersInfos { get; set; }
        public DbSet<UserDetails> UserDetailses { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Dividend> Dividends { get; set; }
        public DbSet<OwnerOTPCode> OwnerOtpCodes { get; set; }
        public DbSet<OwnersFamily> OwnersFamilies { get; set; }
        public DbSet<OwnersNominee> OwnersNominees { get; set; }
        public DbSet<OwnersAttachment> OwnersAttachments { get; set; }
        public DbSet<OwnersProject> OwnersProjects { get; set; }
        public DbSet<RoomInfo> RoomInfos { get; set; }
        public DbSet<SlotInfo> SlotInfos { get; set; }
        public DbSet<RoomSlotAssignment> RoomSlotAssigments { get; set; }
        public DbSet<OwnerRoomSlotAssignment> OwnerRoomSlotAssigments { get; set; }
        public DbSet<AccommodationInfo> AccomadationInfos { get; set; }
        public DbSet<OwnerOneTimeLoginOTP> OwnerOneTimeLoginOtps { get; set; }
        public DbSet<OwnerRoomBooking> OwnerRoomBookings { get; set; }
        public DbSet<OwnerTransactionHistory> OwnerTransationHistories { get; set; }
        public DbSet<CompanyInfo> CompanyInfoes { get; set; }
        public DbSet<DashboardNotice> DashboardNotices { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<tblEmployee> tblEmployees { get; set; }
        public DbSet<EmailandSMSReport> EmailandSmsReports { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Ticker> Tickers { get; set; }
        public DbSet<OwnersPayment> OwnerPayments { get; set; }
        public DbSet<FinancialYear> FinancialYears { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<DividendFailedGroup> DividendFailedGroups { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Audit2> Audit2 { get; set; }
        public DbSet<ConfAllotment> ConfAllotments { get; set; }
        public DbSet<ConfOwnerAvgRoomRate> ConfOwnerAvgRoomRates { get; set; }
        public DbSet<ConfigInvRoom> ConfigInvRooms { get; set; }
        public DbSet<ConfOwnersContribution> ConfOwnersContributions { get; set; }
        public DbSet<ReferralBooking> ReferralBookings { get; set; }
        public DbSet<ReferralBookingRoomDetail> ReferralBookingRoomDetails { get; set; }
        public DbSet<ConfPostCommentApproval> ConfPostCommentApprovals { get; set; }
        public DbSet<OwnerDetail> OwnerDetails { get; set; }
        public DbSet<SystemDate> SystemDates { get; set; }
        private AuditTrailFactory auditFactory;
        private List<Audit2> auditList = new List<Audit2>();
        private List<DbEntityEntry> objectList = new List<DbEntityEntry>();

        public override int SaveChanges()
        {
            auditList.Clear();
            objectList.Clear();
            auditFactory = new AuditTrailFactory(this);

            var entityList = ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified);
            foreach (var entity in entityList)
            {
                Audit2 audit = auditFactory.GetAudit(entity);
                bool isValid = true;
                if (entity.State == EntityState.Modified && string.IsNullOrWhiteSpace(audit.NewData) && string.IsNullOrWhiteSpace(audit.OldData))
                {
                    isValid = false;
                }
                if (isValid)
                {
                    auditList.Add(audit);
                    objectList.Add(entity);
                }
            }

            var retVal = base.SaveChanges();
            if (auditList.Count > 0)
            {
                int i = 0;
                foreach (var audit in auditList)
                {
                    if (audit.Actions == AuditActions.I.ToString())
                        audit.TableIdValue = auditFactory.GetKeyValue(objectList[i]);
                    this.Audit2.Add(audit);
                    i++;
                }

                base.SaveChanges();
            }

            return retVal;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}