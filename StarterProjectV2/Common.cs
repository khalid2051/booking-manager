﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using StarterProjectV2.Models;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using StarterProjectV2.Services;

namespace StarterProjectV2
{
    public class Common
    {

        ConfigurationService _configurationService = new ConfigurationService();

        public static string GetExternalIP()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }

        public static Tuple<string, string> GetLocalHostDetails()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            var hostIPName = host
            .AddressList
            .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);

            return Tuple.Create(host.HostName.ToString(), hostIPName.ToString());
        }

        public static string InsertAudit(Audit obj)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {
                var auditList = adbc.Audits.ToList();

                List<int> auditListIds = new List<int>();

                foreach (var audit in auditList)
                {
                    var id = int.Parse(audit.AuditId.Split('-')[1]);
                    auditListIds.Add(id);
                }

                var auditCount = auditList.Count;

                if (auditCount > 0)
                {
                    auditListIds.Sort();

                    auditCount = auditListIds[auditListIds.Count - 1];
                }
                auditCount++;

                var newAudit = new Audit()
                {
                    AuditId = "ADT-" + auditCount.ToString(),
                    ActionDetails = obj.ActionDetails,
                    ActionString = obj.ActionString,
                    AuditDTTM = obj.AuditDTTM,
                    LogInIp = obj.LogInIp,
                    TaskName = obj.TaskName,
                    TableName = obj.TableName,
                    UserLoginDTTM = obj.UserLoginDTTM,
                    UserLogoutDTTM = obj.UserLogoutDTTM,
                    UserName = obj.UserName
                };

                adbc.Audits.Add(newAudit);
                adbc.SaveChanges();
                string auditId = "ADT-" + auditCount.ToString();
                return auditId;
            }
        }

        public static void InsertEmailandSMSReports(EmailandSMSReport obj)
        {
            using (ApplicationDbContext adbc = new ApplicationDbContext())
            {

                var emailSMSReportList = adbc.EmailandSmsReports.AsEnumerable();

                var lastemailSMSReportId = emailSMSReportList.OrderByDescending(x => x.Id).FirstOrDefault().EmailandSMSReportId;
                int emailSMSReportToken = int.Parse(lastemailSMSReportId.Split('-')[1]);
                emailSMSReportToken++;

                var newEmailandSmsReport = new EmailandSMSReport()
                {
                    IsSMSSent = obj.IsSMSSent,
                    Mobile = obj.Mobile,
                    Email = obj.Email,
                    SlotId = obj.SlotId,
                    Name = obj.Name,
                    TimeOfDelivery = DateTime.Now,
                    NotificationType = obj.NotificationType,
                    EmailandSMSReportId = "ESR-" + emailSMSReportToken,
                    Text = obj.Text,
                    NotificationSerialNo = obj.NotificationSerialNo,

                };
                adbc.EmailandSmsReports.Add(newEmailandSmsReport);
                adbc.SaveChanges();
            }
        }

        public static IEnumerable<Tuple<string, List<Tuple<string, string>>>> getMenuList(string UserName)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var TaskObject = db.Tasks.ToList();
                //                        var ModuleObject = db.Modules.Select(m=>m.ModuleName).ToList();
                var ModuleObject = db.Modules.ToList();
                var query = (from a in TaskObject
                             join b in ModuleObject
                                 on a.moduleId equals b.ModuleId into f
                             from b in f.DefaultIfEmpty()
                             group new { a.TaskName, a.TaskPath } by b.ModuleId
                    into g
                             select new
                             {
                                 ModuleName = db.Modules.Where(m => m.ModuleId == g.Key).Select(m => m.ModuleName)
                                     .FirstOrDefault(),
                                 TaskNameList = g.ToList()
                             }).ToList();


                List<Tuple<string, List<Tuple<string, string>>>> list =
                    new List<Tuple<string, List<Tuple<string, string>>>>();
                foreach (var q in query)
                {
                    List<Tuple<string, string>> tempList = new List<Tuple<string, string>>();
                    for (int i = 0; i < q.TaskNameList.Count; i++)
                    {
                        tempList.Add(Tuple.Create(q.TaskNameList[i].TaskName, q.TaskNameList[i].TaskPath));
                    }

                    Tuple<string, List<Tuple<string, string>>> obj = Tuple.Create(q.ModuleName, tempList);
                    list.Add(obj);
                }

                List<Tuple<string, List<Tuple<string, string>>>> listAccordingtoUserPrivilege = new List<Tuple<string, List<Tuple<string, string>>>>();
                //var userName = Session[SessionKeys.USERNAME].ToString();
                var userRole = db.Users.FirstOrDefault(u => u.UserName == UserName).UserType.ToString();
                var listOfPrivileges = db.UserRoles.Where(u => u.RoleName == userRole).GroupBy(u => u.ModuleName).ToList();

                foreach (var UserDetail in listOfPrivileges)
                {
                    string ModuleName = db.Modules.Where(m => m.ModuleName == UserDetail.Key).Select(m => m.ModuleName).FirstOrDefault().ToString();
                    List<Tuple<string, string>> tempListwithPrivilege = new List<Tuple<string, string>>();

                    foreach (var Task in UserDetail)
                    {
                        var res = db.Tasks.Where(t => t.TaskId == Task.TaskId).FirstOrDefault();
                        tempListwithPrivilege.Add(Tuple.Create(res.TaskName, res.TaskPath));
                    }
                    Tuple<string, List<Tuple<string, string>>> obj = Tuple.Create(ModuleName, tempListwithPrivilege);
                    listAccordingtoUserPrivilege.Add(obj);
                }

                //return list.AsEnumerable();
                //In order to show the menu with user privilege
                return listAccordingtoUserPrivilege.AsEnumerable();
            }
        }

        public static List<UserRole> GetTasksWithRoleForUser(string RoleName)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                return db.UserRoles.Where(u => u.RoleName == RoleName).ToList();
            }
        }

        public static bool SMSSending(string Recepient, string message)
        {
            ConfigurationService _configurationService = new ConfigurationService();

            bool sendSMS = _configurationService.GetSettingValue(SettingCode.SendSMS)?.ToLower() == "Yes".ToLower();
            if (!sendSMS)
                return false;


            using (var web = new WebClient())
            {
                try
                {
                    string url = "https://sms.tense.com.bd/api-sendsms" +
                                 "?user=bwheritagehotel" +
                                 "&password=01777744046" +
                                 "&campaign=PasswordReset" +
                                 "&number=" + Recepient +
                                 "&text=" + message;

                    string result = web.DownloadString(url);
                    if (result.Contains("OK"))
                    {
                        return true;
                    }
                    else
                    {
                        //MessageBox.Show("Some issue delivering", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                    //Catch and show the exception if needed. Donot supress. :)  

                }
            }
        }

        public static void EmailSending_Prev(string emailReceiver, string emailSubject, string emailbody)
        {
            //Check if it is Valid Email
            bool isValidEmail = Regex.IsMatch(emailReceiver, @"^([\w-\.+]+)@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$");
            //Specify the from and to email address
            if (isValidEmail == true)
            {
                MailMessage mailMessage = new MailMessage("hams.system2019@gmail.com", emailReceiver);
                // Specify the email body
                mailMessage.Body = emailbody;
                // Specify the email Subject
                mailMessage.Subject = emailSubject;
                mailMessage.IsBodyHtml = true;
                //Add Attachments if necessary
                //string filePath = HttpContext.Current.Server.MapPath("~/Content/img/Coral-Reef-Logo.png");
                //mailMessage.Attachments.Add(new Attachment(filePath));
                // Specify the SMTP server name and post number
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                // Specify your gmail address and password
                var userName = "hams.system2019@gmail.com";
                var password = "Hamsnia...0";
                //using (ApplicationDbContext adbc = new ApplicationDbContext())
                //{
                //    var admin = adbc.Admins.Where(a => a.Name == "SourceAdmin").FirstOrDefault();
                //    userName = admin.Email.ToString();
                //    password = admin.Password.ToString();
                //}

                smtpClient.Credentials = new System.Net.NetworkCredential()
                {
                    UserName = userName,
                    Password = password,
                };
                // Gmail works on SSL, so set this property to true
                smtpClient.EnableSsl = true;
                // Finall send the email message using Send() method
                //I have added try catch block to prevent to go to Error Page, Email is mostly optional, so it is not wise to go to error page due to failure to send an optional email.Only Notification report will be hampered.
                try
                {
                    smtpClient.Send(mailMessage);
                }
                catch (Exception e)
                {

                }
            }
        }

        public static void EmailSending(string emailReceiver, string emailSubject, string emailbody)
        {
            ConfigurationService _configurationService = new ConfigurationService();

            bool sendEmail = _configurationService.GetSettingValue(SettingCode.SendEmail)?.ToLower() == "Yes".ToLower();
            if (!sendEmail)
                return;

            bool isValidEmail = Regex.IsMatch(emailReceiver, @"^([\w-\.+]+)@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$");
            if (isValidEmail == true)
            {
                MailMessage mailMessage = new MailMessage("slotowner@bwheritagehotel.com", emailReceiver);
                mailMessage.Body = emailbody;
                mailMessage.Subject = emailSubject;
                mailMessage.IsBodyHtml = true;
                SmtpClient smtpClient = new SmtpClient("mail.bwheritagehotel.com", 587);
                var userName = "slotowner@bwheritagehotel.com";
                var password = "Slot8459!";
                smtpClient.Credentials = new System.Net.NetworkCredential()
                {
                    UserName = userName,
                    Password = password,
                };
                smtpClient.EnableSsl = false;
                try
                {
                    smtpClient.Send(mailMessage);
                }

                catch (SmtpFailedRecipientsException ex)
                {
                    throw;
                    //for (int i = 0; i < ex.InnerExceptions.Length; i++)
                    //{
                    //    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    //    if (status == SmtpStatusCode.MailboxBusy ||
                    //        status == SmtpStatusCode.MailboxUnavailable)
                    //    {
                    //        Console.WriteLine("Delivery failed - retrying in 5 seconds.");
                    //        System.Threading.Thread.Sleep(5000);
                    //        smtpClient.Send(mailMessage);
                    //    }
                    //    else
                    //    {
                    //        Console.WriteLine("Failed to deliver message to {0}",
                    //            ex.InnerExceptions[i].FailedRecipient);
                    //    }
                    //}
                }
            }
        }

        public static async Task<bool> EmailSending_Bulk(List<string> emailReceiver, string emailSubject, string emailbody)
        {
            ConfigurationService _configurationService = new ConfigurationService();

            bool sendEmail = _configurationService.GetSettingValue(SettingCode.SendEmail)?.ToLower() == "Yes".ToLower();
            if (!sendEmail)
                return false;

            SmtpClient smtpClient = new SmtpClient("mail.esecuresoft.com.bd", 465);
            var userName = "mailtest@esecuresoft.com.bd";
            var password = "eSecure@2021";
            smtpClient.Credentials = new NetworkCredential()
            {
                UserName = userName,
                Password = password,
            };
            smtpClient.EnableSsl = false;
            //MailMessage mailMessage = new MailMessage(, "hams.system2019@gmail.com");
            //mailMessage.Body = emailbody;
            //mailMessage.Subject = emailSubject;
            //mailMessage.IsBodyHtml = true;


            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("mailtest@esecuresoft.com.bd");
            mailMessage.Subject = emailSubject;
            mailMessage.Body = emailbody;
            mailMessage.IsBodyHtml = true;
            foreach (var emailRcvr in emailReceiver)
            {
                mailMessage.To.Add(new MailAddress(emailRcvr));
            }

            try
            {
                await smtpClient.SendMailAsync(mailMessage);
                return true;
                //smtpClient.Send(mailMessage);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                throw;
            }
        }

        public static bool IsUserLoggedIn()
        {
            //return true;
            return !string.IsNullOrEmpty(HttpContext.Current.Session[SessionKeys.USERNAME] as string);
        }
    }
}